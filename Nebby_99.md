

## If you could write any program, what would you write?
### asked by Anon

Maybe an isolated AI that was much smarter than me, that I could use as a sounding board. Oh man, that's so extremely problematic.
Slightly less paradoxically, an app people would enjoy using that would help them engage with and understand politics/ideology and the way it affects their lives.
Currently I'm working on a to-do list.
___
Answered on June 24, 2015 09:17:53 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129429161904



## What's something that everyone should do every day?
### asked by Anon

Not that I'd make such a broad assertion, but I guess reflect (or meditate). Without reflecting on our priorities, our values and how we spend our time, our lives get consumed by habits, distractions and insecurities.
Reflection is a chance to validate our unconscious assumptions and purposefully set our direction in life.
I don't really do it, despite spending hours on the train every day. Seems like a good idea though.
I guess the further off-course you feel in life, the more scary it is to think about. Especially if you don't know how to move in the right direction. YMMV but it seems like a good thing to do.
___
Answered on June 21, 2015 13:02:01 GMT

__Likes:__ 0

Original URL: /Nebby_99/answers/129408026544



## What hobbies do you wish you could learn?
### asked by Anon

SO MANY. There's nothing I wouldn't like to know, but I'm more interested in ones that seem more achievable. I want to get better at web development, but I'm also keen to get reasonably competent at: - All aspects of game development/modding (coding, 3D modelling, level design/editing, VR) - Mobile apps - Video editing - Electronics & robotics - Artificial intelligence (writing intelligent-seeming simulations)
The stuff I feel is less achievable but that I'd still love to learn to do well: - Visual design - Drawing, writing, any kind of minimally-constrained creative expression - Building physical stuff (woodwork, etc)
There's also stuff motivated by more than just an interest, like gardening and various forms of exercise.
___
Answered on June 21, 2015 11:22:19 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129400721584



## What is the most important thing you need to do today?
### asked by Anon

Study for a job interview. I have not done it.
___
Answered on June 21, 2015 11:01:53 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129343466672



## If money were no object, what would your dream job be?
### asked by Anon

What would a job be, if money were no object? My hobbies I guess. Only I could put them to good use.
Basically doing what I enjoy / am good at to make an actual positive contribution to society.
Being a stay-at-home Dad would qualify, if I got to be any good at it.
___
Answered on June 21, 2015 10:50:55 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129400617392



## What stops you from going to bed on time?
### asked by Anon

The short answer is that I procrastinate.
The long answer is some combination of not wanting to let go of another day, being conditioned by my childhood to hate bedtime, and just generally being a night person. It's also a chance to be at home by myself, which doesn't happen a lot these days. Any one of those could be 80%+ of the reason though, I think.
___
Answered on June 21, 2015 10:47:37 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129400648112



## What's the worst advice you've ever taken?
### asked by Anon

Everything the first Hand Specialist I saw told me to do about my RSI. I gave him the benefit of the doubt for about a year, and it was a full year of my life wasted.
If you aren't satisfied with medical advice, always get a second opinion imho.
___
Answered on July 30, 2015 08:25:50 GMT

__Likes:__ 4

Original URL: /Nebby_99/answers/129343466160



## Were you ever a "nice guy"? https://www.reddit.com/r/niceguys/
### asked by Anon

Pretty sure not. It's hard to retroactively apply new lenses to distant memories, but I can't think of a time where it could possibly have been the case.
___
Answered on July 16, 2015 09:38:56 GMT

__Likes:__ 0

Original URL: /Nebby_99/answers/130265421232



## Do you ask many questions on here?
### asked by Anon

Not as many as I'd like.
So far, uh, three or so :|
___
Answered on July 16, 2015 07:43:38 GMT

__Likes:__ 0

Original URL: /Nebby_99/answers/130263100592



## Do you remember life without technology? What was it like?
### asked by Anon

Wikipedia: "The human species' use of technology began with the conversion of natural resources into simple tools."
So no >.>
I could engage with the spirit of the question, but nah.
___
Answered on July 15, 2015 10:21:36 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/130232564912



## Have you tried yoga?
### asked by Anon

Only if Kinect games count.ie. this nonsense:
___
Answered on July 14, 2015 04:32:05 GMT

__Likes:__ 0

Original URL: /Nebby_99/answers/130164753584



## Thank you for your indepth response to my RSI question - very much appreciated. If our paths were ever to cross I'd shout you a Boost bar. Hell, I'd buy you two!
### asked by Anon

You're welcome, thanks for the question and hypothetical snacks. I hope it useful/interesting to you and others.
One minor clarification while I can - on back/neck stretches I meant 20 seconds, not 20 repetitions.
___
Answered on July 12, 2015 05:51:16 GMT

__Likes:__ 1

Original URL: /Nebby_99/answers/130122250672



## What helps with your RSI? Any tips?
### asked by Anon

- I'm not an expert on this stuff; the below just represents my experience. See an occupational therapist (OT) for overuse injuries, particularly related to hands. See a physio for more general musculoskeletal issues. Try different ones until you find one that you're satisfied with. If you can see a decent GP, they should be able to help you find someone and also diagnose/eliminate common issues (like carpal tunnel). Be wary of specialists (surgeons gonna surgeon) and chiropractors (no personal experience here but I've heard very bad stories). A lot of direct treatments will feel better but only temporarily, so be sure to keep looking for a long term solution.
- The best way to deal with overuse injuries is to reduce your usage, or if you can't, change it up (eg. I swap mouse hands frequently; it's easier than you think). When reducing usage, reduce aggressively but don't go straight back to normal afterwards; ramp up slowly and back off if it starts to flare up. It might take months, but you can build up your endurance again this way. Getting back to something resembling normal is worth the frustration.
- RSI can be complex; muscular tension and load run right through our bodies to keep us balanced/braced/upright. Take a whole-of-body approach. I spent years getting my wrists examined when it turns out the muscles in my forearms were pulling the wrist joints out of whack. Some of my wrist pain was referred from nerve irritation in my shoulders. This concept was a game-changer for me: http://www.myofascialtherapy.org/myofascial-therapy/index.html
- Our bodies are built for dynamic activity, so sitting still for long periods leads to all sorts of problems. Avoid sitting still at a PC/laptop for more than an hour at a time (just take a short break). Smartphones are worse so try not to use them for extended periods. Do regular physical activity that ensures a good range of motion (to keep muscles from tightening up and joints from wearing unevenly) without excessive impact/strain (to avoid hurting yourself if you've been sedentary for along time). Yoga is a good one to start with.
- Regular stretching is good (including of hands when at the PC) but always be slow and gentle. A quick hard stretch will just tense up the muscles and/or aggravate your tendons & joints. I have trouble with this due to impatience. Arm muscles tend to respond well after 8 seconds or so of really gentle stretching, with a few repetitions. For my neck/back I usually do 20 or more. A physio/OT can give you a more targeted (and qualified) set of stretches.
- Every body is different, so while ergonomics guidelines are a good starting point (eg. https://www.osha.gov/SLTC/etools/computerworkstations/positions.html) it's important to listen to your body. Don't ignore discomfort, and remember generally no posture is as good as taking breaks and changing posture regularly. I use a sit/stand desk and some infuriating software called WorkPace.
___
Answered on July 12, 2015 01:53:37 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/130090619312



## Aren't Boosts just off brand Mars Bars?
### asked by Anon

Not even close, read a book sometime.
___
Answered on July 10, 2015 13:33:13 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/130069058224



## Whoever the Boost-hater is should send all their Boosts directly to my mouth, please
### asked by Anon

I stole your idea.
___
Answered on July 10, 2015 11:34:37 GMT

__Likes:__ 0

Original URL: /Nebby_99/answers/129958969264



## A v happy customer rewarded us (me+workmates) with a huge box of Cadbury's Favourites today. That's great anon, but why are you telling me idgaf?, I hear you think. Well, guess which mini bars languished in the box til late & were only eaten when we were despo & bc they were there? THE BOOST ONES!
### asked by Anon

I'm sorry that you have a toilet job with toilet people.(jks I'm sure they're lovely and should not be judged for their deficiencies)
Anyway send the leftovers into my face next time.
___
Answered on July 10, 2015 11:34:11 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/130063290544



## Who is your favorite YouTuber?
### asked by Anon

MathasGames for his taste in games and for not taking himself seriously. Game Grumps for accessible entertainment value. Criken for laugh-out-loud gaming shenanigans.
___
Answered on July 07, 2015 12:41:28 GMT

__Likes:__ 0

Original URL: /Nebby_99/answers/129962532272



## If you could go back to uni and study another subject for free, would you? What would you study?
### asked by Anon

Time is really the main barrier for me atm. If I had the time I'd love (well okay like) to study more. The most relevant subject to my career would be Human-Computer Interfaces or Interaction Design, but as a "for-free" wildcard I always wanted to do an entry-level philosophy course. I'm not sure it'd do me much good though (and I know I'd suck at it).
Honorable mention to Computer Graphics, which I extended my degree 6 months in order to do, only to have it cancelled.
___
Answered on July 07, 2015 12:36:01 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129405983664



## There is only one place where a full-size Boost bar belongs - IN THE BIN.
### asked by Anon

That's no way to talk about a loved one. Think of all the potential it has. Think how it could make people happy, if it could just find people who'd appreciate it's strengths. If it was on the right place at the right time, it could change the world. What a waste it would be to throw it away, untasted.
___
Answered on July 07, 2015 09:30:29 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/129952424112



## That's a very nice thing to say about a sibling, I'd swap mine for a KitKat tbh.
### asked by Anon

That you, Soul? :D
I think the amount of time you spend with a sibling, there's a mountain of stuff to hold a grudge over and a mountain of stuff to appreciate. If you can both focus on the good stuff then it's great, but the negative stuff tends to compound when focused on. Relationships are tricky I guess (what a hot take).
Anyway even for parts you should be able to score at least a full-size Boost bar.
___
Answered on July 06, 2015 13:24:37 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129927394480



## How would you rate your bro? Both past & present.
### asked by Anon

I'm not sure what the standard scale is for bro-rating, but regardless he's just my bro - I love him and there's no one I would rather have grown up with, or have as a brother now.
___
Answered on July 06, 2015 12:58:42 GMT

__Likes:__ 4

Original URL: /Nebby_99/answers/129856563376



## Do you have a "signature" style? What is it?
### asked by Anon

Uhhhhhhhhhhhhhhhhhhhhhh being outwardly pretty boring, but passionate about my values?
Also drafting answers to questions and never posting them.
___
Answered on July 06, 2015 12:38:44 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129471756464



## What were your favourite subjects in high school? Do you remember any teachers fondly? In my whole five years there, only two struck me as being excited about what they were teaching.
### asked by Anon

The subjects I remember liking were Software Design & Development and maybe Physics. My SDD teacher was an idiot but the subject was of enormous interest. Physics was harder and not as exciting, but a super passionate teacher made it a great subject anyway.
I can remember a couple of passionate teachers from high school. I think they're rare and a lot of them must get sucked up by the wealthiest schools. I had others that I liked, but they weren't really exciting. Others were kind of excited themselves but failed to inspire that in me (*cough* Maths).
Teaching is one of the hardest jobs I can imagine in terms of emotional labour as well as the physical work, so I don't begrudge the teachers who failed to excite me. It sucked when they were bad at their job, but I see that as the archaic, poorly designed education system having failed both of us.
___
Answered on July 03, 2015 12:00:18 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129821276336



## why can't you ever just type something and hit send without re-reading it a billion times?
### asked by Anon

WHO IS IN MY LOUNGE ROOM oh its you
I edit my writing a lot. I'm used to business/technical writing where I boil things down to be as concise as possible, but this kind of writing is better being a bit more personal and thought-streamy. Hopefully I'll get better with practice :P
___
Answered on July 02, 2015 13:14:00 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129797071536



## What do you like about being a dad?
### asked by Anon

Having a kid ^^
There's a lot to that though, I guess. Society makes a big deal about the parent-child bond so there are a lot of powerful associations with doing "Dad things", including memories from my own childhood. I'm sure some of it is innate, too. It's super warm and fuzzy (often literally).
I really like comforting him and playing with him. By which I mean I like it when he finds me comforting and enjoys playing with me. I like it when he's happy to see me. I really like seeing him get more expressive and independent, ever so slowly and yet almost every day.
I like having his future to look forward to.
___
Answered on July 02, 2015 13:10:57 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129400661168



## Do you like big butts and feel that you can not lie about it?
### asked by Anon

No. Thanks for an easy one!
You still don't know whether I like big butts though.
___
Answered on July 02, 2015 10:33:31 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129790923184



## Did you have any childhood fears, like monsters under the bed?
### asked by Anon

I used to prefer to sleep with a dim light on, I think. I'd see/imagine scary shapes in the dark, like someone hiding in the corner/wardrobe.
I remember it being a revelation when I understood why it happened a bit better, but I'm not sure if I was already over it by then. The difference between what we perceive and what's actually there hadn't occurred to me when I was that young. The brain is good at matching patterns, especially ones you're already thinking about, especially with all that youthful imagination & brain plasticity. The darkness is like a blank canvas of faint shapes & lines.
___
Answered on June 29, 2015 13:22:16 GMT

__Likes:__ 1

Original URL: /Nebby_99/answers/129406558384



## What is your favourite way to kill time?
### asked by Anon

Spending time with family & friends is always great value.
By myself I like playing video games, or messing around with code. Basically, the things I can't do much because of my RSI. As a substitute I shop for cheap games on Steam, watch people play games on YouTube and sign up for Coursera courses I never end up doing.
___
Answered on June 29, 2015 13:13:58 GMT

__Likes:__ 1

Original URL: /Nebby_99/answers/129429234096



## What do you enjoy about programming?
### asked by Anon

I've always been excited about the idea of building things. As a kid I loved machinery and toys with moving parts.
The trouble with building things like that in reality is that it's hard. The physical world is messy and complicated, which makes the distance from idea to realisation very long. I'm also not great with my hands, which didn't help.
Programming is different. There are so many layers of abstraction between the physical world and 4th-gen languages, that things are much simpler by comparison. The chaos of physical reality is gone - it's just logic.
That said, it's a completely alien environment to work in. It has it's own languages, challenges and a different way of thinking. Once you overcome that though, you can envisage something and build it insanely fast. It costs you almost nothing in terms of resources and you can send it all over the world in an instant.
Compared to learning any one physical craft, the possibilities are much broader. And I'm much better at thinking logically than I am at working with my hands.
Of course, computers also do a lot of inexplicable weird nonsense. That comes with working on top of so many layers of abstraction, and ultimately the physical world. It's still easier than dealing with physics directly.
tl;dr it suits me, it's fun, it's empowering, it's easy to make things that go.
___
Answered on June 26, 2015 08:03:33 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129429152176



## Is there a video game that you never get bored of? What keeps you interested for so long?
### asked by Anon

There's nothing I won't get bored with from time to time, even if only temporarily. I think setting, story and characters have limited replayability, but some game mechanics can keep on giving for hundreds of hours.
A style of gameplay that has had a lot of longevity for me is traffic dodging. I spent over 100 hours just driving pointlessly in GTA 3 because it was mindless fun. Another title that has kept me coming back for years is Audiosurf, which is kind of similar (at least in abstract) in that you dodge obstacles at high speed.
Perhaps it's the relatively simple low-level visual pattern matching and fast hand-eye coordination you need to perform well in those situations, but I find it quite therapeutic.
Honorable mention to Minecraft, which I keep coming back to for its creative and social elements. The progression and exploration aspects had great longevity, but not without their limits.
___
Answered on June 24, 2015 15:44:35 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129520081840



## Did you have any imaginary friends as a child?
### asked by Anon

I've been told I used to have three girls that lived in my top pocket. No memory of that.
I think there was a later one that I used to remember but don't now. RIP buddy.
___
Answered on June 24, 2015 09:20:23 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/129406582704



## How do you feel now that ur clean shaven? Did you get a barrage of "You look so young!" & "What a handsome Devil!" comments?
### asked by Anon

It feels nicer. It looked weird to me initially ("Oh no, now I look weird both ways!"), but I'm mostly settled back into it now. No one really commented that I can recall, but I haven't been back to work yet.
___
Answered on December 27, 2015 11:55:18 GMT

__Likes:__ 1

Original URL: /Nebby_99/answers/134436836784



## Re: beard experiment - what have the reactions to it been like? Mostly positive or negative? Are you shaving it off bc *you* don't like it?
### asked by Anon

Mostly positive, although I don't think many people would be in a hurry to tell me if they didn't like it.
The exception is Haps, who hates it. Her opinion is kind of the important one (next to mine). If I really liked it then when we'd have to figure something out, but the novelty is wearing off anyway.
How I feel when I go back to clean shaven is also part of the experiment. I can always grow it back, but I expect my curiosity will be sated for the foreseeable future.
___
Answered on December 19, 2015 23:16:53 GMT

__Likes:__ 4

Original URL: /Nebby_99/answers/134279522224



## Would you be interested in learning mindful meditation? I think it would be useful for me to calm my thoughts sometimes but I'm rubbish at it.
### asked by Anon

I've used an android app called Calm for this, on the train, and I found it fairly good. I intended to keep doing it, but haven't for a while now.
It deals quite well with the idea of being 'bad at it', by repeatedly making the point that your mind will wander and the point of the exercise isn't to prevent that, it's about what you do when it happens. That is, don't get frustrated or feel like you're failing - just get back to it. Over time it will happen less. It's the joouurney dude, etc.
Anyway I'd recommend trying it out just to see what it's like, if it's of any interest. The app has a 7-day intro course that's free, and you can repeat each 10-min session as much as you want. You can also do sessions of any length with just the ambient noise (no voice).
___
Answered on December 19, 2015 20:19:21 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/129408671152



## You are rocking the lumberjack look, m8. How long have you had the beard? Is it a permanent thing?
### asked by Anon

Thanks. I guess scrawny monochrome IT lumberjack is my brand.
I think I've had the beard for a month or so. It's been a fun experiment, but it's not long for this world.
___
Answered on December 19, 2015 09:06:52 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/134260516272



## Are you gassy?
### asked by Anon

Sometimes, if you must know.
___
Answered on December 16, 2015 23:50:08 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/134219685296



## How are you like a nebula, m8? Are you made of dust, hydrogen, helium gas & plasma?
### asked by Anon

At least 3 of those, yeah.
___
Answered on December 16, 2015 22:19:29 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/134218986928



## My favorite part is that they all proudly read "FRIEND & BEN" because I couldn't type my name in katakana fast enough. Stay beautiful, FRIEND & BEN.
### asked by happyhaps

IIRC all the screens were timed (eg. names, stickers, drawing), which added a frantic urgency to whole experience.
It was a fun time, Friend.
___
Answered on December 15, 2015 20:27:54 GMT

__Likes:__ 1

Original URL: /Nebby_99/answers/134193030064



## Don't lie to these fine people Neb, those are your natural eyes!
### asked by Soul James

Cut me some slack, I was just trying to be beautiful.
___
Answered on December 15, 2015 13:44:17 GMT

__Likes:__ 0

Original URL: /Nebby_99/answers/134187921584



## The picture where your eyes are weird - bottom right-hand corner - is so creepy ;(,
### asked by Anon

I blinked in that one so we whacked a sticker over my eyes. Pretty legit - I'm surprised you picked up on it.
I just can't get over how huge and shimmery our eyes are in all the others XD
___
Answered on December 15, 2015 13:20:50 GMT

__Likes:__ 5

Original URL: /Nebby_99/answers/134187580592



## Where did you go for your honeymoon if you dont mind me asking? Your description of it as epic has got me wondering!
### asked by Anon

We went to Japan, which was the furthest abroad either of us had ever been. Julie had gone there briefly on exchange in high school.
The food was awesome, the shopping was awesome, the history & architecture were amazing, the people were lovely, the transport was novel but very efficient.
It was challenging but safe, and fascinating but not overwhelming. An excellent adventure and bonding experience. I would love to do it again.
One highlight was a terrifying auto-airbrushing photo booth. We nearly wet ourselves laughing.
___
Answered on December 15, 2015 10:11:26 GMT

__Likes:__ 4

Original URL: /Nebby_99/answers/134025508784



## Will you take the test http://www.abtirsi.com/quiz2.php

How fixed are you politically?
### asked by Anon

If you mean fixed as opposed to broken, I am well fixed.
___
Answered on December 15, 2015 09:59:19 GMT

__Likes:__ 0

Original URL: /Nebby_99/answers/134160457648



## What's the best thing about being married?
### asked by Anon

Probably having had a wedding? Not much changed about our relationship, but the wedding was a special time and I enjoy having those memories. The honeymoon was epic as well.
It's a major life event that stands out when you look back. It's good to have a big milestone like that every few years, so that the years don't all just blend together.
I don't really enjoy being married as a status per se, I guess because it feels like a privilege. Or rather, a right that is denied to certain other couples.
My wife is cool too I guess :)
___
Answered on December 04, 2015 12:36:29 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/132370513584



## Do you think your son looks like you? Can you see a resemblance?
### asked by Anon

Sometimes I can see a momentary resemblance, especially now as his face is getting a bit longer due to teeth. He looks more like baby pictures of me, especially early on.
I can't find the baby picture of me to compare to, so you'll have to take my word >.>Here's my current wallpaper though.
___
Answered on October 22, 2015 09:58:43 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/132918903984



## How have you been? Kicked any innocent trees lately?
### asked by Anon

Nice try, coppa. I'm doing just fine.
___
Answered on September 19, 2015 00:26:55 GMT

__Likes:__ 1

Original URL: /Nebby_99/answers/132127286192



## What's the last photo you took on your phone? Post it!
### asked by Anon

wat is he doin
___
Answered on September 03, 2015 12:29:40 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/131742056112



## Is there a story behind your profile picture? What is it?
### asked by Anon

Yeah, it's an old MSN Meego I didn't pay for and have periodically updated (with photoshop & google image search). Example attached from the WinXP & Nokia era.http://www.meegos.com/
I need to update it again...
___
Answered on August 22, 2015 03:05:40 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/131398098096



## Neb and I did boot this tree a lot - as like 6 year olds, so we obviously did only superficial damage to this 1.5 m diameter tree. A cranky teacher saw us and gave us both detentions - my first and ONLY detention! Surprisingly, my dad correctly guessed I got a detention when he picked me up that day
### asked by OlympusMonds

tru fax
___
Answered on August 13, 2015 09:32:21 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/131136655280



## What's this about you harassing trees? What's a tree ever done to you, except help provide you with clean air to breathe? Hey?
### asked by Anon

*sigh*
As a young kid on the school playground I found that kicking one particular large gum tree with my school shoes took a neat little chip out. I tried a few times, I think because the physics of it was unintuitive. The tree was not in significant danger.
Anyway a crusty old teacher saw me, chewed me out something fierce and gave me detention.
My mum (who later worked there) gave the teacher crap about it for years. The teacher did apologise at one point, saying she'd had a bad morning.
So that's 50% of my ridiculous primary school rap sheet. I have no beef with trees. @OlympusMonds can verify/correct my story, he was there for the whole thing.
___
Answered on August 13, 2015 08:00:56 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/131134405040



## What personality type are you out of the 16 personalities?
### asked by Anon

I assume this means Myers-Briggs type? I understand these are kind of garbage and often abused, but I find them useful/compelling when regarded strictly as a crude model for understanding some key differences between people.
I'm an INFJ: The Counselor/Idealisthttp://typelogic.com/infj.html
Introverted (very)iNtuitiveFeelingJudging (maybe more Perceiving in practice)
While I'm getting carried away, on the DiSC spectrum I fall under Steadiness/Stability/Sincerityhttp://1.bp.blogspot.com/-scgm-pVcEXk/UfKSC6uMgNI/AAAAAAAAEbw/RHXg0ZTxIrc/s1600/DISC-Pie.jpg
___
Answered on August 09, 2015 12:50:16 GMT

__Likes:__ 0

Original URL: /Nebby_99/answers/131019215280



## Where do you sit on the systemic problem vs. personal problem spectrum. Like, I would say poor people who commit crime face systemic issues, which may explain why they turned to crime. Conversely, a banker who defrauds is a personal buttface. This seems unfair.. but I don't care much..?
### asked by OlympusMonds

I think there are always elements of both systemic and personal factors, but power dynamics provides a justification for treating those two examples differently.
Both people are within a system that necessarily influences their actions, but one of them is much more at the mercy of that system. What happens if the poor person doesn't steal? No phone? No food? Whereas the banker who doesn't defraud? Fewer cars/houses/shares? If he's in a pickle he might go bankrupt, but not likely to wind up poor.
I think that distinction is important when judging a person's behaviour.
Another key distinction for me is whether people or corporations are targeted (dgaf about corporations personally). Also the wealthier criminals want more, can get more and so tend to steal greater amounts.
The asshole banker is, of course, a product of his upbringing/genes/society. We have to hold people to certain standards of behaviour though, so there needs to be consequences. Them as can afford to pay for their transgressions, ought to. Often we see the opposite.
___
Answered on August 07, 2015 08:08:49 GMT

__Likes:__ 1

Original URL: /Nebby_99/answers/130952222896



## You only dabbled in being an asshole for 15mins? I've done poops longer than that.
### asked by Anon

I've been an asshole pretty frequently, but by dabbling I mean an identity thing. When kids think (on some level) "maybe I can be this person" and back out if it goes badly.
___
Answered on August 05, 2015 14:38:22 GMT

__Likes:__ 1

Original URL: /Nebby_99/answers/130732297136



## What's the worst thing you have ever done? (repost)
### asked by Anon

I wanted to bring this up again because as I was writing my last answer, I was forgetting my best friend's birthday, which has to be up there as one of the worst things I've done
It's a bit of a rich vein actually, because I've given him some shitty gifts over the years. Including forgiving lunch money debt and giving him my old computer parts. He's always been super awesome about it though. I guess I've been lucky in finding friends I don't deserve.
___
Answered on August 05, 2015 14:35:16 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/130901556400



## Is HL2 really that good? Ep. 2 is by far the best HL experience, but even after HL2 came out - I guess i liked it, but I kinda had to, to satisfy the hype I had. I like the design stuff in it too - but every time I play it again, it just doesn't do it for me.. 
What do you think, and am I crazy?
### asked by OlympusMonds

It was innovative, perhaps mainly in the technological sense. I think what it did that was mind-blowing didn't age well. The pacing was very last-generation, and that's the 2nd-biggest reason why I haven't been able to play it all the way through again. It feels very padded now (ie. with sparse story events), but that's what those kind of games were mostly like back then.
Alyx was amazing and unlike any AI companion I'd played with before. The setting was gripping and immersive. All the physics-based gameplay was clever and high-tech. All of that with Valve-grade polish/play-testing made for a great experience, I think. It was compared a lot to Doom 3, which was boring and less impressive in almost every way (except maybe menu music, but how cool was the HL2 menu).
I don't think you're crazy, but I do think it was an incredible game that set the standard for a new generation. I think it lived up to it's hype more than most games with bigger marketing budgets. Hype is hype though - it's never going to be the perfect thing you somehow expect. We're wiser now.
___
Answered on August 05, 2015 13:16:25 GMT

__Likes:__ 1

Original URL: /Nebby_99/answers/130895071152



## Really? I don't think I remember..? Well, I wouldn't worry about it, cause apparently it hasn't gotten to me.
I'm sure I must have repaid the favour in annoyingness in high school :/
### asked by OlympusMonds

It's tempting to assume I just dreamt it up, but I think it happened. I dabbled in being an asshole for about 15 minutes, as kids tend to do at one point or another. I got caught up in some groupthink and told you to "piss off" I think?
It was at school, but my mum was around (which makes it sound like a dream, but that did happen a lot) and she gave me a figurative ass-kicking over it. Good thing, too.
I don't remember you being particularly annoying in high school. I just remember me being needlessly  judgemental about your style/subculture choices.
___
Answered on July 30, 2015 23:19:14 GMT

__Likes:__ 1

Original URL: /Nebby_99/answers/130716665008



## What's the worst thing you have ever done?
### asked by Anon

First thing that comes to mind is when I was a massive asshole to my best friend in primary school.
I'm probably blocking out something even worse though.
More recently I just fail to help strangers a lot. I tend to let my insecurity and aversion to causing embarrassment override my desire to help people who would probably appreciate it. I take note of every failing in the hopes I'm eventually compelled to action by accumulated guilt. #selfhelptips
If anyone knows of something worse I did, hmu.
___
Answered on July 30, 2015 08:33:37 GMT

__Likes:__ 4

Original URL: /Nebby_99/answers/129343465904



## One time, you ate the last red jube in the packet when I was really looking forward to it
### asked by happyhaps

Ugh, again with this.
___
Answered on October 13, 2016 11:17:49 GMT

__Likes:__ 9

Original URL: /Nebby_99/answers/139780642736



## What’s one of the worst things you did as a kid?
### asked by Anon

I'm gonna recycle this, 'cause I'm lazy:http://ask.fm/Nebby_99/answers/129343465904http://ask.fm/Nebby_99/answers/130716665008
___
Answered on October 12, 2016 11:17:58 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/139771799216



## Who has the most beautiful eyes?
### asked by Anon

@happyhaps - I remembered this pic from 2005 and dug it out.
___
Answered on October 10, 2016 09:31:28 GMT

__Likes:__ 5

Original URL: /Nebby_99/answers/139748971952



## how did you get RSI? please say it was from wanking :D
### asked by Anon

I wish it was that simple, but alas no.
There are a few primary factors, based on what I've pieced together over the past 5 years: - I'm hypermobile (undiagnosed), meaning my joints are looser than normal. This affects my posture and means my joints don't work as smoothly as they should, causing increased wear and added strain on the muscles that stabilise each joint. - My poor posture means I carry a lot of muscle tension in my shoulders, which in turn aggravates the nerves (from vertebrae C6 or C7 I think) that go into my arms, causing referred pain in my wrists. - The nerve irritation causes the nerves in my arms to excrete a substance (typically a response to damage) which makes my muscles get tight/knotted more easily. This showed up as an 'edema' on an MRI, meaning the tissue was swamped with fluid. - The knotted muscles in my forearms require regular massage or the tension combined with repetitive movements and low range of motion (if I'm not doing stretches) pull my wrist out of whack causing a different kind of wrist pain and affecting my dexterity.
I can avoid the nerve pain in my wrists (which isn't severe, just makes it hard to work) by maintaining good posture (which I find very difficult). The muscle tension can be managed with stretches and regular (costly) massage.
The most recent addition to the party is joint inflammation in the 2nd knuckle of my right index finger. I have to be careful not to strain it (eg. by massaging my arms) and do most mouse work with my left hand, or it flares up and I can't do much with it for a while (days, or weeks). This is primarily what has killed gaming as a regular pastime for me. I can't do long sessions at the computer and I have to take regular breaks at work. I suspect it's irreversible and the others will follow, but it does seem less frequent if I keep the forearm muscles from getting too tight.
That's more or less the run-down of what I broadly refer to as "my RSI".
___
Answered on September 19, 2016 12:23:54 GMT

__Likes:__ 5

Original URL: /Nebby_99/answers/139477360816



## Do you enjoy writing by hand (as opposed to typing)?
### asked by Anon

Hells no. My RSI means typing is hard enough, but handwriting is quickly painful. My hand & arm muscles get crampy very easily, and my knuckles are starting to go bad.
I can appreciate the benefits of handwriting and I still carry a notebook at work for freeform note-taking, but I always prefer typing.
I've always been bad at handwriting. I had to practice at lunchtimes for a while in primary/elementary school. I got special consideration for my final high school exams, so that I could complete them on a laptop. That was about pain/discomfort rather than my writing being messy. But damn is it messy.
___
Answered on September 19, 2016 07:55:14 GMT

__Likes:__ 4

Original URL: /Nebby_99/answers/139476168880



## Nice hat. Are you a cowboy?
### asked by Anon

Thanks. And no, I'm an Australian with big ears.
___
Answered on September 05, 2016 03:10:10 GMT

__Likes:__ 4

Original URL: /Nebby_99/answers/139237593008



## i don't need to see it, man. i trust happyhaps. what's your sekrit? squats?
### asked by Anon

Uhh, I run up and down stairs a fair bit on my commute. Can't think of any other explanation. Besides genes.
___
Answered on August 03, 2016 10:59:14 GMT

__Likes:__ 5

Original URL: /Nebby_99/answers/138674966704



## Tinman got a heart, Scarecrow got a brain, Lion got courage...what do you get?
### asked by Anon

Constructive self-delusion.
I mean, uhh, hope.
___
Answered on August 03, 2016 10:45:15 GMT

__Likes:__ 9

Original URL: /Nebby_99/answers/138674963120



## hey nice butt man. sup?
### asked by Anon

My pert buttocks apparently.
Thanks I guess, rando who has never seen my butte.
(Context: http://ask.fm/happyhaps/answers/137634290221)
___
Answered on August 03, 2016 10:43:34 GMT

__Likes:__ 6

Original URL: /Nebby_99/answers/138674277040



## How's yer face?
### asked by OlympusMonds

My face is still attached, but it hurts a fair bit. Still really dependent on pain relief, but apart from that it's mostly just a matter of not touching my nose.
(For context, I had sinus surgery a few days ago)
___
Answered on July 11, 2016 03:53:32 GMT

__Likes:__ 4

Original URL: /Nebby_99/answers/138217209008



## Speaking of Twitter, what do you think of Notch's accelerated descent into MRA madness?
### asked by OlympusMonds

I don't know, it'd all be pure speculation on my part as I never knew him and his lived experience is extremely far from my own. So here's some bad and wrong speculation:
I think he was ill-equipped to handle his rise to fame and the stress/complexity that came with it (he's more or less said that himself). Part of that was he couldn't handle the inevitable hate-waves, the way an asshole like Garry Newman can. Maybe because he wanted to be the good guy but couldn't find a way to do it.
Maybe all the nastiness he copped (whenever some dickhead didn't like something to do with Minecraft), and the fact that he sold out to Microsoft in the end (disappointing a lot of people [including me]) messed with his identity a bit. His value set had to change, and he gravitated towards entitled MRA fuckboy.
It's just as (or more) likely that being rich has just disconnected him from reality and the need to pay heed to criticism. Maybe he got into a bitter spiral of pushing away people who care about him because they had needs, or tried to curb his excesses, and is now left with only shallow acquaintances. Privilege + loneliness seems like a recipe for MRA bullshit. Blame the world because no one wants you, when you expect not to have to compromise on anything ever.
I'm sad for him, but regardless there's no excuse for that shit.
This has been ignorant and inappropriate speculation with Neb.
___
Answered on July 05, 2016 10:56:01 GMT

__Likes:__ 6

Original URL: /Nebby_99/answers/138100960688



## How are you feeling about the election? You seemed down on Twitter.
### asked by OlympusMonds

Pretty down about it. There are good aspects and bad aspects so far, but I'm always disappointed to let go of the actual good outcome my idealist brain clings on to.
We could have saved a lot of people from needless suffering. We could have started doing our part to save the climate. We could have taken the government back for the people, to some degree.
Turns out 90% of people won't vote Greens for those things even if they do want them, instead most will vote for one of the majors and then whinge about the continuing shitty outcomes.
To be clear, I don't think people are stupid, I think they're trapped in a false dichotomy and are constantly fed bullshit from every direction. Most people have different values than me, but I think a lot of them want broadly similar outcomes for the country/world. They're just not equipped with the information to leverage their vote towards those outcomes.
The other depressing part, I guess, is that I didn't do more to try changing all that. Greens had zero representation at our polling place. Hopefully I can take that lesson for next time.
It's too late in a lot of ways though. Things will probably have to keep getting worse for years now.
___
Answered on July 05, 2016 08:32:14 GMT

__Likes:__ 4

Original URL: /Nebby_99/answers/138098218928



## Upon rising, can you recall your dreams?
### asked by Anon

Uhh, sometimes.
Happens more often when they're interrupted (that is, if I'm woken mid-dream), as I understand it.
___
Answered on March 07, 2016 19:57:25 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/135860224688



## Whats your current music taste rn?
### asked by Anon

I made the mistake recently of listening to schmoyoho's old Auto-Tune the News series again. Despite being quite dated now, they immediately lodged into my brain as if they'd never left, and I haven't been able to dislodge them. Here's a sample:
Most of the stuff I'm into at the moment is electronica (eg.
with some electroswing (eg.
I'm just tapering off a full-blown addiction to the soundtrack of the game Undertale (eg.
I listened to it obsessively for a month or two, then progressed onto remixes for a while.
I also have some Weird Al and Lonely Island that I've been listening to regularly for ages. Oh and some Pentatonix.
___
Answered on March 04, 2016 13:53:18 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/135797657264



## Omg, I got a great answer and a pic too! I asked yr brother a q once and he was mean to me :/ Thanks for not making fun of my spelling,"petspective" *cringe* but do u have a pet who could give me their perspective too now that I think about it? Maybe they felt left out?
### asked by Anon

Haha, try not to let my bro's abrasive style discourage you, it's not aimed at you personally.
I tend to view spelling and grammar pedantry as intellectual elitism these days, so I leave it alone if the intended meaning is clear.
We have pet chickens - they probably receive less attention these days, but they've been good about it. They were pretty confused about the baby at first but now they've settled into a relationship of occasionally being chased around the yard by him.
Pictured: First chicken encounter vs recent chicken encounter
___
Answered on February 14, 2016 09:21:57 GMT

__Likes:__ 6

Original URL: /Nebby_99/answers/135454644656



## I would like to hear things from your petspective if ur willing to share. How did u come to love yr baby?
### asked by Anon

Sure. It's a complicated thing to express. I remember early on in the pregnancy the uncertainty around it all drowned out any other emotional response for the most part. It was exciting and terrifying.
Hearing the heartbeat was a pretty big thing to me. I didn't really expect it, but it was the first perceptible sign of life (I think we'd had an ultrasound by then but it was pretty unclear and kind of alien). I was a bit awestruck and giddy when I heard it. It still wasn't like love though, because it still didn't seem like a person. It made the process more real and helped me envision the outcome.
I think I initially started caring a lot about it as an investment more than feeling an emotional bond. You're already dealing with the cost and risk and stress, but the upside is all speculative. Haps slogged through the first trimester and if something had gone horribly wrong at that point, I think I would have been upset at the massive wasted effort and trauma more than having lost a hypothetical person (I could be wrong).
As we got later in the pregnancy, there was a growing sense for me that I had a lot of love to give the person once I met them. I don't think it's the same as actually having a bond but the line probably got blurry sometimes. I'm sure I personified the foetus sometimes, and was excited when I poked at him and seemed to get a reaction. It got close to seeming like a person towards the end, albeit one that couldn't be directly observed.
When he arrived I was instantly super protective and nurturing, which was a powerful connection. We didn't really know each other yet, but I was absolutely committed to building a relationship. As we spent heaps of time together and got to know each other (which is still an ongoing process really) I got to love him a lot as a person. The relationship is constantly evolving as he grows and changes (and his life-world does too) but it stays strong.
Pictured: Tiny baby, strong feels.
___
Answered on February 14, 2016 05:07:58 GMT

__Likes:__ 9

Original URL: /Nebby_99/answers/135453826480



## hey. just checking on your peen. is it ok? still in one piece?
### asked by Anon

Well, this got weird.(Context: https://ask.fm/happyhaps/answers/134382651949https://ask.fm/happyhaps/answers/134382749485https://ask.fm/happyhaps/answers/134382819117)
My peen has yet to run afoul of tooth or melon baller. This is partly thanks to my policy of keeping my peen away from melons (except in special circumstances).
___
Answered on February 09, 2016 12:46:50 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/135368527792



## what are u famous for? why are u a celeb?
### asked by Anon

I'm famous for being @Soul_James's brother.
___
Answered on February 08, 2016 10:12:51 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/135346647472



## how much are u selling your lube for, m8? can i get mates rates?
### asked by Anon

Only if you're a Kickstarter backer, at the "Fetch me waders!" tier.
___
Answered on February 06, 2016 09:16:22 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/135310511024



## 6 months ago, on this very website, u said u were going to update your profile pic (scroll back if u don't believe me). do always take so long to do things?
### asked by Anon

Not always.
___
Answered on February 06, 2016 05:28:26 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/135308078512



## Make an anagram out of your name!
### asked by Anon

An Lube™ By Neb
or Able Be Bunny
___
Answered on February 05, 2016 22:44:01 GMT

__Likes:__ 4

Original URL: /Nebby_99/answers/135306154416



## are you muddled at all, or are you used to this?
### asked by OlympusMonds

(Context is an IRL conversation that went until 6am and was followed by this: https://ask.fm/Soul_James/answers/134910058313)
I can't really tell if I'm used to it or haven't really had it happen properly yet.
I guess my view changed on the scientism vs religiosity thing gradually in recent years so it's really only a stark change in retrospect. While the conversation was interesting, I wasn't really shaken on the physical determinism front (quantum physics notwithstanding). I still see the physical universe as fundamental and my consciousness/perspective as a phenomenon within it. I don't think inverting that would have a meaningful effect on my values or day-to-day life, but I'm keeping the possibility in mind so I can become more aware of how it might.
After further reflection I still don't find dualism at all compelling or necessary, but I guess my main take-away there was that my understanding of consciousness might be limited or simplistic in some way that I'm not aware of. I'm not convinced that taking drugs is the path to enlightenment there tbh, but keeping an open mind.
I can see how describing the mind as a 'machine' plays into scientism and the exploitation of people by capitalism, but that doesn't alter my view on what is 'true'. I'd consider using different language, avoiding the topic or entertaining a constructive delusion in public discourse to minimise the harm, but I can't fundamentally change my view of reality for political purposes.
___
Answered on February 04, 2016 12:46:26 GMT

__Likes:__ 1

Original URL: /Nebby_99/answers/135259911856



## Could you see yourself living to be 100?
### asked by Anon

A more pertinent question might be: Can I see myself dying? I don't think I'm there yet, really.
As far as my odds of reaching 100, I don't know. I'm certainly not taking it for granted.
___
Answered on January 30, 2016 11:29:38 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/135181328304



## Were you present for your childs birth? If yes, was it an easy decision to make? I know it's increasingly common, expected even, for the father to be present but should those who choose not to be vilified? A squeamish friend was at the hospital but not in the room & people implied he was a jerk.
### asked by Anon

I was present; I was the 'birth partner' so I stayed there the entire time, providing emotional support and representing her interests when she was incapacitated. I wouldn't have had it any other way. Watching it go down isn't as hard as going through it, so it's the least you can do imho. That said, if you can't handle it you can't handle it.
You've got to understand the process, understand what can go wrong and the alternatives, and (very importantly) understand your partner's preferences. One example of where this came up was close to delivery when the doctor indicated he might do a episiotomy, which I knew was absolutely not acceptable. Haps would have been able to express her position if needed I think, but she was under insane pressure and extremely exhausted, whereas I was ready to punch on if he tried it and I made that pretty clear.
It's only really slack not being there if you haven't bothered to learn enough about it to know whether you can actually handle it. "My mates said it was super gross" etc. Or like, viewing your partner as a sex object to the extent that you can't handle the reality of birth. I've heard guys say some shallow nonsense about "sex being ruined forever" etc. Such a messed up perspective. I don't think your friend should be vilified, but imho he should have a think about what grosses him out about it and why. And if he really, honestly, could not physically cope with being there.
I watched a doco ages ago about a bunch of Dads being birth partners in the UK, and a lot of them were terrified/grossed out/wanted nothing to do with it. By the time they had a couple of lessons on what the hell is going on they were 1000x times more comfortable and ended up doing great. So while everyone has their limits and I respect that, it's worth making an effort.
P.S. Doco was "Don't Just Stand There, I'm Having Your Baby"
___
Answered on January 07, 2016 12:42:18 GMT

__Likes:__ 3

Original URL: /Nebby_99/answers/134693190576



## A couple of men at my work have taken a year off without pay to look after their children. Does this appeal to you? Obviously they're in priviledged positions but it's made some of the other fathers around the office a bit jealous. Not all, some say they wouldn't cope being the primary carer.
### asked by Anon

It certainly appeals to me. I had 2 months off which was great, but going back to work was still awful. It's always been my position that I'd happily be primary carer if the option was there.
Working full-time means parenting part-time, and that sucks for everyone involved.
I'd be jealous, but I sure as hell wouldn't complain - good for him.
___
Answered on January 03, 2016 08:43:22 GMT

__Likes:__ 2

Original URL: /Nebby_99/answers/134603113136
