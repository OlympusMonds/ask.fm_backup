

## why do you like to be called Charlie? is it anything to do with Charlie Brown or Charlie and the chocolate factory?
### asked by Anon

No. My middle name is Charles, and growing up one of my nicknames at home was Charlie. This was actually a family tradition. For generations the first born men were called Charles, until my Granddad was called Edward, but he had Charles as a middle name. He was then called Charlie by his family. My Dad also had Charles as a middle name, and was also called Charlie by his family.So I was always called Charlie at home, but it wasn't until I was sixteen that some friends heard me called Charlie and started using it at school, and for some reason it really stuck. Ever since I've just gone by Charlie, introduced myself as Charlie (or Charles), and sometimes it takes people years before they realise that it is not my first name. But it is definitely my real name,
___
Answered on August 07, 2016 06:42:59 GMT

__Likes:__ 8

Original URL: /TheGracchan/answers/138635384702



## whats it like to maintain a friendship with a guy like soul for so many years? i imagine he talks a lot, you ever get tired of listening? be honest!
### asked by Anon

Soul does talk a lot, but honestly I've never gotten tired of it. In fact it sustains and motivates me. It's my manna.
Soul and I have been best friends since we were about 15, and our relationship has only grown stronger and deeper. We've joked on occasion that our friendship is like that of Professor X and Magneto, except in this universe we came to an understanding with each other. I'm not quite sure how to explain that analogy right at the moment, but it makes sense to us.
___
Answered on August 07, 2016 06:38:28 GMT

__Likes:__ 9

Original URL: /TheGracchan/answers/138554149758



## Have you faced any discrimination for being a ginger? We live in a cruel world.
### asked by Anon

Not really. I was never identified as a ginger until I started growing a beard, as my head hair is actually blonde. I was teased for other things growing up, but never for being a ginger. If it does come up nowadays it doesn't affect me due to not having that childhood history, and I just look at the person like they're a blithering idiot.
___
Answered on July 19, 2016 21:14:08 GMT

__Likes:__ 4

Original URL: /TheGracchan/answers/137979368574



## Are you a hairy pumpkin?
### asked by Anon

@dadcliffe does not lie. The pumpkin part is mostly flavour, but I am indeed a hairy, hairy man. I don't think I've ever had a pet name before so I'm quite fond of it.
___
Answered on July 19, 2016 21:06:55 GMT

__Likes:__ 5

Original URL: /TheGracchan/answers/136886668414



## Beards are just beards, one is not better than another.
### asked by OlympusMonds

Well yes, if we are speaking objectively. Subjectively on the other hand...
___
Answered on July 01, 2016 23:46:37 GMT

__Likes:__ 5

Original URL: /TheGracchan/answers/137980042366



## Who has the better beard, you or @OlympusMonds?
### asked by Anon

Well I think mine's better, but then I'd assume @OlympusMonds would say the same about his. I don't like the big bushy style, or the hipster style that's in fashion. I guess it's because my inspiration was initially Blackadder.
___
Answered on July 01, 2016 23:39:00 GMT

__Likes:__ 5

Original URL: /TheGracchan/answers/137979542910



## ty! it's great. that's a head that belongs on coins  :)
### asked by Anon

It was drawn my @rmaynard85
___
Answered on June 28, 2016 23:36:11 GMT

__Likes:__ 7

Original URL: /TheGracchan/answers/137873830526



## how do ~you~ remember the first meeting with your current partner? what are your memories of it?
### asked by Anon

I remember I was doing the washing up when we were introduced, but I have a feeling I may have confused that with another occasion. My main memory from that meeting was hanging outside around a fire, trying to think of something cool and interesting to say to her. I can't recall what I said but I guess it wasn't anything too embarrassing. After she left, her partner's sister (who was my housemate) asked me if I had a crush on her, as she thought that Lex was just my type. I denied it, but the truth was of course I bloody well had a crush on her!
___
Answered on June 28, 2016 23:35:20 GMT

__Likes:__ 8

Original URL: /TheGracchan/answers/137811569790



## do u usually cavort around the house in just a towel after you've had a shower, hussey?
### asked by Anon

I wouldn't say I cavort, but I'm not opposed to stopping for a chat on my way from the bathroom to my bedroom.
___
Answered on June 22, 2016 10:53:55 GMT

__Likes:__ 6

Original URL: /TheGracchan/answers/137766492286



## r u competitive when u play games with your partner? do u like to win? r u humiliated when and if u lose to her?
### asked by Anon

Why would I be humiliated?
We're both quite competitive, and delight in mocking the other in defeat. She has a stellar record in killing me in Bang! (no matter where I sit at the table, I just can't escape), and we're pretty much even when we play Ticket To Ride on our iPad's. The most outraged I can recall her getting was when I won a very large castle from her in Carcassonne. Apparently she had always shared castles with other players if they were co-building, but I (being new to the game) had read the rule book and realised I could just take it all if I had more pieces in the castle once it was completed. She's never forgiven me.
___
Answered on June 08, 2016 13:15:44 GMT

__Likes:__ 5

Original URL: /TheGracchan/answers/137483606142



## Is there any reason why you chose to be a high school teacher over a primary school one?
### asked by Anon

There are a few. I wanted to out my degree to some use, and I figured that would require students who were able to grasp abstract concepts. I also think I'd enjoy conversing with teens more than children for the same reason. I can't really remember primary school, so when I think of school I automatically think of high school. The other important reason is that primary school students seen much more motivated and enthusiastic about learning. I volunteered as a Primary Ethics Teacher for a couple of terms and that is what really struck me about primary school. Teens on the other hand don't give a fuck, and seem in more need of someone who might actually give a fuck about them.
___
Answered on January 16, 2016 21:01:53 GMT

__Likes:__ 2

Original URL: /TheGracchan/answers/134824990078



## were you raised with a religion? if yes, how do you feel about it today?
### asked by Anon

I was not raised in a religious family, but my grandfather would sing me Christian songs, and I remember having a board book that described how God created the world. While my immediate family was not religious they did believe in God, but as this never seemed to have any bearing on their actions I became rather contemptuous of this belief. The belief in God should mean something, right? In my teens I realised that the term that best defined my beliefs was atheist. My friends were also atheists, and the Christians I knew were boring and lame.I've since come to understand that I grew up in a nihilistic society, which shaped my outlook on life as much as if I had been explicitly raised religious. There's no escape from ideology after all. I could no longer consider myself a Rational Individual. With this understanding also came more positive examples of religious belief and people, and an appreciation for a spiritual or divine aspect to ones life, along with the communal support that sustains it. I have a simple view of Buddhism which gels with some philosophers I felt were on to something, but I haven't pursued it in any real sense. Partly because I'm wary of cultural appropriation and western Buddhism's accommodation with individualism and capitalism, but also because I just can't be bothered and it wouldn't feel authentic. For better or worse I've been raised in a (post)Christian society. Lately I've come to think that any access (for me and other post-Christians) to the divine must be mediated through the language of Christianity. It's our cultural heritage, and some theologians apparently consider atheism to be a specifically Christian heresy. So I'd like to read the bible and some theology. These are mostly intuitions though, I don't think too deeply about them, and while I do feel the loss of religious belief I honestly can't ever seeing myself believing in the divinity of Jesus or a personal God. Not sure where that leaves me to be honest.
___
Answered on January 16, 2016 04:24:57 GMT

__Likes:__ 1

Original URL: /TheGracchan/answers/134821840510



## Why Gracchan? Do you have a keen interest in Roman history?
### asked by Anon

I would say I have a keen interest, though I would not go so far as to say that I am extremely knowledgeable on the subject. I studied Ancient History in High School, and my teacher assigned us all the name of a historical figure. Mine was Tiberius Gracchus. I was taken with the tale of the Gracchi: well meaning reformers fighting against corruption and the inherent contradictions of the empire of the Roman Republic, only to be murdered for their efforts. I then used The Gracchan as my internet handle, and it is the first one that really stuck with me. Before then it was all song lyrics and such, in the days of MSN messenger. Anyway, I went to Uni to study Ancient History, got sidetracked and majored in Philosophy, decided to become a high school teacher and so completed a couple more units of Roman history, and will eventually become an Ancient History and Social Studies teacher.
___
Answered on January 09, 2016 11:26:26 GMT

__Likes:__ 4

Original URL: /TheGracchan/answers/134603720318



## Hey buddy, good to see you here. How's things? Express yourself.
### asked by Soul James

To be honest things are quite good. Lex and I have been together for just over a year (and have basically been living together for that entire time), and I am also employed, which makes life so much less stressful than living off of Centrelink. We're planning a holiday, and I don't have to deal with the bullshit that is University any longer. My main problem now is what to read, and as you well know that is a full blown existential crisis for me.
___
Answered on January 09, 2016 11:16:20 GMT

__Likes:__ 3

Original URL: /TheGracchan/answers/134346646398



## Hahaha! Did you change your name because of the last q???
### asked by Anon

Yes, but only because I didn't realise my name on here was Chuckles, which is what I get for linking with twitter. I had forgotten that was my twitter name too. My usual internet handle is Gracchan.
___
Answered on January 03, 2016 23:23:27 GMT

__Likes:__ 1

Original URL: /TheGracchan/answers/134365171838



## What's so funny, Chuckles?
### asked by Anon

No answer I could give.
___
Answered on December 25, 2015 09:54:07 GMT

__Likes:__ 0

Original URL: /TheGracchan/answers/134318125694
