

## hhttp://pietrzakadrian.blogspot.com/2015/06/awake_46.html can u write comment on my post? thanks! xx
### asked by Anon

No thanks, probable spambot.
___
Answered on June 22, 2015 10:06:07 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130265884875



## What's your favourite book, tv show and movie?
### asked by Anon

Book = The Vampire Lestat. I was massively obsessed with the Vampire Chronicles growing up. #noshame #actuallylotsofshame I'm forced to read so much at work that I really don't feel like reading much these days. And I used to LOVE books. TV show = Nichijou. It's such a funny anime, most of its segments are very rewatchable. Movie = My Neighbour Totoro. One of the earliest movies I recall watching, still the best.
___
Answered on June 22, 2015 10:04:05 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130264715211



## I was pleased to read that you 'literally never shut up'. Don't be silenced, sista! My question - do you wear socks to bed when its cold?
### asked by Anon

Yeah I'm not a shut up kinda gal. The only time I think I get noticeably quiet is when I'm getting increasingly hungry. Then it hits a threshold whereby the quiet turns into hunger induced rage.
*BelindaBruceBanner looks back at camera* "That's my secret, Captain. I'm always hungry."
Anyway, nah I'm not a sock-in-bed person, no matter now cold. I tried it a few times but it didn't catch on. I prefer bare feet tucked under covers, to keep the warmth in and the monsters out.
___
Answered on June 22, 2015 08:39:50 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130263791051



## The way yr husband talks about you on here is very charming/nice. Pretty sure he must love you heaps. Do you prefer him clean shaven and prickle free?
### asked by Anon

DEFINITELY CLEAN-SHAVEN. But, I can totally understand how annoying it is to frequently shave. Plus, once the hairs grow out long enough they stop being so prickly.
However, I do hang onto the hope of one day see more of his face skin once again.
___
Answered on June 21, 2015 04:15:37 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130223712715



## Hey gurl, why don't you post more selfies?

Get some shots of that luscious ass and sweet sweater-puppies out on the web! Oh, and face too. Good face.
### asked by Anon

Well since you asked so nicely, here are some sweet sweater puppies: http://imgur.com/4rczMUw
___
Answered on June 20, 2015 12:43:36 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130196265931



## I've also asked Tim this but who is your fave Friends character? I feel Tim doesn't really appreciate Phoebe's excellent comic timing tbh.
### asked by Anon

Tbh my answer would be similar to Tim's. I like Monica, Joey and Chandler, in that order, most of the time. I think I resonate with Monica's weird competitiveness. I like to think I'm not that bad, but I totally get it.
Joey's just adorable. Chandler is funny but he can be pretty manipulative, like that time he deliberately made Monica depressed so that she'd stop harassing him to exercise with her.
Phoebe is ok, I don't dislike her as much as Tim does. But her randomness doesn't do much for me. What I do like is her self confidence though. That's awesome to see from a female character on a 90s sitcom.
Rachel and Ross deserve to be together, they're such a trash couple.
___
Answered on June 27, 2015 14:26:52 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130440221643



## Is your avi just a random pic or is she from 'somewhere'?
### asked by Anon

Nichijou. She's the teacher on the show. She's adorable, like many of the characters on the show. She gets startled easily, like me!
___
Answered on June 26, 2015 10:38:13 GMT

__Likes:__ 0

Original URL: /cosmicmissb/answers/130412214731



## What was your first thought when you woke up this morning?
### asked by Anon

"Uggggghhhhh... This shit again."
___
Answered on June 25, 2015 08:12:31 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130374207947



## Which activities or hobbies make you lose track of time?
### asked by Anon

Video games.
Sleeping.
___
Answered on June 25, 2015 00:20:01 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130365323979



## How good is green tea? Very good, of course! I'm also partial to the good quality white stuff. A lot lighter than green & maybe a bit bland but great to drink with meals imo. Ginger Kiss (Ginger/Rosella/lemongrass) from The Tea Centre is v nice too. Madame Flavour's Green jasmine & pear teabags
### asked by Anon

Oooh green jasmine and pear sounds very delicious, I will give that a go. I've heard good things about Madame Flavour, I will definitely try it out.
I've been meaning to visit the Tea Centre, I might go today at lunch.  Thanks!
___
Answered on June 25, 2015 00:19:27 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130365149899



## What is your alcoholic drink of choice? What is your non-alcoholic drink of choice? I've been really getting into tea lately and becoming quite a snob about it - organic loose leaf or nothing.
### asked by Anon

I'm not a fan of alcoholic drinks.  Alcohol just tastes awful.  My work colleagues keep trying to get me to drink different kinds of wines but they all taste like garbage water to me.
I always have a cup of tea or coffee in the morning, usually tea.  I've been trying fancier teas too!  Loose leaf stuff rocks but it's a hassle to brew when you're in a hurry.  I tend to like black teas to have with sugar and milk or green teas.
I quite like the Melbourne breakfast (black tea with vanilla flavouring I think) and the Terrific Toffee (black tea with delicious toffee bits) from T2.  I tried a sample of Mumbai Maple from T2 a few days ago and it's really nice chai tea.
If you have recommendations, please share!
___
Answered on June 24, 2015 10:16:44 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130339610571



## If you could live forever, would you want to?
### asked by Anon

Yes, PROVIDE THAT:
1. I remain able-bodied and have a clean bill of health physically and mentally for as long as I live;2. It should be the kind of immortality that is sustained by a superior healing factor (rather than reliance on technology or magic that is outside my control) ala Wolverine;3. The exception to #2 is that I should be able to kill myself if required. I'm thinking that the trigger mechanism should be natural, maybe by sheer will I can "turn off" the immortality? This bit requires more thought.4. There are no tradeoffs for the immortality. No "you're now immortal but you find yourself teleported to Pluto" crap.
Otherwise no thanks.
___
Answered on June 24, 2015 08:23:48 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130338453195



## If you wanted to follow your husbands lead and become 'maxi fat' what foods would you indulge in to get to this rotund state? Any preference for sweet or savoury?
### asked by Anon

Yay a food question! :D I have a preference for savoury foods.  I've relatively recently developed a penchant for steak and I plan to put my newly bought cast-iron pan to some good steaky use soon.  I love dumplings and I recently found out (to my dismay) that they're pretty high calories for something so snacky.  I also love my ramen, and Gumshara in the city does THE BEST tonkatsu ramen, being ramen steeped in this really fatty, marrowy, meaty broth.  I'd eat that erryday if I could (and feel terrible after every meal but never learn my lesson).
I have a FANTASTIC (if I do say so myself) fried rice recipe. It's got eggs, peas, corn, meat (usually special Chinese cured sausages or bbq pork) and spiced with oyster sauce and soy sauce. Deeelicious.  But probably incredibly fattening.
___
Answered on June 24, 2015 05:00:50 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130332826315



## Do you have siblings? If yes, where do you fall in the birth order? Do you have any traits traditionally assigned to that placement? eg 1st born very responsible/smart. Last one spoilt etc.
### asked by Anon

Yes I do! I have a brother who is seven years younger than me.
I guess I've always been the "responsible" older one for two reasons:1. Due to the age gap and how busy my parents used to be, I used to babysit my brother A LOT.  I'd like to think I had a substantial hand in raising him, which means I grew up being the bossy one rather than being a sibling on the same level.2. My parents' English is quite poor and from the age of about 6 and up, I became the household mail reader and interpreter and the person who liaise with English-speaking third parties on behalf of my parents. Thinking back, my parents placed a lot of trust in a kid to understand what bank statements and other written correspondence mean.
I'm definitely the more well-read and sensible one, whereas my brother tends to be more scatterbrained.  However, he's coming into his own these days and become a very respectable young man, so kudos to him.  He's also way more stylish than me.
Oh yeah, he's definitely more spoilt IMO.
___
Answered on June 24, 2015 03:00:28 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130332388299



## Ever since you mentioned Tim's, irritating to you (& now me tbh), Batman obsession, that's all he talks about on here! You need to learn from this, gurl!
### asked by Anon

People need to stop asking him Batman related questions. I have no part in this.
___
Answered on June 24, 2015 02:35:22 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/130332292811



## Do you enjoy your job? Any painful colleagues?
### asked by Anon

Yes and yes.
Unlike a lot of lawyers I actually like my job. Mostly because I'm lucky to work in a nice workplace, have a cool boss and really nice coworkers (mostly). Most of my clients are nice people and I get to solve problems all day. And sometimes talk very firmly with people! And be pedantic and argumentative!
And yeah, there is a sucky co-worker. I don't want to go into details but she... sucks.
___
Answered on June 23, 2015 10:43:09 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130302880203



## What's the most delicious fruit?
### asked by Anon

Mangoes. Definitely mangoes.
___
Answered on June 23, 2015 08:53:14 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130302038475



## What's the most ridiculous diet or 'detox' you've succumbed to despite knowing better? I once ate nothing but apples for 4 days which resulted in lots of headaches/lots of time on the toilet (& and a very temporary flat tummy).
### asked by Anon

I'm not a detox kind of person, the idea of eating really poorly (or not at all) for days is very upsetting.  I guess in the preparation for the wedding we were pretty well disciplined with eating vegie stew for dinner a lot of nights of the week for a few months, but I don't think that counts as detox.  I like my food too much!
The closest thing to a detox is that when I used to get sick as a kid, my grandma would force me to drink absolutely vile, black herbal tea.  I didn't recall it every doing much other than taste disgusting.
___
Answered on June 23, 2015 08:07:05 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130300295115



## Cool, thanks for yr informative answer! Was worried I'd come across as being dismissive of  the awful experiences some women have online which I DEFINATELY am not. I've just been getting some weird 'oh u poor thing, a woman on the internet! how awful does that get?' comments.
### asked by Anon

You definitely don't come off as being dismissive, anon!
I think for most women who use the internet on a regular basis, the internet isn't usually a horrible place.  There are lots of lovely places on the internet filled with mostly great people to talk to and share ideas and interest with (that includes some parts of Reddit even).
However, I absolutely believe that there are parts that are prone to being awful and toxic and attracts the worst people.  That shit needs to be cleaned up, pronto.
___
Answered on June 23, 2015 06:25:13 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130296615371



## How has your experience of 'being a woman on the internet' been so far? I've had it pretty good tbh but I'm not a well-known person with controversial views. Don't get me wrong, I realise a lot of women get hideous abuse but I hate ppl implying that its it a dangerous place for all women. Its not.
### asked by Anon

Pretty good actually!  I was much more actively online around 5-10 years ago and made lots of websites and stuff.  Made a number of friends that way. In the circles I ran, people were generally decent, maybe because we shared common interests a lot of the time.
I used to blog a lot and there were some posts that garnered negative responses but nothing I'd characterise as abusive.  The worst that happened was that one time, someone once completely stole my website layout. >:(
Having said that, the weirdest encounter I had with a dude was on Steam.  I can't remember where we met, maybe TF2.  Anyway, we'd chat every once in a while over a period of a few months I think.  He was really into the game Stalker and he kept talking about it and saying I should play it but I didn't want to.  He then gifted me the game (THAT I DIDN'T WANT) and got super upset when I didn't play it.  He then blocked me. Huh.
___
Answered on June 23, 2015 03:05:18 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130295429579



## Does Tim know that he can have regular conversations with you that don't have to be "anonymous"?
### asked by Anon

I wonder that myself.
___
Answered on June 23, 2015 02:48:14 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130295617739



## You.. shame me...
### asked by Tim Marsh

<3
___
Answered on June 23, 2015 02:47:14 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130295531467



## What traditionally 'girly' things do you enjoy doing, if any?
### asked by Anon

Hmm I like knitting and crocheting, and general crafts.  I'd love to learn sewing and embroidery one day.  I like the concept of cooking, but I'm often too lazy to go buy ingredients.  Cooking a delicious meal makes me feel like some kind of food wizard.
I think I like making things in general when I have the time (which is never), whether they are girly things or not.  Woodwork and small electronic projects are also on my "one day I'll learn it!" list.
Oh, I also enjoy perving on handsome men!  Is that traditionally girly?
___
Answered on June 23, 2015 02:47:01 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130295241163



## Re Batgirl: She started when she was a young teen, and yeah, by the time she was an adult there was already a Batwoman (one of the only consistently written as gay female characters in DC comics)!
### asked by Anon

Ok Tim.
___
Answered on June 23, 2015 02:39:42 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130295502283



## Any thoughts on Batgirl? Why was it Batgirl and not Batwoman? Patronising misogyny much?
### asked by Anon

I wouldn't be surprised if there was already a Batwoman but I don't care enough to find out. And yes, I think the suffix "girl" used to describe any female character over the age of 18 is insufferably patronising, particularly if she's supposed to be heroic.
___
Answered on June 23, 2015 02:36:06 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130295183819



## This anonymity thing doesn't work as well as advertised...
### asked by Tim Marsh

It was just a lucky guess on my part... 😒
___
Answered on June 23, 2015 01:59:35 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130293828811



## On the subject of Batman, it is often said that Batman has arguably the best rotation of unique villains in the superhero genre because each villain parallels Batman himself in at least one dimension of dangerous pathology.

Which Rogue's Gallery villains do you think are the best in this regard?
### asked by Anon

Do you all see this shit? This is what I have to put up with! We know it's you, Tim!
___
Answered on June 23, 2015 01:29:12 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130293532363



## What is Tim's most annoying trait? If you have difficulty keeping it to just one, feel free to add more. Try to keep it under 10 tho pls.
### asked by Anon

He talks about Batman way too much.
I can't think of another answer to this question that isn't Batman related.
___
Answered on June 23, 2015 01:08:31 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/130293097419



## So were you ok with Tim turning up 'maxi fat' to your wedding? Were you REALLY ok with him rolling down the aisle?
### asked by Anon

Yep. This is an accurate representation him rolling down the aisle: http://i.imgur.com/7jKjm26.gif
___
Answered on June 22, 2015 11:58:20 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130268142539



## 5 likes = 10 likes back







10 likes = 20 likes back





20 likes = 30 likes back






30 likes = 60 likes back for sure 


btw i followed u follow back thnku :)
### asked by Funny story (✔)

Can all these spammers just fuck the fuck off? >:|
___
Answered on June 22, 2015 10:17:21 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130265964747



## If you found a real-life, working Death Note, would you use it? If so, please don't list any real, specific names. The Feds are watching.
### asked by Anon

Tim has asked me this exact same question.
I've thought about it at length and I'm going to say, no, I wouldn't use it. At this point in time. I'd keep it though. You know, for the future.
___
Answered on July 16, 2015 07:52:21 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/131094958283



## Do you like using the self check-out at supermarkets?
### asked by Anon

If I'm not buying much, then yep.  I'm probably way less efficient than cashiers, but by doing it myself I *feel* efficient.
___
Answered on July 16, 2015 07:28:52 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/131093791947



## What's this about you being a cheapskate and not approving of Tim's tipping, Belinda? As a former waitress, I am disappointed in you!
### asked by Anon

No lie, I'm a cheapskate.  Having said that, Tim's misrepresenting me (probably again) re tipping. I tend to tip when I go to restaurants, my rule of thumb is around 10% or rounding it up to the nearest $10.  However, I am of the understanding that wait staff in Australia get okay wages and they're not dependant on tips to make a living.
Tim sometimes tips way too much though and yes, that does annoy me especially if the meal is already expensive.  So I guess I'm a cheapskate who gets easily annoyed. :(
___
Answered on July 16, 2015 07:27:36 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/131094356683



## WOW! That Gryffondetuijbvfg bag is AMAZING! Is that the actual one you knitted or just a random pic of one? Was it hard to do? How soon can you finish one for me?
### asked by Anon

I made that! You can get the pattern here: http://www.ravelry.com/patterns/library/hp-house-fair-isle-pouch-bags. It wasn't too hard, it was the first fair isle project I tried.  There are videos on how to knit fair isle.
I can't remember how long it took I was still in Uni when I made it and had more time. I can probably make another one but it'll take a LONG time.
___
Answered on July 13, 2015 12:09:07 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130994629067



## What's this about gryffindor bag?
### asked by Anon

Back when Harry Potter was a thing, I knitted a Gryffindor bag to learn how the fair isle technique. I kept meaning to sew a lining inside it to make it more robust but I keep forgetting.  So I don't use it much.
___
Answered on July 13, 2015 11:15:08 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130994298827



## What things do you knit? I saw a pattern for a nose warmer and wouldn't mind one tbh. I wish wearing one was socially acceptable bc my nose has nearly frozen off this past week.
### asked by Anon

I mostly knit scarves. I'm a bit of a beginner, I haven't tried knitting complicated things that require shaping (ie clothing). The most complicated thing I've knit is a Gryffindor bag years ago.
I've never heard of nose warmers! They can't be too hard. I'm sure if I make one a little bigger it can warmer other things too! :)
___
Answered on July 10, 2015 09:29:36 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130888715467



## That was a pretty awesome gift you gave to Soul! I'm really interested in becoming a good friend of yours now tbh - my friends give crap gifts.
### asked by Anon

Haha thanks, it's really sweet that Soul still remembers (and presumably kept) the origami box! I like being creative with presents when I have time, particularly for people I'm fond of.  It's a pity that Tim doesn't like receiving presents (I know right, wtf), it means I restrain myself from going all out present-wise.  The last thing I made present-wise was a webpage where I got him a bunch of vidya games, you can still see it here: http://beliae6.wix.com/tim28birthday
(none of the links/keys work btw, they've all been claimed)
I like knitting but I don't know any of my friends who would want knitted goodies for presents.
Aside from presents, I don't think I'm a very good friend tbh.  I'm not a social person usually and I have an awful tendency to not keep in touch very often.  My closest friend is thankfully the same as me, and we'd meet every few months to catch up and neither will be offended by the lack of contact.
___
Answered on July 10, 2015 02:39:12 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130881916619



## What are your views on 'alternative medicine'? Do you think it's a bunch of potentially dangerous baloney or do you think there is merit in some practices eg acupuncture/cupping etc .
### asked by Anon

I grew in a family that thinks alternative medicine (specifically Chinese medicine) is as legit as "western medicine". I think that alt medicine is placebo most of the time and yes, I've seen it first hand being dangerous.
When I was a kid, whenever I got sick my grandma would always make this really black, suss smelling herbal medicine. It was always really bitter and I got skilled at downing bowls of it in one go. Incidentally I'm also pretty good at downing shots as an adult and I attribute that to childhood medicine sculling. The medicine didn't make me better noticeably faster but I guess they didn't harm me either.
It sometimes does have effect though and it can get dangerous. My mum had high blood pressure and years ago she took some Chinese medicine to help alleviate the symptoms (she got awful headaches). One night she took some deer hoof thing (I think it was brewed, I dunno) and she almost immediately felt worse and had to lie down. She got heart palpitations or something. I rushed online to see wtf is with what she took and apparently there's stuff in what she took that RAISED blood pressure. Thankfully she got better but I'll never forget that incident.
At least my family is sensible to still take regular medicine and go to the doctor when they're sick.
I'm not one to dogmatically side with western medicine, that shit is trial and error as well and doesn't always work or is just if not more dangerous at times. But at least they have a system of testing theories and medicines. As far as I know there isn't that rigour with Chinese medicine. It's all anecdotes and superstition.
I haven't tried acupuncture but I'm not keen on being stuck like a porcupine.
I've focused on Chinese medicine because that's the exposure I've had. I'm just as leery of other alternatives. Homeopathy, crystals and shit can go jump.  It's all snake oil as far as I'm concerned.
___
Answered on July 09, 2015 08:27:08 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130820366283



## What random stuff would you like to learn at uni?
### asked by Anon

A bit of everything! Here's what I WOULDN'T like to spend too much time on (if any):
- maths- chemistry- accounting- business + management- most languages (I'm just not very good at languages)- engineering- dentistry- music
Either because I don't think I'd be very good at it or because I'm not interested.
___
Answered on July 07, 2015 05:15:39 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130781133003



## Are you are jealous person. Would you scratch the eyes out of any woman who looked at Tim a bit too ravenously?
### asked by Anon

Nah, women are welcome to check Tim out, I know I would! :)
Tim doesn't belong to me, he's his own person. If other people find him attractive as well, then more power to them, hell, it means they have good tastes amirite. Though I'd worry about Tim feeling uncomfortable by all that objectification.
I don't think I'm jealous in the general sense of the word (i.e. resenting that other people have it better than me in some way). If what someone has isn't something I'd be able to get myself, then meh, good on them I suppose. If it's something I can get, well then I'd just work towards getting it, or more likely, get distracted and stop trying.
It doesn't mean I don't have insecurities of course, but isn't that different from jealousy? ¯\_(ツ)_/¯
___
Answered on July 07, 2015 05:10:12 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130781177035



## How did you find uni life? Both socially & academically.
### asked by Anon

I have a pretty crappy memory and the 5 years I was at uni is a bit of a haze now.
The first year was really intense. I had set a goal for myself to transfer into USyd law. I'm not sure why, but I was determined.  I think I just wanted to prove to myself that I could get in even if I didn't get 99+ for my UAI and my parents didn't have a gzillion dollars to pay my way through.
The prerequisites for an external transfer was pretty tough.  I had to be enrolled in a law degree and had to have achieved a distinction and higher for all first year subjects or something like that. I studied the shit out of the first year (minimal social life I recall) and ended up getting accepted for the transfer. I attend the USyd open day and everything, filled out all the forms, got my student card, but while in the line to finalise the enrolment, I thought to myself, "wait, I don't have anything to prove anymore, I've made it. I don't want to go to this snobby place." So I left the line and went back home.
I mellowed in my second year and slowly grew a very modest social life. I because close friends with a girl, met and started going out with Tim, and said girl broke up our platonic friendship, in that order, all within about a year's time. I joined a few clubs, like the anime club, but that didn't go anywhere. My marks tanked in my second year, but weirdly enough it didn't really bother me.
I did quite well academically in the last two years, mainly because all the subjects were law subjects that I got to pick.  Towards the last year, there was growing pressure to get my foot in the legal industry, which I was lucky enough to navigate through relatively okay.
I loved university. Macquarie University was always such a peaceful, gentle place for me. I never had a negative experience with the uni itself that I can remember. It might not have the best lecturers/tutors when I was a student there, but I learnt well enough.  If I didn't have to work I would love to go back to university and be a student again to learn random stuff in that environment. Though I bet it'll be more awkward as I get older and the average age of the students remain in the early to mid-20s.
___
Answered on July 04, 2015 13:22:29 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130683748811



## Omg, someone who feels the same about wing pieces! I can't believe some ppl actually choose them - a tiny bit of veiny meat. Ewwww. What pieces do you fight for?
### asked by Anon

Ooh I have 2. The drumstick: not meaty but so flavourful. The thigh (I think it's called?): it's the chunky, really meaty and greasy piece. So I-hate-myself good.
I don't care for the breast or the other bit (leg? I dunno). Too dry. Oh and wings are the worst.
___
Answered on July 03, 2015 07:41:55 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130648895947



## Is there anything you care less about as you have grown up? What is it?
### asked by Anon

Vampires. :[
___
Answered on July 03, 2015 07:38:33 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130651149515



## Did you always want to be a lawyer? What about the profession appealed to you?
### asked by Anon

Nah, growing up I wanted to be a scientist.  In year 11, I wanted to take all three sciences for the HSC: physics, chemistry and biology. Unfortunately, the biology classes were all full and I was told to go with my second choice: legal studies. This lead to that and here I am, a lawyer.
It always tickles me to think that if there was room in one of the bio classes, things would probably have turned out so differently.
___
Answered on July 03, 2015 04:07:22 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130646939083



## Speaking of KFC, it annoys me when someone finds a chicken head/foot in a KFC bucket & then wants to sue for millions bc trauma! It's all chicken get over yourself. I hope you wouldn't represent such a person, Belinda. ps I do not work for KFC.
### asked by Anon

I wouldn't sue for trauma but I would want my money back/replacement pieces.  KFC pieces are around $3 a pop and head/feet are not really edible (though I feel the same for the wing pieces as well).
I don't do litigation but even if I did, that'll be a reeeeeaallly thin case to run. I'd put my money on KFC winning (or more realistically, the parties settling).
___
Answered on July 03, 2015 04:04:13 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130620458187



## What’s one mistake you keep repeating?
### asked by Anon

Eating KFC. 1/2 the time I get a sore stomach from eating that greasy slop but uuugh it can be so good. I'm always "never again!!!" after I have two pieces, but sooner or later, I'd be like "so... that KFC... it's pretty good huh? Just have one, it'll probably be fine."  It's never fine.
___
Answered on July 02, 2015 05:23:47 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130612422091



## Do you have any preferenced in clothing ie skirts/dresses/trousers or do you just mix it up? I like the idea of dresses - easy to throw on, but in reality, I own very few.
### asked by Anon

My clothes are unfortunately dictated by my height, or lack thereof.  I'd love to wear more pants, but most pants are made for longer legs which means I'd have to either roll up the ends (unsightly) or get them hemmed (a chore).
Most of my work wear are dresses.  Dresses are easy to wear, with or without leggings depending on the weather. Skirts are nice, but I'd have to then work out how to pair it with a suitable top and that's way too hard.  The best are the dresses that LOOK like a top and a skirt, but it's in fact one piece!
There's heaps of work dresses out there, and surprisingly, Target has a fantastic range of nice work dresses that aren't TOO expensive.
My preference is home clothes/PJs, always. If only I can wear them to work.
___
Answered on July 02, 2015 05:21:02 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130614977739



## Do you have any pet names/nicknames for Tim? I'm assuming if you did have some that by now, they'd all have been tossed aside so "my maxi fat darling" could be used exclusively. Would this be a correct assumption?
### asked by Anon

Here's my horribly boring answer: no, I don't really have nicknames for him.  I sometimes call him "Tim-kun" to be annoying. I now call him "hubbo" sometimes since he doesn't like the nickname "hubby", or a variant, "Hubble-the-space-telescope" which is a bit of a mouth full.
___
Answered on July 01, 2015 05:25:52 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130579432139



## Do you have any grey hair yet?
### asked by Anon

I don't think so! It's a bit of a gamble with my genes, my mum didn't get grays until her 50s but my dad started graying relatively early.  Fingers crossed I take after my mum re hair.
___
Answered on July 01, 2015 05:23:43 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130579442123



## Do you like wearing high heels or are you generally against self torture?
### asked by Anon

I generally don't wear heels, they hurt my feet. However, there's no denying that I'm short and sometimes I like to look taller. So I wear heels on special occasions. I try to bring flats with me when I have to wear heels though.
___
Answered on June 30, 2015 10:47:36 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/130554012875



## What assumptions do ppl usually make about you that are very wrong?
### asked by Anon

Hmm I don't really notice people making assumptions about me. I guess I'm lucky enough to hang around cool people.
I guess one thing that some people may think about me is that I'm ambitious. I'm really not. I'm usually pretty lazy and indulge in fun things when I can. I can get competitive though, but again not all the time.
___
Answered on June 30, 2015 10:44:07 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/130553989579



## What was your first paid job?
### asked by Anon

My first paid job was technically working for my parents when I was about 6-7ish?  It was drastically below minimum wage but getting, like, 5 WHOLE SHINY $2 COINS at the end of a day's work seemed like a lot.  Gold!!!
My first paid job by non-family was when I worked at Taronga Zoo as a food-cart vendor after high school during summer.  I had the job of manning a cart full of ice-cream and cold drinks near the seals.  It was a super awesome part-time job and I loved walking around the zoo during lunch.  The parents and the screaming children, not so much, but I definitely had a good time overall.  I'd take a ferry to work every morning from Circular Quay and the views were just magnificent.
___
Answered on June 30, 2015 01:24:47 GMT

__Likes:__ 0

Original URL: /cosmicmissb/answers/130501471179



## Lots of women say they didnt really get to enjoy their wedding day - it was all a blur/went so fast/they worried about everything etc. Is that true for you to any extent?
### asked by Anon

I enjoyed my wedding day, mainly because I wasn't rushed and I didn't plan a whole lot. My wedding was relatively low-frills; I had my hair and makeup done at the same place I picked up my dress, we got a few pics taken at the ceremony venue (Powerhouse Museum!) and the restaurant 100% looked after the food at the reception.
I have to say, I was very lucky to have awesome people who helped out.  Tim's brother picked up the cake that afternoon, he organised the music, our respective parents transported us where we needed to go and my own brother did a fantastic job MC-ing the reception.  I was amazed by how smoothly everything ran!
The only thing I worried about was the speech I had to do at the reception partially in Cantonese.  My Canto is super poor and I was so worried that I wouldn't know what to say that I wrote it all down phonetically in English.  It ended up going okay.
I definitely wouldn't want to do a wedding again though.  It was an exhausting day and being a non-party person, it was super draining!
___
Answered on June 30, 2015 01:20:14 GMT

__Likes:__ 0

Original URL: /cosmicmissb/answers/130519999179



## What do you do for excercise, if anything?
### asked by Anon

I walk as much as I can  when I can be bothered otherwise nothing! I've never been fit and I can't run very far. I'd be one of the first to go in a zombie apocalypse I reckon.
___
Answered on June 28, 2015 07:05:36 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130477448651



## You've shamelessly boasted about your fried rice. What's the secret to a good one? Is it using day old rice?
### asked by Anon

Nah I use fresh rice. I think my secret is a combo of garlic, soy sauce, sugar, sesame oil and oyster sauce mixture that I coat the rice and the other ingredients in. Makes it very flavourful.
___
Answered on June 28, 2015 07:03:03 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/130477418443



## Which teacher in school made the most impact on you and why?
### asked by Anon

Hmm no one particular teacher but I do remember teachers who've left an impression. They include:
Year 1 teacher: a gentle lady with very short hair who was always smiling. It was a nice memory amidst a difficult year 1 and 2. Though at the time I didn't know whether she was a man or woman because her hair was so short and so didn't know whether to use Mr or Ms.
Year 4 computer teacher: she let us play video games on the school's computers. It was my first exposure to video games which became a life long interest.
A sub in high school: he threw a pair of scissors at a student who didn't stop talking in class. The kid was fine and the sub never came back.
Year 12 English teacher: I had a lot of fun talking with her and she gets genuinely seemed to appreciate literature and enjoy texts to a depth that we didn't have. I remember her cackling with joy when watching "Man from La Mancha" play at a theatre while we were all spacing out and I distinctly remember thinking "damn, I want to be able to understand literature as well as she does so that I can appreciate the texts we are exposed to."
___
Answered on September 06, 2015 08:04:33 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/132403610315



## Do you think that you'd be able to work closely with your spouse in an employment situation?
### asked by Anon

No, only because we work in such different fields. I don't see how Tim's lecturing/research would have much to do with my lawyering.
As a lawyer I don't usually work with colleagues anyway, it's a pretty solo job most of the time. I imagine it's much the same with what Tim does.
I'd love to work NEAR Tim though! It's always a treat to go home with Tim when he's working in the city in the afternoons.
___
Answered on September 06, 2015 07:50:04 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/132316342731



## Do you like wearing hats? Do you have a head made for hats?
### asked by Anon

No I don't like wearing hats. I *like* the idea of hats but not wearing them.
The main reason is that most hats have a brim. A brim means that I can't easily see above my eye level, which makes things really annoying and limiting when you're as short as me.
I also have a lot of hair which means my head gets easily overheated with a hat on.
I otherwise have a round head so I guess that makes it good for hats to perch upon, but for the abovementioned reasons it doesn't usually happen.
___
Answered on September 06, 2015 07:46:05 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/132267050699



## What do you believe defines intelligence? Are there different types of intelligence? What kind of intelligence do you possess?
### asked by Anon

I... Don't know really how to answer this question. I accept the usual definitions of "intelligence" that are invariably useful depending on the context.
I guess I would default to "intelligence" to meaning the ability and ease to problem solve, though obviously that definition is not appropriate for all circumstances.
Most people have some degree of problem solving ability.  Like most people, I'm better at mentally doing some things than others. I'd like to think I'm reasonably analytical with an eye for detail. I have sufficient "book smarts" to do my job and hold a conversation.
I have pretty terrible spatial awareness though, which is only slightly improved by occasional map reading I've had to do over the years in video games. I've honestly had times leaving my home and losing my bearings. I used to be a lot better at maths.
___
Answered on September 06, 2015 07:41:59 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/132175096779



## Do you play any instruments?
### asked by Anon

I used to play the clarinet at school but I doubt I'd remember any of it now.
I've always wanted to learn the violin. My brother got to go to violin lessons for about 3-4 years, but my parents can only afford lessons for him, so I used to stand with him every day while he practices and attend every lesson with him. So for a while I could read music and know THEORETICALLY how to play a violin, but never learnt to physically played it myself.
___
Answered on August 20, 2015 04:21:09 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/132173176523



## Are you enjoying Penny Dreadful?
### asked by Anon

We finished the latest season a few days ago and yes, I did enjoy it!  I found the first episode insufferably boring for it picked up. Vanessa Ives is my fav character and the flashback episode was the best one of the season imo.
*spoilers*
I liked the twist with Brona/Lily's character but I dunno, it became a bit farcical with the whole "yeah we're evil now we're going to take over the world", kind of came out of nowhere.
___
Answered on August 19, 2015 06:00:34 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/132115619787



## Your answer to my question re schools was f*cking awesome. Thanks.
### asked by Anon

Glad I could help bro 👍
___
Answered on August 19, 2015 03:07:49 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/132141843915



## What was/were your school/s like?
### asked by Anon

I went to preschool/kindergarten in China, but I don't really remember it.  I vaguely recall naps and black sesame porridge.
I went to 3 different primary schools, roughly 2 years in each.
1. School in Lakemba - I was a wee lass who didn't speak English when I first started year 1 and was immediately bullied. I hung out by myself a lot and hid from other kids. I picked up English fairly quickly and spent a lot of time reading in the library.
2. School in Punchbowl - I made my first friend (kind of) and we had a 2-member club that I founded for a short while.  I still spent a lot of time by myself and would either walk around the school at lunch/recess or read in the library.  I vividly recalled spending what little savings I had to buy used books from the school library during its annual spring clean and haul my bag of spoils home in a cloth bag.  I did well academically, sat a test for Opportunity Class, made it and moved onto the next school.
3. School in Earlwood - I actually made friends! Yay! However, OC slaughtered my self-confidence. Don't get me wrong, the education was great, teachers were good, but my God, the competition was so fierce. The kids got ranked all the time and your rank was ill-hidden from others.  I was stressed all the time and was ranked towards the bottom of the class. I felt so dumb during those two years. Everyone obsessed over the Selective Test, and literally every one in my class made it to a good selective school.  I made it to the "worst" selective school, Sefton, which was everyone's safety school.  One of the hardest times I've cried in my life, I felt like such a failure. Mind you, my parents didn't really pressure me, it was all me,  they were just happy I got into a selective.
I spent all of high school in Sefton and it was overall a great experience. I felt I had a good education despite the school being technically a disadvantaged school. We often lacked things like blinds for windows, we had a sub who threw scissors at a student, the school was located near a gun shop and strip house but overall, it was fine. I made friends from the first day I attended school, which was awesome.  My peers were likeminded people, from similar socio-economic background to each other and who wanted to excel academically. There was a degree of competition but nothing like OC. I'd say I had a pretty good high school experience.
Does Macquarie University count as a school?  Because that's where I went next.  I've spoken about being at MQ before but yeah, loved it.
___
Answered on August 19, 2015 01:37:03 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/132116548811



## Were there any comments made to you during yr school yrs by a teacher that you still remember? eg My yr11 History teacher - "You don't know much, do you?"
### asked by Anon

Hmm nothing comes to mind. I remember having really long in-class discussions with my Ext 1 English teacher, arguing about one thing or another, no doubt being an obnoxious and smug lil shit while everyone else looked on annoyed and bored.
Not something a teacher said to me directly, but I still remember a comment on my Year 1 report card. I had ok grades but the teacher left a comment saying that I needed to work harder on colouring within the lines. That cut, still does.
___
Answered on August 14, 2015 08:54:32 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/131997337035



## What parts of yr body are you happy with? Any parts yr not too keen on?
### asked by Anon

I like my brain the best I suppose, it's been alright so far.  My hair is alright too, there's usually a lot of it and it's pretty manageable.
I'm definitely not keen on my eyes, specifically, the fact that I need to wear glasses. Hate glasses so much, can I have cyborg eyes now, please.  I'm not fond of my tonsils either because I keep getting tonsillitis and when I can be bothered I shall one day have them removed.
___
Answered on August 11, 2015 06:45:11 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/131906190027



## Have you tried Madame Flavours Green Jasmine & Pear tea yet? Don't go thinking I've forgotten.
### asked by Anon

I had completely forgotten! I bought a pack today and am having a cup now. It's nice! Though very very mild on the pear. But definitely very agreeable green tea.
They've clearly got a distinct branding happening, *2* fancy pamphlets in the box! One of which is a guide to meditation while the tea brews... Call me a pedant but I can't relaxed when there's a completely unexplained gap between the sections "Mindful Moment 3" and "Mindful Moment 4", why is there a gap???
___
Answered on August 05, 2015 04:42:49 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/131718496971



## Do you have any grey hair yet? Are you an "embrace the grey" type of person or will you go to yr grave with coloured tresses?
### asked by Anon

No grays yet as far as I can tell! Hopefully I'll take after my mum who didn't get grays until well in her 40s.
I probably won't colour my hair due to laziness mainly.
___
Answered on July 31, 2015 00:09:34 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/131563395531



## If you got to be a man for a day, how would you spend it?
### asked by Anon

1. Play with my ding-a-ling (penis).2. Try to grow a beard.3. Try to see how low my voice can go.
Otherwise there isn't much that a man can do that I can't already.
___
Answered on July 30, 2015 05:08:52 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/131438484171



## Who would you most like to be stuck in an elevator with?
### asked by Anon

Tim. <3
Or someone who could either fix the elevator or have more immediate access to someone who can.
___
Answered on July 26, 2015 23:40:24 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/131412075467



## If a movie was to be made about yr illustrious life, who would you like to play you?
### asked by Anon

Gillian Anderson. Except my life is pretty boring so instead the movie would be about her playing the role of a cool headed, smart and capable FBI Agent investigating paranormal goings-on with her less capable and more naive male sidekick. 3rd X-Files movie pls and less crap this time.
___
Answered on July 26, 2015 05:18:10 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/131349848267



## Sticking a bit to the Friends theme, who would be the 5 celebs on your "freebie list"? Ppl you could sleep with without Tim getting upset. Remember Chandler took Isabella Rosselini off his list and she turned up?
### asked by Anon

Eeeh, the idea of sleeping with a stranger, no matter how attractive, comes bundled with all sorts of expectations, uncertainties and anxieties. CBB, really.
___
Answered on July 26, 2015 05:10:13 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/131373643211



## What will you be watching now that yr done with Friends?
### asked by Anon

Dunno, we're struggling to answer that question atm. Maybe the new season of Penny Dreadful?
___
Answered on July 25, 2015 04:00:01 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/131371788491



## Best compliment you've received? One that really made you feel great bc it was genuine.
### asked by Anon

When my dad said that my wedding speech said in Cantonese was touching and heartfelt and everyone liked it even though I stumbled over the words. I was so relieved that it came out alright (I broke out in hives the night before thinking I'd mess it up) so yeah, best response I could hope for.
___
Answered on July 24, 2015 09:54:11 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/131344981963



## PAP of your spirit animal?
### asked by Anon

___
Answered on July 24, 2015 00:16:42 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/131337826763



## What is yr favourite Friends episode/episodes? Any favourite guest stars? I loved Bruce Willis. Am asking Tim this q too.
### asked by Anon

Eh, I guess that "what if" episode where Monica is still fat, Rachel is married, Chandler is a broke writer, Joey's famous etc.  That was fun.  But otherwise, the show really meanders a lot and nothing really sticks out.  I guess that's what you get with "slice of life" TV shows.
We've FINALLY reached season 10 and my God, if it wasn't for the completionist in me, I would've quit a while back.  It's... becoming quite a slog.
___
Answered on July 20, 2015 02:57:29 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/131182048459



## Would you want to be internet famous?
### asked by Anon

When I was younger, yes. Back in the days where the internet was smaller, I was quite actively involved on internet-y things.  I ran my own blog, forum, a fanfic archive and an online magazine, amongst other things.  I met many people online some of whom I am still friends with to this day.
The older I get though, the more anonymous I'd prefer to be online, so no, I don't want to be internet famous any more. And with work and stuff, I don't have the time or energy to be active online much these days, though I lurk a lot.
___
Answered on July 20, 2015 02:54:45 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/131208486859



## Do you like to wear perfume? Are you loyal to a particular scent?
### asked by Anon

Nah, I'm pretty neutral smelling I think.  Other than deodorant, I don't use perfume.
___
Answered on July 18, 2015 07:33:56 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/131155104459



## My question yesterday (the one where I said I was disappointed in you and accused you of cheapskatery) was written in jest. Just making sure you know that I was just being an idiot. :)
### asked by Anon

No sweat. :)
___
Answered on July 18, 2015 07:32:39 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/131128199371



## Lol @ "f*ck you and yr f*cking machine!" I can't agree more. I even get a bit pissed off at some of the desperate pleading notes on tip jars at the front counter. Just put the prices up ffs if things are so bad!
### asked by Anon

Yep.
___
Answered on July 16, 2015 13:52:18 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/131102025419



## Wait, wait - ppl actually tip in Australia? Man I missed that memo.
### asked by Anon

I think it's mostly reserved for more expensive restaurants, there's no expectation to tip at cafes and eateries where you pay upfront.  Many restaurants don't both asking for a tip and instead just charge whatever is owed and have a tip jar at the front counter which is fine.
But it seems there are more restaurants now (especially kind of posh ones) that have an extra step on their EFTPOS machines where, after you put in your card and before you put in your pin, there's an extra screen where you have to key in an amount for tip or key in 0.00 and feel like a cheapskate.
I hate that kind of pressure, it's really annoying.  I almost feel like NOT tipping in those situations when I feel like I'm being pressured into tipping, especially in Australia where it's completely optional.  It's like, wow I would've been willing paid a little extra for the nice service and food, but your stupid EFTPOS machine will make me feel bad for not tipping so fuck you and your fucking machine.
___
Answered on July 16, 2015 12:58:39 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/131099374283



## Do you have a guilty pleasure? What is it?
### asked by Anon

Problematic favs in video games/books/TV shows/movies etc...
___
Answered on October 04, 2015 02:27:50 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/133336263883



## video games?
### asked by Anon

noun?
___
Answered on October 02, 2015 10:49:06 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/133291796683



## what is the best mobile phone (under $200)
### asked by Anon

what is the best mobile phone (over $200)
___
Answered on October 02, 2015 10:48:50 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/133291745995



## where/when/how did you learn to take such good selfies?
### asked by Anon

I don't.
___
Answered on October 02, 2015 10:47:11 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/133291719371



## What are you like to live with? messy/clean?
### asked by Anon

I tend towards neat and I get increasingly unhappy when the house gets messy.  Actually, mess is fine(ish) but what I can't stand is dirt and mold and other icky stuff.
However, cleaning is not a top priority so a big clean usually only happens when I can't bear the mess anymore.
___
Answered on October 02, 2015 10:42:54 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/133291708107



## LOL @  "constantly reminding Tim that he has a waifu in his laifu"! You said you had a cool wedding ring but it can't be as cool as the one you gave Tim, can it? Because that was the height of coolness.
### asked by Anon

His ring is indeed very cool, but I love my rings. The engagement ring is made up of a platinum band, moissanite stone with blue sapphires on each side. I got to design it so that was a neat process.
The wedding ring is a platinum band with milgrain edging. Simple but I like it and I'm glad I was able to pick both.
Pic of rings (Tim's in the middle):
___
Answered on October 02, 2015 10:37:44 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/133288373707



## Do you believe in monogamy?
### asked by Anon

Ideally, people should be in whatever kind of relationship that makes them happy.
I personally would not be comfortable in a relationship that isn't monogamous.  This isn't the "right" stance (there is no "right" stance) but it's the one that works for me.
In the words of Savage Garden, "I believe that trust is more important than monogamy".  If two or more people decide that they trust each other and want to seek other partners (who also feel the same way), more power to them.
To pre-empt any further "do you believe" questions, just go listen to Affirmation (<3).  It's a bit idealistic and I don't agree with all the lyrics, but it's a pretty sweet song.
___
Answered on October 02, 2015 10:07:19 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/133270535883



## Do you believe in true love?
### asked by Anon

I think you either love someone or you don't.  If you do, then it's true (I guess?) if you don't then it's not love.
Love is an emotional state, it's a bit absurd to ask whether it's true or not. It's like asking, do you believe in true anger?  True sadness?
___
Answered on October 02, 2015 09:59:59 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/133270427339



## Favourite chocolate bar?
### asked by Anon

Crunchies, chocolate bars with nuts, malteasers. I'm not that fond of chocolate and usually I wouldn't eat more than a bite or two.
I much prefer chocolate flavoured things than pure chocolate.
___
Answered on October 02, 2015 02:26:45 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/133270098123



## Can you pretty much get Tim to do whatever you want?
### asked by Anon

My demands are generally reasonable and he generally complies because he's chill like that.
Tim tends to go out of his way to help people, whether they be his friends or students. Much to my chagrin sometimes.  For example, he sometimes is up at wee hours of the morning answering student emails that comes in late at night. I'm sure it'll be fine if the emails go a few hours unanswered!
___
Answered on October 02, 2015 02:24:15 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/133250366667



## Have you every worked in customer service? Did you hate it?
### asked by Marcus

Yes I have! Twice.
I worked at Target for work experience for a week. Spent a whole day in my knees sorting and reshelving socks, only to find it completely messed up again a few days later. I grew a greater appreciation for retail staff as well as a resolve to never work retail for a career.
I also worked at Taronga Zoo for a summer holiday and that was amazing. The journey to the zoo every morning and evening was just beautiful and omg I loved the zoo. I mainly sold ice-cream and drinks from a cart by myself to parents of screaming children which itself was an... Experience. But it was all worth it for getting to work at a freaking awesome zoo.
___
Answered on October 02, 2015 01:39:26 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/133225861835



## What do you dislike about yourself?
### asked by Anon

How much time you got?
Hrm mainly I'd say that I can be neurotic and pedantic at times.  It's useful for my job (where being pedantic is a good thing!) but I don't like being that way about non work stuff and would change if I can.
I can get angry pretty quickly but my anger doesn't last long usually.
I wish I were taller! And didn't have to wear glasses.
___
Answered on October 02, 2015 01:33:18 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/133155504843



## What's the best thing about being married? Do u get tax breaks and stuff?
### asked by Anon

I don't know about tax breaks, will find out soon.
Marriage hasn't really added anything fundamental that we didn't already have. In a way, it was just a fancy way to celebrate our 10th anniversary. But there are some ancillary nice things that come out of being married, such as:
- a nice wedding and cool costumes- cool rings- an excuse to go on an extended trip overseas as our honeymoon - sharing the same last name as each other- getting to constantly remind Tim that he now has a waifu (wife) in his laifu (life)
None of the above are essential to the relationship between Tim and I but they're nice little extras.
___
Answered on October 01, 2015 23:43:36 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/133134481099



## what new music are you listening to this year? my list is decidedly bare ♫ http://spoti.fi/1OBG01n
### asked by Marcus

Thank you for your list, I shall check out those songs.
I don't really look for music, I mainly listen to music I've accumulated over the years from recommendations to hearing it on the radio etc. Hell, my main music folder is still called "Burn" from back in the days when I used to burn music onto CDs.
I listen to a lot of different stuff... a lot of pop, hip hop (I have a fondness for Korean hip hop and kpop), musicals, anime, instrumental stuff.  I've been listening to Taylor Swift's music lately and a lot of it is quite catchy but also a bit samey. But usually what I listen to is really eclectic to so it's a bit hard to describe.
___
Answered on September 28, 2015 10:01:55 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/133204737739



## The Smiths? or The Cure?
### asked by Marcus

The Who?
Haha, I'm so funny.
But seriously, neither.  Nothing against them, but just never got into either of them.  Recommend me a song or two?
___
Answered on September 28, 2015 04:13:11 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/133201741771



## "that's a stupid idea, how would that even work?"

ahahaha piss off Tim :D
### asked by Soul James

I know right? Workable ideas are overrated.
___
Answered on September 27, 2015 04:08:12 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/133176101067



## Where do you get your best ideas?
### asked by Anon

In the shower. I'd sometimes yell them at Tim when they come to mind, which would invariably be followed by him yelling back "that's a stupid idea, how would that even work?"
___
Answered on September 25, 2015 09:07:54 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/133133831627



## Have you ever had chicken pox or measles? Any scars if you have?
### asked by Anon

I've had chicken pox but it was relatively mild, was a bit itchy for 2 weeks. No scars.  Never had the measles.
But! I do have a small scar from a small pox vaccination I had when I was a kid in China. Apparently they stopped vaccinating kids in Australia for small pox ages ago.
___
Answered on September 25, 2015 09:05:55 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/133126673099



## Why do women live longer than men?
### asked by Anon

http://lmgtfy.com/?q=Why+do+women+live+longer+than+men%3F&l=1
___
Answered on September 21, 2015 10:12:16 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/133038584011



## last movie you watched?
### asked by Anon

The Host (2006).  South Korean horror film that was surprisingly compelling!  Monster horror, not supernatural horror.  I recommend catching it on Netflix.
I don't watch many South Korean films, but I have to say, I've liked all the ones I've seen.
___
Answered on September 19, 2015 11:37:24 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/132988335307



## What social characteristic or habits (if any) in ppl make you uncomfortable? I can't deal with ppl cracking their knuckles for example.
### asked by Anon

People who randomly bring up the fact that I'm Asian.  I don't have a problem with being Asian, but it's very otherising when people do it completely out of the blue.
Recently, I had a colleague (won't lie, he was an old white man) who walked by my office and noticed that on my name plate, my last name had change to Tim's after I had gotten married.  He said "Congratulations on getting married! Oh, that's [last name] not a Chinese name." Yeah, no shit, Sherlock.
I have no problems with people bringing up that I'm Chinese/Asian when it's relevant to the conversation, but it makes me uncomfortable when people point it out for the sake of pointing it out.  I'm glad to report it only rarely happens but it is jarring when it does.
___
Answered on September 19, 2015 11:33:28 GMT

__Likes:__ 6

Original URL: /cosmicmissb/answers/132781882571



## We've all heard the lawyer jokes - you probably more than any of us. Do you think they are warranted in any way? Are lawyers a particularly unethical lot?
### asked by Anon

Hmmm... on a whole, I think lawyers are an ethical lot, despite the stereotype.  I think BECAUSE of the stereotype, there's A LOT of responsibility on lawyers to act ethically and have good standing in the community.
To be a lawyer, we have to pass a mandatory Ethics unit at university AND every year we must take an ethics class before we can renew our practicing certificate.  They're admittedly not intensive classes, but I think the heart's in the right place.  Furthermore, there's a set of complicated and lengthy solicitors rules we must abide by to prevent us from acting unethically.  Rules relating to how we can deal with money we receive on trust, how we can engage with clients, other parties and other solicitors, how to handle conflicts of interest etc.  Breaching the rules can result in sanctions such as fines, suspensions or being barred from being a lawyer altogether.
Lawyers tend to be a risk adverse lot.  We're tasked to know the law and know that, in practice, we can't always just rely on the letter of the law without considering the spirit/intention of a law and its application.  I'd say that most lawyers err on the side of caution when needed, and the side of "caution" is to advise clients to act ethically and non-sneakily.  Clients don't always listen to us though.
Lawyers are a diverse bunch of people, which means there are some who are shit people and some who aren't.  I've come across lawyers who are assholes and probably fit the stereotype to a T and who might not give two shits about behaving ethically.  But I'm happy to say that the team I work with are a set of decent folks whom I can confidently say are good people who give good solid legal advice.
Keep in mind that we operate in an adversarial system.  After our duty to the court, our next duty is to our client.  Fundamentally, we cannot advocate for both sides of an issue and there are ever present questions relating to social, class etc inequalities that affect people's access to the legal system.  Lawyers fit snugly into the capitalist model and therein lies fundamental ethical problems that are inherent to being a lawyer.
Tl;dr: imo lawyers mostly try to be ethical but capitalism tho
___
Answered on September 19, 2015 11:23:02 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/132734122187



## Are you good at giving someone the evil eye? Have you got yr dirty look down pat?
### asked by Anon

I don't have a habit of giving people dirty looks, at least not consciously. I tend to be pretty non-confrontational in person though I do take note of people who offend me in some way.
Bonus: my fav "evil eye" image:
___
Answered on September 19, 2015 10:50:44 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/132706670027



## Which is the better big cat the lion or the tiger?
### asked by Anon

TIGER! ALWAYS!
___
Answered on September 09, 2015 06:25:38 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/132709312203



## Is there an embarassing story about you that your family always tells ppl?
### asked by Anon

Hmm not really for 2 reasons:1. My parents' English isn't very good so they don't talk much to people who aren't there friends/family. When they do speak with their friends, I'm usually not in the room or I'd be occupied with something else.2. I was a relatively quiet, serious and obedient child so there probably wouldn't have been that many embarrassing stories about me.
However.  There was an incident in my childhood that I will always remember vividly.  When my brother was just a baby, my parents would always buy small glass jars of baby food to feed my brother.  I would've been about 8 at the time and was intensely curious about what baby food tasted like.  However, for reasons unknown, I was much too dignified to ask for a taste from my parents.
So one day, when I was home alone, I saw a small baby food jar sitting on my dad's work shelf.  I thought that this was my chance for a quick taste! I opened it and scooped a bit of it in my mouth. It was the most foul, bitter, disgusting thing ever I'd ever tasted!  I ran to the sink and couldn't wash it off quick enough. Even after a thorough washing, I could still taste it and my tongue felt funny.
Thankfully my parents came home soon enough and I told them what happened. My dad said, "that's not baby food. That's shoe glue."
There's probably a lesson to be learnt somewhere here.  Might involve not eating from containers on your dad's work shelf and not to reuse containers without proper labelling.
___
Answered on September 09, 2015 06:24:41 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/132654552011



## How would you describe your style? Do you like shopping for clothes?
### asked by Anon

A big part of my wardrobe is work clothes, mostly work dresses. I'd wear pants more but because I'm short, pant legs need to be shortened and I ccb. Lots of monochromes, some colours here and there. Not big on patterns.
Other than that, I'm pretty casual. Jeans and shirts. Love my PJs.
HATE SHOPPING FOR CLOTHES. SO. MUCH.
___
Answered on February 11, 2016 08:45:56 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/136024555723



## If you could only see three people for the rest of your life who would it be?
### asked by Áuryn

1. Tim.
I can't help but interpret this question as meaning that I have some horrible vision or neurological problem in that the world is normal but I can only recognise/identify 3 people, with everyone else being either invisible or somehow not registering in my brain. If this is the case then I guess:
2. the doctor treating my illness3. myself in reflections
___
Answered on February 11, 2016 08:43:26 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/136003175883



## I knew that my own feeling about it was strange (re virginity). It's mostly cultural + RCC upbringing. I also didn't think it was a big deal at the time. I wish I could've waited and not given in, the way that I did. I have been called a prude too. Am I just being a babby?
### asked by Anon

(sorry for the late reply anon) Nah you're neither strange nor a baby. Lots of girls tend to be indoctrinated by their parents, religion, the media, society in general, of the "sanctity" of their virginity. The whole "virginity" concept is a fiction designed to fit alongside the whole women-as-property concept, perpetrated by a bunch of cultures and religions. It's maddening. Women aren't defined by how much sex they have, it's that simple. Whether you have loads of it with loads of different guys or not at all, why is it anyone's business?
There are events where the "first times" are memorable and you'd want it to be good. Like flying on a plane for the first time or going on a date for the first time. If your first time doing those things turns out to be a bit shit, then yeah, that sucks. I'm sorry, anon, that your first time boning wasn't great or if you were pressured into it. That really sucks.
You're not a prude for not being able to help how you feel. It might be a journey that you need to go through to sort out how you feel about this virginity issue. Remember, you and your thoughts are not slaves to your culture or the church. It doesn't have to define any part of you.
I'm not telling you how you should think or feel but you have the ability to change your  views and feelings on this issue if you want. There are plenty of more knowledgeable and eloquent women who have written or spoken on this topic I'm sure that you might want to look into.
___
Answered on February 11, 2016 08:35:16 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/135988200651



## I like Tim! fite me irl.
### asked by Anon

___
Answered on February 10, 2016 23:09:57 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/136200689099



## Could you see yourself living to be 100?
### asked by Anon

Errr not sure what this question is getting at... there's fortunately nothing to my knowledge that is currently PREVENTING me from living to 100 at the moment.  Ideally, I would like to live as long as possible, until such times as I decide once and for all that I don't want to go on any more.
___
Answered on January 30, 2016 08:07:16 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/135984739531



## Have you ever stopped to think about the loss of your own virginity? Do you mourn it like the dearly departed?
### asked by Anon

Nope.  Feel bad for girls who are brought up to worry about things like their virginity.  It's such a gross, antiquated, patriarchal concept that does nothing but objectify women.  The whole concept makes me angry.
___
Answered on January 30, 2016 08:05:06 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/135984660427



## What is the one thing you want most in your life right now?
### asked by Áuryn

Bajillion dollars
___
Answered on January 21, 2016 18:15:41 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/135807067083



## Hey hope you get better soon! TELL ME ABOUT THE FOOOOOD.
### asked by Anon

Thanks I can't believe how sick I got (thankfully recovering!). Caught something awful in London and it hit me bad in Paris. Completely lost my voice, fevers, blocked nose, hocking up horrible things from my lungs... Ick. And I was so looking forward to exploring Paris! We lost 2 out of our 5 days here due to forced downtime.
The fortunate thing I learned is that "paracetamol" is the same in English and in French!
Food! We haven't been eating that much. A few thoughts:- we need Canadian poutine in Sydney. It's so good as a comfort food.- we had a fantastic lobster roll in DC. It was just a bun filled to the brim with lobster meat and seasoning. So. Fantastic. But it amounts to about AUD20 each, so a bit dear.  I've been told that there's a place near Randwick (?) that does something similar so I gotta check it out when I get back. I love seafood. - we tried $1 pizza in NYC from about 4 different places and they were all great given the price and there was even one place that produced probably the best cheese pizza I've ever had and it was $1 a slice. - had a really delicious scone in London.  Had fish and chips too. Was nice but nothing that special. - we went to a lot of museums and weirdly enough, we enjoyed some surprisingly  delicious, fairly priced food at many of them. Our museum cafés back home need to step up their game. - haven't had anything particularly French yet in Paris, but I did drink a lovely lemon ginger tea that helped with my cold. - from what I've experienced so far, I have to say, we have it generally pretty good in Sydney in terms of food and drink (coffees and teas). I have yet to encounter a place that made me think "yep, this place definitely does food better than Sydney." Then again, I've only been able to try a tiny sample of any one city's food.
I've been meaning to blog about our adventures but blah I've been to lazy to do so. Tim's doing a great job with his frequent updates.
___
Answered on January 06, 2016 21:58:44 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/135494238155



## Greetings fellow mushroom lover! Are you adventurous with your mushies? Do you venture beyond the button/flat/shiitake varieties into sea anemone mushroom looking territory? I'm pretty basic with my choices tbh but that's gonna change in 2016!
### asked by Anon

I have yet to encounter a kind of mushroom that I disliked, every kind I've tried have been delicious in their own way. I don't normally go out of my way to try fancy mushrooms though now that you mention it, maybe it's high time I start! But most of the mushrooms I've come across are the common types you'd find in most places.
There is one kind that is a bit more uncommon that I love, and it's a kind of small, black, meaty mushroom that you find in a lot of Chinese cooking (my dad uses them a lot). It has this lovely sweetness to it and is great with broths noodles.
Maybe it's a type of shiitake mushroom? I don't know it's name but it looks like this:
___
Answered on January 06, 2016 21:36:12 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/135438599115



## Let's be friends! I don't want to fight anymore :( A life sans Comic Sans is really no life at all. MERRY CHRISTMAS, BELINDA :D from your 'super ugly' would be pal.
### asked by Anon

Merry Christmas anon!
___
Answered on December 25, 2015 03:40:27 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/135212239563



## Omg, so saying you don't like me and calling me "super ugly" isn't enough? You feel the need to taunt me with obscene hand gestures? You have broken my heart. I hope you're happy.
### asked by Anon

___
Answered on December 18, 2015 01:12:48 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/135069485003



## Will you take the test http://www.abtirsi.com/quiz2.php

How fixed are you politically?
### asked by Anon

I noticed that a number of peeps did this quiz a few days ago but I didn't have a chance to read it all and now the conversation has moved on.  But anyway, here are my results:
You are a: Socialist Pro-Government Multilateralist Humanist Progressive
Collectivism score: 67%Authoritarianism score: 33%Internationalism score: 50%Tribalism score: -67%Liberalism score: 67%
Bit of a problematic quiz.  Seems to be lots of extreme and blanket "foreigners are all pieces of shit, heil Hitler - yes/no/maybe" type questions.
Awww I didn't get the bleeding-heart description... I guess I didn't get sorted into the same house as the cool kids.
___
Answered on December 17, 2015 03:27:43 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/134991151819



## Hi! Comic Sans here - I don't like you either! So there.
### asked by Anon

___
Answered on December 15, 2015 23:43:27 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/135031514059



## Do you want to build a snowman! :D
### asked by Anon

YES. I hope I get to see snow!
___
Answered on December 15, 2015 22:11:15 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/135015794123



## Did meeting that celebrity baby, and hearing about other pregnancies, make you consider having kids of your own?
### asked by Anon

Hmmmm... Still undecided. Internet baby was soooo cute and was so well behaved. But being a baby, he did eventually tire out and get cranky and weepy for a bit... I don't know how good I'd be dealing with cranky babies, particularly ones that aren't as amiable.
I know I'm not ready right now to have kids but who knows how that can change in the future.
___
Answered on December 15, 2015 22:10:55 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/134826175947



## Thanks so much for following up about "married tax breaks" for me! I'd heard they existed ages ago & a work colleague brought it up recently when discussing same-sex marriage ie another reason for it bc same-sex couples missed out, & it resparked my curiosity. Thanks again!
### asked by Anon

Thanks I could help bro
___
Answered on December 15, 2015 22:07:34 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/133868154571



## Do you think 'comic sans' gets unfairly maligned?
### asked by Anon

I don't like comic sans, I think it's a super ugly typeface. However, apparently it is useful for people with dyslexia because each letter looks noticeably different from one another. So I can't give it too much flax if it helps make text more accessible.
___
Answered on December 15, 2015 22:07:08 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/133832326091



## What's the best thing about being married? Do u get tax breaks and stuff?
### asked by Belinda

I was ask this question a while ago.  I found out today the answer the second question, and it's no, we don't get tax breaks.  Apparently people did a while ago, but according to my accountant, thanks to the Liberal government, all that good shit's gone.
___
Answered on October 24, 2015 10:10:41 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/133817481931



## What's a topic that you could just go on and on about, but no one else ever wants to discuss with you?
### asked by Anon

WHAT IS THE DEAL WITH SOVEREIGN CITIZENS??? THEY'RE SO ABSURD!!  They're people who think "they", as flesh and blood people, are distinct from "they" as legal entities, therefore the laws don't apply to the physical people, or something like that.  What the hell?! I can understand one nutty person coming up with this, but it's a whole movement, all across the globe!! It's so bizarre!!
I don't usually have an issue with not having anyone to speak to me about things.  My primary go-to person to speak to is Tim and he's incredibly tolerant of whatever topic I bring up, and he endeavours to engage with whatever thing is on my mind.  Even boring things like outrageous housing prices and fonts (and by fonts, I mean typefaces).
___
Answered on October 24, 2015 10:04:41 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/133667240651



## How good are you at expressing yourself? Can you usually find the right words to say what you want or is it a struggle? Do you find yourself misunderstood often?
### asked by Anon

A lot of what I do at work involve me having to express myself very precisely (whether it be in legal documents or providing advice), so I'd like to think I'm fairly adept in that domain.  I definitely express myself better in writing, when I have time to think about what I want to get across and to edit the sentiment over and over again until I'm satisfied with it.
At work we're encouraged to dictate long documents for our word processing team to type up, to save time.  I'm still a novice at doing that and I still find it a struggle to coherently string together sentences and paragraphs into a dictaphone.  But I think I'm getting better at it.
I do try to be precise with what I say when I can, whether at work or outside of work. Of course, like everyone, I sometimes say things I don't mean, but I'd just correct myself when I catch it.  So I'd like to think I'm not often misunderstood, at least I hope.
___
Answered on October 24, 2015 09:57:33 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/133812522699



## Do you think ur funny? Can you make others laugh? How would you describe ur sense of humour?
### asked by Anon

I'd like to think I'm funny-ish.  I make Tim laugh sometimes, but it could be due to the amount of time we spend together, it's bound to at least accidentally happen sometimes, right?
I don't know how I'd describe my sense of humour... I have a love/hate relationship with puns. I kind of love hating good, bad puns if you know what I mean.
I'm pretty easily amused, so my sense of humour is pretty broad. I like the absurd stuff, the smart, witty stuff and videos of children and cats falling over.
I can't stand toilet humour, or offensive jokes by comedians who think they're so edgy.
___
Answered on October 24, 2015 09:52:04 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/133767974347



## You hang with peeps from high school that you hate?
### asked by Anon

I don't hate them, I just don't have many shared interests with them any more. I came to the realisation shortly after high school that we were friends in high school more so for convenience more than anything. We went to different universities, have different jobs. We mainly get together one or two times a year to reminisce, catch up on each other's lives etc.
Two of my friends in high school I do share interests with and I certainly do not hate hanging out with them.  In fact, they attended my wedding.  We have lots of fun when we do hang out and we spend hours talking about lots of neato things, like books and movies and anime and manga.  I don't really watch anime or read manga any more these days, so it's nice to be able to get a feel of what is out there these days.  One of my friends is avidly into otome games at the moment (also known as "reverse harem" games, basically visual novels with a female protagonist and a bunch of hot dudes to romance, but usually not explicitly), so it's fun to learn about the various iterations of that genre of games.  They are the kind of friends whom you don't see for months (during to busy-ness or mutual laziness/asocialness) but you catch up immediately when you do see them again.
I had a best friend in high school whom I had shared most of my interests with.  But during our university years our friendship ended spectacularly and we don't talk any more.
___
Answered on October 24, 2015 09:46:52 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/133724100299



## In your opinion, what's the best teen anthem?
### asked by Anon

I'd say it'll have to be "Anthem for the Year 2000" by Silverchair.  As silly as it is to say, when I first heard the song and saw the music video on Rage, I was like "woah... like, maybe, The Man is doing stuff that isn't good for us!  Should... should I be doing something???".
Even though I grew up poor, my childhood was relatively sheltered and apolitical. But when this song came out, I was in my mid-teens and the new millennium was around the corner. Changes abound! It made me at least CONSIDER getting to know a bit about what's going on around me.  It made me think about what my role was in society at large, and what society might do for/to me when I get older.
A lot of the video was shot in the CBD.  One of the locations is the MLC centre, which I work across from and can see outside my office window.  I sometimes think about the song and feel bad about not doing enough.  I'm sorry, Daniel Johns, I didn't take the fascism away.
The song is hella catchy still after all this time, it's still in my rotation of songs I listen to.
___
Answered on October 24, 2015 09:34:36 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/133453822923



## Oh Belinda, you are a woman after my own heart (not literally, of course)! Smouldering bitter grudges to the end, I say! :)
### asked by Anon

I think it's actually a character flaw of mine that I hold grudges, I honestly wish I didn't. :/
___
Answered on October 24, 2015 09:14:40 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/133405868747



## How do you cope with and express anger?
### asked by Anon

If something in the world angers me, my anger tends to be explosive (mentally, if needs be) but fleeting.  I forget about frustrations pretty quickly once the situation has passed and I return to baseline content that I usually sit at.
I don't deal with being angry at people very well though.  I don't mean when someone disagrees with me, that's fine, but when I feel someone's being shitty at me for no good reason.  I don't usually get angry at people, and I try to give people the benefit of the doubt. But if I feel wronged by someone... that sticks.  Forever, it seems.
I don't think it's a healthy response but it's one I have very little control over, no matter how I rationalise it in my head. With time, the smouldering negativity gives way to long-term memories that would involuntarily come to mind every once in a while to sour my mood.
I'm not sure why it's such a sticking point for me.  I guess I just expect people not to be dicks to each other (and I do try to live by example) and I feel very slighted when I feel that someone's been a dick to me for no good reason.
___
Answered on October 24, 2015 09:13:20 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/133362896843



## Rebounding off Soul's discussion of religious tax-exemption and the freedom from capital pressures to pursue the public good, can you share one of your many insider stories about Catholic organisations that use their vast wealth to help vulnerable people in the community?
### asked by Tim Marsh

You know, in the media, the Catholic Church is often depicted as this conservative, authoritarian, out-dated monolith that spends all its time either hiding paedophiles or stopping gays from marrying.  Not to say that there hasn't been people within the Church that do that, but having worked with numerous agencies and people within the Church, the vast majority of people within the Catholic Church system spend an awful lot of their time and energy helping people.  Just straight up focused on helping those in need, or helping those who help those in need.  They help the poor, the disabled, the homeless, the aged and infirmed, refugees and other vulnerable people in society, regardless of race, religion or creed.
The other week I was contacted by a small Catholic order, who needed help with a loan agreement. They operate a charity that helps settle refugees and would sometimes lend refugees money when necessary.  On this occasion, a refugee was getting on his feet in Sydney and wanted to buy a truck to start a small business. He asked if the order would lend him $4,000 to allow him to buy the truck.
The order asked that I draft up a loan agreement between the order and this man, but they stressed that the agreement must be AS SIMPLE AND BASIC as possible. It must be in plain English so that they can explain each clause to the man without "scaring him off". The terms of the loan required a modest weekly repayment over a few years, interest free, and no security over the vehicle. They were confident that he'd eventually repay the money even if it takes him some time, particularly if he has to cease payment for a while to settle his family in Australia down the line.
How beautiful is that? You would never, EVER see that level of flexibility, trust and generosity from a bank or other capitalist institution.  But the Church can do it because they have the means and objectives to do it.  The objectives may be religious in flavour but I can tell you they aren't about making money or accounting to shareholders, they'll be about helping whoever it is they want to help.  Money is not the end goal, it's a means to achieving different goals.
From what I've seen, the number of Catholic orders, organisations and agencies that have have primary goals to do public good outweighs those that don't.  I'm not saying the institution is perfect and yes I acknowledge that their end game involve spreading the word of Jesus and his teachings.  But you cannot deny that they help an enormous number of people. And for that reason, it doesn't make sense to tax them. Tax dollars are supposed to be used to benefit society and many of these organisations already, directly, benefit society.  To tax them would be to take the money they would be using to help people, and instead spread it thinly through the Government, to fund bureaucracy, subsides mining companies and the like until a small portion of it trickle back out to help people. How is that efficient?
___
Answered on April 23, 2016 05:46:33 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/137542772939



## how many years are there between you and your bro? my older sister better lose it if anything happens to me :)
### asked by Anon

7 years.  Ever since my bro was born, my parents have honed the expectation that I look after my brother.  TBH I was more like his caretaker than sister, particularly since my parents were heaps busy when we were young and I looked after him whenever I wasn't at school.
Sometimes I wish we were closer in age, then I'd feel less responsible for him and we'd likely have more in common!
___
Answered on March 17, 2016 22:35:25 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/136847910603



## Is there a cartoon character that you can relate to?
### asked by Anon

The first name that came to mind is Maleficient (the Disney one, not the Angelina Jolie one).  Not sure why, I guess I just like to think about her sometimes.  I don't know if I can say that I *relate* to her, but I do like her moxie.  She doesn't have some convoluted back story or angst, she straight up curses a newborn baby because the king/fairies were rude to her.  She was all like, "lol oh *I'm* not wanted, oh I see, well here's something else else you might not want, a cursed baby *zap* so long bitches, I'm outta here." Mic. Dropped.
All I'm saying is, *I'd* invite her to my party, she seems way more interesting than all the other vanilla, boring people in the kingdom.
THEN later she turns into a goddamn dragon and summons the powers of *HELL* to fight Prince Whatever.  She so cool, I can only hope to be half as cool.
Huh, that doesn't really answer the question.  Here's the answer:
One of my favourite animated movies is My Neighbour Totoro which I've watched and re-watched since I was a kid.  Once my brother was born, a part of the movie took on new meaning for me.  There is a part of the movie (minor spoilers ahead) where Satsuki, the older sister, thinks she's lost her younger sister, Mei.  Satsuki runs frantically around looking for Mei, her panic increasing over time as she can't find Mei anywhere and the townsfolk can't help either.  For the first time in the movie, Satsuki breaks down and starts crying because she's at a complete loss about what to do.  Uuuugh that scene KILLS me every time.  As an older sibling, there's something so viscerally terrifying about the idea of something happening to your younger sibling.  I relate to Satsuki because I'd fucking lose it if something happened to my brother.  Don't even get me started on Grave of the Fireflies, jesus.
I've watched so many animated series/films, I'm sure that there are other characters I relate to.  I will revisit this question if I think of more.
___
Answered on March 17, 2016 01:11:22 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/136807984075



## Hey Belinda (aka love of Tim's life) :) Did you like St Tim straight away? What were your first impressions of him? If too personal tell me to push off!
### asked by Anon

He was a friend of a friend when I first met him, and my first impressions were that he seemed smart, funny and had a weirdly American accent.  We hit it off as friends pretty much straight away and I thought it was pretty cool how many interests we had in common.
The first time he REALLY impressed me was when I had asked him to be my witness as part of the law competition called "witness examination" which I had joined.  It was a mock trial in which I pretended to be a lawyer acting for a client in a legal matter and Tim was my witness.  As part of the process, I had to examine him on the witness stand and he also got cross examined by the opposing law student.  Well, he blew everyone away with his answers, answering each question with wit and confidence.  He definitely outshone me and the opposing law student.
So after being thoroughly impressed by him that evening, he offered to drove me home and I *think* that's when I started getting a crush on him.  Which was interesting, because I've never had a real crush on a guy up to that point.  In fact, I hadn't even seriously thought about dating anyone at the time.  I thought dating will either come along later or if it didn't, I'd grow up into a cool, single lady.
Anyway, I didn't really know what to do with the realisation, but one thing lead to another, we eventually went to watch a movie together, he said he liked me and I said I liked him then the rest is history.
___
Answered on March 11, 2016 08:58:09 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/136739516363



## If your former friend is happy, we should fix that. She sounds awful.
### asked by Soul James

Yeah she was... The worst part was how I completely did not see it coming. She was so cool once upon a time.
Who knows how she's doing now, she stopped coming to our group catch ups. She probably found herself a new group of friends. No matter, I have better people in my life now. 😊
___
Answered on March 08, 2016 11:58:23 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/136685825739



## Do u think Tim's voice is sex-say?
### asked by Anon

Ya 😉
___
Answered on March 01, 2016 03:11:31 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/136553423307



## Give us the inside goss, where do you get these chinese magic sausages Tim mentioned? china?
### asked by Anon

He's referring to "lap cheong" (may or may not be the legit way of spelling it, it's the closest translation of the word from a Cantonese dialect).  They are a kind of marinated, cured pork sausages I think.  Wikipedia just calls them "Chinese sausages": https://en.wikipedia.org/wiki/Chinese_sausage
They are pretty fatty, but so delicious and flavourful.  It's really nice with fried rice because the rice soaks up a lot of the fat so it doesn't taste greasy.
And they're pretty common, available in most Asian grocery stores and many Woolworths.
___
Answered on March 01, 2016 03:00:20 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/136552816843



## Omg! How awful! I've had painful friendship losses that I still think about but nothing like that! What an absolute *insert word that rhymes with runt here (in capitals)*. Who knows what was going through her mind then? You're better off without her, obviously, but that doesn't always stop the hurt.
### asked by Anon

Thanks. Yeah, it was an awful time. I didn't really have anyone else to turn to at the time except Tim. Most of my high school friends, who were also her friends, knew something was wrong but didn't want to get involved (I didn't want to drag them into the mess anyway) and the only one who might want to get involved was in another country (she's since returned to Australia and we're good friends so yay!).
I later got to know Tim's friends better and that helped a lot. ☺️
I'd love to talk to said ex-bff about what happened now that years have elapsed and we have the benefit of experience and hindsight. But probably better to just move on.
___
Answered on February 25, 2016 07:18:24 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/136462583499



## You seem a bit perplexed by your former friend's decision to end the friendship. Was there no clear reason? Do you know what she's up to these days?
### asked by Anon

Oh man, where do I start?  We were best friends for many years since mid high school.  We had a lot of overlapping interests; video games, anime, blogging, etc.  We saw eye to eye on almost everything.  We worked on many online projects together and were pretty inseparable.  After the HSC, she'd come over every day for two weeks straight and I watched her play through FFVII from start to finish.
We went to different universities and at first, nothing really changed.  We still chatted for hours online every day.  Sometimes all nighters. But by second year, she had her own group of friends, I started dating Tim, she got her own boyfriend and things changed.
It's one thing if we just drifted apart.  I'd be sad about that, but things like that happens sometimes.  But I dunno, she seemed to kind of change overnight.  "Matured" or outgrew me, something like that.  She started being a lot more interested in doing more "adult" (not the fun kind!) things, like kayaking, going to the Hunter Valley, drinking wine, eating at expensive restaurants.  Since I wasn't interested in doing those things, I think she eventually made up her mind that I was too immature and not worth keeping around as a friend.
And the real kicker was how she ended our friendship.  She had already stopped talking to me very much but one day, she invited me to meet up with her at a cafe.  It'd been a while since we met up together and I was excited about finally catching up with her.  But no, there was a purpose to the meeting, and it was for her to list out all the things that were wrong with me.  She then whipped out a list (like, written on paper!!) and read out opinions people had of me that she had carefully collated.  For example, a person I'd sat next to ONCE at a dinner, found me to have been "rude". It's like I got fucking Yelp'd by her and the general consensus of me was in the vincinity of 1 out of 5 stars.  But see, now she has EVIDENCE that I was a piece of shit, she wasn't making it up!
Anyway, the conversation ended with her saying that if I changed, she could be stay friends with me.
Tim drove me home after that meeting and I cried and cried in my driveway.  It was definitely one of the top 5 cries I've had in all my life.  Tim did his best to cheer me up but I was pretty inconsolable.  It fucking cut to the core that someone whom I had considered my best friend would say those things about me.  The sad thing was, I actually still tried to patch up our friendship afterwards, but she had lost interest and gradually stopped talking to me altogether.  Then blocked me on all social media.
I recovered... SLOWLY... I don't see her much any more, except when my high school friends occasionally meet up.  We are civil to each other but don't talk outside these meetings.  She had gotten married then divorced her husband, apparently because he wasn't hardworking enough.  So to some degree I don't think she's changed much from University.
I wonder whether she's happy.
___
Answered on February 25, 2016 05:02:13 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/136461164747



## If you could write a guidebook for your past self to live by, what pieces of advice would you include?
### asked by miss teary s.

1. Be nicer to your brother.
2. The person who was your best friend will decide to stop being your friend during University.  Don't be so invested in your friendship with her.
___
Answered on February 25, 2016 01:51:52 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/136376821451



## Do you remember your first friend?
### asked by Áuryn

I've been told by my parents that before I moved to Australia when I was 5, I had neighbourhood kids who'd come over and play and watch movies with me.  I only have incredibly hazy recollection of those kids so I don't think they really count.
When I moved to Australia, I found it hard to make friends.  I wasn't too bothered by being alone, it was certainly better than being picked on.  I spent a lot of time by myself and read.
When I was in year 4, I made my first proper friend, though we weren't that close. I remember creating a secret club of which we were the sole members.  There was a box of club-related paraphernalia that I had created, but that's long gone.  But as I said, we weren't that close and I wasn't distressed when I had to move schools for years 5 and 6.  I made my first "proper" friends during those years.
___
Answered on February 25, 2016 01:41:44 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/136343746763



## You're not worried about the ravages of age like most people are?
### asked by Anon

Sure I am.
___
Answered on February 18, 2016 03:20:21 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/136328961739



## What are some unusual skills or passions you have that you wish you could do for a living?
### asked by miss teary s.

Sleeping long hours, then wake up in time for a nice nap.
___
Answered on February 18, 2016 02:58:54 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/136287668939



## If the world is a stage, where does the audience sit?
### asked by Anon

In... in space?
___
Answered on February 18, 2016 01:05:15 GMT

__Likes:__ 6

Original URL: /cosmicmissb/answers/136329209035



## I hope this doesnt come across as weird but it probably might. I saw ur pic on Tim's ask.fm and you have a really beautiful, kind face. I am not creepy, I promise! :)
### asked by Anon

Aww, thanks anon!
___
Answered on February 18, 2016 01:04:38 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/136258949579



## Do you buy fancy pjs and have lots of them?
### asked by Anon

Following the tradition from my parents, a lot of my PJs are just old, worn shirts and pants that aren't decent to wear in public any more.  They may be full of holes, missing buttons and are threadbare, but I don't mind since they tend to be very soft after a billion washes.
For the above reason, I don't usually buy PJs.  I also still rotate through PJs that my parents bought for me (the benefits of not growing taller, I can still wear baggy clothes from when I was 13) and THOSE have been worn to bits of the years and I don't have the heart to throw away.  Tim's parents likes to get me clothes sometimes for Christmas and sometimes they're comfy PJs.
I've tried wearing fancy, silky PJs but I didn't like them so much.  I have a silk nightie but it rides up as soon as I lie down, so that doesn't get worn much.  So nah, no fancy PJs for me.
___
Answered on February 18, 2016 00:55:41 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/136237821899



## In which areas of your life do you feel most underestimated by others? Do you like how this gives you element of surprise, or would you rather be more accurately appraised for your talents?
### asked by miss teary s.

I don't get the sense that people underestimate me.  Maybe because I'm opinionated and bossy. :/
The only example I can think of where people underestimated me was when my work friends became delightfully surprised to find out that I can scull a shot quicker than any of them and hold a few, despite not drinking most of the time.  Little do they know that I had honed my sculling abilities from having to drink gross herbal tea a lot when I was a kid.
I think, on balance, people having an accurate estimate of me is a good thing.  If people around me have a more or less accurate idea of who I am as a person and my abilities, then it means there's less misunderstanding and disappointment.  I'm not much of a schemer therefore there isn't a lot I would do with an element of surprise.
I tend to stupidly overestimate my abilities, particularly when faced with a challenge. This happened a lot when I used to play DDR.  I'd see people pass really hard songs, my first reaction would be "hell yeah, I can do that!" only to fail quickly and miserably.
These days, I overestimate my cooking skills a lot, repeatedly.  I'd read complicated cooking instructions and my running thought would be "this is just a list of food stuffs and steps to put said stuff together to make an edible thing, there is no way I can fuck this up and for the end product to not turn out exactly as pictured". It always turns out to be more complicated and harder than that, but thus far I'd say my efforts haven't fallen below a D+ or C- (Tim may disagree...).
Oh! And another example of me overestimating myself is with knitting.  I'd see a pretty lacy creation and I'd think, "shit yeah, it's all knits and pearls at the end of the day, easy stuff, I'd have a cool lacy shawl in no time".  Then when I get started, it's all "knit 1 stitch in the front then in the back, drop two stitches then purl 2 together through the back loops" and I'm like, "errr...".
I have to say, I love a good challenge and to push myself and at least learn something along the way.
___
Answered on February 18, 2016 00:44:17 GMT

__Likes:__ 7

Original URL: /cosmicmissb/answers/136226398411



## Why is seeing yourself in reflections important to you? Is it because ur a babe?
### asked by Anon

Ya.
Besides, I'd be curious to see how I look as I age.
___
Answered on February 17, 2016 23:46:29 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/136222763211



## "stressed as balls"? do balls get stressed?
### asked by Anon

I think it's just a turn of phrase. I don't have the necessary equipment to properly answer your question, but I'm going to guess "yes".
___
Answered on February 12, 2016 03:47:36 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/136218332363



## Who is the dude in the imagination gif? He looks a little familiar.
### asked by Anon

I've been told it's from a Tim and Eric sketch.
___
Answered on February 11, 2016 09:33:57 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/136205171403



## What's a "RCC upbringing"?
### asked by Anon

If I was going to guess, I'd say Roman Catholic Church? Im not a Catholic but we use that acronym at work.
___
Answered on February 11, 2016 09:17:51 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/136205154763



## What's your favourite thing to treat yourself with?
### asked by Áuryn

This is going to sound a bit wanky, but a really good massage. I've only had one once and it blew my mind. I initially hated the idea of massages and people touching me, but before my wedding, a friend of mine convinced me to get one because I was stressed as balls.
Well I went to a fancy spa and had an hour long massage that costed >$100. Oh my god was it good. The lady knew what she was doing and I left the place feeling like a floaty, warm, stress-free marshmallow.
I'd like to do it again one day, but I'm waiting for stress levels to hit the "yep, I'm okay with spending >$100 for someone to knead me for an hour" levels.
___
Answered on February 11, 2016 09:05:32 GMT

__Likes:__ 6

Original URL: /cosmicmissb/answers/136093256651



## Do you believe that global peace is attainable?
### asked by Anon

Sure, depending on your definition of "global peace". Countries stop sending soldiers to fight each other one day? Yeah, I can see it happening, maybe. The end to all conflict between humans? Eh, not so much.
As a wise person once said, "at the end of the day, as long as there's two people left on the planet, someone is gonna want someone dead."  Humans are shit like that.
___
Answered on February 11, 2016 09:01:17 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/136089838795



## How powerful is your imagination?
### asked by miss teary s.

___
Answered on February 11, 2016 08:56:25 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/136084543947



## What’s one mistake you keep repeating?
### asked by Áuryn

I keep eating KFC and it usually makes my stomach hurt. I tell myself "never again" after a KFC meal but lo and behold I'm stuffing my face again a few weeks later.
It me:
___
Answered on February 11, 2016 08:49:41 GMT

__Likes:__ 8

Original URL: /cosmicmissb/answers/136065161931



## What do you find uninteresting but? Do you prefer direct questions?
### asked by Anon

Direct questions are good. Can't know whether a question is uninteresting until I see it. I guess there are some things that are broadly uninteresting. Don't ask me about:- economics- politics- plants- where I see myself in 5 years- what are my strengths and weaknesses- topics I don't know anything about- the sea- side scrolling games
___
Answered on June 03, 2016 04:40:26 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/138280492747



## I am sending this question to people I follow and rly like but don't know very well. Are there any topics you like talking about or getting questions about?
### asked by Anon

Ask me anything as long as it's interesting.
___
Answered on June 03, 2016 04:28:19 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/138222815947



## I really love efficent use of space too, have seen some amazing ideas. i've seen loft beds and like the look/idea but how practical are they, i wonder? what if ur not feeling well? climbing a ladder wouldnt be fun then, neither would night time trips to the loo.
### asked by Anon

That's a good question, loft beds are not very accessibility friendly.  When I was a little (<5), my bedroom was in a loft. I don't recall really having much issue going to the bathroom etc. Might also explain why I have such nostalgia for lofts though.
I imagine if you're not feeling well, you might have to stay on ground floor on the couch or something.
___
Answered on May 28, 2016 06:01:44 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/138168048587



## Can we get a picture of a tiny home you really love please? Maybe you should divorce your husband for asking someone if he should divorce you? Just a suggestion  :)
### asked by Anon

Oh man, there are heaps of tiny homes that I love! There's actually a couple of aspects to this fascination:1. I llike well organised spaces;2. I like efficient use of space, and have a fondness for lofts; and3. I like miniature things in general, including doll houses, tiny furniture etc;4. I like looking at well designed houses and interior decorating.
I also really like huge living spaces, so my tastes are not restricted to the tiny. :)
Re Tim's comment, 'twas just a joke on his end.
Anyway, here's a pic I like:
___
Answered on May 28, 2016 05:31:22 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/138165610955



## How do you stay strong for others at times when you can't even be strong for yourself?
### asked by Anon

I'm lucky enough to be in circumstances where I don't usually feel like I'm not strong for myself. I'm a much better advocate for others (for my family, clients and otherwise), but I'm not usually a push over.
I find being strong for others to be much easier than being strong for myself. Might be practice and experience thing, I dunno.
___
Answered on May 28, 2016 05:10:50 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/138133762251



## Are you ambitious in regards to work? If yes, what would you like to achieve? Do you want to be a partner in a law firm?
### asked by Anon

Apparently I used to be quite ambitious. I say "apparently" because I don't recall feeling particularly ambitious but my high school friends swear I used to be.
But no, I'm not particularly ambitious. I try hard to deliver good work for clients but other than that I'm definitely a "work to live" not "live to work" kind of person. I don't particularly want to be a partner either because there's a lot of politics in getting to (and staying at the top and I don't need that drama. It might still one day happen, but I'm not going to bend over backwards to get there.
___
Answered on May 28, 2016 05:08:10 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/138112159179



## Oh my. I spent a good year learning about the Chinese Cultural Revolution. Fascinating stuff but not at all appealing to have lived through. I feel like your parents and family in general should have appeared in our textbooks - would've made lessons far more fun haha!:)
### asked by Habiba

We had to read "Wild Swans" for English class in year 12. As you might know, that book was all about the Cultural Revolution. I was fascinated and (probably insensitively) incessantly bugged my parents about whether the contents of the book were true. Most of it were. That was when I a learnt a lot about my parents' history.
___
Answered on May 28, 2016 05:05:08 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/138093793739



## Do you know much about your parents' lives before you were born - i.e. where they were born, what their childhood was like etc.?
### asked by Habiba

A bit! My parents don't like to talk about it though. They grew up during the Cultural Revolution in China and it SUCKED. A few things I've gleaned over the years:
- Before the Revolution started, my dad's family was quite wealthy and "bourgeois" and was hit hard by the Revolution. All of their wealth were confiscated and they were left financially ruined.- My mum loved learning but she was a kid/teen right in the middle of the Cultural Revolution. The high school she was attending was shut down and so were universities. She would've loved to have attended university but she missed her opportunity.- My mum loved reading but reading non-Communism approved texts were hugely frowned upon (possibly illegal at the time?). My mum's uncle hated the party and secretly smuggled translated Western books (like Robinson Crusoe and the likes) for my mum to read. He was quite the rebel and were severely beaten up a couple of times by people from the party.- Instead of school, my parents and most kids of their generations were sent to farms to learn farming. They said it was horribly boring and a waste of time.- My dad said he once saw a guy being decapitated by the blades of a landing helicopter. :/
So yeah, they went through a lot. Even though I grew up mostly in a poor household, I had safety, certainty and a roof over my head. I'm never not grateful that I didn't have to go through what my parents had to.
___
Answered on May 23, 2016 11:35:52 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/138079792075



## Thanks 4 being straight w/ me! I think I'm mostly annoyed that these people seem to enjoy appearing like buffoons or infantilising themselves. I hated when ppl tried to infantilise me as a teen. That adults want to do it to themselves annoys me. Thanks again, you seem way less judgemental than me :)
### asked by Anon

No problems anon! I get annoyed by heaps of things and I personally am not a fan of people who infantilise themselves to be cute either. It's not cute, it's irritating.
But you what IS cute? This:
___
Answered on May 23, 2016 11:24:33 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/138076096715



## I don't know why you so often think I'm angry. I'm such a chill guy! At least when I assume you're angry, I'm usually right (just playing the odds, really). 
If you were going by base-rate frequencies, you SHOULD assume I'm always depressed. Good probabilistic returns on that one.
### asked by Tim Marsh

"At least when I assume you're angry, I'm usually right"
Look at all that shade being thrown.
___
Answered on May 23, 2016 11:16:28 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/138077842635



## If you could choose a special talent, what would you pick?
### asked by Áuryn

Being able to easily learn languages. I'm so grateful of the fact that I can speak Cantonese, and am sad that I never properly learn how to read or write Chinese. I used to be able to speak and read a smidgen of Japanese, but that's all faded now.
Being able to speak a second language is so useful and allows me to communicate in a completely different way to English. Imagine if I know more languages! At the moment it's an up-hill struggle, I just don't have the brain (or time, frankly) for it.
___
Answered on May 22, 2016 07:03:00 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/136654953163



## How emotionally intuitive would you say you are? Can you easily pick up on the feelings of others?
### asked by miss teary s.

Not great to be honest. Often times I think Tim is angry but he assures me he's not, he's just blank or thinking. You'd think I'll be better at it 10+ years into the relationship!
___
Answered on May 22, 2016 07:00:08 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/137864231371



## Are there any topics on which you feel more educated or experienced than others? If so, how did you gain that understanding?
### asked by miss teary s.

Da legal system. From university and work.
___
Answered on May 22, 2016 06:58:55 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/137848641227



## The monthly target that you're expected to reach at work, do you think it's unreasonable? If yes, what would you like it to be? What happens if you don't reach it - a stern talking to, a kick up the arse, then you get sacked?
### asked by Anon

My monthly target is pretty standard for the legal industry. Is it reasonable? Well, I hate the system as a whole. It's such a crude way to measure performance. If we did away with it, I honestly think we'll be just as productive, just without a "monthly target you have to meet" hanging over our heads everyday. But hey, it's not likely we'll get rid of it any time soon because it's a neat and easy way for management to see whether their employees are maximally meeting profit margins they had forcasted. Ugh.
I work with a great boss and I don't think it'll be the end of the world if I don't meet my target. It sometimes happens, most often when there just isn't enough work to go around or if you spend a lot of time on tasks that are non-billable. My boss is understanding of this.
How well you meet your target affects things like whether you'll get salary increases, promotions etc. If you constantly don't meet your targets, you may be made redundant or pay freezes if the reason is because there isn't enough work for you to do.
___
Answered on May 22, 2016 06:58:06 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/137900872395



## When people use the term 'adulting' for basic tasks like paying a bill or turning up to work on time it makes me unreasonably angry even though most are sort of joking. I'm wondering if you can relate at all because I think my migrant background comes into play. My friends and I often acted as 1/2
### asked by Anon

The other parts: "interpreters for our parents and paid bills, read letters etc from an early age. I dont think we felt particularly 'adult' these were just things we could help with. That pampered 20 something people want to deliberately infantilise themselves or think they deserve a clap for paying a bill on makes me nauseous, especially because if they didnt pay it, it'd probably be picked up by their parents anyway so there wouldnt be any real consequences. I sound bitter, hey? lol I'm bitter at them, not my childhood which I mostly enjoyed!"
Yes I can relate to a degree. Being a kid of migrant parents, I too had to take on many "adult" tasks as a kid. Like you, I started deciphering bills and letters, speaking on the phone and in person on my parents' behalf since primary school due to their poor English. I recall that I used to be embarrassed and resented my parents and wished that they'd tend to these "adult" problems themselves like other "normal" parents.
But my parents were doing their best to keep a roof over our heads and provide for us and the least I could do was be their spokesperson if I was best equip to do it out of everyone in the family.  The embarrassment and resentment eventually faded and it became just another regular chore.
Do I feel angry that there may be young adults who make a big deal about "adulting" (stupid word btw, if I had to be angry about anything it's making "adult" into a verb)? Not really. I'd come to realise that what I had were skills I learnt from being raised the way I did. To be honest, I feel sorry for adults who don't know how to do basic adult tasks like paying bills. These tasks should be taught in schools for gods sake! I don't understand why we're taught algebra, history, geography etc but not basic time and money management skills, how taxes work etc, things that adults actually need to know. Maybe they're taught now in schools? I hope so.
I don't think it's fair to assume that just because someone doesn't have basic adult skills down pat, it means that they are otherwise pampered. Why would you assume their parents would pick up their bills? A lack of adult skills could be for all sorts of reasons. And even if their parents could pick up the tab, so what? There are people who don't have to work a day in their lives, ever, are you angry at them?
Hate to break it to you, but the system is rigged. It turns us against each other and makes us angry and bitter at our lot in life, for not having more, at those who have more, for not being those people. And there are those who are less privileged than you and I, who are angry and bitter that you had an enjoyable childhood while they had not.  Those who had to learn to be an adult in more ways and earlier than us anon.
If anything, I'm angry at the capitalist system, at the injustices it creates, not necessarily the people (almost everyone) trapped inside it. I don't have a solution to this, would be neat if I did though!
___
Answered on May 22, 2016 06:49:02 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/138057163467



## What is something that is "uncool" that you do anyway?
### asked by Anon

I tuck my PJ top into my PJ pants before I go to sleep so that my top doesn't ride up when I sleep.
___
Answered on May 20, 2016 06:28:45 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/137990405835



## Do u enjoy ur job and the place u work at? Do u ever think  "Hey, I'm a lawyer. A lawyer ffs! Lil ole me a f&cking lawyer!".
### asked by Anon

Yes and yes to the first question. And no, not really, re the second. The only surprising aspect of it all was when I realised that I got the grades to go into law school. But once I was in, it was a hard, 5-year slog through one law class after another. It was a slow transition; from being a law student, a law graduate, then a fledging lawyer. I suppose there was an element of incredulity, of "holy shit, I did it", when I stood up and was "moved" to become a lawyer before Supreme Court judges in my admissions ceremony. But it was more so a feeling of relief, of "I made it", rather than "I can't believe this is happening".
But yeah, I generally enjoy my job. I'm very fortunate to work with mostly decent, kind and happy colleagues. One of them is a bit intolerable but in an eyerolly way, rather than an actively bad way. My boss is a thoughtful and reasonable guy. Some of my work can be pretty boring (LOTS of reading), but my days are pretty varied. I spend a lot of my time analysing documents, drafting letters, speaking and meeting with clients and other lawyers. I am thankful that most of my clients are nice people. Most lawyers are nice people too, but sometimes, not so much.
Take a conversation I had yesterday. I had to speak on the phone to another lawyer acting for the other side about a disputed issue, a partner of a large firm.  He was polite, but I could hear the thinly veiled impatience in his voice, his exasperation that he had to waste his precious time to pick up the phone and speak to someone obviously less experienced but was refusing to budge on an issue. I listened while he patronisingly explained some fundamental aspect of the law to me and why I was therefore in the wrong. In those situations, I gotta just stick to my guns and be reminded of my instructions and confidence in my conclusions and views. It's pretty gratifying when I get to respond with (words to the effect), "nah, I respectfully disagree."
I do need to emphasise though, most lawyers are reasonable and fairly easily to deal with. They’re just people and most know that you’re more likely to resolve a problem if you’re not a dick about. But without a doubt, a big part of my job is about attempting to resolve conflicts and disputes, which is why I like to keep my personal life as drama free as possible. So far, so good.
The most irritating part of my job (and this goes for most lawyers in private practice) is that I have a budget I am expected to meet. Every day, there are a certain number of hours (6.5 in fact) that I am expected to bill. There’s a monthly target that I am expected to reach. It’s pretty draining having to account for most minutes of my day, every working day. I get anxious at the end of every month, wondering whether I would meet that month’s budget. I tolerate it but I certainly don’t stop wishing I could be rid of it.
___
Answered on May 07, 2016 03:52:25 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/137744033739



## What TV show has gone off the air that you wish would come back?
### asked by Anon

Ch-ch-ch-Chippendale, Rescue Rangers! Now you have the tune in your head.
But nah, there's this really obscure TV show that used to come on Australian TV after Art Attack, like 15-20 years ago. I don't remember the name, but it follows this crazy lady (maybe a witch?) named "T-Bag" and her indentured servant/hostage teen boy side kick "T-shirt" and they go on all sorts of crazy, magical adventures. But no one I speak to about it seem to have heard of it and I worry that I may have dreamed it. So yeah, that show needs to come back on to vindicate me.
___
Answered on May 06, 2016 01:12:40 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/137774829259



## You and Tim are my fave ask.fm couple for realz. He always talks about u so nicely and respctfully. Please never break up. For my sake even though we've never met and probably never will. Thank you in advance.
### asked by Anon

Just for you anon, just for you. *puts away divorce papers*
___
Answered on May 05, 2016 10:22:03 GMT

__Likes:__ 7

Original URL: /cosmicmissb/answers/137760417483



## Whoa that is compelling stuff, I would read the sh1t out of that. I'm tempted to write it, damn
### asked by Soul James

Doo iiittt. Also I'd just like to see more discussion about how weird the Pokémon universe is if you think about it.
___
Answered on May 05, 2016 05:53:00 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/137757583819



## I'm not sure why there is a small contingent of followers, only on Ask.fm, who vaguely ship @Soul_James and I, but I eagerly await the weird fanfiction.

What would you title a Soul/Tim fanfic, and which young adult book series would you set it in?
### asked by Tim Marsh

I don't read YA fiction so I'm going to pick another work.
Ok, you guys are Pokémon trainers, both trying to be the very best, like no one every was (ever was). Naturally, you guys are rivals.
BUT, for some reason you two are the only ones in this world who realises how fucked up the Pokémon universe is, including the concept of capturing animals in balls and battling them.  You're both compelled to do what you do for personal reasons, but the doubts and thoughts gnaw at you both day and night. And you both think you're the only one who is plagued by such thoughts. Until you find out one day that the other ALSO feels the same way.
Will you guys remain rivals? Will you team up to try to change the world? What Pokémons would you have?
___
Answered on May 05, 2016 02:49:43 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/137756865483



## Is there one person in your life that can always make you smile?
### asked by Anon

No, thank fuck. Can you imagine having a Kilgrave-esque person in your life that always makes you smile?
I suppose a charitable reading of the question is to say who is the person in my life that has most success in making me happy or cheers me up. Unsurprisingly the answer is @TimMarshPhD. He's a very good listener who is patient and often knows what to say when I'm sad to cheer me up. Or says nothing and just gives hugs. And gets me food when I'm hungry. All these things usually makes me happy.
___
Answered on May 02, 2016 07:16:58 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/137704499659



## How do you define 'family'?
### asked by Anon

Family means nobody gets left behind or forgotten.
___
Answered on April 30, 2016 01:42:38 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/137666337995



## alright alright alright alright alright alright alright alright alrightalrightalrightalrightalriiiiight
### asked by Anon

___
Answered on April 23, 2016 09:26:47 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/137545932747



## just how cool r you? r we talking icy cool? cooler than cool? the coolest?
### asked by Anon

___
Answered on April 23, 2016 07:40:34 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/137545688011



## Ok, I will! I'll get back to you soon.
### asked by Anon

You better deliver, anon!
___
Answered on August 11, 2016 00:54:08 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/139656129483



## What inspired your mood? The ghost?
### asked by Anon

Nah, just been stressed lately.
___
Answered on August 11, 2016 00:52:14 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/139642267083



## I need new accounts to follow. Instead of tagging the people you normally tag, try and find someone different. It can be someone you don't even follow!
### asked by Anon

*You* go find me some new people to follow. Make sure they're interesting.
___
Answered on August 11, 2016 00:51:47 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/139655453899



## Have you witnessed any paranormal acitvities?
### asked by Anon

YES I HAVE! Well... it was more like a "glitch in the Matrix"-esque event rather than spoOOOoky ghosts.
Picture this: me, mid-teen, at home one evening. My folks were having their friends over and was preparing dinner and socialising. I was upstairs in my room, chillin'. I hear a call from my dad telling me that dinner is ready and that I am to come downstairs.
So the stairs between ground and first floor stops near the front door (i.e if you were to come in from the front door, the first thing you'll see is the stairs up). The front door was one of those heavy, self-locking doors, that swings close automatically unless you propped it open. Once closed it auto-locks unless you turn the lock from inside or, if you're outside, you unlock the door with a key.
Anyway, so I was at the top of the stairs about to head down and I see a man standing at the foot of the stairs inside the house. The front door was closed behind him. He was a middle-aged looking Asian man, thin, pallid in an informal suit, no tie, neutral expression. The most distinctive thing about him was that he had a massive, old-timey camera hanging from around his neck. He watched me walk down the stairs and didn't say anything, didn't move.
Now, I thought he was one of my parents' friends whom I hadn't met before. I didn't think it was strange, didn't think anything of it at the time really. As I squeezed past him when I got downstairs, I may have given him a polite smile before heading to the dining area. When I got to the dining area, I saw the table was set, and my brother, parents and their friends were all seated. Everyone was accounted for. I asked my dad whether they were waiting for anyone else and he said no and that I should hurry up and sit down. I looked back (you could see the front door from the dining area) and the man was gone.
After dinner, I asked my brother whether he saw a man with that description, he said he hadn't when he came downstairs before me. I asked my parents and they said no, and that I must've imagined it. BUT I DIDN'T!!
Was it a ghost? Was I hallucinating? Was he just a rando who picked the front door to get in? He couldn't have gotten in from the back of the house, that's where my parents and their friends were hanging around. He couldn't have gotten in (or out) through any of the ground floor windows. I was pretty spooked afterwards and searched the house (it wasn't that big a house) and of course, didn't find anything.
SO WHO WAS MAN???
___
Answered on August 08, 2016 04:31:58 GMT

__Likes:__ 9

Original URL: /cosmicmissb/answers/139600292555



## Do you believe people 'become their praise'? If so, have you become yours?
### asked by miss teary s.

Don't people get praised for aspects of who they already are?  If you're praised for being good at basketball, wouldn't it be because you're already good at basketball?  Perhaps I'm misunderstanding.
When I was younger my parents used to praise me for reading a lot.  Having a kid interested in reading was probably nifty in a number of ways - it implies she's keeping out of trouble, it's easy to keep your eyes on her, she's learning, she's "smart".  I read because I liked reading and because I didn't really have friends at the time.  It was a wonderful escape.  As I got older, I continued to read a lot or played on the computer.  It got to the point where the praises turned to concern, of "don't you want to go out with your friends? Why are you always reading?"
I guess you can't win.
My parents are firmly the kind of people who want their kids to be successful, but only moderately, to not stand out in the crowd.  It was good that I read a lot, but I shouldn't read too much.  When I recently got promoted, the sentence out of my dad's mouth after the initial congratulations was, "well I think that's enough promotions, no need to get to the top."  So praises from my parents always had the implied caveat of "it's good what you're doing, but don't be exceptional".
___
Answered on August 07, 2016 01:56:47 GMT

__Likes:__ 6

Original URL: /cosmicmissb/answers/137900224715



## Do you think your dad would b good at teaching you how to cook? My dad's a great cook too but terrible at teaching or inparting recipes. He says 'a little bit of that' instead of giving me proper amounts and leaves out whole ingredients when giving me recipes. I have to physically watch him to learn
### asked by Anon

My dad's definitely a bit of this and that kind of guy.  He doesn't follow recipes at all.  One time he prepared something new (which was delicious) and I asked him whether he followed a recipe.  He said no and just shrugged and said "it's just a Chinese dish".  Cryptic.
So yeah, if I were to learn, the only way I could do it is to either: a) hover around and film while he's cooking or b) hover around and furiously take notes.  Ingredients would have to be measured by the number of pinches, handfuls, bundles etc, measurements using specific bowls and scoops specific to his kitchen and a great deal of eyeballing.
___
Answered on August 07, 2016 01:25:34 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/139017575371



## What's one thing people do on public transport that really, really, really annoys you?
### asked by Anon

Standing in front of train doors when people are getting out.  It blows my mind that people don't understand the simple concept of standing to a side to let people off first.
Most people understand and stand to a side, particularly at rush hour, but you do get people now and then who don't know or remember train etiquette who stands in front of opening doors. Usually to be barged to a side by a horde of departing commuters.
The worst example I've seen was when Tim and I were in LA a few years back and took the train. It was during offpeak hours and when the train arrived, people were huddled right in front of the doors.  I couldn't believe it.  It took twice as long to get onto the train as people jostled in each other's way to push their way on or off the train.  One was almost tempted to call it... a train-wreck.
___
Answered on August 07, 2016 01:20:44 GMT

__Likes:__ 6

Original URL: /cosmicmissb/answers/139505177035



## did you have any posters on your bedroom wall during high school? if yes, of who or what?
### asked by Anon

I had two.  One was a promo poster for a Final Fantasy symphony orchestra that was signed by Nobuo Uematsu himself that I had treasured.  I don't have a photo of it because it's rolled up and stored somewhere in my apartment right now.
The other was a coloured copy of the Escher print "relativity" (see below).
___
Answered on July 09, 2016 07:51:46 GMT

__Likes:__ 6

Original URL: /cosmicmissb/answers/138886736075



## How did you learn to knit and crochet? Did you go to lessons or learn online?
### asked by Anon

I learnt online.  My mum also knew how to knit and crochet but she was super rusty when I started to get into it, so I didn't really learn from her.  There are so many great free tutorials on the internet (I learnt mostly from videos), it's completely possible to pick up knitting and crochet for very little cost.
___
Answered on July 09, 2016 07:47:11 GMT

__Likes:__ 6

Original URL: /cosmicmissb/answers/138893402059



## Oh I must have forgotten to uncheck the ask anon button. Come to pottery next term! It's $250 for 8 weeks. 10-1 on Saturdays but there's night classes and week days.
### asked by A Lexa

Thanks for the recommendation! I shall consider it! :)
___
Answered on July 01, 2016 12:37:26 GMT

__Likes:__ 6

Original URL: /cosmicmissb/answers/138738991051



## Is that a yes? :D
### asked by Anon

___
Answered on July 01, 2016 12:36:52 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/138855371723



## Hey Belinda, you definately are a cutie. I earn way more than Tim and am pretty cute myself. Wanna go out for a drink?
### asked by Anon

___
Answered on July 01, 2016 12:24:26 GMT

__Likes:__ 8

Original URL: /cosmicmissb/answers/138854317771



## Daaaaamn that double knitting is insane! I don't know why I can't get the hang of knitting. 
If you eventually have time to do a woodworking project you can use my dad's workshop. He showed @TheGracchan how to make cool boxes last year. Lots of scary power tools.
### asked by Anon

Hi I assume this is @dadcliffe! I'd love to try woodwork, thanks for the suggestion! The last time I tried it was in high school and we were tasked to make a trophy out of wood. Unintentionally, mine came out looking like a coffin and I threw it away. But yeah, all those power tools are a bit scary... I was thinking of sticking to more manual tools, like hand saws and hammers. I guess that limits what I can do.Oh yeah, I'd also like to try my hand at pottery, we also got to make clay artworks for a term in high school and it was so much fun! I love that yarn bowl you're currently making!Re knitting, I think one of the hardest early part of the learning process is deciding whether you want to knit via the "English" method or "Continental" method. The main difference is which hand you hold your yarn. I started learning use the English method (hold yarn in my right hand), though recently, for double knitting, I've learnt both methods so I can knit with two colours at the same time. Youtube tutorials have been an absolute godsend! Good luck to you if you ever want to pick it up, I'd be happy to help if you have any questions!
___
Answered on June 26, 2016 03:12:18 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/138720587467



## Do you have a lucky talisman you keep with you? PAP!
### asked by Anon

Ever since I was a kid, my mother would always insist that I carry around SOME kind of luck or protective trinket, either in my bag or wallet. One year it was a tiny jade pagoda to ward off the bad luck that would befall me that year, as it was supposed to have been a particularly bad year for me. Spoiler alert - it wasn't any different from any other year, but who's to say it wasn't the charm that warded off all that bad juju? Hint, it wasn't.
Currently I'm carry around a little (about the size of a 20c coin) jade thingy in my wallet, see below.  It's supposed to be a deer I think. My mum gave it to me years ago and I like to carry it around because it reminds me of my mum more so than for superstitious reasons.
___
Answered on June 26, 2016 02:55:23 GMT

__Likes:__ 6

Original URL: /cosmicmissb/answers/138737494987



## lmao. what have u got against plants? u don't have a green thumb, i gather?
### asked by Anon

I was gifted a cactus once, and it died 3 days after I took it into my care. So no, I don't have a great time with plants.  I'm not a fan of cut flowers either.
HOWEVER, I've got nothing against them, I just don't know anything about them. There are some really interesting kinds of plants and flowers out there.
___
Answered on June 25, 2016 06:47:00 GMT

__Likes:__ 1

Original URL: /cosmicmissb/answers/138635282891



## did you have trouble finding work when you first left uni or did you have a smooth transition into the workforce?
### asked by Anon

Thankfully not. I started working part-time at a tiny law office when I was halfway through my degree and I transitioned into full time at the same office once I graduated. It was more of an "internship" (i.e. unpaid labour) when I was part-time, but I learnt a ton and got heaps of practical experience so I guess that made up for it. As soon as I graduated though, I knew I had to get out of there ASAP and I eventually did.
It's a really tough market at the moment, at least for law students, it's kind of expected that you have some legal experience by the time you graduate. My brother is at the tail end of his law degree and I certainly don't envy how much stress he's under to land a decent job.
___
Answered on June 20, 2016 04:29:53 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/138608366283



## do you think women who let their bra straps show are 'tacky' or 'sloppy' with their dress? i'm actually quoting a girl who said this.
### asked by Anon

No I don't. They are just straps for goodness sakes. Why are they tacky? Because they remind people of bras and boobs? Heaven forbid, someone give me some pearls to clutch!!
I think women who police what other women wear are tacky.
___
Answered on June 20, 2016 04:23:01 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/138611476171



## Yeah, I know there's still a lot of harassment about but a colleague, woman in her 50's, said her boss pinched her butt years ago and I laughed at the absurdity of it! Then I thought surely no-one would do that today! Guess I was wrong!
### asked by Anon

Unfortunately it's not as absurd as you think. Many asshole guys do bullshit like that because they think it's harmless, or just a bit of fun, or that women would take it as a compliment or some shit. Well it's none of those things. But many women don't complain when it happens for fear of ridicule, or causing trouble.
I'd like to think that with that kind of behaviour in the workplace overall is reducing over time but it's far from disappeared. And it really depends on the workplace. I hear that waitresses get often treated like shit and have to endure all manner of unwanted touching from gross patrons. And of course, work places aren't the only place women get harassed.
___
Answered on June 18, 2016 01:02:29 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/138567381451



## Do u spend much time in court or arent u that type of lawyer? Do u know if people really say "I object!" in the court room? I sure hope so.
### asked by Anon

Lawyers who go to court do litigation and I don't do that these days. I mainly draft documents, provide legal advice and carry out transactions so I'm in my office 95% of the time.
In my old job I did do some litigation though and yes I did get to go to court sometimes. Twas a lot of fun but also stressful.
And yes people (usually barristers) do object, but they usually say "objection, Your Honour..." followed by the reason as to why they are objecting.
___
Answered on June 18, 2016 00:34:44 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/138529261515



## Have you ever had your butt pinched by a stranger? Does that shit really happen?
### asked by Anon

It's never happened to me thankfully but a work colleague had a guy slap her butt out of nowhere.
We were both walking back to the office from a meeting with other lawyers and I was drag a huge suitcase full of files. This was in the city, full of people. As we turned a corner, she suddenly yelped and shouted "what the fuck!" and turned. I thought I heard a guy's voice saying "hey gorgeous" or something like that. I asked her what happened and she said that some random guy walking the other way just slapped her butt. We kind of saw his back as he was walking away but we decided not to give chase and instead hurried back to the office. Obviously she was rattled by that.
It was super gross and upsetting and whoever thinks doing something like that is "cheeky" or "just a bit of fun" can go jump. Don't fucking do that to people.
___
Answered on June 18, 2016 00:30:03 GMT

__Likes:__ 7

Original URL: /cosmicmissb/answers/138567039435



## congrats on ur fishing successes! fishing's always grossed me out but! not being judgemental, or a PETA advocate, but dragging a fish around w/ a hook in it's mouth, and then it comes out of the water and flips around until it dies. eww. does it flip around til it dies? or do u knock it on the head?
### asked by Anon

Yeah hooking fish out of water is pretty gross. The two times I went fishing I was with my family and I'd let my dad handle all the gross fish killing stuff.
The worst part was all the sea birds that would hang around the boat while we were at sea. I was deathly afraid that I'd accidentally hook a bird and the grossness of it struggling and flapping would surely be so immense that I would no doubt die if it happened. But thankfully it didn't.
___
Answered on June 11, 2016 05:20:20 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/138395681995



## Capitalism or Gluttony? :) Either way you are my hero!
### asked by Anon

Capitalism IS gluttony!
___
Answered on June 11, 2016 05:17:22 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/138410408395



## Do you know any big gossips?
### asked by Anon

I know gossip of all sizes, tell you what, for today only you buy a big gossip from me and I'll throw in a medium gossip completely free, whatdoyasay?
___
Answered on June 11, 2016 04:40:45 GMT

__Likes:__ 6

Original URL: /cosmicmissb/answers/138431153355



## What always sounds like a good idea but never actually is?
### asked by Anon

Eating a pile of churros followed by a banana split.
Capitalism.
___
Answered on June 09, 2016 04:30:08 GMT

__Likes:__ 6

Original URL: /cosmicmissb/answers/138394608075



## Lmao. I agree, fuk the sea! All the creepy shit lurking around deep in there. No thanks! Can you believe people got on cruises for enjoyment? Wtf?
### asked by Anon

Yeah I'm not a fan of salty water or swimming (on the account that I can't swim). Drowning is high on my list of things I don't want to be the cause of my death.
Seas can be pretty to look at with their many shades of blue. Beaches are lovely as long as the day isn't too hot, the sand is soft and the beach is devoid of other people.
Yeah I don't understand cruises... Just bobbing around slowly with nothing around you but water and 100s of obnoxious people for days or weeks on end. No thanks.
I like fishing though. I've been twice and had caught lots of fishes.
___
Answered on June 03, 2016 05:55:09 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/138280631499



## Teeth dreams are fukt. FUKT I TELL YOU! Not that I have to after your last ones but I have weirdo teeth dreams too which always leave me feeling disturbed. My teeth are pretty good but and I wonder if people with manky teeth get them and if its more disturbing?
### asked by Anon

I had braces put in relatively late (late teens/early twenties). I didn't remember getting teeth dreams before I got my braces, but I definitely remembered getting them during and after braces. I think it was the ongoing fiddling with my teeth that made those dreams more stark. I imagine that those with teeth issues (whether it be fixing teeth or having manky teeth) would likely have more teeth dreams.
___
Answered on October 18, 2016 09:20:03 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/140673029323



## When I am stressed I dream either that I have to go back to high school (as an adult) and re-do my VCE... or that my teeth are falling out. When I was at uni I dreamed of my teeth falling out almost every night.
### asked by Anon

I had dreams of taking exams, being late for exams, missing exams, missing assignments etc for YEARS after I finished uni.  It was only a few years ago that those dreams ceased. School was a long, stressful period of my life, I can totally relate.
Having teeth dreams every day is pretty full on though, hoped they've gone away.
___
Answered on October 17, 2016 10:30:15 GMT

__Likes:__ 3

Original URL: /cosmicmissb/answers/140672795339



## Please never tell me about your nightmares ever again, holy crap. 0_0
### asked by Soul James

___
Answered on October 16, 2016 08:58:49 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/140671854283



## That's really rough dude, I hope you recover swiftly :(
### asked by Soul James

Thanks
___
Answered on October 16, 2016 06:22:27 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/140670275531



## What makes you feel really uncomfortable?
### asked by Anon

Still not understanding what someone said after having already asked them to repeat themselves more than once.
___
Answered on September 24, 2016 09:26:26 GMT

__Likes:__ 8

Original URL: /cosmicmissb/answers/140385366987



## Finish the sentence: Never have I ever...
### asked by Anon

Died.
___
Answered on September 23, 2016 08:05:07 GMT

__Likes:__ 8

Original URL: /cosmicmissb/answers/140371737547



## Do friends or aquaintances ask you for free legal advice a lot? Does it become annoying if they do? Do you feel like screaming "PAY ME MUTHAFUKAS!". I probs would.
### asked by Anon

Nah, my friends are pretty cool about it, no one's pressured me for legal advice. I've read a couple of contracts for friends who bought properties to give a second opinion but that's about it. It's not good form for me to give legal advice outside work so I'm glad friends don't put me in that position.
I freely offer my services to friends to certify documents and statutory declarations though, but I have yet had anyone take up my offer...
___
Answered on September 16, 2016 11:17:58 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/140242949835



## Do you value your legal secretaries? I worked as one part-time while at uni and was condescended to by the female lawyers, sadly, which used to upset me. When I finished my degree one said now I'd be able to get a "proper" job so I asked her if she thought being a legal secretary wasn't a "proper" >
### asked by Anon

"> job. She replied with "Do YOU think it's a proper job?" I told her that we could do a test, I stop doing it next week and she could do her own admin, like proper. Maybe I was being touchy but she apologised."
I 100% value my secretaries. A good secretary makes my life so much easier on a day to day basis. I tend to get along well with my secretaries and I do my best to treat them as nicely as I can.
I can't stand people who treat secretaries and support staff poorly. Support staff are just people doing a job like everyone else in the firm, they aren't second class citizens. Sometimes secretaries are super busy and good ones are godsends, able to juggle 50 things at once while keeping their cool.
The first secretary that worked for me was someone I admired a lot. She was meticulous, hard-working and tirelessly makes sure my letters were formatted per the firm's style guide and keeps our calendars orderly. Too bad she eventually left.
___
Answered on September 16, 2016 11:14:16 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/140193819851



## What you miss the most from your childhood?
### asked by Anon

Free time.
___
Answered on September 16, 2016 11:07:30 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/140274059467



## if you could only pick one: Superhuman Speed (you can run at the speed of light without any negative side effects), Strength (you can lift objects of any weight/shape, have extreme durability & immune to diseases), or Intellect (You possess knowledge of all things currently known by mankind)?
### asked by Francis Vivek

By extreme durability and immunity to disease do you mean effective immortality? That'll be pretty neat.
I was originally going to go for intellect, but if it means all I have is a lot of knowledge but I'm not actually smarter at being able to process and use the knowledge beyond my current capabilities then it probably won't be much use to me. I guess in that case I can sell some of that info? But might I be targeted for knowing secrets I'm not supposed to know?
___
Answered on September 14, 2016 00:16:23 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/140242207947



## Hmm, not in any way, iyo?
### asked by Anon

How can knowledge equate to things? How can knowledge equate to a banana or a car? If you can reshape your question so I have a better understanding of what you mean I might be able to answer better.
___
Answered on September 13, 2016 23:45:34 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/140242245835



## How does "all knowledge" equates to "all things"?
### asked by Anon

It doesn't.
___
Answered on September 13, 2016 23:40:25 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/140242197707



## i laughed at the pOrn obsessed incompetent lawyer story but fuck im angry now. ive seen this a few times. lazy, shitty men who just know how to talk the talk getting jobs and promotions. which would never last as they'd fuk up but it would often take a while coz no-one would 'watch' them.
### asked by Anon

Yeah, it was pretty infuriating. You'd think someone that incompetent would be fired pretty quick, but no, he lasted over a year at our firm! WTF! It was his whole alpha-male, "oh yeah everything is under control" schtick that completely blindsided my boss for so long. He got paid a lot of money too even though he was such a screw up.
Thankfully, I think my boss has really learnt his lesson and will be more cautious about hiring people based on schmoozy confidence. And the guy in question is no longer practicing so at least he's no longer inflicting bad advice on people.
___
Answered on September 10, 2016 11:57:07 GMT

__Likes:__ 7

Original URL: /cosmicmissb/answers/140182725579



## The attitude your parents had to success, do you think it was shaped by their culture or by being migrants? My migrant patents were similar in that they wanted me to be successful but not stand out too much. They feared it would inspire a kind of jealousy from True Blue Aussies.
### asked by Anon

Both! My parents lived (survived) the cultural revolution, which was just A MESS to put it incredibly lightly. They then came to Australia with pretty much nothing, and had to start over from scratch, trying to adapt to a new culture, language, social expectations while also making enough money to not die. And to raise a child (me). Then another one (my brother). It was super tough. So I can totally understand why they'd have a "don't rock the boat" mentality. They had worked so damn hard to get to where they, (and my brother and I by proxy) are today, they don't want their achievements to be jeopardise unnecessarily.
I don't think their thinking is because they fear that our success will inspire jealousy from white Australians. I think it's just a background fear that standing out could trigger unknown variables and the unknown is scary and risky. THEY had to face decades of fear, uncertainty, alienation etc, they just don't want us to have to go through it. Fitting in is safe and dependable, so why not stick with that? It's that kind of thinking I think.
___
Answered on September 10, 2016 08:58:00 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/139637966539



## Are there any Asian stereotypes that you relate to?
### asked by Anon

Errr... well I like eating rice. It's a very nice kind of staple food. I like it in many forms, boiled, fried, sushi, congee. Congee is my chicken soup; the food I crave when I'm sick.
___
Answered on September 10, 2016 08:45:56 GMT

__Likes:__ 5

Original URL: /cosmicmissb/answers/139754301131



## Her name is Belinda, she's a lawyer lady. So imma gonna keep her number, for when I'm acting shady. She doesnt post a lot on here, but it's worth waiting for. And if dont agree wit me, I'll nail you to the floor.
### asked by Anon

D'aww thanks anon!! :D
___
Answered on September 10, 2016 08:43:30 GMT

__Likes:__ 6

Original URL: /cosmicmissb/answers/140181715915



## Can you tell us the boss scrapbook story please?
### asked by Anon

Sorry for the tardy response.  FYI, it is in response to this question: http://ask.fm/rmaynard85/answers/139309588602
So the person wasn't my boss, he was a more senior colleague. This "colleague" was the perfect picture of a sleazy, fast-talking, bullshitting lawyer, really gave the profession a bad name.  He was this tall guy, mid-30s, muscular, incredibly confident, sharkish, quite skilled at giving great first impressions (hence why he got hired into our firm for a senior role I suppose).  But once you get to know him, he was just THE WORST.  Here's a few things about G (there's probably way more but I didn't know him very well):
- He was terrible at his job. He'd make promises to clients and either doesn't deliver because he's lazy (and off-loads it to someone else in the team) or delivers usually subpar quality work (spelling/grammar mistakes, clearly rushed work etc).
- He would sometimes spend days out of the office, doing God knows what. One time he tried to charge our client 7.5 hours of work described simply as "strategising".  That would've been over $3k for nothing. Thankfully my boss noticed and pulled the bill before it was sent.
- Apparently he offered cocaine to our team's secretary at our Xmas party.
- His office, which had a glass wall (all our offices do), was located at the end of a corridor. He repeatedly petitioned to have frosting put over the glass, claiming that seeing people walking around all the time disturbed his concentration. The glass was eventually frosted.
- Not sure why he wasn't fired immediately but he was eventually fired. This leads me to the "scrapbook story". So after he left, our secretary was clearing out his shelves and found a draft document that he was presumably working on. But as soon as she flipped the page, it turned out to be pages and pages of explicit, p0rn0graphic (is this a word that ask.fm doesn't like?) pictures that he had curated, downloaded and printed in black and white, then stashed in his cupboard. WTF. Firstly, what does this say about his understanding of technology? Also, did he not know we had colour printing all along?? Explains the frosted glass I guess. So gross.
- Before he left, one day he took us out to lunch because he was super excited about the fact that his girlfriend was pregnant and he was going to propose to her. But after he left, I heard that they broke up because she discovered he had cheated on her, multiple times. She had an abortion and he commented on her facebook publicly calling her a "baby-killer". Even his sister called him a disgusting pig.
- We heard he moved to another law firm after ours (not sure how he got references) but he was quickly fired as he was found one morning drunk at his desk after consuming a lot of the office's alcohol.
I'm running out of room, but yeah, quite a "character". We were all glad that when he left. I just checked, he's no longer listed as a practising lawyer.
___
Answered on September 10, 2016 08:39:47 GMT

__Likes:__ 8

Original URL: /cosmicmissb/answers/140147087307



## Do you ever feel Tim psychoanalysing you on the sly?
### asked by Anon

I'm a-Freud so.
___
Answered on August 27, 2016 00:46:33 GMT

__Likes:__ 8

Original URL: /cosmicmissb/answers/139934002891



## You gotta admit, bebop had some pretty shit episodes everyone pretends werent there. Like the weird balloon man killer.
### asked by Anon

Excuse you, if you're talking about the episode about Mad Pierrot, then you gotta rethink your tastes buddy, because that's one of the best episodes.
However, I'll admit that there were definitely some dud episodes, such as the one about magnetic feng shui, that was stupid.
The music though!! And the characters! And the visuals! So good.
___
Answered on August 26, 2016 09:55:54 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/139930082763



## *whispers* I might have a job for you. How much do you charge for.....terminations....if you know what I mean.....
### asked by Anon

My rates are reasonable but I only honor requests from dead people. So send in you request along with your cremated remains crushed into a diamond bullet and I'll see what I can do.
___
Answered on August 19, 2016 12:21:35 GMT

__Likes:__ 6

Original URL: /cosmicmissb/answers/139806504907



## yeh, exactly! can you answer this but. i just want it on record that i called you a badass mofo and i pity the fool who gets a late night phone call from you telling them that you gon shoot dem and shoot dem good.
### asked by Anon

Thanks anon! 😄
___
Answered on August 17, 2016 04:24:47 GMT

__Likes:__ 4

Original URL: /cosmicmissb/answers/139770064587



## are fukn Ask.fm at it again? :( i read ur answer before but went back to read it properly now imma on lunch and its gone! wtf? at least i saw it! guess m*rder and k*ll might be censored words. lol.
### asked by Anon

Yep, one of my answers disappeared. I wish they'd at least give a warning first!
___
Answered on August 17, 2016 04:05:10 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/139769358539



## would you be willing to shoot someone and risk a jail sentence for your man after he's ded? would you be your own lawyer? can people do that for realz? be their own lawyer in a criminal case?
### asked by Anon

Shooting wouldn't be my go-to method of murdering someone because I'm not a very good shot. The method of choice would naturally depend on who the victim is, where and when the murder will take place etc... if the murdering is for Tim, then I'll try to at least let the victim know beforehand that I will be shooting his or her body with a diamond bullet made from the remains of Tim, if circumstances allow. After the murder, I'll shoot the body with the bullet just to complete the process, but will likely retrieve the bullet (if it is locatable and hasn't shattered) and retain it as a keepsake.
No I will not be my own lawyer.  As the saying goes, a man who is his own lawyer has a fool for a client.  But yes you can defend yourself in court if you want (regardless of whether you're a lawyer or not).  It's not a wise thing to do though.
___
Answered on August 17, 2016 00:28:31 GMT

__Likes:__ 7

Original URL: /cosmicmissb/answers/139767661771



## Name your next adventure. PAP of inspiration!
### asked by Anon

The great beyond.
___
Answered on August 11, 2016 00:59:35 GMT

__Likes:__ 11

Original URL: /cosmicmissb/answers/139656196555



## Dw, I will deliver and deliver for free!
### asked by Anon

Only the best please.
___
Answered on August 11, 2016 00:58:15 GMT

__Likes:__ 2

Original URL: /cosmicmissb/answers/139656160203
