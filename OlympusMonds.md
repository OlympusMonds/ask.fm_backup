

## Speaking of rocks, have you been to Uluru/The Northern Territory?
### asked by Anon

I have been to the NT, to the Entia Dome, on a 14 day field mapping trip with my phd supervisor. It was pretty full on, as I had never camped before, and I'm a pretty crap field geologist. Overall, it was good though. The rocks were extremely complicated, and I didn't really know what was going on, but I did learn a lot. I also walked a lot.
I was working on Uluru briefly, but only using it as a topographic example of erosional processes, but because I'm a noob, I accidentally used a very similar big rock about 50 km east of it...
It's pretty different out there, so I would recommend people take a look.
___
Answered on June 30, 2015 10:33:39 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/129011288126



## Would you like to have laser surgery on your eyes so you wouldnt need to wear specs anymore or are you ok with face furniture?
### asked by Anon

Hmm, maybe in the future, but I do kinda like my glasses. They're a pretty major face feature, so it would be a pretty mega change. Currently I have no real motivation to get rid of them, and the >$3000 to get it done is a pretty big disincentive.
___
Answered on June 29, 2015 12:56:29 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/128978094398



## Anyone who can't see you are so much more than a humble rock scientist is a blind fool.
### asked by Soul James

Well I managed to misidentify basalt today, so that's a good thing I'm not only known for that! But really, I don't mind being known as a rock dude. One day, someone will need or want to know something rock/earth related, and rather than google it, they will instead contact me, and through the sheer joy of education, I will learn them so good. So good they will never forget the power I instill in their head. So good that they will thank me as they wander away, stunned that something they have always scoffed at or simply ignored has been there, around them and beneath them, their entire lives. It is actually quite interesting.
___
Answered on June 29, 2015 12:51:50 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/128977828926



## Do you regret joining ask.fm after that last question?
### asked by Anon

It put a mirror to the one dimensional persona that I must project.
___
Answered on June 29, 2015 12:39:18 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/128977731646



## If you ran into Jennifer Lopez on the street and she started singing "Don't be fooled by the rocks that I got..", would you interrupt and say, "I wouldn't be fooled, Jenny, I'm a geologist"?
### asked by Anon

I would be fooled, because the rocks she refers to indicate wealth and social status, when what she really is saying is she is still Jenny "from the block". It is confusing that she mixes the two topics in one sentence, given the polar opposite meanings, and so again, yes, I would be,  and am, fooled by her rocks.
___
Answered on June 29, 2015 12:32:53 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/128977598270



## Your beard is pretty impressive tbh. How much time do you spend maintaining it? Do you use hipster beard oils & stuff?
### asked by Anon

When did you see my beard...? Anyway, yeah, it's pretty mega atm. I do occasionally shampoo and condition it, and comb it, but that's the extent really.
___
Answered on June 29, 2015 11:40:32 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/128976488510



## What drew you to geology?
### asked by Anon

I had an extra spot in my timetable in semester 1 of first year. My sister suggested geo, since one of her friends did it.My initial goal in uni was to become a physicist - however, my many years of relying on natural talent in school helped me to fail semester 1 physics very readily! Probably because of this (geology is easy to not fail), and because I did find the structural side of geology kinda interesting, I kept going. I was also doing computer science, which was also good.By the time I got the 3rd year, I did get my act together *a bit* more, and I did do better. Though this may be a surprise, big parts of geology are very dull, so I did struggle in those areas, and my grades reflect this. Earth structure, geodynamics, and geophyics I found very interesting.Towards the end of 3rd year, I had a mini-panic about what I could do afterwards: mining didn't sound super fun, and anyway, I was a very crap geologist, so why would they hire me? (looking back, being crap wouldn't have been a problem...). But we had just been shown numerical modelling, which is essentially using computers to simulate long-term Earth processes. I spoke to the prof about it, and explained my comp-sci background, and he managed to get me excited about what we could do!Anyway, without going into the details of my post-graduate study, I am now a much better geologist, and find most aspects very interesting! I also kick myself whenever I remember how much of a drop-kick I was, which is pretty much every time I have to teach current under-grads, who are making all the same mistakes I did...
___
Answered on June 21, 2015 12:34:23 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/128688419390



## Did you have any imaginary friends as a child?
### asked by Anon

Not explicitly, no. I did come up with various fantasies with my toys, but no straight up friend.
___
Answered on June 21, 2015 12:05:37 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/128687933758



## What's with the nose ring? Are you trying to come across as a degenerate? Nah, srsly, it looks good but why did you choose to have a piercing there? How often do you take it out and clean it?
### asked by Anon

I don't have one..? I have a lip ring, if that's what you mean?
___
Answered on July 10, 2015 05:28:14 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129342560830



## That's interesting about weight training ie yr able to switch off yr brain much easier than w cardio. Cardio is so boring in a gym as opposed to running/cycling outdoors. You have convinced me to become a buff dude. I am going to muscle up & come fite you. Srsly, thanks for your informative answer.
### asked by Anon

No worries, I look forward to this clash of titans.
___
Answered on July 09, 2015 04:09:26 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/129309308478



## Do you find yourself flexing in front of the mirror more now? Not that there's anything wrong with that. Flex it loud and flex it proud, I say! Ever been sprung mid pose?
### asked by Anon

Occasionally, but more often I just prod various bits of myself, and go "woah ho ho!". Then continue getting ready.
___
Answered on July 08, 2015 13:33:36 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/129275387198



## Have you changed your diet at all? Protein drinks etc.
### asked by Anon

Nope, not at all. You only really need protein shakes if you don't get enough from your diet, which I probably do.
___
Answered on July 07, 2015 13:22:23 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129247194430



## You've spoken about how your gym work now focusses almost exclusively on weight lifting. What's the main appeal of this? Do you like the aesthetics of looking muscular? Feeling strong?
### asked by Anon

At the beginning, it was almost entirely because my cardiovascular fitness is bad, and you don't need to breathe so hard doing weights as you do running. On top of that, as I mentioned before, the result of muscular workouts is much more immediate (at least for me). This is both in terms of the amount you can lift, which is  easy enough to track, and in your actual muscles, which get bigger relatively quick, especially when starting out. I suppose with running or whatever, you probably do get better soon enough for it to show up in the numbers, but I found the reward to ambiguous. I did (and plan to again, one day) ride my bicycle to uni, which is 6 km one way, and i did notice getting faster as I did it more - but what you really notice is when you take a week or two off, and it kills you. In terms of time, weights seem to be quicker. 5 out of 6 of the workouts we do in a fortnight take about an hour, with one that takes 1.5. Afterwards I'm sweaty, but not drenched as I would be for cardio, so it's a bit more practical in that sense. Though in saying that, I'm sure if you did that much cardio, you would also see nice results and probably sweat less. Finally, with weights, I find my brain is able to switch off much more easily than cardio. With cardio, I need music or a podcast or anything, else my mind races and I get bored. With weights, even though you have lots of down time, my mind is quiet, and I can just focus on the conversation at hand, or nothing at all with no probs. Also, I'm huge now, so that's nice. (but srsly, it is a new experience to be kinda built - I occasionally will find a spot on my body that can be flexed to rock hard status, and it's a genuine surprise. It's like finding you have a new body part.)
___
Answered on July 07, 2015 12:32:50 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129244768574



## What music have you been listening to lately?
### asked by Anon

My music taste is now all over the shop, but most recently I've been listening to New Project, which is a heavy metal band. I was introduced to them because a PhD student at uni is their drummer, and his wife the guitarist. Anyway, they're pretty good! I also have a lot of electronica, dubstep, chillout electronic, pop covers, punk, emo, and Taylor Swift. I'm not great at finding new music, so I tend to listen to the newer stuff I do find until I get sick of it.
___
Answered on July 07, 2015 12:16:15 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129244818750



## Have you faced any discrimination in Australia bc you are a 'ginger'?
### asked by Anon

I have brown hair, and a somewhat ginger beard. No, I have not.
___
Answered on July 07, 2015 11:57:10 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129244785214



## How long did you stay bald, or partially so? What's the maintenance like? Do you just shave in the shower every few days?
### asked by Anon

There was zero maintenance - I just reset to bald one time. I guess it wasn't TRULY bald, because I didn't actually use a normal razor to shave it to nothing - it was a beard trimmer with the guard taken off. I think it  ended up a fair bit shorter than in the head pic I posted. Probably something like 1 mm.
Anyway, so I was essentially bald for as long as it took my hair to grow back, which was something like 2 months to back to normal male hair length. My hair before I shaved it was quite long, especially at the front. My fringe was down to my chin! (when pulled). So yeah, big change.
___
Answered on July 07, 2015 05:40:57 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129239184446



## It's nearly impossible to put on a woollen hat with head stubble? This is v good to know. I read an account by a female cancer survivor who said that she when she shaved her head, after it'd started to fall out, she started to get hit on by lesbians. Apparently it's not an uncommon experience?
### asked by Anon

I don't think I'm quite the right person to answer the experience question...
___
Answered on July 07, 2015 05:28:07 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129238269246



## That balding progress pic might be a bit too sexy for ask.fm. Be careful m8 - you don't want to get banned. I'd miss ya!
### asked by Anon

Scantly clad heads are very risqué.
___
Answered on July 07, 2015 05:27:12 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129231534910



## Is looking shorter really a con of shaving yr head for you? Are you height-challenged ie a shortass?
### asked by Anon

Well, I wouldn't put it like *that* - but I am generally a bit shorter than most dudes. 175 cm or something?
___
Answered on July 07, 2015 05:26:15 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129231490878



## Do you have any siblings? If yes, where do you fall in the birth order?
### asked by Anon

Yep, one sister, who is 2 years older than me. She lives in England now.
___
Answered on July 07, 2015 05:25:40 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129211536958



## Dude that looks fucking sick when was that pic?
### asked by Soul James

That was the day I became bald. I considered stopping at that point, but decided to go whole-head.
___
Answered on July 07, 2015 05:25:21 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129212448830



## A balding progress pic - this was the start of my bald journey:
### asked by OlympusMonds

Ta da!
___
Answered on July 06, 2015 13:47:36 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129212316990



## I'm surprised to hear that indoor rock-climbing is so accessible tbh - thought it would attract a narrow range of ppl? But what do I know? Not much, obviously. How long does it take to climb an indoor rock? And once you have, do you come back down & climb up  another section?
### asked by Anon

Yeah, I was surprised too. I think it's because it is fun, but also its hardness is based on your body weight, and not really your strength (to begin with anyway). Essentially, if you can climb a ladder, you'll be fine.
A single climb only takes a few minutes normally, and once you reach the top, or wherever you want to stop, you just sit back into your harness and sort of walk back down the wall (this is much quicker than going up). Then you swap out with your partner, and they climb while you do the ropes. Normally when we go, we do maybe 10 climbs in a session. You get pretty tired after that, and your forearms are very tight.
Good fun though! Let me know if you want to try it out sometime, anon.
___
Answered on July 05, 2015 12:41:58 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129173666622



## Did you like having a shaved head? What were the pros & cons?
### asked by Anon

Yeah, it was good! First things to note: it was much colder, and putting on woollen hats with head stubble is nigh on impossible. It was also nice just to mix it up.
Cons: people said I looked a lot more threatening, more of thug. Not really the look I'm going for, so that wasn't great. I guess the only other thing is that I probably looked shorter, as my hair is typically so big.
___
Answered on July 05, 2015 12:33:29 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129138812478



## Do you have any tatts?
### asked by Anon

No tats for me, though I don't mind them.When I was younger I had great plans for various ones - but looking back on the ideas now makes them seem pretty silly. I guess if I had gotten them, it would be a landmark for that point in my life, but meh.
___
Answered on July 05, 2015 12:29:58 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129173672510



## What was Canada like?
### asked by Anon

Cold, but fun. I appear to be naturally good at winter sports, such as skiing and ice-skating, so I enjoy it a lot.
Most of the time it was hanging out with Kayla's family, with trips here and there. It was very relaxing, so that was good too.
___
Answered on July 03, 2015 08:19:32 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129103085118



## Thank you for saying you appreciated my ancedote about 'Kaylas' when, in reality, it probs bored you to tears. You are a v nice man and your not-named-after-a-soapie-character Kayla is a lucky woman.
### asked by Anon

Thanks m8
___
Answered on July 03, 2015 08:15:17 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129107331134



## When you said 'it's pretty different out there' (referring to NT/Uluru), what did mean? Can you expand a bit pls? I assume there's good and bad?
### asked by Anon

I mostly meant in terms of the landscape - it's dead-flat, red, with hectic rocks sticking out of the ground (especially in Alice Springs - there is a ring of rocks surrounding large parts of it).
There are also no people around - like, at all. The closest township to us was about 100 km away, so yeah, pretty isolating.
We did go to an aboriginal community to buy some food from a store, and it was pretty normal except for signs saying no alcohol allowed in town.
So yeah, it is just a completely different environment.
___
Answered on July 03, 2015 05:43:41 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129103254846



## Speaking of names is Olympus really yours? Are you a Greek God or something?
### asked by Anon

No, it's my most common gaming tag. Olympus Mons is the largest volcano in the solar system - about 20 km high, on Mars (Everest is about 8 km). By adding an extra 'd' to Mons makes it resemble my surname quite a lot - given I'm also a geologist, it seemed very apt.
I'm also a huge volcano.
___
Answered on July 02, 2015 10:50:09 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/129074019134



## Is Kayla your girl? I worked with a smart/cool girl named Kayla once - her sister worked at the same place too. One day she let slip that Kayla was named after a Days of Our Lives character - there was a couple Patch&Kayla. Cruelly, we all laughed & Kayla was livid at her sister.
### asked by Anon

Kayla is engaged to me, yes.Kayla does seem protective of her name - any hint at confusion with a TV character with the same name is swatted down immediately. This is typically fair enough, because they don't make TV characters about normal, grounded, nice people.I appreciate your anecdote about Kaylas.
___
Answered on July 02, 2015 05:01:19 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/129071171134



## Have you made or restored anything with your new woodwork skillz? How did you find out about the Shed too? I saw something about men's Red Sheds years back & thought they sounded like a fantastic idea. Wondered how successful they'd be & if/how they'd reach their targeted demographic.
### asked by Anon

I'm currently making a box! It has dovetail joints, and is made out of fancy hardwood, etc. I just fit the hinges on Tuesday, which took many hours.
I'm also attempting to make a timber framed house (at much smaller scale) to learn how to hand cut joints that (mostly) stay together with gravity or wooden pegs (think ye olde wooden houses). It's going OK.
I heard about the men's shed because I was looking at how I could get access to things like a table saw or bandsaw. No tafes near me do any courses; privately run courses are >$800 for like 10 weekends to make a breadboard; and schools and stuff that have the tools clearly won't let people take ohs risks.
I eventually found the men's sheds, but from their website, it was surprisingly hard to see if it was appropriate for me to join, as they push the mental health thing big time. I didn't want to intrude on that, so I emailed them, and they invited me to have a look. Signed up that day, and go about once a week.
They do really push the social aspects - for example, they have set lunch times where no work can be done, and you sit and chat, which is good. Some of the men have gone through a bit of mental decline, but everyone is very supportive of them.
If you are considering joining (which I would recommend), you probably will need to be relatively accommodating - some of these fellas are set in their ways, and will simply talk over each other to say what they want to say. Some of them just want to chat, so you may get less work done than you planned. They also appear to be slightly more racist and sexist in their general joke telling, so there's that too 😡. Conversely, mental health has zero stigma there, so it's all over the shop.
Anyway, overall, it seems like a good project that is getting better with time.
___
Answered on July 02, 2015 02:20:34 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129036082494



## Your commitment to the gym this year sounds impressive. What motivated you to start going?
### asked by Anon

Thank you.
Not really sure - in Nov of 2014, I started going a bit, because Kayla hassled me. It was just me going to the gym, doing some cardio, and lots of leg weights. I was going to Canada, and they have lots of leg-based activities (skiing, hiking, skating, etc.), so I thought I'd better get better.
Even with that relatively short time, my legs bulked up a fair bit, which was nice. Then after Canada, my office-mate wanted to gym for muscles, so we did so together. We started with doing the same all-body workout every session (Mon, Wed, Fri), and amazingly increased the weight every single time. We started out fairly low compared to what our "max lift" would have been, so we kinda toned into it, which in hindsight was good. After a few months though, your muscles can't recover in time for the next session. Now we do isolated groups, steadily increasing weight each week. One week is for high-weight, low-rep; the next week is for high-rep, low weight. The high weight week is more fun, because it takes less time, and it's more impressive for yourself - but the high-rep week is more satisfying, where you felt like you actually did some serious work.
To actually address motivation: having a gym bud seems critical. Someone who will be disappointed in you if you do not attend what you said you would. In saying that, I'm surprised at both our resolves. I guess it's nice to see easily measurable results (both numerically and reflectively (in the mirror)), so it's not as bad as trying to loose weight, were it's very ambiguous about how you're going until a fair while in.
Unfortunately, in my day-to-day life, I have almost zero need for strength, so I rarely get to test how practical it is. Lifting the mattress to put on a fitted sheet is about the only thing, and it's still a pain in the ass.
Anyway, to show off, as I have learned all muscley people do, when I started, I was probably benching like 45 kg. It probably wasn't max, but I didn't feel comfortable with more. Compared to now on high weight/low rep, I do 80 kg, and my low-weight/high rep I do 67.5 kg. Those are my most impressive numbers - the other core exercises: dead-lifts and squats - have sort of gone all over the place as we try to get the proper form and technique. ATM they are 80 kg dead-lifts and 60 kg squats (but they are the deepest of deep squats, baby).
All of these numbers are puny to proper athletes, but I could not give the slightest crap. I think being in a gym with massive muscley people (rugby dudes at mine) is actually better - there is no chance you can out-class them, so you are forced to compete with yourself.
___
Answered on July 01, 2015 14:23:27 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/129043655486



## Wut? You mean you're not TRYING to come across as one dimensional? Seriously, what are some of your other non-rock related interests/passions, rock dude. Lets get this stuff out there!
### asked by Anon

Fine fine.
In side-project style things, I do some programming, varying across many subjects. The one that garnered me some fame (e.g., more than 0) was my circleriser program: https://github.com/OlympusMonds/PyCircleriserI also managed to misattribute Starry Night to Monet, rather than Van Gohg, so there you go.
For the past 2 years or so I have watched literally hundreds of hours of woodworking videos on YouTube. Not really sure why, but I really like them. This has lead to me joining the Waverley Men's Shed about 2 months ago, which is pretty much a free workshop where they have tools and junk. It's largely designed to combat things like post-retirement loneliness, and to expose men to mental health resources, but they welcome any dude who want to do some woodwork (I'm not 100%, but it does seem to be just dudes - no gurlz allowed). Anyway, it's been really good.I'm now trying to do more hand-tool carpentry at home, but while hand tools are cheaper than electric ones, they can still be very expensive. If you have an old unwanted hand-plane, I would be interested in taking it off your hands.
I know you said non-rock-related, but I occasionally go indoor rock-climbing, which is quite fun. It also seems to be weirdly accessible. Like people of all sorts (short, tall, big-muscles, wraith-thin, etc.) do it, and manage to get to the top. Kayla is scared of heights, and she even does it (though sometimes she bails half-way up).
Since the start of the year, I have also been going to the gym 3 times a week, pretty much exclusively with weight lifting. I am now swole as hell, and am out-benching my teenage self by a significant margin. I also dropped a pant size, yet gained about 7.5 kg. I'm still a fat dude, but now it's leaning towards the strong-man fat-guy look, which is arguably better. My gym-bud and I go to the Sydney Uni gym where all the rugby guys go, so we are still almost the least strong people in the gym, despite at least doubling all our starting weights over the past 6 months. It sounds like a bad thing, but I guess it gives a pretty good goal or objective: try to not be the crappest people here. Not that I really am intending to be huge, but hey, not gonna complain if it happens.
___
Answered on June 30, 2015 12:54:50 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/128996948286



## I with you about helmets m80. Helmets aren't the reason why ppl don't ride ffs!
### asked by Anon

Good. To be fair, I'm sure having to have a helmet doesn't *help* people ride - but yeah, as mentioned, the having to ride on the scary roads was a much bigger issue.A really bad example of how helmets hurt riders is in Melbourne - they introduced those hire bikes that you can pickup and drop off anywhere in the city. Sounds great - except you can't hire a helmet, and you have to wear one. So they were panned pretty hard.
___
Answered on July 15, 2015 09:51:16 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129513245758



## Are there really actual people that would ride a bike everywhere if only they didn't have to wear a helmet?
### asked by Anon

People do feel very strongly about this - but I think in reality, the people you talk about just DO ride without a helmet. If you're in Newtown, something like 30% to 50% don't wear them. Cops don't really care, unless you do something to annoy them, and they have a solid thing to bone you on.There is some lady in... Victoria or Tasmania..? who is a massive advocate for no-helmets. So yes, people clearly care. Side note, she also just rides sans-helmet.
___
Answered on July 15, 2015 09:46:16 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129511664446



## Have you had any bad accidents on your bike? Ever been car-doored?
### asked by Anon

No, never doored, luckily. In regard to accidents in general... I don't think I did, or at least nothing major. I am an assertive cyclist, which is probably because of my time riding my motorbike. For example: when I would ride down King St in Newtown, you often have continuous parked cars along the side of the road. From what I have seen, cyclists often ride close to the parked cars, so that cars driving behind you have a chance to overtake - this is typically a mistake, IMO. You run the risk of dooring, and cars trying to overtake way too close to you when they see a gap they might make. You need to ride smack bang in the middle of the lane in this case, so that the cars behind will not be tempted to even try to overtake. Sure they might get slightly cranky, but YOU are in control in this situation. When you next see a gap in the cars, you then pull in, and let the build up pass you (or just ride faster). Giving cars hope, when there really should be none, is very dangerous, so you have to be assertive instead.
___
Answered on July 14, 2015 16:36:44 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129447945534



## Sorry, I was referring to the helmet specifically there, terrible wording on my part. Helmets are hard to store somewhere. You either have to carry them around, have somewhere to put them, or chain them to your bike.
### asked by Soul James

Slightly, I guess. They are very light though, so I would typically just strap it to the outside of my backpack, or as you said chained to the bike.
___
Answered on July 14, 2015 12:45:30 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129486671678



## What size is this bed of which you speak? We just got a King-size. Roomy.
### asked by Anon

I think I got a queen. It is mega-comfy, I think the mattress was like $1600, but we got it half price. Totally worth it.
___
Answered on July 14, 2015 12:38:19 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129486575166



## What kind of bike do you have? I want to get one, there are some great tracks near me. I know sfa about bikes tho. How much $$ am I looking at for a decent bike?
### asked by Anon

Umm, I have an apollo brand, I think.They are crazy expensive. I bought some crappy bike at an auction house for $100, and took it to a place in Marrickville, and the guy in there said it was worthless. He then showed me some new bikes, and they started at $400. I caved, and got my apollo for like $450. I had to haggle hard to get him to trade in the auction bike - in the end, he traded it for a water bottle holder :\
My new apollo was so nice and smooth! Wow, totally worth it! Oh no, almost immediately, my spokes were snapping, my gears jumping, and my brakes wearing unevenly. Took it back to the place in Marrickville to explain this to the guy. Here is a transcript:Me: "Here are a list of problems - what gives, I just bought this for $400!!"Guy: "Pfft, what did you expect, it was only $400".Me: "I expected more."Anyway, he did not help me. I then moved to Randwick, and took it to a new mechanic. He explained many things, but a big issue with "cheap" bikes like mine are the wheels. They suck, and should not be trusted. I ended up buying two new wheels ($90 a pop), and got him to work on the gears. Now it is much better, but man, so expensive.
In summary, try to get a good deal on gumtree, because cycle places will most likely try to bone you.
___
Answered on July 14, 2015 12:37:40 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129447922750



## Describe yourself in 5 words pls.
### asked by Anon

I enjoy bed too much.
___
Answered on July 14, 2015 12:32:10 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129447967038



## Seatbelts don't fuck up your hair, which can be a big deal for a lot of people in terms of riding to work, & also they're difficult to store somewhere, which is much less a problem for going to work but more of a problem going anywhere else. Harder to chain up than an entire bicycle.
### asked by Soul James

I feel ya, buddy, but the alternative is potentially having your head smashed in. Regarding storage, are we talkin about bikes, cars, or seatbelts now?
Overall, while it's probably not the best for the govt to mandate helmets, it is probably the most effective way to get people not to die on bikes. More education (so people actually want to wear them) would be better, but it hardly seems like a silver bullet. So yeah, for now, put yer bloody hemlet on, m8.
___
Answered on July 14, 2015 12:31:08 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129449181758



## Do you remember your first exposure to pornography as a teen or younger and how you felt/reacted to it?
### asked by Anon

I think it was either trying to get a mod for the first Tomb Raider, or a VHS movie that had some topless scenes. In both cases, I think I was very nervous about someone walking in.
___
Answered on July 14, 2015 12:27:42 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129476937278



## Seatbelts don't discourage people out of cars & onto bikes, tho. Having said that, I doubt I'd ride on a road w/out a helmet. Cycleway tho, defs.

Real reason for this q is to say I just saw your ask.fm profile background, that pic of your head in the massive ball. Everyone check it out it's amazing
### asked by Soul James

No, they don't, because cars are so handy. But the "annoyance" is still there, having to wear this constricting strap. Yet we don't even notice it now, it's just automatic and only is of note if something goes wrong. I feel with helmets, it's the same once you're used to them. They just are part of it, and that's ok. Sure it would be a bit nicer not to have them, but in the current Australia, you def need it.
Thank you, it was a globe projector, typically used for displaying the Earth. My slowly rotating head was a good dataset instead.
___
Answered on July 13, 2015 05:41:18 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129445106750



## RE bike helmets, how about the simple reality they discourage bike use? Like, maybe people's reasons are spurious or whatever, but if it's preventing a lot of bike use, maybe that's worth considering seriously? The "cyclist" as an identity only exists because cycling is considered fringe, after all.
### asked by Soul James

Yeah, I have heard this. I think it may be the case, seemingly from a discomfort or fashion problem. However, in much the same way that seatbelts are uncomfortable and unfashionable (I guess), I feel helmets are sufficiently good at saving you that it's worth it. It would be good to get more people cycling, but sans helmets would surely have a big human life cost. Also, as already mentioned, I really don't think the main problem with cycling is the helmets - it's the extremely scary and confronting need to ride on roads full of assholes. Either cycleways or major attitude adjustment would remove this issue - then I would consider removing the mandatory helmets (but I probably wouldn't).
___
Answered on July 13, 2015 05:28:17 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129444900158



## So when will the Wedding Of The Century be happening? Have you made an approximate date?
### asked by Anon

Maybe mid next year? May be in Canada too? Well see
___
Answered on July 13, 2015 05:12:27 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129441607486



## I think I read earlier that you used to ride yr bike to uni? How do you feel about compulsory bike helmets? I'm cool with it but I know it drives some cyclists mad.
### asked by Anon

On initial reaction, I think those people are really stupid, and they should stop their protestations. I could explain more about level of government control in our lives, and my own somewhat conflicting thoughts on different issues (eg. legalise weed: sounds ok; legalise helmetless bike riding: rawr no!) - but I'm on my phone, so it's hard to type. In summary, I've yet to hear a good reason not to wear a helmet. Things like: they are uncomfortable, they make cyclists take more risks, they don't wear them in Denmark, etc. Nup - those are not good reasons not to. For cyclists safety improvements - better cycle paths, and somehow culturally shift  most car drivers into not massive fuckwits.
___
Answered on July 13, 2015 05:05:26 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129444223038



## capitalism, yo
### asked by happyhaps

Dang
___
Answered on July 12, 2015 08:56:41 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129412628030



## AHAHA! Not gonna lie, you DO look pretty dopey when yr unconsciously playing with yr lip ring - great pic. Is that yr only piercing?
### asked by Anon

Yeah, it's a great habit. I only have that one.
___
Answered on July 12, 2015 08:56:34 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129413073470



## I wanted to spend money on photography & video for our wedding because I also have a terrible memory. I didn't care about flowers, so we pretty much didn't do that. Don't feel pressure to spend on things you don't want because "it's your special day" imo, just think about what is important to u guys
### asked by happyhaps

Indeed, I will do that, danke. I suppose I'm pushing back against the normalisation of heaps expensive stuff as what is important to people.
___
Answered on July 12, 2015 07:31:42 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129410610750



## YOU FELL ASLEEP A FEW TIMES? HOW F@CKING LONG DO I HAVE TO BE IN THERE? Sorry for shouting m80.
### asked by Anon

Please no more shouting.I dunno, my one was like 45 minutes or something. You get $50 for essentially doing nothing though, so I wouldn't complain too much.
___
Answered on July 12, 2015 05:01:15 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129409796926



## I've read that those who elope have lesser divorce rates! Might want to keep that in mind. Also, I know a couple who didn't even have a photographer - they relied on friends to take shots and got some great ones.
### asked by Anon

Interesting! I'd probably have to elope with some friends though, kinda defeating the purpose...
Yeah, we know a guy who had to have two weddings (one in each partner's home country) and they did that for one, and got a pro for the other. He said that the friend-cam one was OK, but your friends are also interested in the wedding, and so forget to take photos, or just simply suck, so he thought the pro was worth it. He was also the guy who said the $10,000 was normal :\
___
Answered on July 12, 2015 05:00:21 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129409734974



## As a photographer and a cheapskate, I can understand both "holy crap $3500 is a lot!" and "when you think about what you get, it's fairly reasonable."
### asked by happyhaps

Hmm.. maybe. For me, I think it's a mix of a few things: 1) photography is (unfairly) devalued, because I can just take a pic on my phone, look, it's easy; 2) Even if I did decide that $3500 was OK, it's ONLY for the photographer! What about the food, flowers, clothes, etc. - if every person in this chain charges me proportionately this much, I'm boned! 3) I don't like the argument for "but it's your big day, you only get it once, you may as well spend big" - it is an easily slippery slope, and every asshole in the industry is pushing it bigtime. In saying that, photography is probably the one most worth spending on, since I am forgetful. But yeah, it triggers me with annoyance to hear that argument.That said, I hope everyone assumed that I don't dislike it when people have big weddings - in fact, if I am invited, I thoroughly enjoy them, because it's a sick party. Alas, I have only been to two weddings in my adult life :(
___
Answered on July 12, 2015 04:58:15 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129409674046



## No way, $10,000 for a wedding photographer is pretty crazy, man. I think ours was $3500-ish, which is expensive, but much more normal. I think our whole wedding was under $15,000. The wedding industry is nuts though
### asked by happyhaps

Yeah, I mean, I get it, if you pretty much only work weekends, you need to cover yourself. But even then, $3500! Jeez. I gotta feeling Kayla's and my wedding is gonna be pretty chill in that regard - we both feel pretty similarly about the whole thing.
___
Answered on July 12, 2015 04:46:46 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129409557822



## Omg, you've had a MRI? Can you tell me what it was like pls. I'm having one on Friday - partaking in a study and getting paid a fifty for it but I'm a bit scared. Should I be?
### asked by Anon

Yeah, I did it for a study too. I found it very relaxing actually! At first I was a bit nervous, because the tube is pretty small. But yeah, they gave me some headphones with radio on, and just followed their instructions. It was quite warm too, so I did fall asleep a few times. Before I went, people were saying to expect the sound of Jack hammers near your head - this was mega untrue, and I don't know why they said it.
___
Answered on July 12, 2015 04:44:05 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129409364286



## How did you meet Kayla? Are you looking forward to getting married and living what is sure to be a long life of wedded bliss?
### asked by Anon

We met at uni - she was a PhD student, and I was an Honours student. We knew each other for about a year before we got together.Yeah, I am looking forward to it, but I imagine it won't be too massively different to our current living conditions, which are relatively blissful in all relationship aspects.I look forward to the wedding party, but I am not looking forward to organising said party. It seems like a massive and expensive pain in the butt. Do you know people say "Oh yeah man, $10,000 for a wedding photographer is normal and recommended"? I refuse to accept that, and all the other expensive stuff that is "required" to get married. Even buying the engagement ring required many incredulus looks at jewelers when they were saying "Oh yeah, you have to remember to have space for the extra stones you'll add to the ring when you get married, and when you have a kid, etc... sir, don't laugh - we're a business that helps you think about these important long-term things!" - get out
___
Answered on July 12, 2015 04:02:15 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/129275400766



## Don't be fooled by the woks that I got - still need peppers, onions & some brocc. Can you cook? What's yr fave cuisine?
### asked by Anon

I see the Block theme is continued not just in my head - others are affected too.
Yep, I cook real good. I do a lot of it as well, and typically enjoy it - though it can be stressful when timing problems come up. Hard to say with cuisine - I'm pretty flexible. A good curry will always be welcome.
___
Answered on July 12, 2015 03:56:59 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129342485310



## I did mean a lip ring - looks like I am the degenerate, sorry. Please substitute lip for nose ty.
### asked by Anon

Indeed."What's with it" - it's just a piercing, relax, it ain't no thang. I am glad you like it.
I very rarely notice it now, I have had it for many years. It takes literally zero maintenance, so unless someone brings it up, I typically forget it's there. I unconsciously play with it with my teeth all the time, so I look kinda dopey (see attached image).
Yeah, like I said, zero maintenance. I haven't taken it out for at least 2 years (I think when I had an MRI), and it doesn't seem to need cleaning or anything. It is a massive pain in the ass to take out, because you have to somehow spread the ring apart - I typically use a closed pair of scissors, and then open them with the tip inside the ring. Very annoying.
___
Answered on July 12, 2015 03:54:54 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129346437950



## Are there any board games you like to play ie Monolopy/Scrabble? If yes, do you play to win/get very competitive? I do.
### asked by Anon

I enjoy playing Settlers of Catan (now re-branded as Catan) and the Battlestar Galactica board game. Both can take multiple hours, but are generally very fun and intense. Good excuses to hang out with some buds, have a few drinks, nice food, and banter.
___
Answered on July 12, 2015 03:49:04 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129377114686



## Why did you like having black hair so much? What was is like going back to your natural colour after so long? Did a few ppl get weirded out?
### asked by Anon

Because it was awesome. It was a hassle though, dealing with it all throughout that period. It's a fairly slow process to go back to your natural colour, so no one was too surprised.
___
Answered on July 23, 2015 17:21:27 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129759059774



## Don't get a tatt, you can't afford it - esp with an upcoming $30,000 wedding to pay for.
### asked by Anon

😲Dont u worry m8
___
Answered on July 23, 2015 17:19:27 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/129759076414



## The one I got was a cool hundo, not sure if I mentioned that or not. That's really the minimum though, most you'd expect $150 or up, depending on size/detail.

Sorry if you already knew this, just thought I'd chime in.
### asked by Soul James

Yeah, I would expect it to be pretty pricey. Good to know the details though.
___
Answered on July 22, 2015 15:23:42 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129734402110



## Do you really want to get a tatt?
### asked by Anon

Meh, maybe, if it's good. I gots no money though.
___
Answered on July 22, 2015 13:40:55 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129731423294



## Is yr handwriting legible? Just wanted to check to see if you could ever become a doctor.
### asked by Anon

No, my handwriting sucks. I only write by hand now when I have to do some maths, or marking. In both cases, it starts out neatish, and then quickly degrades to crap as my hand gets tired.
___
Answered on July 22, 2015 08:25:39 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129727089470



## Have you ever dyed your hair?
### asked by Anon

Yeah, from grade 11 to like 3rd year uni, I had black hair. It was awesome.
___
Answered on July 22, 2015 08:24:21 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129727097662



## Isacc Asimov once said, "Violence is the last refuge of the incompetent". Do you agree?
### asked by Anon

I think it depends on the context. Perhaps in high school, or some dude at a pub - yeah, I agree. But looking at the rioting in America from the black deaths from police, it's more like the last refuge of the disempowered. The violence isn't good, but not because of the people doing it, but because it has come to the point where it's the only option left. Everyone saying that non-violent protests are the way to go - sure yeah, but if no one gives a shit about it, if no one is inconvenienced or frightened by your protest, then it's not much of a protest. If people don't want violent protests, maybe they should listen to all the non-violent stuff that is said before that point.
So in summary, to say violence is just for dopes is very simplistic, and notably the people saying it are often the people who would be protested against (e.g. I'm sure Asimov was probably not oppressed).
___
Answered on July 22, 2015 08:23:43 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129727076158



## Do you feel you have a natural talent for woodwork?
### asked by Anon

Ummm.... it's hard to say. I don't think I have done enough to say if I'm great at it. I would say that a large part about being at least OK at woodwork is being patient. I spent like 3 to 4 hours fitting some hinges to my box, and I enjoyed it a lot. The other dude in my class did it in an hour, and just sanded away the mistakes. I think the difference between us is that he wants a box, and I want the journey to the box.When I started the box making course, I was super pleased to be able to use all these power tools I've seen all the YouTubers use, and to build mad stuff. But as soon as I had cut my dovetail joints for the box on the router, I was left feeling unsatisfied. It was too easy. People had for hundreds of years had to cut them by hand, and I just ignored all that and pushed some wood back and forth about 10 times. All the craftsmanship seemed lost.So now I'm on a massive kick in the opposite direction. As mentioned, I bought a set of chisels, and having been trying all manner of hand-cut joinery. It's quite fun and satisfying, though the joints are pretty rough when compared to machine cut. But I managed to get a lot better quite quickly, so it's definitely doable. I also bought a book on timber framing (building a house out of wood, ye olde style), and I devoured its contents very quickly.So in summary: I'll let you know. I'm enjoying it so far though.
___
Answered on July 21, 2015 14:00:53 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129699200318



## Did your Dad enjoy being a youth care worker? Did he talk much about it?
### asked by Anon

He seemed to enjoy it well enough. When I was young, before school, he used to take me with him. I remember the kids were quite nice to me, and one gave me a little transformers toy that I still have. It's also where we adopted my cat Rosie from, who lived for about 20 years from that point. At the refuge (where the teens lived) they called her Lucky, because she was black. He does talk about it, when prompted, without hesitation. It is/was a large part of his career (social services and child protection), so he can say quite a bit about it.
___
Answered on July 21, 2015 13:33:00 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129580717374



## Do you follow any sport?
### asked by Anon

No, not really. I'll watch some if I'm with pals -  state of origin, cricket, and so on. Ice hockey is pretty good too.
___
Answered on July 21, 2015 11:35:18 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129699067710



## Did you get any sex education when at school? If yes, was it useful?
### asked by Anon

Yeah, we got some in year 6 and in year.. 8 maybe? Perhaps once more in year 10.I don't really remember much, just general stuff about condoms and birth control, STIs and such. Looking back, I don't think I've ever been 'confused' (as per all the sex ed examples: "what's happening to my body?!"), so I guess the sex ed + whatever I found on the internet was effective enough. In regard to gender equality, fluidity, sexuality, etc., it really could have been better.
___
Answered on July 20, 2015 09:20:21 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129660678974



## You talked about how dangerous riding a motorbike can be, did you feel comfortable having a passenger or did you mainly ride on yr own?
### asked by Anon

Yeah, it was ok having a pillion passenger. I didn't think about safety much more than normal anyway. Kayla wasn't a huge fan, because it is scary, so it didn't happen very often. It did make the bike handle very differently, so I was careful with that. Having a passenger also makes your arms very tired, because every time you brake, the pillion slides forward. To stop yourself also sliding or leaning forward, you have to brace with your arms.
___
Answered on July 20, 2015 09:12:25 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129630876734



## Would you want to be internet famous?
### asked by Anon

Yeah, but I suppose mainly because it would mean that I was pretty good at something. Fame from a nice open source project or a woodworking thing would probably mean I could keep doing those things as main deals in my life.
___
Answered on July 20, 2015 09:08:13 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129663734334



## Pt2....pretty tired. The tube looks scary but once yr in there it's ok. It felt like I was in there for ages MAINLY BC I WAS! It was supposed to be 45mins but ended up being 1hr & 20mins bc there were problems. Anyway, it wasn't so bad! Having another in month :)
### asked by Anon

Yeah, after a while I found the enclosed space quite comforting. 1.3 hours is a long time though.
___
Answered on July 17, 2015 08:35:35 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129578999358



## Hey Olympus! I'M ALIVE! I'M ALIVE! And fifty bucks richer! Got to watch a nature doco during the MRI too - not fussed on the salamander but there was a majestic Canadian bear feasting on some fish. Eat my pretty! Had to do some excercises as well - identifying shapes etc & I was getting.....pt1
### asked by Anon

Good to hear. I only had radio in mine. I also had to some breath holding exercises, so they could get a more detailed one.
___
Answered on July 17, 2015 08:34:24 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129578965566



## Do you ever eat breakfast cereal for dinner or a midnight snack? If yes, which ones are best for these rebellious acts?
### asked by Anon

I did so, last night, with some Bran Flakes, which were surprisingly tasty.One of my parents (I think my Dad) isn't a big fan of doing this, because when he was a youth care worker with teenagers it was all they ever did. I think he wanted them to put in a bit more effort into looking after themselves, etc., so it would be annoying if they were like 'nah, cocopops'.
___
Answered on July 17, 2015 08:33:17 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/129551530558



## Were you ever a "nice guy"? https://www.reddit.com/r/niceguys/
### asked by Anon

I think in early high-school, I had some of the same thoughts - "I'm nice, why no gurls?", and putting girls who I had never talked to on pedestals. I never got into a creepy weird rage, as the reddit nice-guy examples seem to show.I think when I started to have more female friends that I saw daily (mostly from year 10 and up, embarrassingly..) helped a lot, and it all sort of transitioned from that point.Not a good time.
___
Answered on July 16, 2015 09:53:42 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129549039934



## I am having my MRI tomorrow so I'll let you know how it goes even though yr probs not interested. Pretty excited about the fifty tbh. :)
### asked by Anon

Good luck. Don't spend it all at once.
___
Answered on July 16, 2015 06:50:03 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129546264126



## Do you like using the self check-out at supermarkets?
### asked by Anon

I want to, but the constantly mess up when you try to use your own bags. So whichever is shorter queue time.
___
Answered on July 16, 2015 06:13:45 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129546165566



## I'm surprised motorbike seats are hard on yo ass tbh, they look real comfy?
### asked by Anon

Depends on the bike, but essentially anything that is not a cruiser will destroy you after more than an hour.
___
Answered on July 16, 2015 06:12:43 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129539947326



## Are you in England right now?
### asked by Anon

I am
___
Answered on July 15, 2015 11:27:15 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129517902398



## Do you remember life without technology? What was it like?
### asked by Anon

I remember trying to use the internet with dial-up. I accidentally modified the phone number, so it called some dudes house. He picked up the phone (the modem played all this through it's speaker), and said hello, and then was assaulted by modem noises. He seemed very confused and angry at this (modems were quite new at the time), and I didn't know how to hang up, so he just had to take it until he hung up about a minute later.Very confronting.
___
Answered on July 15, 2015 10:13:25 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/129517025086



## You used to have a motorbike? Tell me more pls? When you got one - pros/cons etc.
### asked by Anon

I did! It was a Honda VTR250, and I had it for about 5 years (I sold it earlier this year).When I got it, it was pretty great. They are very fun to ride, and great for getting around (parking and such). They are also verrryyyy dangerous. Sometimes I would be riding along, and then realise that if one of these suckers around me does something stupid, I will be dead - not just hurt, but deaaddd.And those suckers DO do stupid stuff all the time*. It would be at least once a week someone would try to merge into me, or something similar. It really makes you build up your "road craft", which is what the training people call having awareness of other cars on the road at all times.Addressing the * - so when I first started riding, I had this issue - like once a week, almost death. But after a few years of riding, my attitude went from "this is fun, AND I get from A to B" to just "I just need a mode of transport to get from A to B" - and when that happened, my near death rate dropped significantly - to something like once every six months. I was really surprised by this, because even in attitude 1, I thought I was a pretty cautious rider, and really aware of the road. But clearly not - in attitude 2 (and still to this day), I just don't sweat any of the little stuff anymore. Make room for people, hang back (no need to drive close behind people), all that stuff. Makes your life much easier and apparently safer.
Overall though, I really loved my bike. It has many downsides - you get wet and cold when it rains, they're not super comfortable on your ass, they are very loud (both engine and wind noise), it's hard to carry groceries, you have to carry around all your gear - but even though all that, it was great. If someone asked me if they should buy one, I would say yes - so long as they are older than ~22. You can die sooo easily, and thinking back to all the stupid shit I would do in my car below that age... yikes.
___
Answered on July 15, 2015 10:11:29 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129513220414



## I was sorry to read about yr bad experience with purchasing a bike but I will learn from it so thanks. I was gonna just go to Kmart & get one but I will now rethink this!
### asked by Anon

Yeah, I heard Kmart ones are pretty rough. Bikes are going through some pretty major stresses, so you can see why you might need to pay a bit extra.
___
Answered on July 15, 2015 09:59:07 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129513188414



## Can we have an update on yr latest woodwork project? I need to live vicariously through you until I get myself to a Men's Shed to do my own manly stuff. Ty.
### asked by Anon

My fancy box is now structurally complete - I fitted the hinges last time I was there, which took about 4 hours total! Well worth it though.I also started making a set of shelves at home, which is essentially just a wooden frame with shelf arms sticking out. The frame uses lap joints in the corners, which I cut by hand with a tenon saw and a chisel. They are definitely not the best joints, but I got much better from joint 1 to 4. My plan is to let the shelves into the wood frame by cutting mortises. Should be interesting!Whoever you are, anon, if you need help, let me know.
___
Answered on July 15, 2015 09:57:28 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/129513288254



## Yeah, I was referencing yr question to Soul & have also asked him a q on the same topic. Thanks for the link, will check it out. We weren't taught anything about voting/Parliament at my school but I remember taking my 1st vote very srsly.
### asked by Anon

I was more serious than my friends, who essentially did donkey vote by voting for the Shooters Party or the Sex Party, purely because of the name. But I didn't really know what I was doing, as per most things when I was 18.
___
Answered on August 07, 2015 08:37:05 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130234182974



## When you started to vote, presumably at 18, did you feel you had a good understanding of how the voting process worked?
### asked by Anon

No, not really. I just kinda followed my parents' Labor leanings, and voted for them. I think I thought I knew what I was doing, but in hindsight I really did not. I understood preferencing was a way to put the Libs and Christian Demos at the end, but despite that, I voted above the line.After a *bit* of reading, I said I was following the Sex Party, which luckily I never ended up voting for. It sounds like a donkey vote, but on face-value they are about internet freedoms, drug-reforms, etc., etc., things which I agree with. I did not realise they are actually libertarians, who preference other small-government parties including the shooters and fishers party, the motoring party, and some other weird stuff. Now I am a Greens voter, and am somewhat more confident in my decision as such.
These videos: https://www.youtube.com/playlist?list=PL7679C7ACE93A5638 are a nice way of getting an understanding of how voting systems themselves can be biased, and what it actually means when you put down preferences.
I assume you may also be referencing my question to Soul about what should be taught in schools - I do remember learning about the parliaments and general stuff, but it wasn't practically useful when it came to actually voting (except that I had a sense of PRIDE for voting properly!). I'm sure more thorough education of what is up (for example, combatting the common "I'm in a safe seat anyway" trope) would be good - but really, it's not super hard to go to the Greens website and look at their table of policies, and then vote for them because they good.
___
Answered on August 07, 2015 08:22:43 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130233874750



## What do you mean you've been away for the past 3 weeks? Where are you? Get back here!
### asked by Anon

I am back now. I was in England.
This is a weird question to get anonymously.
___
Answered on August 07, 2015 04:14:08 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130230163262



## If I thought it was appropriate to call you out for something I would, but unlike some people (me) you don't go around just asserting things, so it isn't ever necessary.
### asked by Soul James

I herd u liek mudkips
___
Answered on August 04, 2015 08:10:29 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130137859902



## lol "look at u asking questions, whatre you trying to prove huh" XD
I like to know who's asking so I know whether to yell at them, in part. I'm not rude to you because I know you, I know you're not trying to start shit with me. I'd love to give everyone that benefit of the doubt, but once bitten...
### asked by Soul James

Indeed.Though just because I know you should not preclude yelling if necessary (not that I think it has been so far).
___
Answered on August 04, 2015 07:39:07 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130137258302



## I just opened ask.fm on the computer for the first time, instead of my phone, and your background scared me shitless. I hope you're happy.
### asked by Anon

___
Answered on August 04, 2015 07:37:05 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130136980798



## Ok, I just worry that knowing its me who's asking the question may prejudice the answer for good or bad ie they might be nicer/reluctant to tell me where to go if they know its me. You can make ask.fm only let non anon ppl ask qs if anon ones really bothered you.
### asked by Anon

Yeah, I know what you mean. If I start to think Soul's answers are not representative of what he thinks, because it's me asking, I will revert back to anon questions. So far seems OK.
I agree anoning would be better for personal questions, but most of the stuff I ask is not really about his personal life.
___
Answered on August 04, 2015 05:36:28 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130135643454



## Why do you put yr name to the questions you ask? Is it a way of showing off ie look at the great deep questions I ask? No judgement, bit jelly, my questions are too shite to put my name to :( I'm trying but.
### asked by Anon

Soul said he preferred to know who it is. I still ask anon questions to other people.
I agree that it is intimidating somewhat, especially when you see some of the answers he gives to some anon people. But for whatever reason that he likes being asked presumably basic stuff, I don't think he *wants* to call an asker a fuckwit, unless they are deliberately riling him. As you can see in my public questions to him, he hasn't said anything offensive to me, even if the question itself has problems.
It's not a case of showing off - I typically ask questions to find out the answers or to get another opinion on something I think or have come across. I'm not trying to think of questions for Soul per se, but more that I now have a place and format to ask them (good job, ask.fm). My questions get long though, because it's not super easy to ask follow-up questions, or have a reply that would prompt further discussion. Maybe I should talk to him in person sometime :P. This is all true of the other people on Ask.fm who I ask questions of. I suppose by this reasoning, I should reveal my identity to all people on Ask.fm, but meh, whatever.
Anyway, I would recommend giving it a go, and see how you feel.
___
Answered on August 04, 2015 05:25:24 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130135248702



## Do you like Subway? There's something weird & stinky about the bread imo. You with me?
### asked by Anon

No, I like it well enough.
___
Answered on August 03, 2015 12:08:55 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/130108817982



## Haha at thinking "a purely scientific culture would be ideal" at one stage. I've been there! Were you raised with a religion at all?
### asked by Anon

No, quite the opposite. I went to scripture in primary school, but that was about it.It wasn't necessarily just religion though - as mentioned, I thought any non-sciencey task wasn't really worth doing: sports, arts, and so on. I'm not really sure how I managed to hold this in my head AND also my strong love of music at the time. Just goes to show I wasn't nearly as clever as I thought I was.
___
Answered on August 03, 2015 07:26:05 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/130102417726



## Wot u got against The Arts, m8? Why do most want to attack that first? What about all the $$ our sporting ppl get so they can go to The Olympics? Explain that to a homeless person.
### asked by Anon

Haha, true. I don't hate The Arts, I like them a lot, and think they have great value. In terms of visual arts, my high-school/undergrad-uni opinions of "pfft, I could do that", that it's boring, or that a purely scientific culture would be ideal, have thankfully been left behind. I had similar opinions to the Olympics and other sporting things - however, I have come to appreciate them to some extent. I would veto the Olympics over The Arts, but they all have an important role in our culture. Olympics does at least promote healthy stuff, and in the same way someone can paint something amazing, the feats Olympians can achieve are quite incredible.I'm sure the homeless person would be unimpressed with this.
___
Answered on August 02, 2015 08:50:34 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/130075222078



## How much is 'the same', m8?
### asked by Anon

Umm, without testing it:- bench: 78 kg- squat: 70 kg- dead lift: 80 kg- bicep curl: 17.5 kg- tricep pull down: 35 kg (both arms) - lat pulldowns : 90 kg
___
Answered on August 02, 2015 05:22:18 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130072542782



## How much are you able to lift now?
### asked by Anon

About the same, I imagine, because I have been away for the past 3 weeks.
___
Answered on August 02, 2015 03:30:56 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130047144510



## What were some of the best things about the 90's? Buffy's gotta be up there imo.
### asked by Anon

I just watched all of buffy, having never done so in the 90s. I watched Charmed, and that was alright.
___
Answered on July 31, 2015 08:44:28 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/130008071486



## Just watched the How It's Made-Copper vid! I had no f@cking idea! Thanks, and yes, I can see why yr not keen to work in oil, gas, or mining. Looking forward to checking out some of the other How It's Made vids too.
### asked by Anon

Yeah, it's pretty confronting to see how it's done. Often one's quality (like iron ore, for example) is measured in grams per tonne - so to get a few hundred grams of the stuff you want, you gotta process a tonne of rock. Granted a tonne of rock is only about 1/3 of a cubic metre, but still... Pretty crazy. Also, yes, I appreciate modern society needs copper and stuff to function, but that doesn't mean I have to like it, nor think that huge green lakes of heavy metal water is wise. Also, I do love a bit of how it's made.
___
Answered on July 31, 2015 08:43:45 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129976956990



## Thanks for answering my q re your dad's work. I have an interest in child protection myself especially as I have relatives involved in that kind of work. The stories can be pretty heart breaking tho. I'm following the RC quite closely & just when I think I've heard it all....more horror.
### asked by Anon

No probs. Both my parents were in child protection for large parts of their careers (mum was a social worker in general for over 50 years). As a child I found their dinner table conversation boring, but now it's just the opposite. It's kinda hard to fathom the crap they have had to deal with, and even harder to imagine what the kids think. Anyway, if you want to know about my mum and dad's work, go look up the NEWPIN social program. They brought it to Australia, did research to show with evidence that it works, and have expanded it across NSW with great success. It's about "breaking the cycle of abuse" or more colloquially, how not to become your own abusive parents.
___
Answered on July 29, 2015 10:16:30 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/129725358398



## Why don't you want to work in oil, gas, or mining?
### asked by Anon

- check out between 1:20 to 2:30 in particular.This is one reason.
___
Answered on July 28, 2015 20:02:31 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129916701758



## What do you hope to make when you are more experienced in woodwork? I wouldn't mind a nice coffee table if yr taking orders tbh. Are you good at picking out good quality wood?
### asked by Anon

I don't really know. I would rather build something for the experience. I'm sure I'd be proud of it, but whatever it is, I don't mind.
___
Answered on July 27, 2015 20:24:55 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129759121726



## Saw your q about halal slaughter ie it requires the animal to be conscious. I've also read that halal slaughter in Australia still requires the animal to be stunned first? I'd like to know about the certification process in Australia tbh.
### asked by Anon

No idea. I would be very surprised if Australia adopted the same policy, so I guess it's not super important.From my vague understanding of general meat production, animals are essentially tazed to unconsciousness, and then killed.
___
Answered on July 27, 2015 20:21:45 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129882377278



## Can you see yourself living in Canada? Would you like to for a few years?
### asked by Anon

Yep, and yep. I like the cold, and winter sports (skiing, ice skating)
___
Answered on July 26, 2015 12:22:18 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129790453566



## Opals are a bit ugly tho. Weird how this gem is so beautiful EXCEPT for when it's made into jewellery? Then it loses it's magic? And put that middle finger away pls!
### asked by Anon

Well, the middle finger came out in response to the Joe Hockey comment. Yeah, they ain't the best jewellery, not only because they are relatively soft and can be scratched easily. They are very beautiful in place though.
___
Answered on July 25, 2015 14:51:04 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129772384062



## What is your fave pasta shape? Do you like penne, spirali, shells, farfalle, spaghetti, fettuccine, or something more exotic with your sauce?
### asked by Anon

Bows? What's their Italian name?
___
Answered on July 25, 2015 10:10:52 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129819026238



## Can you make fart noises using yr armpit and hand? This is a skill I once had but have now lost.
### asked by Anon

No, never, unfortunately.
___
Answered on July 25, 2015 10:09:02 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/129825221694



## You need to go near Africa or to the pacific ocean to find diamonds? I thought Australia was the worlds biggest supplier of diamonds? Well it was when I used to watch Oprah between studying for exams - that's where I picked up this info. I remember being surprised/intrigued. Is it bullshit?
### asked by Anon

No, you are right, I was being slightly facetious in my answer. Lots of diamonds do come from Australia (and other places). But the name of diamond bearing rock is Kimberlite, named after a town in South Africa, and the processes required to produce kimberlites now are currently under Africa and the Pacific.
Essentially the deal is, diamonds are only stable at like 200 km depth - we're talkin around 1300 C, and probably like 9 gigapascals of pressure - which is a lot. If they were to come up to the surface through some normal means (typically continental rifting, which would take between 3 to 20 million years), they would become unstable on the way up, and retro-grade back to some other boring form of carbon. You need those bad-boys to come up quick. That's where mantle plumes come in! There is a big one underneath Africa and the Pacific. They make things hotter, and therefore easier to flow, so you can move the diamonds up to the surface much quicker. I read something saying that they can move at speeds of >1 metre per second at some points - which geologically is like the speed of light.
Anyway, as the continents move around, plumes hit underneath them at various times, forming kimerlites in various places. Unless something happens to the kimberlites (like they get buried in a mountain range), then they will just hang out until we find them - hence why you find them all around the Earth.
It is obviously much more complex than this, and I am really not an expert, so take this with a grain of salt!
___
Answered on July 24, 2015 17:55:23 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129772706878



## Why have you gots no money tho? Just get a good job that pays good money, like Hockey said. It's that easy. Can't you use yr rock knowledge to go find diamonds or something? Maybe even make some counterfeit ones?
### asked by Anon

We'll start with a middle finger. Moving on, kimberlites (the rocks that host diamonds) are of course difficult to find, and even the processes that would lead to them are mostly near Africa and in the pacific ocean. Synthetic diamonds require a lot of expensive machinery. Synthetic opal would be much more feasible - theoretically you only need clay and some acid. Also, I don't want to work in oil, gas, or mining, so no money for me.
___
Answered on July 23, 2015 17:28:12 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/129750586686



## Omg, that friend answer was so lame I felt bad for laughing.
### asked by Anon

Haha, glad you enjoyed it. No offence to the asker, but it's not exactly the sort of question to pull an amazing answer out of me.
___
Answered on August 20, 2015 08:38:58 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130628991038



## In three words, how would you describe your best friend?
### asked by Anon

A good friend
___
Answered on August 20, 2015 07:22:46 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130627526462



## Are smart phones, social media and the like really making ppl atomised, distant, anti-social, etc? Or is it this technology expanding within the current -material conditions-?
### asked by Anon

Clearly, I'm really not an expert - but despite that I would say probably not. It is kinda rude if you are talking to someone and they start using their phone, but taken as a whole, I would say the internet has generally allowed people to connect to more people overall, even if they don't necessarily meet them face to face.
It really boils down to how much do you want to impose your style on others (or I suppose if you think all the tech is addictive in a way people can't control). Do you want to force people to be face to face social with you, when they really don't want that? Or would you rather let them do their thing, and you go talk to people who also want to socialise?
Coming back to my brackets, given that children are raised in this environment, is it fair to let them perhaps be warped by easy-win online social interactions? Answer: I dunno, everything in moderation I guess.
Social issues are hard, and so is clearly writing out my ideas and arguments apparently...
___
Answered on August 19, 2015 08:52:13 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130596969534



## who told u about my "hippy army to get up in monds' grill asap"?? do we have a mole? fucking stoners smh
### asked by Anon

Why you raging on hippies?
___
Answered on August 19, 2015 00:09:30 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130576052542



## Hey Olympus, I'm holding another cool fifty in my hands after my 2nd, and last, MRI for the trial! They told me they'd keep me up-to-date with the trial results - probably two years from now. Did you get to see your trial results? Anything interesting?
### asked by Anon

I asked the researcher for my mri data, so I could visualise it on my computer, and she also said she would let me know when the paper came out. Neither has happened, and it was a while ago, so I've kinda lost hope. My one was about how much fish oil vs olive oil helps weight loss, and specifically fat in the liver. It was sponsored by Blackwell's (or whatever that natural pill company is), so I imagine the result probably pointed to one of their products.
___
Answered on August 19, 2015 00:09:05 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130567977534



## Can you relate to hippies at all tho? Are you a pot head? Do you like to reek of patchouli or sandalwood?
### asked by Anon

Hippies are just people who want to chill out. So long as they don't form some sort of hippie-army to get up in my grill, let 'em be, I reckon.
___
Answered on August 17, 2015 05:17:30 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130535508798



## What is your opinion of hippies?
### asked by Anon

I dunno, seem fine...?
___
Answered on August 17, 2015 03:29:36 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130534495038



## Were there any comments made to you during yr school yrs by a teacher that you still remember? eg My yr11 History teacher - "You don't know much, do you?"
### asked by Anon

In year 6 we had some basic sex ed, and my teacher told me to chill because I was being childish. It was the first time it had happened, so I was relatively taken aback. He wasn't overly angry, just like "Hey! Keep it down, back there, OlympusMonds!".
I'm sorry Mr B., you were a good teach.
___
Answered on August 14, 2015 05:46:54 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130449215806



## Ok, that makes more sense but I guess it depends on the field too? ie maths is pretty cut and dry as opposed to parenting. Also, I like the questions you ask and reading the answers to them too.
### asked by Anon

I suppose somewhat - maths would be an extreme example, I imagine, since it is typically pure logic. But from my experience in science, it's hardly cut and dry. There is a lot of room to manoeuvre in what you can say, and a lot of stuff published (in fancy journals!) is simply crap. A lot of politics, etc., not the idealised meritocracy that is claimed.
I suppose it SEEMS more cut and dry because science has physical evidence, which in a lot of cases (not all - see above!) is hard to argue with; whereas social stuff either has no evidence (in that, it's not really appropriate..?) or reliable evidence is very difficult to get, for example, surveys.
Coming back to what I said earlier too, most people don't know that their black granite bench tops are essentially chemically the polar opposite to actual granite (it's probably gabbro if it's black), so when I say stuff about geology, they have no frame of reference to compare against. So if they were to disagree, it would essentially be purely because they're a butt-face, not because they somehow know better. In social issues, this is not the case - people have been raising babies for millennia, so some "expert" baby doctor can say xyz, but now you can contextualise what they've said into your experience. If it doesn't gel well, you could potentially say that they're an idiot and you don't like them. You may even be right, but my main point was that there is a disparity between how experts across different fields are treated, and I don't think it's necessarily fair.
ALSO, my other point was that people should ask more questions of experts, regardless of their field, rather than instantaneous reaction or acceptance.
___
Answered on August 14, 2015 04:43:03 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/130449082174



## You seem to want to take the opinions of experts without question? Don't get me wrong, I like to consult experts but it's not like a lot of them even agree w each other.
### asked by Anon

No, no, not at all. To properly clarify the context here, I'm talking about when I, OlympusMonds, ask an expert a question, with my own brain and stuff. Not just reading stuff in the newspaper. Just to be clear.
Anyway, when I ask an expert a question, I don't immediately say "No, you are not correct, I once saw a rock on a hill, and it wasn't like what you said". I take what they say with the idea that they *probably* know what they are talking about, see how it gels with my lived experiences or knowledge, think about it. If something isn't clear, or I'm skeptical of something they said, (if appropriate) I will ask follow up questions until it is clear that they were right or they just made it up.
I guess my mini-rant was more about people's treatment of experts, not because the experts are offended or whatever, but because it's just a waste of time for all involved. If someone immediately rebukes some expert's opinion, the expert has gained nothing, and neither has the asker! The expert is a deep well of knowledge about a particular topic - you may as well try to explore it as much as possible before calling them an idiot.
Asking different experts on the same topic is even better, precisely because they do disagree with each other! In the past, I have asked Soul to publish his works more widely (this was pre Ask.fm), purely so I can see some other expert come through and disagree heartily with his conclusions. Even if the new expert ends up being wrong (in my opinion), it would be super interesting, and most likely extract even more interesting stuff out of Soul. However, being called out by another expert in your field can be very confronting (especially if they're an asshole), so you can see why people wouldn't want to invite this.
Obviously you shouldn't explore the depths of someone's expertise by knocking on academics doors in the middle of the day - but given we're on Ask.fm, and Soul can refuse to answer question with no retribution, I'm quite happy to ask lots of questions, to try to understand more and more about the things I ask.
___
Answered on August 14, 2015 04:19:05 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130448608574



## I can't believe you are victim blaming that poor tree! What a scandal!
### asked by Anon

Yeah, or perhaps you are victim blaming Neb and I! You don't know what that tree did!
But we do...
___
Answered on August 14, 2015 02:57:28 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/130447388734



## Hey Olympus, can you verify Neb's tree mutilation story pls? Not that I don't believe him but what did YOU see? Do you think he deserved the detention? Can trees now feel safe around this beast of a man we call Nebby?
### asked by Anon

The tree had it comin'
___
Answered on August 14, 2015 01:11:38 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130421195838



## Expertise is socially determined - I don't act like an expert, or treat my opinions with the responsibility of an expert, because I don't think of myself as one, because no one treats me as one. It creates a vacuum in social knowledge, where "truth" is impossible, because expertise is impossible.
### asked by Soul James

I see what your saying, but: no u r
___
Answered on August 13, 2015 21:20:52 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130427264830



## Your bulging muscles? Pics or it didnt happen. Show me ya guns!
### asked by Anon

Needless to show, they are like 43 cm in circumference. http://www.menshealth.com/fitness/bigger-biceps says the average is 33.2 cm for someone my age.I'm juiced as.
___
Answered on August 13, 2015 05:10:22 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130418196542



## You must really respect Soul's opinion?
### asked by Anon

Yeah, I suppose so. I don't necessarily agree with him all the time, but he typically documents his ideas very well, so at the very least I understand where he is coming from.He is also essentially an expert in social stuff, so if I have a question he is the natural person to ask. He is also very active on ask.fm, so that helps. If someone wanted to know about some aspect geodynamics or geology, I would presume they would ask me in the same way.
This is a tangent, but it seems like with social stuff (in like what Soul says, or in child protection, like my parents), people ask the experts, but then readily disagree with their answers, either because they don't like it, or it just doesn't sit well. These people are EXPERTS.
If someone asked me a question about geodynamics, it would definitely stick out as very odd if the non-expert asker did the same thing, and was just like "nah, that doesn't seem right to me".
I suppose since we live in the social world, everyone can have an opinion, but that expertise is simply ignored because of this - whereas my geology expertise is taken as gospel, even though there is a ton of uncertain stuff I could (and am) saying. Just because it's science, people don't seem to ask follow-up questions, they just take what I say happily.
My parents suffered from this, where people would ask for parenting advice from them (with +70 years of professional and personal child raising expertise), and they would just ignore it, and do whatever. Fair enough, I guess, but like... why bother asking? These people are EXPERTS!
___
Answered on August 13, 2015 04:15:24 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/130417108030



## What parts of yr body are you happy with? Any parts yr not too keen on?
### asked by Anon

My bulging muscles. I would prefer to be taller, but I'm pretty chilled on body image now.
___
Answered on August 13, 2015 03:51:38 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130354629950



## You're stuck on an island. How would you spend your time?
### asked by Anon

Looking for Desmond.
___
Answered on August 13, 2015 03:51:03 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130417057086



## What is your favorite thing about ask.fm?
### asked by Anon

Asking questions. Not many of the questions to me have been too difficult, so I spend most of my time on here trying to succinctly ask complicated things to Soul James.(no offense to all my askers)
___
Answered on August 13, 2015 03:50:48 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130356048958



## Do you spend money on good trainers? Do you really need to if you're just doing weights? What about gloves? You got fancy ones?
### asked by Anon

I have a pair of trainers that were about $100, which replaced another barely used set of $100 trainers that hurt my shins. If anyone is size 12, I will give you these shoes (I think they just didn't gel with me, not that they hurt everyone). So that was expensive, but that's pretty much the extent of my spending. I used to use gym gloves when I was in high school, but I just have calluses now, and they do mostly fine. If I was to buy anything else, I would buy squat shoes, which have a built up heel and are relatively solid. For deadlifts, apparently chucks are good, so perhaps no trainers are needed at all!
___
Answered on August 10, 2015 07:04:08 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130295137342



## Haha! Yes, I know what you mean - many dismiss The Greens without knowing the first thing about them/their policies bc hippy! I assure you my reasons are more valid. For realz.
### asked by Anon

Well don't leave me hanging then! If they are indeed valid, then you will be the first I've spoken to! Everyone else has wrong ideas or is not a lefty.
___
Answered on August 08, 2015 07:34:49 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130264327486



## Bit hard to get into here w character limit but some Green policies I dont agree with - they seem naive/jumbled at best - and my local Greens candidate was/is a tosser!
### asked by Anon

Given that there is such a character limit it's hard to judge, but this is always the response I hear: vague insinuations about bad policies, being jumbled, or they dont know (or need to know) what they're doing because they'll never get in. The greens are by no means perfect, but those accusations are typically unfounded. Not that you said this, but the one that gets me is "they're against nuclear power! It's so much better than coal, so wtf". Not only do we not have any nuclear power plants, no party will ever bring them in in the near to mid term future, so hush!
___
Answered on August 08, 2015 07:18:36 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/130264083006



## Apologies if my question/comment came across like I was ridiculing your choice (to vote Green). Not my intention at all, I'm just disappointed in them as well, I guess, and dont see them as a viable option. Just my opinion, of course.
### asked by Anon

Why are you disappointed, and why not a viable option?
___
Answered on August 08, 2015 06:58:08 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130263865150



## I would die before voting for the Greens tbh. Independent all the way.
### asked by Anon

Would you really die? I'm not saying I'll vote greens for life - just that at the moment, they line up well with my thoughts on most things. Being die hard for a particular party seems like a bad thing, especially given how they can change over time. I have nothing in particular against independents, although I imagine a whole party allows specialisation into different areas of policy.
___
Answered on August 08, 2015 06:45:53 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/130262558014



## Glad the memory is a funny, not a sad one. Lol at following a "let's hang" suggestion with no speaking but you eventually talked to this girl again, right? Do you know how she remembers what happened?
### asked by Anon

I barely talked to her again, literally years later. I think we spoke in year 11 or 12, and it was just hello what's happening. It wasn't awkward, just normal.
___
Answered on August 08, 2015 03:33:18 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130259172158



## Have you ever given or received a Valentine's Day card?
### asked by Anon

Never received. I think I gave them to my ex girlfriend (when we were dating), but outside that, no. I did ask someone to "be my valentine" when I was like 14, and she responded "what does that mean?" (this was all on MSN messenger). I didn't really know either, so I just said it means that we should hang out at school. I followed this up by being too nervous to talk or see her, and we did not speak again for many years. Maybe they should teach how not to be a noob in school. This was a funny memory to recall (no sarcasm) , man I sucked at humans back then.
___
Answered on August 08, 2015 01:29:10 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/130258819902



## What have u got against onions? Did they make u cry once? It's ok for men to cry, Olympus. You are still a man.
### asked by Anon

Uncooked onions are gross and too crunchy. Also, get fucked.
___
Answered on September 01, 2015 00:17:47 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/130966444606



## Interesting. Pls elaborate. I thought Maccas was uniform across the globe & that was supposed to be it's main appeal?
### asked by Anon

No, no way. (the following is all unverified!) The CEO of global maccas was aussie for a while, and he started mccafe and stuff in Australia as a good test market. Somehow this has lead to Aussie maccas being good, and every where else shit. Ask any regular American, where does maccas fall in their rankings of places to eat. The answer is when there is nothing else. They find it weird how we often suggest maccas, cause to them it's gross. When I had some in LAX, I saw the arches and was so pleased to have been saved by delicious maccas - alas, I was presented with the crappest burger I ever had 😰
___
Answered on August 31, 2015 13:43:47 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130951315006



## Can you be more specific, m80? I mean, I love Maccas but fuk fishburgers. What's your meal/drink of choice?
### asked by Anon

Medium quarter-pounder meal, no onions, coke for the drink.If I'm particularly hungry, add a big mac burger or 6 mcnuggets with sweet and sour sauce.
Please note: never, ever, eat Maccas outside of Australia. This may seem like a strange warning, but just trust me...
___
Answered on August 31, 2015 13:26:15 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130951202878



## What's your fast food of choice?
### asked by Anon

Maccas or Oporto, m80!
___
Answered on August 31, 2015 13:22:11 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130951161150



## What's yr opinion on those who say "I dont care about politics" or words to that effect. How can someone not care about something which affects them, their families, their children on a day to day basis?
### asked by Anon

Yeah, dunno. On the one hand, you are right, but on the other - does it affect people on a day to day? Some people, yeah, but for most folk probably not. If you didn't read the paper or watch the news, you'd probably not notice all the effects of the govt.So it's kinda understandable in that way.But yeah, I mean, people should try more (me included), but whaddaya gonna do, uh?
___
Answered on August 31, 2015 13:21:29 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130946727998



## What topics get you fired up?
### asked by Anon

Typically politics.I got into a heated debate about gay marriage with my friend the other day - it was rather heated from my side of things, less so from his, so I felt bad about that. But in saying that, I'm not going to lie, so yeah.
In case you were wondering, I'm all for GM.
___
Answered on August 31, 2015 13:18:30 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130948827966



## How good of a person do you honestly feel you are?
### asked by Anon

Tough question! Even weirder from anon!
To be brief - yeah, not too bad. However, I'm sure most people think they're not too bad, even when they do crazy bad stuff. In terms of official niceness: I haven't donated to charity in a long time, so that's no good. I also don't have very much money as a PhD student, so yeah.In terms of interpersonal niceness: I think I'm nice enough, but I do make mistakes, for which I regret. I also get fired up on some topics, which can be somewhat confronting to people I care about.
My main barometer of if I'm a good person is what Kayla thinks of me - she is essentially an objectively nice person, and if she thinks I'm alright, I can't be too bad.
___
Answered on August 31, 2015 03:59:19 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/130885704766



## Are you saying you understand women now? Is that what you're telling me, Olympus? Is it?
### asked by Anon

Umm, no? I'm telling you that I was stupid in regard to such things in large part because I never spoke to women; and then after I did, I was less stupid about such things.
___
Answered on August 31, 2015 03:43:55 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/130942564926



## Where do MRAs come from? My theory is, bikini babes are enjoyable to look at, and when some men hear about this thing called feminisim, they fear their bikini babes will get taken away from them, and they get very outraged and defensive at the prospect.
### asked by Anon

I'm sure your theory has something to do with it.
But I reckon for a large percentage of MRAs, and from my own experience being a teenager and young man, it's to do with idealism and respect. So, I think that most people think they are nice, good people. Not many people are Disney style evil, plotting intently at all times. Probably more like, these men have an idea of what they think women want and what they need. However, as for me as a boy too, it's a warped idea of chiverly and a that, where women are put on pedestals and fought for honour, etc. So when they attempt this ultra-nice act of kindness to women, and the women push back, either by rejection or by humiliation, you can see why they'd be so angry. They were super nice, and it was reciprocated with assholery! They are the good guys, so why are they being punished??
So rather than trying to understand why this happens (which boils down to: women are people too, so fuck off), they simply assign the blame to feminists and SJWs, who now in their eyes become the enemy, and those Damn feminazis need to be plotted against and abused and exposed for their evil villainy!
I hope this isn't too flippant, because this is a very serious problem. I can't say I browse a lot of MRA forums, so this is just my speculation from what I've seen. I'm sure there is many other reasons within the MRA community too, but I feel that my guess is the least extremist, and most easy to fall into version. In my youth I never got to the 'get angry' phase, it was more the 'guess I just don't understand ladies' phase - which surprise surprise was countered by talking to women! Who would have guessed, eh?
___
Answered on August 30, 2015 07:01:40 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/130915208510



## Where have all the good women gone?
### asked by Anon

Looks like one of them is in Panorama city:http://m.imdb.com/name/nm0328709/
Also, 😠
___
Answered on August 30, 2015 06:44:26 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/130915163710



## Can we get a pic of you with a sexy face pls?
### asked by Anon

Umm, haven't you seen my profile picture?
___
Answered on August 28, 2015 09:02:18 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130861380670



## When there's something strange, in your neighbourhood, who ya gonna call?
### asked by Anon

Police, I imagine.
___
Answered on August 28, 2015 05:46:56 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/130858934590



## What do you think about recycling? More trouble than it's worth? I think it is in a lot of respects, at least on a smaller level - all these fuking different coloured bins.
### asked by Anon

No, no, it's no problem to sort things. I only have 1 type of recycling bin. Anyway, in terms of less boning the planet, it goes: reduce, reuse, recycle.
So recycling is the least best, because it is typically very energy intensive, but is generally better than using up resources.
I agree on the small scale that it seems a bit pointless, but every bit does help. I'm sure the biggest offenders of not recycling would be industry, so it would be nice for them to do more.
But as resources become more scarce, or have crazy price fluctuations (e.g. Oil, which is where plastics come from), it is often cheaper for companies to recycle. It's all about the capitalism, baby!
___
Answered on August 28, 2015 05:46:39 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/130858894910



## That's really interesting re: animal extinctions due to humans - working out why it's happening so we dont get our ass bit rather than the emotion over animals. I mean, I love animals, but in terms of extinction, I'd often think, just let the poor bastards go, why drag it out.
### asked by Anon

Well, it's not an answer I would be particularly proud of, given it's motivated by purely selfish reasons, but it does look very pragmatic. But I don't agree with your second part, because to not drag it out would essentially mean to F the environment so hard it bones animals quickly. Even if you meant conservation programs, I think they are important, and have had successes, e.g. wolves in Yellowstone. But yeah, prevention would be better..
___
Answered on August 27, 2015 09:14:40 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130832431166



## Home made bread is also VERY DELICIOUS
### asked by happyhaps

Heeeeeellll yeah
___
Answered on August 27, 2015 06:23:46 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130831371326



## What do you think of the seed-vault idea, and do you think genome data for all living species should be included for future resuscitation, so to speak.
### asked by Anon

Yeah, it seems like a nice idea. It kinda depends how you approach the whole concept, though. From what I understand, ecosystems are very reactive, and can reach new equilibriums relatively quickly. Your interpretation of the impact is what matters really. From a geological point of view, whole species dying out is essentially a blip in the radar, it happens all the time (looking from 500 million years ago to now), and is part of life. But if you are looking at it from a human perspective, entire wheat crops being wiped out from plague is extremely worrying (famine, etc), and and anything that could be done to mitigate that is clearly critical. From an animal perspective, I think it's important to try to prevent extinctions from human caused things, if for no other reason than whatever we're doing to cause the extinction will probably come back to bite us. Obviously I also think the animals should have a fair crack, since they are essentially defenceless against a human assault.Future resuscitation is a noble goal, but its main flaw is that it assumes that humans will change and somehow make room for all these animals again. Noble, yes, but likely...?
___
Answered on August 27, 2015 03:42:24 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130829071422



## Wtf? Look, I'm not denying that long handled teaspoon is fine but who fukn has one at home? Except you? I dont think I've even SEEN them for sale. I am, however, interested in the breadmaker, tell me more pls. Do you REALLY use it? Or did you only use it excessively for a month after purchase?
### asked by Anon

Look mate, it's not my problem if your having teaspoon handle length issues - I have reached teaspoon nirvana, and you should be happy for me, not jealous and spiteful. Bread maker was an excellent investment. While more recently our bread making has been a bit reduced, we did use it consistently for more than a year (perhaps 2 years..?) after we got it. Not quite sure why we haven't done it as much recently, but it's good to have around anyway for when friends come over.
___
Answered on August 27, 2015 03:29:22 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130826561342



## How good is the teaspoon??
### asked by OlympusMonds

F'in great, m8
___
Answered on August 26, 2015 11:57:37 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/130807955518



## At what point in history did humanity really start to fuk things up for the planet?
### asked by Anon

Don't worry about the planet, mate, it'll be here for a looong time. It's been through some hectic climatic changes, and it will get through this one no trouble.The humans, and I'm sure a large percentage of current animals, are much less likely to be so permanent. The industrial revolution is a pretty good marker of when it turned bad for them, though I have heard a things (for which I have no evidence) that general human removal has cooled the climate in the past, e.g., Gengis Khan killing heaps of folk; black plague, etc.
Anyway, in summary, the Earth ain't gonna give 2 craps about this climate event that we have induced, but everything on Earth probably will. Soil production (sun and ice and whatever breaking down rocks) is a very effective way of removing carbon dioxide from the air, so probably within a few thousand or hundred thousand years (sans humans), the Earth will be back to it's current temperature.
___
Answered on August 26, 2015 10:08:27 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/130803630398



## "An area of SOLID content". lol.
### asked by Anon

( ͡° ͜ʖ ͡°)
___
Answered on August 25, 2015 09:55:08 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130775498814



## You still laugh at fart jokes? Ever lit one of your own farts? Do you hope to in the future?
### asked by Anon

I meant general toilet humour, but yeah. I mean, they have to be good jokes, but it's an area of solid content. No, and no, for the last two.
___
Answered on August 25, 2015 09:52:27 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130775443518



## I was only joking about being pissed at yr "a good friend" a and sly dig at my q. I thought it was funny! In future though, if you want to, you CAN edit an a - removing it from the front page just puts it back into the inbox for you to either reanswer or delete.
### asked by Anon

Ah right, good to hear, on both counts!
___
Answered on August 25, 2015 08:17:17 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130773630782



## Do you feel like an adult?
### asked by Anon

I have been thinking about this question a lot, sorry for the delay.OK, so do I feel like an adult? Yes and no, and it's different to what the question assumes.
For the Yes:When compared to teenagers, or even young adults (20 to ~23), man, I feel old and adulty. I teach undergrad students at uni, and nothing quite like that puts into perspective how much older than them you are. I think the fact I am teaching them helps highlight this, since they clearly see me through some sort of Adult lens, where clearly I know what I'm doing and know heaps of stuff. I will address the fact I do not in my No answer. Going out for drinks with youths also can highlight one's oldness too.But yeah, some clear differences that these youths highlight in me is understanding more long-term cause and effect, and general planning. Furthermore, explaining this to them results in nothing happening, whereas I generally love to hear life advice.But for my own internal adultyness - for example, I was trying to get some food out of a container, and I looked into my cutlery draw and saw a tea-spoon with a long handle, and I was so pleased, and I remembered getting them from a friend, and also being pleased to finally have some long handled teaspoons. It was great. I also have a bread-maker, and it's the best. I bought a nice dryer on the weekend too, for a steal, which was exciting.
For the No:In the teaspoon example, I wasn't just pleased. I was stoked, when I got them, I was like "awww shit, yea! Long-handled teaspoons! Sick!". Same with all the other stuff. The childish revelry in stuff isn't gone at all, it just transfers to different things. I think this comes back to the long-term planning thing - I know how helpful those long-handled teaspoons are going to be, so I get pleasure out of that.More directly as No - I still do dopey stuff all the time, like rotating paintings in my house, or laying out Kayla's clothes into the shape of a person on our bed, or simply jumping out and scaring her.Also getting drunk should not be considered adult.Coming back to the teaching thing - students will often look up to you for answers or explanation, and are completely shocked that I would have no idea. Despite me telling them I just google stuff all the time, they never believe me. Like, I'm only 5 years older than some of these kids, and there is some sort of haze of authority between me and them that gets in the way.
For the different:I guess this question assumes (at least it did for me) that you will somehow hit some point in your life where you switch from CHILD to ADULT, and become a whole different person. As far as I can tell, this does not happen. I am different from who I was 15, 10, 5, 2 years ago, but you could never say on one day I became an adult.There are a few points in my life that are notably adult: living in my own (rented) apartment, buying my motorbike, working my first 2 weeks full-time. But I still laugh at fart jokes.
___
Answered on August 25, 2015 07:59:11 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130627530046



## What fictional character reminds you most of yourself?
### asked by Anon

I'll first prefix this by saying: essentially no-one, and that's probably a good thing, cause some crazy stuff happens to fictional people.The closest I can think of is Neo, or more clearly, Thomas Anderson pre-Matrix. He seems like a pretty unassuming guy, relatively quiet, doesn't like night clubs, can sometimes get obsessed with stuff on the web or computer. Less so on the cyber-crime, but hey, close enough.
___
Answered on August 25, 2015 07:39:15 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130657090110



## Are you satisfied with science's theories of the origins of life, m80? Hope this question is deep enough for you, especially as you ridiculed my last one ( which was - describe your best friend in 3 words).
### asked by Anon

Sorry about that, I do feel bad about my response. I didn't mean for it to be ridiculed, but on Ask.fm there is no edit button, so when I read it again, it was either delete it, or leave it up.In saying that, I honestly can't think of a better answer to the original question than my sassy quip, which I know isn't a good answer, so I guess that's were my criticism of the original question came about.
Now, in the same vein - I cannot think of a better answer than this: Yes, I am satisfied to the extent that I know, and any uncertainties will undoubtedly become more clear given time.
Sorry bud, I do appreciate the questions!
___
Answered on August 25, 2015 07:36:11 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130686131262



## What emoji do you wish existed?
### asked by Anon

There are a lot of emoji already, so I'm probably already covered. I mean, smiling poo emoji is in there.. what else could we need?
___
Answered on September 05, 2015 04:28:43 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131073310782



## Who was your celebrity crush as a teen?
### asked by Anon

Jessica Alba, I guess?
___
Answered on September 04, 2015 11:56:45 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131026662974



## Wtf? I can MAYBE deal with you not wanting to try a KFC nugget but don't you go dissing Pepsi.
### asked by Anon

I'm just saying the truth.
___
Answered on September 04, 2015 03:25:56 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131046061118



## I'm just about to eat lunch - 6 KFC nuggets, large fries, & large pepsi. KFC might not be getting yr $$ but they're getting mine. What do you think of them apples, Olympus?
### asked by Anon

Everything about this is wrong. Nuggets: we talked about; KFC chips << maccas chips; Pepsi is bad, while coke is good.
So you enjoy your subpar meal.
___
Answered on September 04, 2015 03:04:33 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131045145918



## Yeah, I didn't mean bad, I guess. I laughed at the KFC nugget scenario from yesterday!
### asked by Anon

Indeed, good to hear.
___
Answered on September 03, 2015 05:37:00 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131021921342



## Hmmmm. Your ask.fm is attracting some 'interesting' ppl lately, hey?
### asked by Anon

Perhaps, but I don't mind it so much. Being anon means I can be extra sassy in response.
___
Answered on September 03, 2015 04:51:44 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131021388862



## Might be time to get yr eyeglass prescription reviewed, smartass.
### asked by Anon

Or just turn your head. It's a dryer, idgaf.
___
Answered on September 03, 2015 04:26:28 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131021279294



## Could you maybe post the photo the right way up next time? Thks.
### asked by Anon

Yeah, sure!
___
Answered on September 03, 2015 04:11:02 GMT

__Likes:__ 6

Original URL: /OlympusMonds/answers/131021097790



## What's the last photo you took on your phone? Post it!
### asked by Anon

I just put my old broken dryer up on Gumtree for free, so there you go.
Some well-meaning old chap called me at 7 in the morning to tell me how I could fix it. I appreciated the effort, but he wouldn't bugger off. Man: "You know how much it costs to fix that, mate?"Me: "I dunno, mate",Man: "Nah, nah, just guess"Me: "Yeah, I really don't know"Man: "Just have a guess, mate"...and so it went on. I was in bed!
___
Answered on September 03, 2015 03:57:54 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/131020968510



## The cables for your electronics; are they bundled neatly, coiled and tied, or is it like a jungle of vines and dust bunnies?
### asked by Anon

Under my desk or behind my TV - they crazy. I have a massive collection of spare cables too, which were also very crazy - but I had a rare need-to-clean freak out, and tidied and sorted them all.
___
Answered on September 02, 2015 12:33:14 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131001254206



## FEEL FREE, ANYONE WHO READS OLYMPUS'S ASK.FM, TO SHARE SOME KFC NUGGETS WITH HIM. HELP A BROTHER OUT. I COULD DO WITH A BIT OF SUPPORT HERE, THKS. ANYONE?
### asked by Anon

No, only if you naturally happen to have KFC nuggets, and I happen to be there.
___
Answered on September 02, 2015 09:52:35 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/130998694718



## Look, Maccas ARE miles ahead of KFC but not with the nuggets. They are SO different. Try dem. Please. I'm asking nicely :(
### asked by Anon

I'm sorry, but the only way I will try one is if someone offers me one - but only of they bought them for themself, and they generously donate it. I don't want someone to go out and buy some just so I can try them, because I'm trying to not give KFC money.
___
Answered on September 02, 2015 09:35:32 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/130998469694



## Make those fuckers eat it. About time people weren't afraid to make marketing suck up the negative aspects of their propaganda. You beam fanciful bullshit into the populace constantly, sometimes you alienate a customer accidentally. Too fucking bad. Stick to your principles m80!
### asked by Soul James

Never doubt it!
___
Answered on September 02, 2015 09:25:12 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/130998373438



## Olympus, m80, you can eat a whole chicken nugget tho - there's no bones in dem. Give 'em a go, maybe? It's good that yr trying to stick to yr principles and all but Maccas are deceptive w their ads too. Wut happened to all day brekky? Hey? Assholes!
### asked by Anon

Oh, I know you can with nuggets. But why should I repay KFC with business when they lied to me so thoroughly. Maccas has never shown a food item that appears to be fully edible, but actually is not, so they are miles ahead of KFC.
___
Answered on September 02, 2015 09:21:58 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130997968446



## Have you had a chance to compare the chicken nuggets from Maccas against those from KFC? They are very, very different and I would appreciate your opinion.
### asked by Anon

I almost never goto KFC, because when I was a child they betrayed me. I used to see the ads of their bucket of chicken, and people happily munching into them. When I finally got the chance to try one, I did not realise it's actually mostly bone. For some reason I thought the whole thing was edible, and so to find out that only like 10% is was a rude shock. Therefore, KFC betrayed me when I was most vulnerable, and I'll never forgive them, and hence I have not tried their nuggets, nor will I ever.
___
Answered on September 02, 2015 05:16:13 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/130996126270



## Why do you fear cancer? Do you have family history of it or is it just a general fear? Do you smoke?
### asked by Anon

Meh, I don't fear it all the time - it's more like I'm more conscious about checking for it and stuff. Every now and again I see a mole or something and feel weird until I get a doc to check it out. About every year or so.No family history though. No smoking either.
___
Answered on September 01, 2015 10:33:09 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130972965182



## gone so hard about GM and seem to be oblivious about other issues in the LGBT community? Obviously giving support to GM aint a bad thing but it's be great if other things got more coverage. Anyway, thks again!
### asked by Anon

...probs
___
Answered on September 01, 2015 10:31:59 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130971063358



## Thks for your answers, I think I know where ur coming from. This isn't the best place for a proper discussion! I wasn't accusing u of barging in tho, & yr right, I did ask you 4 your opinion. Obviously you can care about multiple issues at once, just puzzled why the majority of straight ppl hve
### asked by Anon

No...
___
Answered on September 01, 2015 10:31:52 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130971054910



## - pretty surprised that you want  ppl to say they support GM in public, even if, behind closed doors they don't. I think that's strange but I respect your opinion. I'm not interested in parroted shite. I want to hear what ppl really think, then maybe we can work on changing their minds.
### asked by Anon

Well, clearly it would be better if they DID like it - that would be the best case scenario. But if they don't, is it worth hearing the same old arguments (that I mentioned are actually disrespectful and insulting!) again and again? The things these people are saying actually affect people! Saying that it's bad for children, or that can't be the same as a "normal" marriage - the audience of these things isn't some emotionless logic-bound group of people. Gay people went and go through a lot of shit socially, so if I had to choose between knowing exactly what everyone thought (which are the same ideas, parrotted again and again), or having a society where people are relatively socially bound to not being an asshole all the time, I think I would choose the latter.
___
Answered on September 01, 2015 05:54:34 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130970001726



## - the expense of, what seems like, everything else? I am genuinely puzzled about this. A colleague who yells his support of GM at every opportunity, when questioned further, knew sfa about what rights same-sex couples already have. He had no idea, nor I suspect, did he really care. Lastly, I'm  -
### asked by Anon

I can't say I really know what you mean with this, in that I've not met someone like you describe.
I agree that the LGBT community has many issues facing it, including continued social and professional discrimination. I also agree that straight people just bumbling in and shouting how stuff should be done/fixed is not good (remember, you did ask me about what I thought on this topic). We should be listening to the LGBT community to see what they think/want/whatever first. Simply legalising gay marriage will clearly not make everything rosey for gay people in Australia - but it does remove government sanctioned discrimination, which is a pretty important step, and demonstrates a social shift away from discrimination (although this is happening with the push for GM too).Is that what you mean by the expense of everything else?
___
Answered on September 01, 2015 05:48:17 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130969986622



## Hey, I like ppl getting fired up! But you've misunderstood me. I didn't mean you should be talking about children dying in Africa/racism instead of GM. I was talking about rights/things which need to be accomplished with the LBGT community - why have hetero's latched on to GM with such fever at  -
### asked by Anon

OK.
___
Answered on September 01, 2015 05:37:40 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130969965374



## When you were presented with the crappiest Maccas burger ever, did you still eat it? I bet you did.
### asked by Anon

Hell yeah, I did. It was just sad.
___
Answered on September 01, 2015 04:10:44 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130969709374



## Have you ever had an existential crisis?
### asked by Anon

You may need to be more specific about what you mean. Like, have I freaked out due the meaninglessness of life? No, but I have revelled in awe and wonder of life having no meaning, and the universe and Earth being crazy hectic and cool.
As I have been getting older, I have started to fear things like cancer more, but not for long ;)
___
Answered on September 01, 2015 03:53:32 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/130942540094



## ...be judgemental or an asshole, I'm really confused by the whole thing. If you feel strongly about gay rights, there are a million other way more important things to support. Yet it seems we have hoards of middle class white men/women smugly saying "I support GM" in a bid to look progressive?
### asked by Anon

"Wow! You're right! I never realised that while I've been so busy supporting gay marriage, I have missed out thinking about the environment, or politics, or racism. I was such a fool!".Coooommmeee oooooonnnn! This is the worse argument. "Let's get back to focusing on the REAL issues, people are dying in Africa!" says the person not doing shit about shit anyway! (while this is somewhat directed at you, anon, it is also to all the people saying that). Look, gay marriage IS a real issue, and if you're so desperate to get back to the "important issues" that we all somehow forgot, then support the shit out of it so it does get passed ASAP.
Coming to your last point about smug people - oh no, people saying they support something that is good. What could be worse? As mentioned in my last answer: I dgaf if all those smug assholes go back home and secretly punch pictures of a gay wedding - if they say they support GM to the public, and keep their shitty feelings to themselves, fine! Until they come up with an actual reason to prevent gay marriage, I don't need to hear the bullshit, and neither do all the gay people in our community.
I told you I get fired up a bit.
___
Answered on September 01, 2015 03:48:36 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/130967718718



## Why do you feel so strongly about gay marriage (re yr heated argument with a friend)? Don't get me wrong, I support it too - don't give a rats who gets married tbh but I'm surprised at how many straight ppl (I'm assuming yr straight) have picked up this cause? Is it bc it's easy? I'm not trying to..
### asked by Anon

I don't see why this is surprising. A chunk of our population has legal discrimination against it, and I think that is wrong. If someone disagrees, I will tell them exactly why I think it's wrong, and that their arguments are bad (yet to hear a good one!). I get fired up because the people I'm arguing with are throwing bullshit out like "oh what about the rights of children?!", which is just straight out disrespectful, and for what? So they don't have to see two dudes make out? So their institution of marriage, so thoroughly abstracted away from the church through govt., can have its last gasp of power? Who knows, but it's very annoying and insulting.In regard to easy - it sounds like your version of support is easy - that is, you don't give a shit. Sorry, mate, but without action, your support is essentially meaningless. It is the public expression of support that is critical - your private thoughts means nothing. Even if you hated GM, I would much rather you be forced via political correctness and social norms to say that you do support it. In that way, we're all spared from the assholery associated with your thoughts.So coming back to easiness - I'm sure I'm capable of taking much more dramatic action for GM: protesting, punching ministers, whatever. Fair enough. But for me the not-so-easy thing has been to try and talk about this with friends and people who feel differently, because it can be very confrontational. Especially when it's a boss or in a social situation. Abstracted TV personalities are too easy to dismiss - when your friend tells you you are being a butt, it hits much deeper. This is the support I would expect from you.
___
Answered on September 01, 2015 03:36:03 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/130967692094



## How's the wedding plans going??
### asked by happyhaps

Poorly, but with some progression. Unfortunately, I don't have more to say.
___
Answered on September 07, 2015 13:07:36 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131132194878



## Don't you know there's no way to distinguish between people other than gender! Just yesterday I mistook a random man for my father! 😂
### asked by happyhaps

lmao
___
Answered on September 07, 2015 13:07:08 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131132123966



## Do you see any differences between your mum and dad or are they just genderless blobs to you? Can you distinguish one from the other. I can with mine. Anyway, last comment from me.
### asked by Anon

How is this relevant? What does this matter when it comes to governmental forms?
Also, here is a pic of my parents:
___
Answered on September 07, 2015 05:16:54 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/131131795774



## Non-gendered parent fields. lol. Guess we should scrap Mother's and Father's day for Non-Gendered Parents Day too?
### asked by Anon

Yeah, non-gendered parent fields. How hard is that?
I don't do anything for Mother's Day or Father's Day, so sure, scrap 'em, idgaf.
___
Answered on September 07, 2015 05:10:09 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/131131671870



## I'm with you re: two non-gendered parent fields on birth cert, although for genetic issues for the child, there should be some legal paper trail for bio egg/sperm provider too.
### asked by happyhaps

Yup.
___
Answered on September 07, 2015 04:31:49 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131131325246



## Also, names on birth cert hold some legal weight re: financial support and decision making for the child. Of course you change it to the names of the legal guardians, whether it's bc of adoption or surrogacy.
### asked by happyhaps

Also totally, that makes sense. My main point about the certificate was that I didn't think the act of putting a dad in the mother field was inherently misogynistic, just that the form was out of date in asking for gendered parents (rather than just "parent 1, parent 2".
___
Answered on September 07, 2015 04:26:58 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131128572990



## I think it's interesting that your gay parenting conversation is generally only covering 2 men and not 2 women. When you involve a surrogate, especially commercially from a disadvantaged country, it gets ethically muddy yes, but for two women to have a child is a matter of simple sperm donor IVF.
### asked by happyhaps

Yeah, totally. The whole discussion got warped around this unfortunate scenario. Even with 2 dads, I have heard good stories where a friend offers or is asked to be a surrogate, with much success. I would say it's largely a loving and ethical process, in any parent sex combo.
___
Answered on September 07, 2015 04:23:52 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131127724862



## I dont know enough about surrogacy to give an informed opinion - read a little after the recent "baby Gammy" story. There r a lot of ethical issues. That anon obviously thought that if u were unable to have children, bad luck, which is very harsh. He/she was right about $$ talking re options though.
### asked by Anon

Indeed.
___
Answered on September 06, 2015 12:22:47 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131110220350



## Just tryna lighten things up after all that surrogacy stuff - got a headache reading it!
### asked by Anon

Haha, I appreciate the easy questions. My har har was light hearted.
Yeah, me too. Can you give me a quick summary of your thoughts on the whole thing? Am I right? Am I wrong? Should I just shut up the whole thing? Wtf do us two random people know about surrogacy? Sorry to ask, but please let me know.
___
Answered on September 06, 2015 12:06:22 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131109802302



## My God, Olympus! When were aware that milk came from a cow? 25?
### asked by Anon

Har, har
___
Answered on September 06, 2015 11:55:27 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131109645630



## When u get a popcorn craving, do u buy kernels to pop in a pan or do u take the lazy bastard option & buy the packet microwave stuff?
### asked by Anon

I didn't even know manual popping was an option until I was about 17, so microwave, yes.
___
Answered on September 06, 2015 11:51:14 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131109582910



## Lastly, you said my question was 'so loaded its gonna blow' like the scenario I described wasn't already happening more than you think. But hey, as long as people get their baby....its their right...
### asked by Anon

Did you read my response? "... you dip shit. It's clearly an abuse of someone else's rights, so OBVIOUSLY I'm not for it. Fuck...."
I said I was NOT OK with your scenario! NOT OK! I do not think that is a good scenario.
I then followed it up by saying that commercial surrogacy is illegal in many places, and I am also not OK with this.
___
Answered on September 06, 2015 11:38:16 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131109174078



## That's exactly what I'm saying, my rules apply to infertile heteros as well bc children are not a right! And who's having children this way too - only those who can afford it. Babies bought/manufactured by the rich bc its their right. Fuk that. Anyway, we're going around on circles. Cheers & gnite.
### asked by Anon

You cold, man. I don't want to be flippant here, but by the same logic, disabled people don't deserve help either? You're not born with it - sorry mate, nature says no.
Making IVF and similar fertility treatments only available to the rich is a problem. In Australia, the govt. does provide rebates to reduce costs, but it is definitely not cheap. Clearly your opinion would be to stop the support, while mine would be to make it cheaper so it is accessible.
___
Answered on September 06, 2015 11:35:09 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131109113662



## That's not true, I cared before, but gay couples can't have a child via traditional methods (like infertile heteros). Do I think it's ok for 2 dads to buy an egg online from an American woman & use a surrogate from the 3rd world to carry their child bc its their 'right' & they can afford it? No.
### asked by Anon

"That's not true, I cared before, but gay couples can't have a child via traditional methods (like infertile heteros)." - I don't understand, are you saying your views apply to infertile heteros too? Or that infertile heteros HAVE traditional methods (???), and gay people don't, so not OK?
"Do I think it's ok for 2 dads to buy an egg online from an American woman & use a surrogate from the 3rd world to carry their child bc its their 'right' & they can afford it? No." - You loaded that question so much it's gonna blow! 1) yeah durr, of course I don't advocate abusing 3rd world women as surrogates, you dip shit. It's clearly an abuse of someone else's rights, so OBVIOUSLY I'm not for it. Fuck. I'm not a fan of commercial surrogacy (and nor is the law in many countries). 2) in the same way, I ALSO don't advocate it when hetero people do this, which does indeed happen.3) "Why would people go to such crazy lengths to have a kid? It's almost like it's something humans value as a fundamental thing!" - see other responses for context.4) Coming back to point 1 - come on, if you ask a question like this, it's no wonder people tell you to shut up about this. I really want you to understand how bullshit this question is. Like, it's mega bullshit. You must know that I would never respond to this in a way that would suddenly change my mind.
___
Answered on September 06, 2015 11:19:42 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131108576830



## Also, I didn't say I wanted to prevent same-sex couples having children. I just think you should be able to ask questions about what it might be like for the child without being shouted down ie does the child have a right to know their birth mother or sperm donor etc.
### asked by Anon

My issue is in that both the scenarios you present (birth mum, sperm donor) - these things are an issue, right now, for people who do IVF, or do adopt, or whatever.Why is it now, when it's gay people, do you suddenly give a shit?
___
Answered on September 06, 2015 10:57:16 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131108215870



## You really think having a baby is a fundamental right? Are you kidding me?
### asked by Anon

Is this a controversial statement..?
___
Answered on September 06, 2015 10:51:26 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131108106814



## - a baby by any means possible and god forbid anyone question anything. I'm not saying gay ppl cant be good parents, I'm just saying questioning what affect it MIGHT have on a child doesnt mean ur a homophobe.
### asked by Anon

Part 3.
"I'm not saying gay ppl cant be good parents, I'm just saying questioning what affect it MIGHT have on a child doesnt mean ur a homophobe.", and also "Why can't we talk about this in a rational manner?" - I agree that children do need protecting, and that we should do our best as a society to make sure they're OK. But how hard do you want to go? How serious are you about this? Why are you asking this question when gay marriage comes up? Why aren't you asking why we don't have mandatory parenting classes? Why don't we say people have to have jobs before they can kids? Poverty is bad for them, no?I'll tell you why - because it's crazy. It would be crazy to do those things. While I do advocate for a lot of government support, I also think they should butt out of my actual life as much as possible. The freedom to raise my own kid how I feel best is massively fundamental, which is why it's a massive deal when children are removed from a home for their protection. To discuss this whole thing rationally would be nice, but it's not like some couple will heartily agree with you and shake your hand when you say that their application for a child is denied because of potential societal risks. They would be pissed as hell, with good reason! Rationality is OK in stuffy abstract debate, but this shit is people lives!
Given that you are anon, and I have no context, I will call you a homophobe for asking this. This is because your conditions for preventing gay people having children is that it *MIGHT* cause problems, while you stand silent (I'm using my social barometer to infer this for you) on other shit that children go through with hetero parents that is clearly and demonstrably worse. Unless you like eugenics, the solution would never be to prevent these people having children - it would be to provide support! It is because of this hypocrisy that I have deemed it so.
___
Answered on September 06, 2015 10:37:22 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131107117886



## - of young boys who commit juvenile crimes lack a father figure. Why gloss over it? Also, Elton John had the mother of his sons name removed from the birth cert. Now his partner is listed as 'mother'? Misogynist much? There seems to be this ridic notion that everyone has the 'right' to have -
### asked by Anon

Part 2.
"It's well known that the majority of young boys who commit juvenile crimes lack a father figure." - I'm just gonna briefly say that it's hard to say this, while simultaneously saying that studies suck.Anyway.
"My point was that a child growing up with same-sex parents will have a diff experience than those raised with hetero parents or a single parent." + above quote - yeah, you're right. We SHOULD ban single mothers from being allowed. Their children commit more crimes! We all know this, let's just ban them. Give more social support to single mothers? What, are you crazy? No, no. Sarcasm over (for now). While I'm sure you are right, they have a different experience, BUT it's a pretty big presumption to go from that to saying that they will be worse off - especially given that what helps children develop best is a stable, loving home. Even if there was some difference - should people be denied the right to children because of this? My opinion is no, and I would bet heavily that any difference that comes up (with SS parents OR single parents) is because of a lack of support. I'm sure if you got funded parental groups in communities (just like my parents did: http://www.newpin.org.au/ (woop woop)), it would make this wave of single-sex/single-parent offspring criminality disappear.
"Also, Elton John had the mother of his sons name removed from the birth cert. Now his partner is listed as 'mother'? Misogynist much?" - I don't know anything about this, but my first reaction was that the form should just have two fields for "parent". Is there some controversy with the birth mother? My experience with English bureaucracy is that it's pretty out of date - at my sister's wedding, the fathers of the couple were required to put their names and professions, while the mothers' names were all that was required. Also, this story is a data point of 1.
"There seems to be this ridic notion that everyone has the 'right' to have a baby by any means possible and god forbid anyone question anything" - phoar, mate, you are going hard. Having a baby is a preeeetttttyyy fundamental right for people. Like, it doesn't get much more. To tackle the body of what you mean though - so no IVF for heteros then? You're answer can be yes, but I'll think you're a pretty big asshole.
___
Answered on September 06, 2015 10:37:10 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131107058238



## You can get a study to say anything depending on who's funding it. My point was that a child growing up with same-sex parents will have a diff experience than those raised with hetero parents or a single parent. Why can't we talk about this in a rational manner? It's well known that the majority -
### asked by Anon

Part 1
___
Answered on September 06, 2015 10:36:57 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131106991422



## You said earlier that you were offended by ppl saying re gay marriage "what about the children?" & "it cant be a normal marriage". What's wrong with expressing concerns for children of same sex relationships? And gay marriage ~cant- be 'normal'. Ppl arent genderless blobs, you know.
### asked by Anon

Children stuff: if it is bad for children, it's too late. Same sex couples can already adopt children, so if this your concern, then SSM is not the issue to be dealing with that with. Having concerns for children in same sex marriages leads to various problems. If you think you need a parent of either gender, then it gets a bit iffy for you when a single parent walks in. Is it because the parents aren't biological? I guess we should stop regular adoption too, then. Is marriage about procreation? Guess people who don't want kids should be banned too. Is it that gay people suck at parenting? Studies say otherwise, but even then, how the f would you even measure this?
From this, and coming round to your "normal" -  what I think this boils down to is how abstracted do you think marriage is from religion organisations? There is a bunch of stuff I could do re: marriage that is "crazy": go marry someone I met today, get married many times, get married and choose to have no kids, etc.. Why can I do these things, that are most likely in direct opposition to the church's values? Our idea of marriage has become abstracted away from religion to become a legal affair. So why is gay marriage suddenly the exception to this rule? Why is marriage suddenly so tied to the church that we must follow tradition now? To me, it all sounds like hypocrisy.
Really what they should do is just make all marriages turn into civil unions, and then if you want to do church stuff or whatever, go do that. Leave me, and the legal system, out of it.
___
Answered on September 06, 2015 09:28:22 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131103294014



## I appreciated yr response re "white guys", and am following up as requested. What did I think of what you said? Well, it was straight out of the text book. Look, I know "white guys" have won the lottery & need to relinquish some of their power/privilege but this "white guys" know shit is boring.
### asked by Anon

While I appreciate the sentiment of the text book crack, I'm going to be a bit facetious here and say... text books are typically thought of as good sources of well accepted info. Why do you disagree with this general idea?
I don't quite understand what you mean when you say '"white guys" know shit'. You may have to follow up.
But "boring"? This I really don't get. Do you mean you're bored of being lumped in together with other white guys? Or bored of being told you're privileged? Or bored of hearing the same argument? In any case, bored is pretty much weapons-grade insensitivity. "but in my original question, people were being insensitive to me!! Why shouldn't I be back?" - yeah true, but you don't have to fear the police or suffer daily insults or have laws directly targeted at you or etc. etc.. So you CAN be insensitive, but from everyone else's perspective, your dickish behaviour is a double whammy from on high, in a king midas-style desire to retain power (AKA, keep the status quo).
Again, please follow up.
___
Answered on September 05, 2015 10:32:09 GMT

__Likes:__ 7

Original URL: /OlympusMonds/answers/131077492030



## If you had to choose one of the conspiracy theories out there on whatever topic to name "more believable than the rest", which would it be?
### asked by Anon

Probably the 911 stuff or the illuminati is *easiest* to believe, because it's all human based deception. There are a surprising number of conspiracy theories in geology: expanding earth, hollow earth, nuclear reactor earth, abiotic oil, to list a few! One common thread I always find when reading conspiracy theorist's work is the writing style. Even if I don't know anything about their work, I can pick up the tone very quickly, do some quick googling, and discover that it's a conspiracy. It's typically a very aggressive writing style, very combative, and revels in surprising the reader with some killer fact to blow them away.
___
Answered on September 05, 2015 08:45:15 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131073990206



## who's your top 5 twitter crushes? And who's 5 people you think are absolutely followable?
### asked by Anon

I am bad at twitter, so I'll just say: soul James, nebby99, and happyhaps. Bonus person-who-ive-never-met/science-crush: Lorena Barba, who wrote a paper on how a particular snake can fly/glide through the air, and open sourced all the code and wrote excellent tutorials on computational fluid dynamics, which helped me in my PhD.
___
Answered on September 05, 2015 08:30:36 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/131076310846



## Is "white guys" a racial slur? Because it's dismissive, dehumanizing, and sure as f*ck offends me.
### asked by Anon

No, not really. Does it really affect you when people say it? Does anyone actually stop listening to white guys when this is said? About the only time I can think of where people actually DO occasionally stop listening is where white guys are talking about stuff that they should probably shut up about, e.g., "booing Adam goodes is not racist!". But only occasionally. I'm sure if some white guy issues come up that are of similar magnitude to the gender or racial issues we see today, your voice will be very welcome.
I would like you to follow up with me about what you think of this response, and more about your general concerns.
___
Answered on September 05, 2015 08:18:59 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/131073898302



## Favourite bird?
### asked by Anon

Crow. They're black, clever, and rather handsome. They also make good noises.
___
Answered on September 05, 2015 05:19:34 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131072491070



## A workmate is obsessed with overpopulation. Do you think this is a serious concern for the future? I don't tbh.
### asked by Anon

It probably would be better if there were less people.I have heard that if you assume that all countries are going to reach a first world state (a big assumption), then the number of children people are likely to have drops, and you would get a stable population around 10 or 11 billion. Much consolation? Probably not.
___
Answered on September 18, 2015 05:59:08 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131412973886



## "This study did not address appeal of sexist games to nongamers". Interesting point but do they even care about nongamers? Does Kayla take much interest in what you're playing?
### asked by Anon

Well clearly not, because they didn't study them. My point was more a broader commentary that just not getting more sexist is not a great benchmark. Obviously it would be worse off the study did show that it made gamers more sexist, but just because it didn't doesn't mean all is well and good.
Kayla does take a small interest, asking me what's going on and such. But yeah, she is not super impressed with some of the stuff she sees.
___
Answered on September 17, 2015 08:20:37 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131392778814



## Anyway, we're going around in circles here. As for current restrictions, they keep moving, I don't think it's unreasonable to question where we're going with it. Yours truly, buttface.
### asked by Anon

.
___
Answered on September 17, 2015 05:59:16 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131314444350



## No, I wouldn't want to impose those restrictions on everyone. That would be crazy. It IS "unnatural" which ever way you look at it - its called "Artificial" insemination for a reason but that's not what bothers me. The whole "its my right to have one/or however many I desire/by any means" does.
### asked by Anon

"The whole "its my right to have one/or however many I desire/by any means" does." - yeah, but you are saying this is NOT crazy for people who can naturally have kids! That is my problem with you, you massive fuckwit.You are applying your rules/judgements differently for people, who through no fault of their own, are unable to do something. We have the technology, the evidence points to it being all good, it has nothing to do with you, so what's the fuckin problem? "Oh no, it's unnatural!" - fine, don't do it then. Also don't enjoy medicines, and cars, and computers and shit, cause those all also fall into your definition of unnatural.
___
Answered on September 17, 2015 05:59:07 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131314411582



## Forbes recently reported on a study claiming that video games were not a cause of sexism despite their frequent correlation. Would you agree?
### asked by Anon

I read the Forbes article, and the actual published article, and I must say forbes was very biased journalism. Also when I typed I to google "forbes sex", on the way to spell sexism it came up with "forbes top 10 sexyest women", so there you go.
Anyway, from reading the actual article, it does seem to support your question. However, I'm going to be an asshole to you and say it needs further study. I know, I know, shitty answer - BUT in my own field, you really can't rely on a single study, no matter how longitudinal, so I don't see why this subject would be any different.
Also, while this research idea is ok, it is a bit sucky to report on it. Like, even if it's true, it still would be better to have good representation of women in games anyway. On this same point, this study did not address appeal of sexist games to nongamers. I am oft embarrassed for Kayla to find me playing very violent or sexist games, especially if my best answer is "it doesn't make me more sexist".
___
Answered on September 16, 2015 10:31:28 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131234409278



## What joints do you crack besides knuckles?
### asked by Anon

My neck and back, as much as possible. As much as I don't want to be the high school guy who cracks joints because it's cool, it is actually very satisfying, and I almost rely on it now.
___
Answered on September 16, 2015 10:23:28 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131335526462



## How do you feel about boomers?
### asked by Anon

Meh, I don't know enough to really say. I guess they should probably get F'ed, but I really don't know.
___
Answered on September 16, 2015 10:22:16 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131362886206



## Ok. Fact. It would be virtually impossible to place the same restrictions on those who can conceive naturally. And as I wrote earlier, I accept IVF is here to stay, regardless of what I think. It may not be 'so terrible' but someone has to make restrictions. Someone already is.
### asked by Anon

But my question is, would you WANT to impose those restrictions to everyone? Because if so, that's crazy. If not, then you're a massive buttface to people unlucky enough not to be able to have kids.
Is it just that it's "unnatural"?
___
Answered on September 14, 2015 03:57:15 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131292749374



## Pt3 struggling to have a child, conceiving. I just dont want the ugly side of it to be glossed over. And there should be heavier restrictions imo. If you already have a baby, no go, for instance.
### asked by Anon

Well, you are not actually engaging with what I said, so please do: - Do you want the same restrictions for baby-makin' to people who can naturally have babies?
- If not, why not? We have helped people do other things with medical science - why is this so much more terrible?
- There is ugly sides to everything in life, but we don't immediately give up and stop. I don't see why the rare case of irresponsible parenting (NOT conception method) of someone should prevent others from having children.
___
Answered on September 13, 2015 12:12:00 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131292252222



## Pt2 medical technology that some see having a baby as their right. Not just A baby but as many as they like/can afford. IVF is here to stay but someone HAS to make restrictions. Ur all "Vote 4 Olympus! Cheap babies for everyone!" to look like the good guy. I'm happy for those who have been
### asked by Anon

2
___
Answered on September 13, 2015 12:11:44 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131292174398



## I'm having a lot of trouble with yr baby as a 'right' attitude, as u can probably tell, & I dispute that having a baby is someone's entire biological purpose if they can't have one biologically?? What did ppl do before IVF if unable to conceive? Adopt? Foster? So its only with the advance of pt1
### asked by Anon

1
___
Answered on September 13, 2015 12:11:40 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131292062526



## Pt3 I guess what I'm saying is that children are definately not a right. You dont deserve a child just bc you want one or by any means possible. Restrictions on IVF use are needed although I dont support it at all.
### asked by Anon

OK, I'm going to start by saying that the examples you provide are essentially guaranteed to be (extremely) non-typical. I am sure that ~99.9% of people who attempt IVF go into it very seriously, and are willing to put in a lot of time and effort into ensure it works. Some people try for years. So any sort of wild drunk couple who are desperate for a baby are, by definition, less likely to go through with the whole thing. This line of thinking also implies that parents of IVF children may take things more seriously. Anyway, I don't think this is a good point though, because I think people inherently have the right to have children.
You're argument is essentially saying that people who are unable to naturally have a child need to be subjected to rules determined by  about whether or not they are allowed to have a baby. Conversely, if you ARE born with the natural ability to have a baby, go for it. Have as many babies as you want. You're unemployed - whatever, have 2!This is very troublesome, because it either means you are an asshole to the select group of people who cannot naturally have a baby, or you would like to impose some sort of eugenics style rules on the general populace about who can have a baby. (Actually, in either case, you sound like an asshole.)
If it is the latter, who will decide these rules? Who will decided if I am a capable parent or not? What will happen to children who are born outside these laws?  
What it all comes down to is that having a baby (or the choice to have a baby) is a MASSIVE deal, ESPECIALLY in our current society, which overly promotes/pressures women to have children (e.g., criticism of Julia Gillard being "barren"). But even if you ignore the current society thing - just biologically, having a baby is what your entire purpose is. I would put this right on the same level as right to life - even if you commit crimes, or just suck as a human, you are still allowed to live.
Please explain why you do not think this is the case, not in the context of IVF.
___
Answered on September 13, 2015 11:15:23 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131280324158



## Pt2 potentially in the future, you just advocate for it to be more accessible? There was that Oz woman who already had 2 children but wanted one with her new partner. I guess bc it was her "right" & everything. She had IVF which resulted in quintuplets then her partner ran off w the baby sitter.
### asked by Anon

2
___
Answered on September 13, 2015 11:15:14 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131280290622



## You and happyhaps both support making IVF more accessible. That's a pretty broad statement. For who, exactly? Anyone who wants a baby bc its "their right"? What about age restrictions? What if someone's single or living in poverty? I love how instead of looking at the problems, both now & pt1
### asked by Anon

1
___
Answered on September 13, 2015 11:15:09 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131280212286



## Do you ever put some booze in yr coffee?
### asked by Anon

No
___
Answered on September 12, 2015 09:05:27 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131260363838



## What is your drug of choice?
### asked by Anon

Booze and coffee.
___
Answered on September 12, 2015 09:02:00 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131260195134



## Does it matter what brand or are you pretty easy?
### asked by Anon

There is only one brand: http://shop.coles.com.au/online/national/arnotts-biscuits-shortbread-cream
___
Answered on September 12, 2015 04:29:06 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131257014846



## Hey, I'm going to Coles - do you want me to get you anything?
### asked by Anon

Shortbread creams.
___
Answered on September 12, 2015 04:14:32 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131256960830



## I'm a nice person & genuinely tried to be happy for you & yr fancy surround sound speakers score but I'm really bitter tbh. WHY WASNT IT ME?! Have you found anything else good?
### asked by Anon

Sorry bud, luck of the draw. You should channel the bitterness into patrolling Randwick for good finds.
___
Answered on September 11, 2015 14:27:57 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131235446334



## To whoever asked me where MRAs come from
### asked by OlympusMonds

I thought this was an interesting read: http://www.salon.com/2015/01/10/the_plight_of_the_bitter_nerd_why_so_many_awkward_shy_guys_end_up_hating_feminism/?utm_source=facebook&utm_medium=socialflow
___
Answered on September 11, 2015 14:26:25 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131240797502



## What social characteristic or habits (if any) in ppl make you uncomfortable? I can't deal with ppl cracking their knuckles for example.
### asked by Anon

When people have perpetually wet lips. Surely that must be annoying! Also, I crack the crap out of all possible joints. So satisfying.
___
Answered on September 11, 2015 07:54:01 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131229660990



## OMFG! Suround speakers worth over 4 grand?! I thought an IKEA lounge was good! Brb, heading East.
### asked by Anon

Yeah, tell me about it. It was hard to carry home because the subwoofer weighs 25kg!But yeah, look up the Randwick schedule, and go nuts.
___
Answered on September 10, 2015 08:42:27 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131208004926



## What's the best thing you've ever found on the street during council collection?
### asked by Anon

When you live in the eastern suburbs, council collection is a different kettle of fish. For example, I picked up a set of 5.1 surround sound speakers on the street. Turns out they're worth over $4k! This is not a secret though. Around clean up time, vans with enterprising drivers pick through the piles, so you have to be quick!
___
Answered on September 10, 2015 07:57:25 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/131207582782



## Have you ever taken a swig of coke or milk straight out the bottle/carton and put it back in the fridge hoping to infect all the other ppl who live at your place with your boy germs?
### asked by Anon

Yes, I have - BUT I have only lived with either significant others (who I presume love my germs), or with housemates that don't share milk/coke.And I would do it again.
___
Answered on September 07, 2015 13:08:54 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131132223294



## Do you prefer jelly beans or jelly babies?
### asked by Anon

Jelly beans are super gross, so jelly babies for sure.
___
Answered on September 07, 2015 13:07:54 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131132237374



## Are you taking the piss, m80? What I mean is, I don't even care if there IS a valid reason why Lefties love the K's - my reasons for disliking them are valid too (they boring etc) & I'm dismayed by the no. of ppl feigning interest in them bc it'd be politically incorrect not to.
### asked by Anon

Well you did ask me "Why do I feel like a bad leftist for not liking them?", and finding out the valid reasons would probably be a good first step. Since I clearly have zero clue, I deferred you to the most basic of ways to find out. I even was reminded of a potential reason for Kanye's supposed leftisms when I generated that link, which was that his mother was an English professor, who are (to generalise) typically more left.
From the second half of you're question, I recognise you as a previous asker. You seem deeply offended by people doing things for looks or superficial reasons. First of all, given you yourself have admitted to not even want to know what these reasons may be, I would say your beliefs must be equally superficial at the very least. You don't have a balanced knowledge, how could you judge so strongly? Secondly, even if you are right, who cares? Let people do what they want. Supporting left wing ideas sounds good to me. Thirdly, would it be politically incorrect not to?? As mentioned, I don't know much about the West's, but I didn't think they came anywhere near free from scorn?
___
Answered on September 29, 2015 10:25:10 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131684030782



## I'm having trouble understanding the link myself tbh, I just know that a large no. of my female friends who identify as Lefties have an obsession with Kim/Kanye?? Fukt if I know why!
### asked by Anon

Well I sure as hell don't know, so here you go:http://lmgtfy.com/?q=left+wing+kanye+west
___
Answered on September 29, 2015 09:51:13 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131683486782



## It's no pun, son! It says right there in yr bio that you is a "big lefty". I lean towards the Left myself but fuk I'm sick of people talking about Kim/Kanye. I love pop culture but give not a fuk about the Kardashians! Kim's the poster girl for so many Lefties. Yawn. Zzzzzzzz.
### asked by Anon

Yeah, I got that my profile says that, I didn't understand the link to the Wests.
Now that you have elaborated what the connection is, I still have no idea what you're talking about. Are they left wing leaders? As in, politically? I'm in Australia, btw, so maybe that's why?
___
Answered on September 29, 2015 09:08:59 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131683379518



## You're a big lefty, hey? How come I feel like a bad leftist just because I don't Kanye or Kim Kardashian?
### asked by Anon

I feel like this is a pun, but if so, I'm not convinced. Left and right are relative directions, where east and west are cardinal.
___
Answered on September 29, 2015 08:55:50 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131683136062



## Why? Pls explain.
### asked by Anon

Well, they do produce pretty shitty pizzas, but it stems from more childhood drama.
My household was a pizzahut house, and the pizza of choice was ham and cheese. When we would go to our family friends house, they would have domino's ham and cheese - but somehow green capsicum was always also on it??? That was enough information for me to never eat domino's unless deathly hungry.
As a side note, I do like capsicum now, but it's too late.
___
Answered on September 28, 2015 14:11:58 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131664404542



## Is there any other fast food joint on yr blacklist or is KFC the only one?
### asked by Anon

Dominos is also blacklisted.
___
Answered on September 28, 2015 13:40:11 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131661784894



## I swear some people keep a Facebook a/c just so they can whine about it on twitter. Do you still have Facebook? If yes, how often do you use it and why?
### asked by Anon

I haven't really seen anyone whine specifically about Facebook, so I don't really know.
I do have a FB, and I use it every day to see what's up.
___
Answered on September 28, 2015 03:10:16 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131633142846



## Who do you think would win in a war between China and America if no other countries got involved?
### asked by Anon

Probably no-one.
___
Answered on September 28, 2015 03:09:40 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/131636282174



## The Smiths? or The Cure?
### asked by Marcus

I don't know the smiths, and I only vaguely know the cure.
___
Answered on September 28, 2015 03:09:12 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131657600062



## Hey, I asked you a few woodwork questions waaay back which you were nice enough to answer thoroughly ie telling me about Men's Sheds etc. Are you working on anything now?
### asked by Anon

Not really, to be honest. I've been pressed for time, and haven't been to the Men's shed at all lately. Most of what I've been doing relating to woodworking has been learning how to sharpen tools - hand planes, chisels, and saws. Seems to work, but as always I don't have all the necessary tools to do it fully properly. Still fun though!
___
Answered on September 26, 2015 07:49:35 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131588984638



## It's cool. I think everything's been said.
### asked by Anon

Ok!
___
Answered on September 24, 2015 23:03:20 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131582663486



## Thanks for the ~sort of~ support. In my experience I save 5/10 dollars per fare using Uber. Huge amount for a student. My main concern is getting home safe/cheaply & I make no apologies. PT is sometimes scary at night too (when avail), especially on w/end - lots of drunk guys about/bad lighting etc.
### asked by Anon

I think it was the tone of the question that was the issue. Ask/confront Soul James.
___
Answered on September 24, 2015 23:01:07 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131582589758



## Nor is the problem with Uber the people who use it, but there's a principle involved with people talking shit passive-aggressively on the social networks I dominate.
### asked by Soul James

Fair enough.
___
Answered on September 24, 2015 12:34:03 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131570369342



## Tell your Uber-riding buddy his broke class-traitor ass should walk home.
### asked by Soul James

Lol. May not be wrong though, workers doing shift work are probably rarely mad money makers, and public transport blows after a certain time. They can hardly be blamed for using/being pleased with a cheap mode of transport to get home (although I don't know that uber would be cheaper, giving the supply/demand pricing they do).
___
Answered on September 24, 2015 12:21:13 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131570260542



## I guess the tone of a q can be hard to read, wanted to be assertive but def not aggressive. I was just pissed at ppl, not you, pontificating about uber and the ppl who use it, some who have no real choice. Judgemental pricks. Look at the system, not the poor bastards who use it bc cheapest op.
### asked by Anon

Zok!
___
Answered on September 24, 2015 08:41:11 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131567859774



## You're LUCKY you got that last * in bc I was about to go ballistic! lol. What kind of shiftwork did u do, if u don't mind me asking?
### asked by Anon

Haha, sorry, just teasin'!
I used to work from 4pm to 9pm* at the ICT Helpdesk at USYD, when I was doing my Honours degree.
* to continue this theme, I understand that this is not proper shift work, because public transport was still available at this time of night, I was just being sassy in response to a slightly aggressive assertion/question.
___
Answered on September 24, 2015 08:08:00 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131567682878



## Taxis are a 'not required'? You've obviously never done shift work & not had a license/car. Taxis were the only option I had to get home or to work in some instances.
### asked by Anon

Yeah, yeah, yeah, I know, chill. It's hard to be verbose on Ask.fm. I wasn't having a dig at people, just trying to be concise.
Also, I have done shift work with no car, and it's called a bicycle*.
* yeah, yeah, yeah, I know, chill. This only works well if you're within a ~10 km radius of work/home.
___
Answered on September 24, 2015 07:59:35 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131567426110



## Lol. Do you attract crazy ppl irl? Your ask.fm can be entertaining though - especially with IVF and chicken nugget dude. Hope IVF guy is done but. Nugget guy seems harmless. I feel he may be a KFC employee or manager though!
### asked by Anon

Yeah, had some odd questions on here. Nugget-pro is clearly nice, I can tell their desire for me to try KFC nuggets comes from a good place. Just fate that gets between us. IVF-nut is quite odd, I've never met anyone with views like that. Ah well.
I don't normally meet too many odd people in real life. Some here and there, but I'm mostly fairly sheltered even from meeting new people, so it kinda makes sense.
___
Answered on September 22, 2015 09:59:54 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/131518170686



## Ok, you drive a hard bargain but I understand, m80!
### asked by Anon

Thank you.
___
Answered on September 22, 2015 06:18:39 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131516937278



## Omg, Olympus, you are breaking my heart. Do you really want to do this over a chicken nugget? Are yr principles worth it? What about the human cost?
### asked by Anon

You are hurting yourself here - I never presented my self as keen to try them or whatever. If I was at George St, metres away from the KFC, I would have come in.
But as mentioned, I'm not giving any money to KFC, and time is money, so it really will have to be the most natural confluence of factors for me to eat a KFC nugget.
___
Answered on September 22, 2015 05:42:39 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131516891966



## JFC! You're so hard to please! THEY ARE ALL GONE NOW ANYWAY, M80!
### asked by Anon

Good.
___
Answered on September 22, 2015 04:37:37 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131516235070



## Hey Olympus, come to the KFC at George St, Sydney Central Plaza within 30 mins and I will give you one of my chicken nuggets to try, m80!
### asked by Anon

Nope, needs to be way more natural than this.
___
Answered on September 22, 2015 04:29:42 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131515723070



## Cracking joints might be "very satisfying" but it can't be good for you! How strong is the urge to have a crack?
### asked by Anon

Nah, it's fine. Doesn't cause or not cause anything bad. It's just gases being very rapidly undissolved in your joint fluid. That being said, I don't know why it is so satisfying. Sometimes it feels like a joint needs to be cracked, and if I don't, it's kinda noticeable, or my muscles get a bit tighter around there, or something. Perhaps it's just a mental thing now, but I really like it.
___
Answered on September 21, 2015 00:44:58 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131470762558



## Shave the beard, hey? Is yr facial hair usually seasonal, m80? Are you prepared for the "Omg! You look sooooo young" comments when you do shave it off?
### asked by Anon

No, it has never been this big before, it's getting quite beardy. It's not much of an insulator, more like a wind breaker, so it's not super seasonal in that sense.
___
Answered on September 18, 2015 10:07:37 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131416766014



## I get the feeling this Summer is gonna be an absolute scorcher! You heard it first here, on ask.fm, from me. Any thoughts about shaving yr noggin again?
### asked by Anon

Nah, my hair is much longer now, abs I've experimented already. Also, sun burn! I'll probably shave my beard though.
___
Answered on September 18, 2015 06:00:16 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131413945150



## did you realise that last question was also rhyming?
### asked by OlympusMonds

nope, because I'm dopey AF
___
Answered on October 15, 2015 11:31:40 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/132057577534



## Olympus, m80, why all the hatey?
I only want the best for you, so stop being such a poo.
KFC nugs are where it's at, so get some down yr throat quick stat.
### asked by Anon

maybe you do things differently where you're from, mate, but I don't write limericks for people I hate.also, no u
___
Answered on October 15, 2015 11:20:08 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/132057378110



## I had a chicken nug craving today and there was no KFC around so I had to go the Maccas ones. JFC! What kind of Frankenstein shit are they! They do not even compare to my KFC babies. Still ate them though. Just a reminder of what yr missing out on, m80! Hope ur well.
### asked by Anon

Maccas nuggets are the best,
to say diff is surely jest.
keep your ugly ass nugs,
probs with gross bugs.
I decline your ongoing quest.
___
Answered on October 15, 2015 10:17:48 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/132055736126



## Tomb Raider is about a woman killing lots of men, no female enemies anywhere. What would you think about a game with a male protagonist killing only female enemies?
### asked by OlympusMonds

PART 2Kind of on the same point (equality doesn't necessarily mean women become men), let's go over the last Tomb Raider. Lara gets shipwrecked on an island, and that island has a ton of crazy dudes on it, and literally zero crazy women (except for perhaps the ghost queen). Your objective as Lara is to get your friends back, and survive, but 70% of the time this boils down to having to kill ALL these dudes. Lara herself can also die, and in common circumstances this results in terrifyingly gruesome death scenes, often with post-death twitching or some other sort of horrifying detail. It is very traumatic, I really don't like them at all. An interesting question is: why is there such asymmetry between the deaths of Lara and the people she kills? One obvious one is that she is the main character - but the game does push a story element relatively hard of Lara's struggle with having to murder someone. Showing the men she kills dying in the same horrific way as she does could act as a strong hammer to drive home this point (though Sniper Elite kinda douses this point). The men in TR are essentially shells, not human at all. You have zero feeling for them, but one presumes they have/had families, a mother and father, a whole life leading up this point. And it was snuffed out in an instant by mere coincidence of the various disasters leading Lara to this point. If you were to replace these men with women, fundamentally nothing would change. They would still be lifeless husks, put there to simply slow you down. Sure there would be some outcry (quite rightly) - but all this does is expose how masculinity (here as the anti-feminism) has failed so thoroughly. We see it as OK for Lara to gun down these men (but not women), because they are men. I would rather see more games where gunning down hordes of humans wasn't the primary game mechanic - but then I'd be a "pussy little bitch" or some other derivative for not wanting ACTION ACTION ACTION!Moving on to older TR games, my hazy memory has Lara as a relatively hard/cold person. She's here to raid some tombs, and if you got a problem, get F'ed. I can't say I have a major problem with this attitude - on the one hand you could say it goes back to making her more "man-like", or perhaps she is just doing shit cause she wants to. Either way, I'm less concerned, since she does kill many fewer people, and many many more wolves.
Though the questioner presumably was trying to be antagonistic, I found answering this question quite interesting. I would appreciate you (the asker or reader) leaving some form of feedback or your own views on this general theme.
___
Answered on October 14, 2015 13:08:22 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/132036948030



## Tomb Raider is about a woman killing lots of men, no female enemies anywhere. What would you think about a game with a male protagonist killing only female enemies?
### asked by Anon

PART 1Well, to start on a minor point, Tomb Raider is not "about a woman killing lots of men", it's about a person raidin' tombs! Particularly in the earlier ones, most of the time you are by yourself, perhaps killing bears or mummies or something. However, that being said, in those early ones, and ESPECIALLY in the last one, Lara does have to kill people, and you are right in saying that they are all men.The tone of this question (that I have chosen to infer, since you left it anon) is relatively antagonistic; the innocuous question "Oh, I'm just curious what you would think of a man killin lots of ladies, hmm??" is the most transparent sheet over the presumed message of: "feminism doesn't care about men" - or so I interp this question anyway.
What now follows is me declaring a bunch of stuff about feminism. I essentially only have a superficial knowledge of feminism, so remember to read this as my opinion of stuff, and not a general representation. This is a terrible qualifier, but I didn't really know how to discuss this stuff without relying on these declarations, so sorry about that.
First thing: I'm not sure I would say Tomb Raider, especially the last one, is the most feminist of games. It does have a female protagonist, and she is pretty resourceful, but after that... For example, in the last game, you could almost describe it as a "bad shit happens to young woman" simulator. Almost nothing that happens to Lara is because of her actions. Her actions only minorly mitigate the various disasters unfolding around her, and typically this is where the player is in charge, rather than story-Lara. So in that sense, it doesn't really empower Lara much. However, she does get mighty good at killin men-folk - but is that the sort of empowerment or equality that feminism wants? I don't know, ask feminists. My personal idea of what I would want from equality in gaming (in how the end product changes) is not exactly-the-same-games but with 50% of the time the main character is swapped out for a woman. This perhaps is similar to the debate of women being allowed to serve on the front-line of the military - some women say yes!, some women say why would we want to become like men? (I am certain this is a terrible representation of this debate). Equality doesn't (have to) mean women become like men, I would hope it would allow both genders to move around between masculinity and femininity as they please, rather than everyone towards masculinity.If you look at earlier TR games, Lara is less powerless to the whims of the forever disaster prone environment - but you do get the much bigger emphasis on her overtly saucy figure and stuff like that, so I'll not say much more, since its a pretty well covered topic.
___
Answered on October 14, 2015 13:05:40 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/132009015614



## "overwhelmed by gun choice" lol, probably not a problem for American players. What games are you getting into now?
### asked by Anon

Yeah, I'm a bit of a hoarder, which is not the way to play Borderlands.
I finished GTA5 recently, and then I was at a bit of a loss at what to play, so I went back to Mass Effect. But really I mean Mass Effect 2, because ME1 is a bit too rough for me now, and I've played it like 1.75 times already. ME2 and 3 I've only done once. It's very good though, I'm enjoying the replay a lot so far!
___
Answered on October 13, 2015 00:21:21 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131986383678



## Lot of folks like Burch end up being "progressive" just by virtue of being professional critics, which automatically opposes you to GamerGate's ideals regardless of their existing views on justice. Being an intellectual sometimes picks your side for you, if one side are deliberately ignorant.
### asked by Soul James

Yeah, perhaps that explains what I was trying to say without overly criticising the Burch. I've not seen heaps of progressive stuff out of him, but he is definitely not a bad dude. But yeah, the obvious juxtaposition of gamergate makes him look like a saint.
___
Answered on October 11, 2015 00:32:08 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131953566526



## I've never played Borderlands/2 and hadn't really heard of Burch until I stumbled across an interview of his. He seems to get a lot of hate for trying to be "too progressive" so I was curious about his writing. He's a funny dude though.
### asked by Anon

Yeah, mostly from HAWP I've inferred his progressiveness, particularly from their own self-criticism for making light of domestic violence in earlier episodes (though now it seems to be becoming more of a metajoke/crit, but anyway).I can't say I noticed any progressiveness or nonprogressiveness in borderlands 2, but I didn't play very much of it. Perhaps someone can verify this?I'd like to see more of Ash's stuff, but this family seems to be one I cannot remember to look into.
___
Answered on October 10, 2015 23:37:53 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131952952382



## Re: Anthony Burch - have you ever played Borderlands/Borderlands 2? If yes, what did you think?
### asked by Anon

I played a little bit of borderlands, and slightly less than that of borderlands 2. I think Burch was only on 2 though.
I didn't like them very much to be honest. while I was playing it was OK, but then I got overwhelmed by gun choice, and the "story" was too intermittent for the amount of running and gunning I was doing. perhaps I approached it wrong - maybe if I thought of it more as an RPG or something I could have engaged better. it was much more fun when playing co-op though.so yeah, coming back to Burch - I can't say his rep as a writer really shone through to me - I only really appreciate him for HAWP, and that appreciation is medium high - the things he says about games in HAWP as a parody of himself are often more interesting to me than his comedic role in it (eg the metagame around far cry 2).
___
Answered on October 10, 2015 11:03:52 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131932935486



## Ok, chill dude. The down with Kim remark was made in jest & I don't rly care for any discussion about her.
### asked by Anon

I don't care so much that in going ask a 3 part question about her on a website!
I can't tell jest from nonjest via an anon question, and even then, meh.
I don't know what you wanted from this whole interaction. From my perspective, you appear to be the same "crazy" you quoted.
___
Answered on September 29, 2015 22:36:50 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/131699036478



## ..anyway, down w Kim but Kanye for president, I say!
### asked by Anon

Pt3 - how is it now I am the goto point for all discussion of this topic? I know nothing about them! Go ask the people making the feminist claims what they think!
Also, "don't hate her, she's just not interesting... Down with Kim!"
___
Answered on September 29, 2015 22:25:29 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131698400574



## ....the poster kids for lefties but Kim seems to be the poster girl for a lot of feminists? I havent gotten the "3rd degree" when I've said I dont care for K but some have questioned me further about it ie asked if I hate pop culture etc. I dont, just Kim, altho I dont hate her just not interested..
### asked by Anon

Pt2
___
Answered on September 29, 2015 22:22:51 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131698383678



## I don't like agreeing with the crazies on yr ask.fm but I do a bit w the guy ranting about the Kardashians - watched an ep of their show years back & they were being extremely patronising to a homeless man. It made me sick & I've loathed them ever since. Anyway, I dunno about the K's being.....tbc
### asked by Anon

Pt1
___
Answered on September 29, 2015 22:22:42 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131698292286



## Do you miss teaching?
### asked by Anon

Yeah, I do. There are many things bad about it (particularly the courses / course structure at my uni), but getting that flash of understanding on dem kids faces is so good.
___
Answered on September 29, 2015 12:49:29 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/131685725758



## "I don't have to care about/research the thing I'm arguing about." - Ancient White Proverb. I have a question this time tho: this made me reflect on how unusual it is for men esp to not see arguments as win/lose situations. How did YOU learn that, or did you never invest your ego in your debating?
### asked by Soul James

lmao.
I'm not sure that discussion was the best example of not seeing it as win/lose mentality. For someone to declare they have won an argument is antagonising to me, generally because people only say that in annoying contexts.
For me, it really depends on the person I am arguing with (or rather, what they are saying). If they are stubborn, evasive, or not actually arguing, I get annoyed, and so even if they do make a good point, I'm less likely to admit to it. If it's a proper argument or debate, even if heated, I'm relatively happy to concede to good points and either stop, or move on to other territories.
So yeah, even that description is still in a pretty win/lose mindset. But anyway..
This is most likely derived from my scientific training! This is because you either have to:1) argue with people who potentially know a loooooot more than you, so antagonising them is dangerous, or2) You are arguing with your friends/colleagues about something, and so you don't want to be a dick.
In either case though, your overarching goal is to either learn something, or find out what is wrong with your work, so getting jacked up ain't gonna help.
___
Answered on September 29, 2015 12:48:14 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131685373246



## Nah, my victory flag is firmly planted, m8. I have no doubt yr mate takes my dislike of KimmyK as firm evidence that I support Armenian genocide. As for my "refusal to look", I don't have to care about/research every celeb - even if I found out the K's come from a long line of Lefties.....pffft.
### asked by Anon

OK, well don't ask me more questions then.
___
Answered on September 29, 2015 11:50:04 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/131685059902



## There's nothing to take up, his hysterical over reaction proved my point. I win. Thank you & good night!
### asked by Anon

Come on, you can't leave it hanging like that. You gotta at least ask him a question or deanonomise!
Also, you addressed none of the criticisms I had with your points (e.g., refusal to even attempt to look into it), so I'd be less hasty with that victory flag, matey.
___
Answered on September 29, 2015 11:40:27 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/131684899646



## Yeah, but yr mate just proved my point. I say I don't care for the K's & he rushes in implying I have something against all black celebs? Wtf? I don't care if Lefties like them, but not liking them doesnt make you a terrible person.
### asked by Anon

Well take it up with him, then!
___
Answered on September 29, 2015 11:27:17 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131684574014



## I only see one pissant who can't fight his own battles here, mfer. ALRIGHT I'M OUTTA OLYMPUS' BUSINESS! Peace *mic drop*
### asked by Soul James

oooooooooohhhhhhhh shiiiiiittt
___
Answered on September 29, 2015 11:24:42 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/131684567614



## Nah, I love Goodes, was devastated/ashamed at the treatment he got. I don't care for/have no interest in the Kardashians - just hate ppl reading more into it than I just don't care 4 them, there's no conspiracy. You can dislike other celebs, no worries, but u seem to get 3rd degree over disliking K.
### asked by Anon

Well that is news to me as well (getting the 3rd degree), as pop culture internet seems to take the piss out of both of them all the time. But on the point of reading into too much - how can you make that claim when even you have said you actively don't want to know?! Maybe they are massive socialists, and there is a good reason for lefties to like them? Idk, but if I was "dismayed" or "hated" people for it, there is an easy way to resolve or relish those feelings, accessible via the link I posted earlier.
___
Answered on September 29, 2015 10:58:11 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/131684414014



## Those pizza toppings seem really good - fuk thick crusts though!
### asked by Anon

No way, forget about it. So dry!
___
Answered on September 29, 2015 10:45:10 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131684303678



## Oh FFS! I wasn't complaining about "black celebs" in general. There are HEAPS of boring white celeb dudes. You seem like a nice guy, Olympus, you should dump that Soul dude who thinks ur not capable of fighting ur own battles.
### asked by Anon

Well I'm still not sure what your actual complaint really is! Here is what I got from you: I don't like the Wests, or anyone who does (because they must be faking it for some PC reason), and I know this to be true, because I just do.
You may be honest in your declaration of non-generalisation, but it stinks of Adam Goodes style "we're just booing 'im cos he's shit!". Like, yeah maybe, but it's pretty coincidental.
You should either ask Soul questions, or ask not-anon so we can all understand each others view points better.
___
Answered on September 29, 2015 10:44:39 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/131684291646



## JFC all I hear are fucking noisy brogressive white boys complaining about black celebrities being popular because they're "boring," as if all the famous white shmucks on television are constantly electrifying.

Sorry I keep fighting your question askers by proxy, they're just so rubbish. :(
### asked by Soul James

Haha, gotta work hard to get questions like mine, boy!
___
Answered on September 29, 2015 10:33:25 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/131684205118



## When you said yr "mostly fairly sheltered, even from meeting new people", what did you mean? Do you live in a dungeon or something?
### asked by Anon

No, just new people don't come into my life very often. I'm not teaching anymore, so I don't meet students; I have a stable group of friends with long term partners, so nothing really exciting there. I'm medium at meeting new people: sometimes I can click right away, sometimes it takes me a long time to warm up.
___
Answered on September 29, 2015 10:31:59 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131636354110



## Is ham & cheese still your pizza of choice? Do you like thin or thick crusts?
### asked by Anon

I always like thick crust. Nowadays I don't get fast food pizza, I either make it from scratch with the bread maker, or order from local pizzerias. I either get a good magarita, or eggplant, mushroom, cherry tomato, artichoke pizza.
___
Answered on September 29, 2015 10:28:13 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/131683438910



## Have you booted any trees lately? Like you did when you were a deliquent child?
### asked by Anon

hey buddy, i appreciate that you're trying to be fun and friendly, but this question comes off as very aggressive from an anon.
___
Answered on December 21, 2015 20:07:14 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/133582151742



## Break given. It is says Real Name: Unknown so maybe his real name WAS Slink? An interesting looking dude. His DURABILITY is a sad 2 tho :(
### asked by Anon

Perhaps.
___
Answered on December 16, 2015 07:08:49 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/133477124670



## I could really hit a Splice right now, thanks...
### asked by happyhaps

humph
___
Answered on December 16, 2015 07:07:23 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/133477153086



## Of course I'm now obsessed with finding this Slink dude. All I've come across so far is Adam Slink (DC comics) but he doesnt fit your description :/
### asked by Anon

In a flash of clarity this morning, I now remember that the character I was thinking of was actually called "Splice".
And whoa!! This was the actual card I had! http://www.littlestuffedbull.com/images/comics/bubblegumcards/splice.jpg
Anyway, I think I tried Splice for a while as a screen-name, but we have an ice cream called Splice in Australia, so it kinda lost its scary/intimidating-ness. I think from this, Slink eventuated.
Also, if you are a kid, and don't know about 'slinky' dresses and stuff like that, it just implies sneakyness and stealth, so gimme a break.
___
Answered on December 16, 2015 06:12:31 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/133458378558



## Pls explain this Slinky business. Does you walk in a slow, sinuous, provactive & sexy way? Or does you cartwheel down steps like a slinky. Which one is it, m8?
### asked by Anon

I had a collection of super hero trading cards when I was a kid; Marvel or DC, not sure - but one of them was a dude called Slink, who had a black and green outfit, with Assassin's Creed style knives, but on the outside of his arms (and they were thicker), and I thought he was mega cool.It seems the Internet has no record of this character :(
___
Answered on December 15, 2015 11:52:51 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/133434970430



## Your screen name was "Slink"? Slink?
### asked by Anon

correct
___
Answered on December 14, 2015 08:40:33 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/133434569534



## What score did my favorite Lefty get in the political quiz? Let me guess - A Pot Smoking Greenie Communist?
### asked by Anon

Hey, you're back, fantastic.
You are a: Socialist Pro-Government Interventionist Bleeding-Heart Libertine
Collectivism score: 67%Authoritarianism score: 17%Internationalism score: 33%Tribalism score: -100%Liberalism score: 83%
___
Answered on December 14, 2015 05:18:01 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/133432777022



## How did you come up with 'OlympusMonds'?
### asked by Anon

Unfortunately, there is an answer to this in my stream, but Ask.fm has pretty rough scroll back.The biggest volcano in the solar system is Olympus Mons, which is on Mars. A single letter addition makes it look like my name a lot. It also replaced my old screen name of "Slink", for which I received a lot of flack for once out of high school.
___
Answered on December 03, 2015 22:04:32 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/133119962942



## HAPPY BIRTHDAY, MONDY
### asked by happyhaps

thanks m8
___
Answered on November 03, 2015 11:12:57 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/132505591358



## Do you think ur funny? Can you make others laugh? How would you describe ur sense of humour?
### asked by Anon

I can get a chuckle. My humour is often quite sassy, perhaps too much.
___
Answered on October 26, 2015 08:57:30 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/132218528574



## That's pretty impressive. Is your weight stable, if you don't mind me asking.
### asked by Anon

Like my body weight? Since I started I put on 7 kg of weight (putting me at a scary ~110 kg!), but apparently I look a bit thinner, in the fat department. I am by no means lean.
If you meant lifting weight, it had plateaued for a bit, but we're doing some isolating exercises to fix that up.
___
Answered on October 26, 2015 08:56:53 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/132316067902



## How's the weight-lifting going?
### asked by Anon

I just had a two week break, because my forearms were really sore, but like, in the bone part (or so it felt). Even things like pulling my belt tight was kinda painful.
So I took a break, but I just started again today, and managed to bench 75 kg for all 3 sets, unassisted,  no problems. This is surprising, because my PB was 80 (as in, I could only juuuust do it), and I haven't been in 2 weeks. I then followed it up by doing unassisted dips, which I have never done before.
So there you go.
___
Answered on October 26, 2015 08:50:08 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/132316017982



## Yeah $6k is serious coin. In my world, at least. What do you mean by flights? Are you not getting married in Oz?
### asked by Anon

Cananda, mate.
___
Answered on October 21, 2015 06:54:50 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/132197587262



## You don't look very happy bout it in the pic, but it looks good. You look totally different but.
### asked by Anon

Thanks mate.
___
Answered on October 20, 2015 11:20:11 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/132127232830



## R u still saving up for the ten grand you need to spend on a wedding photographer?
### asked by Anon

We managed to find someone who would do it for a lot cheaper. It looks like the entire wedding may come out < $6000 (except flights), which would be very good.
But also, seriously, my motorbike was $8k new, and it was a lot more useful than a wedding. I'm not begrudging the wedding here, because I do want it to be good and fun, etc. But this is serious money we're talkin' - it's pretty nuts.
___
Answered on October 20, 2015 11:19:59 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/132177284414



## R u and Kayla married yet?
### asked by Anon

nuh
___
Answered on October 20, 2015 09:06:26 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/132177264190



## Have you shaved your beard off yet? PAP!
### asked by Anon

yeah like 2 weeks ago
___
Answered on October 18, 2015 08:19:13 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/132127094590



## What's a topic that you could just go on and on about, but no one else ever wants to discuss with you?
### asked by Anon

I guess programming or geodynamics. I got to the Sydney Python developers meetup now, so the first is covered OK. At uni, I do talk to people about geodynamics, but it's on a high level. It's more fun to explain it to a layperson or student.
___
Answered on October 18, 2015 02:25:32 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/132123266878



## How good of a listener are you? Do you ever find it difficult to stop talking and give the other person a turn?
### asked by Anon

I am excellent. Why would I want to listen to me talk, I already know what I think. The role of talking for me is to help the other person either explain themselves better, or help them come up with new ideas.
___
Answered on October 18, 2015 02:23:10 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/132098010174



## Yeah, I realise that once I ask a question, especially anonly, its in your hands to interpret as you wish. That's fine. I'm keen to check out Gone Home. Any other games you'd recommend as 'different' from the mainstream? But still good, of course.
### asked by Anon

Have a crack at gone home and see what you think.
___
Answered on October 18, 2015 02:21:32 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/132077701694



## What do you do when you feel lonely?
### asked by Anon

I don't often feel lonely, because I'm quite happy to be alone. But when Kayla does go away for a while, I typically just get engrossed in something, like TV or computer.
___
Answered on October 17, 2015 01:31:36 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/132096804158



## You got me there, I meant to write poem format, not limerick, but OMFG, we can't even agree on sauce! *sigh* Goodnight, my work is not done, obviously, but I need rest.
### asked by Anon

No probs, just being pedantic. I actually write a lot of limericks, typically in < 5 mins, so I know the format well.
Yeah, it looks like our culinary circles will never intersect.
___
Answered on October 15, 2015 12:09:11 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/132058064702



## Pt2 - I really just want to have fun & kill erryone! I don't have a problem w the insane body proportions of female characters, its just fun. Can u elaborate a bit more about what you'd like to see in games re equality? You said before that you want the main character swapped for a female %50.
### asked by Anon

Part 2OK, good to know I guess, but @SoulJames has really well explained how when you ask an anon question, it no longer is your question, it is mine. And as such, I can do whatever I want to it. And I did. But yeah, assuming this is the same anon, fair enough.
You feeling weird about killing women but not men is not a great sign - but it is definitely something I do/have suffer(ed) from in the past. My most distinct memory was playing Saints Row, and being weirded out because I was mowing down female police. So yeah, as I said before, this is not a great sign of our society setup. Why is it OK to kill men (in games) and not women? Would it be better if you/I DID feel OK murdering women? Perhaps it would be better if we felt bad about killing men (or more generally, killing humans), because it's pretty nuts that it is so mainstream. If you think of movies, sure there is lots of killing and torture porn - but it is a specific genre, rather than the dominant one.
In regard to your point about critique of games and fun - these are not mutually exclusive things. I did enjoy the new Tomb Raider for the most part. Thinking about it is also enjoyable, in a different way. You should do both.
Re: saucy ladies - yeah, it may not be a problem for you, but a lot of people are saying it's a problem for them, so some variety might be nice. In the future, your desire for saucily clad ladies killing hundreds will no doubt be its own genre. But as I said earlier, at the moment it's almost the only genre. So you can see why people would be demanding more.
On the elaboration: no I said I DON'T want just a superficial swap out of man for woman. Given that women are 50% of the population, and they are different from men, one might imagine they have some interesting things to say or do that may be very different from what we currently always see. For example, Gone Home was a game with a female lead designer, female main character, and female antagonist(??), and it was super different and super good. No killin' either!
___
Answered on October 15, 2015 12:07:59 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/132048643902



## My q wasnt meant to be antagonistic, genuinely curious. I would feel weird playing a game where I killed only women, but am cool with only killing men. The diff between punching up & punching down maybe? Dunno. With all the talk/critque of games I've just been thinking about it a bit more but - pt1
### asked by Anon

Part 1
___
Answered on October 15, 2015 12:06:30 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/132048451134



## HAHAHAHA! I wrote it in a limerick format, like you did yrs, but ask.fm didn't post it that way - asshole.fm amirite? Can we agree on sauce at least? Barbecue, yes?
### asked by Anon

Uh oh, on multiple levels.
While what I wrote to you was not good by any stretch, it did attempt limerick format - that being:
7 syllables
7 syllables
5 syllables
5 syllables
7 syllables
Where the last word of all the 7 syllables rhymes, and the last word of the 5 syllables rhymes.
So while I did appreciate your poem (once I realised it was one!), it ain't no limerick!
Secondly, BBQ sauce is ONLY (and I mean OOOONNNLLLYYY) good on a kebab with garlic sauce. At all other times it should be banned. For nug consumption, Maccas sweet and sour all the way.
___
Answered on October 15, 2015 11:51:26 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/132057656382



## dunno Olympus, for the hundreth time, IF YOU DISABLE THE ANON FEATURE YOU COULD KEEP TRACK. anyway, im still working on my fred account. ill be back!
### asked by Anon

Oh, I finally see where you are coming from now.Even still, I'm not the one who cares much about this - I said it to Soul, and he made some good points, and then I have been blasted by anons freaking out about it ever since. I'm just answerin', not being annoyed or anything. As evidenced by all of this, if you ask me an anon question, I will answer it as per usual. I was just saying it's easier for everyone if you're not anon.
ninja-edit: being non-anon would also force me to not assign random personhood to each question. I'm sure that would be satisfying for a hyped-up teenage Canadian woman like yourself.
ninja-edit2: turns out anything in < or > brackets gets wiped!
___
Answered on January 17, 2016 12:15:50 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/134192120382



## no! i'm the spanish woman studying economics. stop misgendering me please. seriously, all these people whining about anon questions, yet still answering them are hypocritical af.
### asked by Anon

Yeah wow, if only there was a way to keep track of who was asking the questions, but still remain unknown to the recipient.
___
Answered on January 17, 2016 12:04:41 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/134192071742



## hola!
### asked by Anon

This must be from a Mexican middle aged man, who works in a factory.
___
Answered on January 17, 2016 12:00:56 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/134192013630



## what kind of weird qs are you getting that make you so obsessed with this, Olympus? you do know where the disable anon feature is, don't you? do it.
### asked by Anon

None, I have only deleted questions out of boredom, not weirdness.
Let's be clear - I am not obsessed with this, I just said: "I don't see why one would be anon, when you can make an account that no-one would know anyway", and then people came out of the woodwork telling me I'm crazy for suggesting such a thing.This series of questions has illustrated that this would be useful - have you asked me all the previous questions? Did you ask some? Is this your first question on the topic? Who knows? Even if you ask another question to clarify this, I can't know it's you. This isn't a big deal for me, as I said, but it just makes it harder for you to get a good answer out of me.In the spirit of this whole thing, I've decided that you are a young woman from Spain, who is at university studying economics. Who would have thought you would be asking an Australian fella such a thing?
___
Answered on January 17, 2016 11:58:06 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/134191853630



## I like to ask questions anonymously in case they're stupid af. Or if I don't want our existing relationship to colour their answer.
### asked by Anon

See my other answers about having a pseudonymous account. You still will not be associated with the dumb af question, and there will be no colouring.
___
Answered on January 17, 2016 11:33:16 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/134156337214



## Uh I'm pretty sure everyone on here has a registered account anyway, we all have "pseudonym accounts," just some are posting questions anonymously. :\
### asked by Anon

Yeah, but I'm talking pseudonymous - where you're pseudonym can't be used to identify you IRL (my account name is definitely know to peops on this site).I'm going to imagine that you, anon, are some random dude from Chennai, India. You're account name is "Tamil_is_the_best". You have never answered a question on your own account. None of your friends in India use this site.If you had not ticked the anon box, I would have equally zero idea who you are - but if you asked a follow up question, or at some later date asked me a different question, I would be able to know you were the same anon.That's it, that's all I'm advocating.
___
Answered on January 17, 2016 11:32:26 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/134172583486



## I might make a pseudonym account just for you, Olympus, because I like you/will like tormenting you. I'm thinking 'Fred'. I think adding a name protects you from well deserved abuse sometimes. I've seen friends ask boring qs to their pals, qs which should've got a sarcastic ans but didnt bc name.
### asked by Anon

I look forward to it.
___
Answered on January 16, 2016 06:27:55 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/134157668670



## No, I don't think ask.fm do that (list). If u think you're getting multiple qs which are annoying & u suspect are from the same person, maybe block em? Sometimes, I get distracted when a name is attached to a q tbh - not on mine, on other people's.
### asked by Anon

Gotta live wild - I recommend you make a pseudonym account right now, and ask some hectic questions, and revel in the questioning continuity that results.
___
Answered on January 15, 2016 15:07:38 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/134134034238



## Sorry to jump in, and anonymously at that, but I'm still not sure what your beef is? I think inferring that anyone who asks a q anonymously does so for dodgy reasons is not only wrong, but extremely rude and arrogant. My question for you - why have you not disabled the anon option if it bothers you?
### asked by Anon

Whooa, buddy, chill out. I'm not saying one shouldn't ask anon questions, I just don't see the point - if you make an account with a random username, you are functionally still anonymous (or pseudonymous, as I have recently learned).
To clarify: - Using ask.fm's anon functionality: seems pointless - Making an account that no-one knows is you: fine
There are many reasons why this is better than straight up anon, but largely it's this: almost all anon questions I've gotten have had some form of follow-up question. Was it by the same person? Sometimes they say it is, but how would I know? It could be someone else. If you just attached a name - any name - it would save you space in your question, and I could address it better.
Since Ask.fm does not let users easily see what another user has ASKED, if you never answer any questions that you get on your pseudo-account, you will be essentially unknown to everyone except the people you ask questions of.
Finally, asking anon questions is just riskier for you, the asker. To quote @Soul_James, once you ask an anon question: it's no longer your question, it's mine. You lose all rights to protest about misinterpretation or whatever - if I decide you're a huge butt from my reading, then guess what - any follow ups could be some other random anon, and that question is left hanging as a testament to your bad questioning skills. "Oh but that's the point of anon, that's what it makes possible!" - sure yeah, but I don't care that it's left out, other askers have even had them as badges of honour almost, to show off the ridiculousness they deal with - only you know how crap the question is, like a splinter in your mind, driving you mad.
___
Answered on January 15, 2016 15:06:08 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/134138913086



## For the record, what you're describing is being "pseudonymous," not properly "anonymous," which is when there's NO identity whatsoever. Having a fake name is a pseudonym.
### asked by Soul James

Ok, good to know. I think my general points still stand under this change in definition.
___
Answered on January 15, 2016 04:09:43 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/134137635390



## I guess the option to return fire is in your response? I admit it'll be good to know in the way of how many qs a certain person was asking but as I said before, you can switch anon off so people have to attach a name. Anyway, this & my other q was meant to be mostly humourous so I'll add a smiley :)
### asked by Anon

But you can not answer any return fire questions, so your just an asking-named-anon-machine. In fact, does ask.fm even let you see what questions a user has asked? Like in a list?
I would say the only reason to ask anon-anon questions is if your previous questions have made the person want to not answer your questions - and if that happens, you should probably stop anyway.
They should make it that if you do ask an anon question, it gets assigned a random username, and then if you want to ask a followup or whatever, you can choose to use the same random name again. Or force people to have the same random name if the ask another question within a short period.I suppose it would make the questions that are broadcasted out feel stranger though.
___
Answered on January 14, 2016 23:06:57 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/134133695806



## Does it seem a bit weird when you think that you're going to be a husband, have a wife even though it's kind of just a formality?
### asked by Anon

No, not really. I imagine it would be weird for couples who don't live together before marriage and stuff, but that's probably because everything is crazy different.
___
Answered on January 14, 2016 22:51:32 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/134118285374



## Did u really just ask someone why people chose to ask questions anonly on a site specifically made for doing just that? Olympus pls. What are they teaching kids at uni these days? If it really bothers you can switch anon off so people can't.
### asked by Anon

Yeah, I did.
If you make up some random name for your account, you ARE anonymous. No-one would know it's you.
The only drawback would be that people could return fire, or at least be able to track what you had asked. If you are recognisable from your writing style, then you should try not to be..?
___
Answered on January 14, 2016 22:50:21 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/134133370686



## Just a quick thanks for the worm farm info! No need to 'answer' this. I *was* worried about the smell tbh!
### asked by Anon

;)
___
Answered on January 07, 2016 09:20:19 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/133963208766



## "I can get pretty meaty when left to my own devices" LMAO. Would you like to rephrase that?
### asked by Anon

no u
___
Answered on January 07, 2016 05:28:33 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/133961750590



## Is this vego thing permanent? I went mainly veg as a student bc lack of dollars rather than ethics tbh. I eat meat occasionally now & mostly prefer veg options anyway. Ppl eat way too much meat.
### asked by Anon

2 months. i can get pretty meaty when I'm left to my own devices, so this is a good goal to have.
___
Answered on January 07, 2016 04:47:26 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/133938246206



## I'm pretty keen on getting a worm farm. How long have you had yours? Sometimes things seem like a good idea but turn out not to be or too much trouble. Can you keep them inside? We're in a small unit.
### asked by Anon

We have had ours for like 3 years now. Randwick council were giving them out for free, so i went and got one.it's almost no trouble. we keep ours in the kitchen, and it doesn't smell or anything (i also have a small unit). you can't put all your scraps in - things like meat are obviously a no go. the only issue we had was we left the drainage bucket unemptied for too long, and mozzies started breeding in it!
___
Answered on January 07, 2016 04:46:37 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/133961497406



## You said you "freak out" about wastage and are down with anything that's not useful. How does this reflect in your day-to-day life? Are you a keen recycler, do you avoid packaged foods, buy second hand, cut down on meat eating etc. or are you all talk, m8!
### asked by Anon

I said it "kinda" makes me freak out. And xmas is the major time for such needless waste.In my day to day, I do recycle, I gots a worm bin for my scraps, almost all my furniture is old (aka second hand), I'm vego at the moment (for 2 months at least). In terms of wastage though, I'm not ridiculous about it. Like, a coke bottle is useful enough in holding liquids that I'm not opposed to them. It's the crap I'm not keen on. Here's a good example of the stuff that "freaks me out": toys from averagely priced christmas crackers - do we need them? Do we need to extract oil from the ground, ship it to a refinery, refine it, send that to some factory, get it made, ship it somewhere else, blah, blah, blah, just so I get some rough toy that I barely look at? No, we do not.
___
Answered on January 05, 2016 09:06:53 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/133917194046



## Do u reckon you'll get married this year?
### asked by Anon

I f'in hope so, it's all booked and stuff.
___
Answered on January 05, 2016 08:57:34 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/133917200958



## Do you have any weights at home or do you do all your work at the gym? Also, did you get a PT to initially show you how to use weights correctly? If yes, how many sessions did that take?
### asked by Anon

Nah, I had some weights at home, but it was not enough, and I never used them. All my working out is at the gym.Also no PT, cause I think I thought it was embarrassing, and it's also expensive. My friend and I just kinda started doing stuff based off experience when we were teenagers, but we sucked pretty hard.When we started doing more complex lifts (deadlifts, squats), I started watching some videos online. There is a TON of so called 'broscience', which is essentially buff-dude anacdotes about their gains, and they can often be hyper-aggressive in their masculinity or mega-hyped, which I really don't like. However, there is a dude called Alan Thrall who I really like (example vid:
Not really sure why - perhaps it's his attitude of like "do what you want, asshole, but here is how I do it, here's why, and here are common pit-falls". He isn't really about the looks, just the strength and technique.Anyway, I learned from him that our squat technique sucked badly, and we fixed it from the vids. When we first started doing deadlifts, a mega-buff/fit dude kept looking over at us, and eventually asked very politely if he could give us some advice. We totally accepted, and he helped us get the technique right, which was super helpful. We saw him again a few months later and asked him if we're still OK, and he was very happy with it. So that was nice.Anyway, I took like a month off over Xmas (yikes). I went back yesterday, and now I am sore.
___
Answered on January 03, 2016 13:14:12 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/133299768126



## Do Lefties, especially Big Lefties, even celebrate Xmas? All that consumerism....
### asked by Anon

Yes I did, and you joke, but I do get pangs of despair seeing the extreme amounts of crap being sold.As mentioned long ago on my Ask, seeing how environmentally crazy producing copper is, and knowing that other things like plastic are similarly hectic, it kinda makes you freak out about the wastage - particularly for things like wrapping paper, which are supposed to be waste - but really anything not useful I'm pretty down on.
___
Answered on January 03, 2016 13:03:53 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/133645045822



## Did you spend much time at the teenage refuge your dad worked at when you were young? Visiting, I mean. If yes, what were your impressions? Did you understand when younger what some other kids went through & how lucky you were?
### asked by Anon

I was only around 3 or 4 at the time, so I have limited memories of the place. I did used to go with him somewhat regularly, since Mum and he were both working. I liked it well enough, it was a pretty cool house - lots of rooms, stairs (my home had no stairs), a big TV, and a cat called Lucky. We ended up adopting Lucky, who became Rosie, and she was my cat for 19 years after that, so that was definitely a plus.The kids I met were always nice from my memory - as I said, one of them gave me their trading card collection, and another one gave me a little transformer toy which I still have somewhere.I definitely did not understand how lucky I was, or rather, how heavily crapped on these kids were. To be in a teenage refuge typically means you've been through more than a few foster homes, and the system has kinda given up. To my young self though, everyone seemed nice enough though.
___
Answered on January 03, 2016 12:58:06 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/133752560190



## Do you take selfies much?
### asked by Anon

Nope.
___
Answered on January 02, 2016 10:30:31 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/133556616766



## Ok, sorry mate, didnt mean to come across as aggressive. You win some, you lose some. I'll chalk that one down as a loss.
### asked by Anon

yeah, i guessed you weren't trying to be.
Not heaps of wiggle room in the original question though either.
___
Answered on December 21, 2015 21:42:55 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/133593936190



## Were you a hard core trading card collector? I would've sold my kidney for one when I was into it.
### asked by Anon

nope, a guy at a teenage refuge that my dad worked at gave then to me. i appreciated it a lot, and used to read and look through them a lot (it was a stack of cards about 2 inches thick), but that was about as much as i got into them.
___
Answered on December 21, 2015 20:09:48 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/133534741822



## Humility is as admirable a trait as any, but too much leads to uncertainty, & from there, inaction. Especially when everyone codes good deeds they don't want to perform as arrogance or stupidity. "Avoiding suckitude" IS nobility, whatever your motive, if it's too high a price for ur peers to pay imo
### asked by Soul James

F*ckin' tall poppy syndrome, m8, gotta keep this sh*t under wraps, ay.
Thanks for the words.
___
Answered on February 21, 2016 12:12:24 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/134860190270



## Yeah, while smugness IS a bit douchey, it at least avoids a direct conflict & leaves a channel open for them to later be like "oh yeah I get it now, I sold my soul to Rio Tinto & now Satan himself has repossessed my company Hummer why did I lack your ethical fortitude" etc
### asked by Soul James

Indeed.
___
Answered on February 21, 2016 12:07:40 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/134859714366



## Ok, but anon or not I dont see why jumping into violent imagery is ok? Anyway, no need to answer this, I was just a bit surprised. Thnks.
### asked by Anon

See each other's answers.
Imagery is used to get the message across.
___
Answered on February 21, 2016 12:06:35 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/134860069950



## Yeah fair comment, to be clear it was a warning, not a threat. I'm not drilling anyone, but this is what happens when everyone sits on the fence waiting for everyone else to do something: "everyone else" can be p indiscriminate. Better that ppl make the effort before drastic shit ensues.
### asked by Soul James

See each other's answers.
___
Answered on February 21, 2016 12:05:56 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/134860031038



## I also respect ur decision, because it's the more morally-righteous. That doesn't mean if you hadn't done it, you'd automatically suck. What DOES auto-suck is ppl trying to imply what you did wasn't brave or righteous, so they don't have to follow ur example. Same goes for vegans, incidentally.
### asked by Soul James

I dunno, I feel given my mega privilege, it would be kinda sucky for me to have taken it.My actions are driven by not wanting to be a sucky person, rather than nobility, hence the extra questioning of other parts of my life.
I have complex emotions around answering this question, not sure why.
___
Answered on February 21, 2016 12:05:05 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/134859381054



## Can I say one final thing please? I don't think making a polite comment warrants a friend of yrs saying I'll get a "drillbit in the brain" and u and others liking it. It might be nice if u and yr friends showed a bit of "ethics" in other regards ie towards people if ur gonna make it yr thing. Cheers
### asked by Anon

First of all, I don't think it was implied as a direct threat, more an illustration of what Soul was trying to say: attitudes such as yours, which is essentially the status quo, don't encourage change in our society (hence my unsteady resolve on my decision, thanks). So when people get sick of this, and it all comes to a head and revolt, those who held said beliefs will presumably be held accountable.So it's less saying that we'd want you to be drilled, and more a prompt for you to consider your views. I understand it's not a great format, but you didn't give much reasoning behind your ideas, and why I was indeed a noob.
Secondly, as I and others have said before, if you're going to ask anon questions, then get ready to lose all rights associated with it. Without context, there can be no barometer for your intent, and so each person must make up there own interpretation. I have known Soul for almost 25 years, so I read his question much differently to you (I presume).
___
Answered on February 21, 2016 11:57:35 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/134859769662



## I think bringing about change by setting a strong example is more feasible/fruitful than the sleeper-agent approach. I've been called naive for acting on my values, and they've come back later saying "I get it now". Don't give them any ground imho - they'll find it harder to BS themselves.
### asked by Nebby Nebula

Yeah, I do agree.
I guess my reason for asking originally (and now I wish I had asked more broadly) was that I felt I didn't have the tools to defend/explain my actions from adversaries or even friends and colleagues - and that made me worry if I was even sure I hadn't made the decision lightly.
As Soul said, it is kinda awkward with some people now who have done the opposite of me. They all have reasons, and they can be good (e.g., beggars cant be choosers), but some aunt so much. I dunno, I don't want to be an asshole about it, unless someone is actively giving me shit. I suppose as you say, they can always ask me if they change their mind.
___
Answered on February 21, 2016 11:33:24 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/134858004798



## 2/2 than what u purport to stand for. Just like the fact u arent a full vego/vegan doesnt mean u dont care for the environment. You are a young man about to get married, maybe start a family. Money, job security etc are very important imo. Anyway, again I respect yr decision.
### asked by Anon

I'm just gonna post these.
___
Answered on February 21, 2016 11:11:43 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/134857603390



## I appreciate you taking the time to respond to a nullified anon! Don't get me wrong, I respect yr decisions and as you pointed out, you have to live w yr concious. I disagree that yr reasons for taking it, if u did, money/to stay in Sydney etc say that those things are more important to you 1/2
### asked by Anon

I'm just gonna post these.
___
Answered on February 21, 2016 11:11:31 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/134857578814



## where is the mythical 'difference-making person' who unlike every single normal person like you can actually 'make a difference?' Who is going to end Big Oil all by themself? Everything happens by individuals imho, you did good.
### asked by Anon

Thanks bud
___
Answered on February 21, 2016 09:19:37 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/134857563966



## Look at this way, then: when ppl finally get fed up & inevitably over-react to a problem left to fester way too long, your anon friend is going to get a drill-bit in the brain, while you'll be held up as an exemplary citizen. So that's your consequentialist amorality argument covered.
### asked by Soul James

Let it be known.
___
Answered on February 21, 2016 09:08:23 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/134857499966



## I think you're a little bit of a noob albeit a good intentioned one. Nothing changed by you not taking that position. More than a hundred others were lined up for it. Change does happen from within.
### asked by Anon

Despite the anonymity nullifying it completely, I appreciate the risk you took of asking this question right after Soul advised me to be smug AF to anyone questioning my stance. I admit though, your question matches more my line of troubling thought - would my personally major action have little to no effect on the world?
Excluding deeper thought, I think the thing that made me turn it down (regarding the oil stuff) was that I knew I wouldn't feel good. If someone who I respected asked me to explain my actions, it would essentially have to be: I need money, I want to stay in Sydney, and I want a job in academia, which therefore implicitly says that those things are more important than all the stuff I purport to stand for. In fact, this did happen - when the whole idea of this job was kicking around, I ran into my old boss on the street, and excitedly told him about this great opportunity. He asked me to explain myself, re the moral implications, and I tried and failed miserably. I was trying to make the double-think stick, but he would just immediately pierce it. This was like a 5 minute conversation, btw. His non-malicious question sent me off on this downward spiral to eventually declining the job. I recently bought him a coffee to thank/curse him for this, and he then tried to unconvince me with some of the arguments in my original question plus some others. While he was doing this in the spirit of debate, I wasn't pleased by this confusing turn, but anyway. He's a good guy.But in summary - I no longer have to worry about feeling bad for working for or with an oil company, so yay!
Regarding nothing changing, and 100s of others - as is not unusual in academia, my role was particularly specialised, and I'm pretty OK at it, so there are literally not 100 people in the world who could just pick up what I was doing. But yeah, I take your point, someone else can and will do it. My one gotcha is that now there will be a lag time of anywhere between 6 months to >1 year for that recovery (assuming they went down the same path as me). So something did change!
"change does happen from within". This was always the weakest argument to me. I really can't think of a good example of this ever being true. I've know people who were relatively green, they went to work for the mines, or an oil company for money reasons (but saying "change from within") and have come back saying "I just find it funny people wanting to stop oil or iron mining, and yet they're on their phones, and they're in their cars". I don't think this argument is fair: it pretty much requires adherence to the status quo, or complete removal of yourself from society. Obviously there are shades of gray, and one of them is to not work for a company I think is very crap, while still needing to rely (as a society) on their product. Also, companies are not democracies, so change coming from within is only gonna happen if you are big up in the company, and by that point you are indoctrinated AF.
___
Answered on February 21, 2016 08:48:28 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/134856774974



## I remember hiking along the creek bank and coming out in the scout camp and climbing over the fence to get out. I don't remember if the raft was part of that journey or on a different day. I can also only remember rafting to the far side though, and not back. I guess we walked back around somehow?
### asked by Nebby Nebula

hmm, it is a bit of a swamp of memories..
___
Answered on February 13, 2016 00:07:43 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/134622218814



## I'm chuffed that you remember our traffic-barrier-raft adventures. Them's some happy memories. Hazy though - did one of us fall in?
### asked by Nebby Nebula

Yeah, it was pretty rad. It was also hazy for me, I was worried you or Soul might tell me it never happened!
I don't think we escaped fully dry, as that would be pretty unlikely. I'm also not sure how much exploring we did on the scouts side. I also don't remember rafting back over to the other side! Did we walk through the scout camp?
___
Answered on February 08, 2016 10:31:11 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/134621327166



## Ok, ok, sorry. I'm going, fuking right off but have u thought that u might be reading them too abrasively? Cheers.
### asked by Anon

Yeah, I had thought about it, but then I realised it is not my responsibility to read a question non-abrasively, since I have no context in which to interpret your anonymous question. Because of this, I have complete power over how it is interpreted, and you have no power.
So in summary, if you would like me to answer your questions, it is completely up to you to phrase them better, not for me to change. The other alternative is to ask non-anon questions or asking me on Twitter, so I can build a context.
___
Answered on February 08, 2016 10:29:20 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/134620610366



## I'm finding this "raft" story a bit hard to believe. Are u shitting me? If not, how many peeps sailed down the river on it without dying?
### asked by Anon

Oh no, an anon doesn't believe my answer!
Why are you always asking stuff so abrasively? Here is a better way to ask the same question: "wow, that raft story is like one of those 1950s children's stories! How did it work, and did you run into any troubles?"
Look buddy, every time I get a question from you, I read it and feel immediately irritated. Can you get f*cked?
___
Answered on February 08, 2016 07:31:05 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/134619819070



## oohhhhh, sorry, let me unfark orff you, m8. i geddit now. i'll never achieve King Lurker Status. i need to be heard!
### asked by Anon

Hmmm... I'm not sure I agree.
___
Answered on February 08, 2016 00:47:50 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/134616981822



## fark orf, m8! the people luv and wanna see me tweet responses.
### asked by Anon

You asked how to be a lurker, I gave you the answer.Also, they probably don't.
___
Answered on February 08, 2016 00:43:35 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/134616929854



## what "exciting things" did you do at the creek?
### asked by Anon

i remember we found a plastic road block thing. one of the red and white ones that link together, and they fill with water.well, it floated, so we used it as a raft to goto some mega boyscout camp. pretty awesome.
___
Answered on February 08, 2016 00:00:55 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/134350373950



## Can u give me some lurking tips pls. You claim to be da King so give it up.
### asked by Anon

you know when you see something on twitter, and it either excites you, or makes you angry or annoyed? any sort of response, really. and in response to this, you want to write your own tweet, as a comment or retort?
just don't.
___
Answered on February 07, 2016 23:59:03 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/134598067518



## what enjoyment do you get from watching other people play games? i don't get it. why don't you just play them yourself?
### asked by Anon

Try it out, and perhaps you will see.
___
Answered on February 03, 2016 11:34:54 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/134528686142



## I thought you were Fred Flint? Somehow aliases have made this MORE confusing.
### asked by Anon

What
___
Answered on January 20, 2016 21:20:49 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/134267306558



## Latest eg. Was told I’m heartless for not sympathizing with Aussie’s who travel o/seas to join ISIS, then read of Leftists comparing willing ISIS recruits to sex grooming victims. This infuriated me. ISIS openly advertises enslavement, rape, torture. I have no sympathy for anyone attracted by that.
### asked by Anon

OK, let me preface this with saying I undoubtedly don't really know what I'm talking about - but I'm gonna give it a crack anyway!
Obviously you are right to a certain extent - ISIS is clearly not good, and joining ISIS is also not good. My interpretation of your view (and I guess more broadly, the right wing view) is that you would stop here. ISIS is bad, and Australian's who join ISIS are bad.
I think a more typical 'left' response would be to vaguely agree, but to ask questions. Why would someone want to leave Australia to join ISIS? Is it because of ISIS grooming, and if so, what could they offer that would be appealing? Is it because they have some link back to ISIS, or their territories? Is it because Australia is relatively racist, and so it's not great to be Muslim here? Is that particular person mentally ill?
I think it's probably reasonable to say that most of the people in ISIS, including recruits, wouldn't think they are evil or bad. They may have to do bad things, but only because of the circumstances, or because the West has boned their country, etc.. Herein lies the struggle between personal responsibility, and systemic/social forces. People are in control of their lives, but are also affected by their circumstance. Does this excuse their behaviour? I don't know, I would want to say no, but like I said, I can't imagine these people in ISIS literally think they are evil - so they must have good reasons (or social reasons) to be in ISIS, and doing stuff. Without thinking more about it, your options to deal with this issue are pretty limited: shutdown mosques, stop people going to the Middle East, send in the army, and so on. I personally wouldn't call you heartless on this particular topic, but I would try to extend the conversation past that they're just a bunch of d**kheads.(btw, how big is this issue? Are there hundreds of Australians joining ISIS??)
___
Answered on January 20, 2016 11:53:00 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/134250269246



## A pretty "big lefty" hey? What core Left principles do you identify strongly with. The victim mentality of the Left drives me fukn nuts tbh and I used to think of myself as slightly left leaning.
### asked by Anon

What's an example of the victim mentality you've come across?
___
Answered on January 19, 2016 00:45:20 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/134227399998



## Sup? Do u like my avi? I found it under "young man thinking".
### asked by Anon

indeed
___
Answered on January 18, 2016 02:56:53 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/134209622590



## I would like to request pt2 of the Soul/Tim fanfic please. Thank you in advance!
### asked by Anon

The pub had slowly been emptying over the night. Now it was just the bar tender, and Soul and his new friend. The bartender sighed, slowly cleaning a glass. He had seen Soul in the bar before, and the last time he had seen him this animated, the barman didn't get home to well past 2 am. He put the glass down and picked up another. Soul was now pacing back and forth in front of the fire, listening deeply to whatever this new friend was saying. His eyes were burning, almost brighter than the fire.
[this will do for now - I don't know if this should go more physically explicit, or explore the more emotional/intellectual relationship - or if it should continue at all! I feel I should have friendly nods from the parties involved. I also don't know much about non-hetero relationships, so I worry I would just add more misunderstandings into the mix.It has been nice to try creative writing though - I haven't done it in probably more than a decade (perhaps that is obvious)]
___
Answered on June 13, 2016 11:18:33 GMT

__Likes:__ 6

Original URL: /OlympusMonds/answers/136881075262



## have you ever been mad and nude online?
### asked by Anon

No, I don't believe I have been that specific combo of things online.
___
Answered on June 13, 2016 11:03:28 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/136980454462



## LMFAO!
### asked by Anon

¯\_(ツ)_/¯
___
Answered on June 10, 2016 01:15:56 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/136914521918



## Walked "past" :(
### asked by Anon

Yep
___
Answered on June 10, 2016 01:02:56 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/136914483774



## It's been a while, m80, but just checking in to see if you have tried the delicacy that is a KFC nug yet.
### asked by Anon

I literally just walked past KFC at town hall, and thought of you.
___
Answered on June 10, 2016 00:59:12 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/136914449470



## According to MY recollection, @TheGracchan eventually hit you with a shovel because he couldn't handle my rapidly-deteriorating mood due to nicotine withdrawals. -_-
### asked by Soul James

Yeah, they left out the option of being shovelled when they taught us about peer pressure and smoking and stuff in primary school.
I would say lesson learned, but I can be very annoying..
___
Answered on May 31, 2016 08:38:52 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/136720171582



## Have you ever asked someone else to quit smoking?
### asked by Anon

When I was about 11, I pestered the crap out of @Soul_James, using various annoying techniques  that were fundamentally about hiding them.
This was usually followed up with some sort of riddle or trail of breadcrumbs to find the cigs.
From memory, he loved it
___
Answered on May 31, 2016 08:26:51 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/136720152894



## Holy shit dude, that is PUNGENT
### asked by Soul James

haha, glad you like it. part 2 on request.
___
Answered on May 05, 2016 08:29:03 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/136252278334



## I'm not sure why there is a small contingent of followers, only on Ask.fm, who vaguely ship @Soul_James and I, but I eagerly await the weird fanfiction.

What would you title a Soul/Tim fanfic, and which young adult book series would you set it in?
### asked by Tim Marsh

Soul had been traveling for years now. It had been well over a century since he was turned, and while his pale skin and gaunt face had lead to more troubling questions back then, modern folk more tended to ignore him. Every now and then he would settle in a town or city for a while, but once he had enthralled too many people, either with his intellectual discussions or strangely compelling sexual appeal, it was usually time to move on before the authorities took notice.
This time was different though. A tall man, broad and tanned, had walked into the pub Soul was resting in. Even with all the people Soul had met over the years, something about this man stirred something in him he hadn't felt in a long time. Soul stared intently at the man. As he watched the man take a drink from the bar, he noticed that he seemed to move with more grace and care than expected.
The man noticed Soul. His gaunt face stood out from the background, a striking vision from the dreary pub. He took his drink to a nearby table and sat down. He tried to steal glaces of Soul, but each time he was met by piercing eyes. Perhaps encouraged by his drink, the man rose and walked over to Soul's table. "May I get you a drink? I'm Tim, by the way, nice to meet you."He extended his hand, but Soul hadn't reacted at all. Finally, Soul's eyes slowly moved to Tim's feet, gradually tracing their way up his body, lingering on certain areas a second longer. He finally extended his hand, and firmly grasped Tim's. His grip was strong, and his hand felt warmer than Tim had expected.Soul finally met his eyes. He spoke in barely a whisper, but Tim was fixated on every word."Hello Tim, yes, please sit down..."
___
Answered on May 05, 2016 08:24:25 GMT

__Likes:__ 7

Original URL: /OlympusMonds/answers/136248841534



## Is there one person in your life that can always make you smile?
### asked by Anon

My partner. She is a good person.
___
Answered on May 02, 2016 07:09:46 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/136196258110



## If you could have a video game made for you, what would it be like?
### asked by happyhaps

I was thinking about this the other day: an end of the world simulator.
The idea is you join a server (I'm thinking kinda MMO'ish, but maybe with a player limit of 50 or 100), and it's set in current day world. On the server is an "end of the world" variable, e.g., volcano nearby explodes, tidal wave, asteroid, aliens, zombies, bear revolt, reckoning, whatever you could think of. This EOTW cause also has a time frame, ranging from like a few hours to like 6 months or something, in real time.
No-one on the server knows what the EOTW cause will be - but they can try to find out. If it's a volcano, they could find seismometers to see earthquakes, if it's aliens they might be able to detect radio waves. (now I think about it - maybe when you join the server you inhabit an existing NPC - like, you are randomly assigned to an NPC, and they are a volcanologist, so you might get lucky). The data available to you would be relatively tough to understand - it wouldn't be something really obvious unless the server timeframe was very short.
If you manage to figure out what the EOTW is, you can then attempt to prepare for it. Nuclear war? Build a bunker. AI revolt? ...live in the woods, I guess. And so on. If you manage to survive the EOTW, your character is allowed to continue to a new world, with whatever riches or tools you have crafted (or maybe just visual appearance stuff, so everyone knows you're a mad veteran). These rewards would have to scale somewhat to the time frame of the EOTW - someone who lived through a 6 month campaign may deserve more, etc. Perhaps there would also be dynamic challenges, so only a few people can actually make it through.
I'm imagining the dynamic between players is a balance of:a) they all want to know what the EOTW is, but there are too many options to feasibly find out what it is by themselves, so you need to work together, butb) if you do find out what it is, should you share this info? If it's a flood, do you ask for help building a dam or an Arc? If it's a bomb, do you want people fighting to get in your bunker? So on and so forth.
I can imagine all kinds of human-creativity driven stuff springing up - misinformation campaigns to make people prepare for the wrong stuff, religious style groups to call on people to work together, marauders, etc.
Anyway, it sounds very difficult to make, but I think it really taps into some of the best parts of games - the people playing them. The game would just provide a structure for humans to make their own amazing stories and journeys, and it would be awesome.
___
Answered on April 25, 2016 12:47:19 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/136073437758



## You do intellectual work m80, don't be ashamed of your downtime imo. You're not a zombie because you take a rest now & then.
### asked by Soul James

Well, I seem to be OK. Just something I had to begun to notice. Danke for your concern.
___
Answered on April 15, 2016 13:12:54 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/135861946430



## Is "doodad" a technical term and are you good at developing them?
### asked by Anon

Yeah, I'm OK at developing them. This is the doodad in question:https://github.com/OlympusMonds/ask.fm-extractor
I've made some other nice doodads:https://github.com/OlympusMonds/PyCircleriserandhttps://github.com/OlympusMonds/Convecterm (really needs to have a gif though)
I'm working on a project right now that I'm quite excited about. I will post somehow when there is something to see!
___
Answered on April 15, 2016 13:07:48 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/135896524606



## Bro, where can we get the thing @Soul_James mentioned, that allows one to extract the content of an Ask.fm account into a searchable file? It would solve a lot of problems, for me.
### asked by Tim Marsh

UPDATE: I don't make websites normally, and I think it might not be threaded... maybe.. Meaning maybe only one person at a time... That can't be right though... Anyway, in summary, it may be buggy and crap. /UPDATE
The code is available here:https://github.com/OlympusMonds/ask.fm-extractor
But don't use that! I made a website (kinda)!http://olympusmonds.pythonanywhere.com/
It is pretty basic, but essentially, if you want my answers, you would go to:http://olympusmonds.pythonanywhere.com/olympusmonds
If you want yours:http://olympusmonds.pythonanywhere.com/TimMarshPhD
And so on.Once you have the text, you could either save it as a html doc, or print to pdf, or whatever. If you would prefer some formatting (like bold questions, for example), or URL links back to the original questions, I can do that in no time.
DO NOTE: It is a bit slow, it's requesting one page at a time from Ask.fm. Hopefully they don't get annoyed.It also has a commandline usage, but eugh.. so many dependencies.. Pretty much installing Python on Windows is a nightmare, and OSX has enough weird stuff to also be annoying. I'm sure someone who knew JS could reproduce this in about a second and it would just use a browser. Oh well.
___
Answered on April 15, 2016 12:55:27 GMT

__Likes:__ 7

Original URL: /OlympusMonds/answers/135895373886



## What was so bad about the courses/course structure at your uni? Was there any hope for change?
### asked by Anon

Speaking of answering questions...
I have left this question for 6 months!
- In the past, courses have had to be combined, and so now very different subjects are put together (e.g., fossils and structural geology)- By reducing the courses, this also means that each subject has a reduced amount of time to be taught. For example, the total amount of structural geology theory taught is about 4 weeks - other universities typically have at least 4 courses on it.- There isn't enough courses for students. In 1st year, there is one straight-up geology class, in 2nd semester. That is the only pre-req for 2nd year. In 2nd and 3rd year, there are only 4 geology courses per year. That is only half a load, so you have to fill your degree with other stuff. By comparison, in universities in Europe, it's 8 subjects per year. So for a student at the end of 2nd year at my uni, they would have done 5 courses in geo, where as the european student would have done 16...- The whole system is designed around getting enrollments. 1st and 2nd year geo are actually very easy to pass (at my uni). There is no real maths, mostly just descriptive stuff, and only one real field trip. Plenty of students stay enrolled because it is so easy ("Ps get degrees!"). Then suddenly in 3rd year, you suddenly get hit with maths (though it is pretty easy), and then geophysics, and the students freak out. "This isn't geology, where are rocks!". Trust me bud, it should have been like this from the start. Particularly damning evidence of this enrollment idea is for engineering geology. The engineers *have* to do 1 geo course, and they *have* to pass. Lots of high-paying international students in engineering, and a lot were failing (the most basic stuff) and complaining. So rather than maybe changing how the course worked, or even just re-writing the test, they just reduced the pass mark from 10/20 to 6/20. Great!
Anyway, it's pretty boned. It only has gotten by so far because the mining industry would just pick up anyone, and then train them in their specific mining stuff. Now the boom is slowing...
___
Answered on April 13, 2016 00:51:51 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/132263722046



## How often do you daydream or go on private flights of fantasy? What kind of things do you fantasize about when you let your mind wander?
### asked by Anon

I used to do it much more often, on public transport or whatever. Now I have podcasts, which strip my mind bare of my own imagination, and replace it with some author's.
This may be a bad thing, but particularly if I'm stressed, it is a nice distraction. Also, if the content is interesting, I do process it and think about it, so I'm not completely devoid of brain power.
The more I think of it, the more I realise how often I try to stop my brain thinking. Doing exercise, woodwork, listening to podcasts - all these things allow my brain to stop for some reason, and I seek out doing these things..In saying that, I feel like I am not a mindless zombie who knows nothing of present happenings, and some level of critical thought about them. Not sure when that is happening though.
Perhaps I should answer more Ask.fm questions... if I ever got any :''((I understand there is a feedback loop here that I am ignoring)
___
Answered on April 13, 2016 00:02:04 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/135858974526



## What podcasts do u like? When do u listen to them mostly? Like do u listen when doing other stuff(driving/cooking etc) or do u like to concentrate on them alone.
### asked by Anon

Just to clarify, podcasts should never replace conversations. If you have the opportunity and desire for a conversation, you should have it. I'm talkin' to you @SoulJames !I listen to podcasts when I am on the bus, when I'm cooking, or when I'm cleaning. These are times when I can't really do anything else (I get carsick when I read or watch vids on the bus), and so podcasts fill this gap very nicely.
Now on to podcasts I like: 1) The longest shortest time - this one is about havin' babies, parenting, typically from mothers' perspectives. Man, this one has blown my mind in different ways. One way was just the physical stuff - I had no idea some of the medical stuff that can happen when you give birth and afterwards; apparently your baby can take a crap before being born; apparently a lot of women just can't breastfeed - it just doesn't work, or not enough; and a bunch more stuff. Another way (my mind was blown) was the associated mental/societal issues that come with it all. Like aforementioned breastfeeding: the women speaking about it aren't like "yeah, I just couldn't so 'oh well'" (although some do say this!), they are like "I felt like a failure as a mother, and some of the people around me would pretty much force me to keep trying, and it was awful". Heaps of examples like that, where people in society have all these expectations, and you are just like "I don't know WTF I am doing, please help", and they're like BAD parents!This is where the medium of podcasts really helps I think - you hear these people tell these really personal and private stories, and you can hear how hard it is/was for them, and I've never found that to come through with written word.
2) This American Life - it is very good most of the time. The one I just listened to was hectic, I must insist @Nebby_99 listens to the first story: http://www.thisamericanlife.org/radio-archives/episode/470/show-me-the-way?act=1#play - I was mega-emotional by the end. I MUST insist!The show can be hit and miss with some of the spoken word essays though. I much prefer the journalistic stuff.
3) Dan Carlin's Hardcore History: very long form - there was like a 10 hour series on world war 1. If you can get over his very american accent, it's really good. He really manages to convey the human side historical stuff, mixed with the big picture goings-on. The WW1 was particularly harrowing for this.
4) The Bugle: John Oliver and Andy Zaltzman - very amusing, but has dropped off in frequency.
5) The Misandry Hour - I just started listening to this. Clementine Ford and guests talk about the modern state of feminism and what they have to deal with. Very interesting, as she gets a really diverse range of guests.
And many more!
___
Answered on March 23, 2016 01:04:28 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/135392334398



## Olympus, m8, you got me there re: your Greeny pals, but yeah, I DO ask myself tons of angry qs about the shit my Party is doing, or rather,not doing. Same sex marriage is being used as a political football and that's pretty sad really. No where near as sad and disgusting as how we treat refugees but
### asked by Anon

OK
___
Answered on March 16, 2016 03:56:05 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/135303113022



## The guys in class were mostly asian. My campus is pretty asian. I figured I was the least knowledgeable one there. Sorry, that was racially stereotypical at any rate!
### asked by Anon

Yeah, whatever mate.Do what you want.
___
Answered on March 15, 2016 12:41:28 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/135292605502



## I'm asian.
### asked by Anon

...and?Overall though, I was trying to give you help with a pretty big personal thing, and the response is "yeah, too many Asians anyway, eh". Thanks for carefully considering my advice.
___
Answered on March 15, 2016 12:39:22 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/135292548414



## Academia would have in store for me. Not only that, having to compete with asians way smarter than me even if it did pan out well. Trying to pool some background knowledge before I make the decision. It's making me anxious. I appreciate your answer! Your field is cool, btw! 2/2
### asked by Anon

followed by unexpected racial stereotype!
___
Answered on March 15, 2016 12:27:23 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/135292176702



## Hey, thanks heaps for your answer! I was pretty down and out about it. I studied a maths unit last year and whaddya know the lecturer and the coordinator was cold and bashed arts in the very first class. I was pretty afraid. Deathly afraid of the cold rationale. 1/2
### asked by Anon

looking good...
___
Answered on March 15, 2016 12:26:36 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/135292134718



## I'm struggling to figure out my next move at uni. The lecturer that I had stuck around for--as she was a calming force--has disappeared on me! I only ever want the freedom to observe nature in its abundance. This is the only bit of certainty I hold onto. At a crossroads with STEM or Screen Media 1/2
### asked by Anon

"My question, what is the most rewarding/satisfying thing you have observed in your field of expertise? Do you feel the freedom to explore. Any insight is much appreciated! 2/2"
I can't say I have ever had "the freedom to observe nature in its abundance" in any part of my degrees. To a certain extent it was because of my more computational topic (though fundamentally I am a geologist), but even still. This was particularly true in undergrad, where I was merely bouncing around trying not to fail everything. This was a function of the effort I put in as a student - I was not a good one - but even so, you are pretty much completely at the mercy of the lecturers and course set out by the university. Having a good lecturer or tutor can make a really big difference here. During my own teaching, I would always manage to get a few students to connect everything up and reach that epiphany moment, which is very satisfying - but in terms of nature, I was helping the describe and understand the maths and physics behind subduction zones. It is, in a way, an observation of nature, but very abstracted - and really, it has to be, we are too small and short-lived to understand them in any other way. Going into the field is more in-line with your literal statement, but it can't be done either all the time, or without lots of text-book context. "Real" geologists probably spend at most 4 to 5 weeks in the field per year - they gotta do something the rest of the time.This is where a see a lot of students come into STEM, and then realise what it really means. "I just love science", or "I just want to know how everything works" - sure, great! Now here is one year of hard AF maths you have to do. "Cool, we get to actually look at rocks now!" - awesome! Now categorise them into a table, listing all their minute differences. Most of STEM is doing "boring" stuff to figure out what is happening. I actually enjoy doing this stuff - data analysis and processing is very interesting and entertaining to me. So you really have to be ready for the real stuff if you want to do STEM.
STEM and screen media seem vastly different to me. I'm pretty down on science as a whole recently (as you may have noticed) - if you do choose it though, just remember that it's run by humans. As much as science espouses its amazing capabilities of rationality and understanding, the scientists themselves are often pretty big assholes, and will follow incentives just like anyone else. The idea that scientists or doctors or whatever are somehow above this or infallible is pretty much crap - particularly because scientists don't get any sort of training regarding this; and any that is offered, such as ethics classes, are scoffed at and dismissed.
So f*ck it, go with screen media. Doing science stuff has been great, and learned heaps, but it left a big void in my understanding of the world, which I'm trying to fix right now.
___
Answered on March 15, 2016 10:24:09 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/135152925502



## Ha! The Greens voted AGAINST marriage equality! Explain that, fan boy. I'm sure you'll say they had a very good reason. Let's hear it!
### asked by Anon

Man, you have your finger on the anti-greens pulse.
Anyway, here is the reason:"...the Government put forward a list of bills it wanted debated by the end of the sitting week — including voting reforms which will make it harder to elect crossbenchers."
"Liberal Democrat David Leyonhjelm also tried to suspend standing orders, in order to allow debate on a same-sex marriage bill first introduced by Greens senator Sarah Hanson-Young."source: http://www.abc.net.au/news/2016-03-15/what-just-happened-in-the-senate/7248274
To explain a bit more: government and the greens want to have the senate voting method changed, largely (I imagine) because it will benefit them, but also probably to a certain extent so that people with < 1% of the primary vote manage can't become senators.The govt. put forth this stuff today into the senate, and in an attempt to stop this from happening, in a pseduo-filibuster way, a few senators tried to "suspend standing orders" to discuss other things. Ricky Muir put forth a thing to reinstate a construction watchdog, and Leyonhjelm put forth the gay marriage debate - which was a pretty crafty ploy.The Greens didn't take the bait though, and instead asked if it could happen on Thursday, which is a time for private members bills, which the gay marriage one is (or at least I think it is, since Sarah Hanson-Young introduced it..).
So to address your question literally - no, the greens did not vote against marriage equality, they voted not to discuss it today. To address your question in the spirit of your anger: it looks like they did (and Labor are pushing that hard too), but it seems that gay marriage was used as a political tactic to not discuss senate voting laws.
Also, I'm sure you ask yourself tons of angry questions about all the daft shit your supported party does that is legitimately messed up - refuges being issue number 1.
___
Answered on March 15, 2016 10:00:06 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/135289588286



## Whats your current music taste rn?
### asked by Anon

Currently two songs have just dominated my listening:1)
I hadn't actually seen the video before, and they clearly hadn't *quite* mastered the art of editing. I know they're trying to copy Eminem, but yeah... not quite. Speaking of Eminem - yeah, problematic - but man, this song cover gets me going.
2)
This is more my typical bag right now. Mixing electronica, with screamo, and normal singing *kisses fingertips*.
Some other very different stuff:
3) https://zweihander.bandcamp.com/track/baphomet-on-the-keysThis guy does like 8 bit sound tracks, and this track in particular is excellent.
4)
I have a few songs like this, where it seems to be building and building, but there is no drop. This sounds bad, but is not. Listen for that bass guitar coming in in the end.
5)
I also love me some acoustic/percussive guitar. This guy is particularly good.
6)
A good mix.
7)
similar to song 2, good mix of screamo, metal, with normal singing.
8) https://breakmastercylinder.bandcamp.com/track/westwood-instrumental-version-reply-all-closing-credits-themeSolid bassy electronica, I guess?
9)
Though probably not what I would call it, this is classic emo/alternative. Brand New were the best of us.
That'll do for now.
Let me also take this opportunity to apologise to all my friends during highschool - I was, I imagine, insufferable when it came to music, so sorry about that.
___
Answered on March 05, 2016 08:35:33 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/135082131518



## Whoa, you DO look gaunt there, damn son.
### asked by Soul James

A gaunting visage, truly
___
Answered on August 09, 2016 13:24:18 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/138158600254



## it's just ur forehead man idk where ppl are even getting gaunt from
### asked by Anon

Indeed, but if you view the whole image it becomes clearer. Also, my forehead is super gaunt
___
Answered on August 09, 2016 13:12:01 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/138158407998



## Soz I'm just not into beard
### asked by happyhaps

But you asked me to reveal it! (kind of)
___
Answered on August 09, 2016 13:07:23 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/138158304574



## i like ur cat shirt and also your beard is ok I guess
### asked by happyhaps

😒
___
Answered on August 09, 2016 13:04:09 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/138158287166



## are you deliberately obscuring your new (shorter) beard?
### asked by happyhaps

No way
___
Answered on August 09, 2016 13:02:11 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/138158251070



## wait gaunt is GOOD? god damn it
### asked by Soul James

You sayin my avi ain't good?
___
Answered on August 09, 2016 12:57:39 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/138158106686



## well now that you mention it you do look gaunt af. the gauntest. but hey, you got someone to agree to marry you despite that. well done, m8.
### asked by Anon

Despite that? Gaunt is good.
___
Answered on August 09, 2016 12:48:30 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/138157987134



## ty! you're pretty. cool hair.
### asked by Anon

I'm pleased with how gaunt AF I look in it.
___
Answered on August 09, 2016 12:41:45 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/138157927230



## what's going on in your new avi, m8?
### asked by Anon

Fixed it up for you, mate.
___
Answered on August 09, 2016 12:28:55 GMT

__Likes:__ 0

Original URL: /OlympusMonds/answers/138157748030



## Have you witnessed any paranormal acitvities?
### asked by Anon

Yes, I went skydiving once, and I made it to the ground safely.
___
Answered on August 08, 2016 01:07:59 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/138131827518



## Are you excited for the wedding? Getting your beard trimmed up all nice, m8?
### asked by Anon

My beard has already been heavily trimmed. Dropped like an inch and a half off it. Yes, I am excited though.
___
Answered on August 06, 2016 13:29:45 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/138098395198



## Hey, what do you want people to wear to your wedding? >.>
### asked by Nebby Nebula

Whatever you want. The theme of the wedding is having a good time, so whatever makes you happy. I think most people are dressing up a bit, but yeah, don't wear uncomfortable stuff.
___
Answered on August 06, 2016 13:22:00 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/138101451838



## That's what really gets me, about science under capitalism: The very same pressures which make the jobs so unpleasant that only unpleasant people do them, are ALSO the very SAME pressures that compromise scientific integrity & flood the literature with useless, ill-replicable findings. Terrible!
### asked by Tim Marsh

Indeed, it is very terrible. I suppose I wouldn't mind it so much if we all acknowledged it, but what gets my goat is that science is deemed such a noble profession filled with noble characters - largely promoted by some of these assholes I'm talking about it, doing this shady stuff. It's double-think, and it is painful to watch the people you once looked up to just do it like it's nothing.
Even in my new job (a very different branch of science (where I am not a scientist)), they have a much much better working environment, where students are treated with respect and there are systems in place to promote diversity and collaboration - even then, some of the other points I raised come up - scientific thought invasion being one of the biggest ones.
I guess I'm just disappointed to see the system I regarded so highly fail so badly. (As an aside to the general reader, I'm not saying science can't find sh*t out, just that it doesn't shut up about how great it is at doing that while simultaneously punching a phd student in the head with 50 useless papers).
___
Answered on July 21, 2016 00:46:05 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/137776806974



## science wrongness continued
### asked by OlympusMonds

One thing I forgot:- scientist's self-sacrifice: science as a profession seems to push the work-life balance majorly to the work. Pretty much all the professional scientists I know frequently, if not always, work weekends and late nights. I've known people to live apart from their partners (like, internationally) so they can both have jobs. This is the "life of a scientist". It's almost a goal, or a way to prove your doing it right. As you might have guessed, I really don't like this, and unfortunately it seems to be a self-propagating cycle (people who work crazy publish more, therefore do better, etc.). Why do all this? For SCIENCE! I'm a vanguard of figuring sh*t out! (trumpets blare in the background). I get it, I know the rush of figuring stuff out, really exploring something new and amazing - but it's not worth it. Not worth the constant stress, not seeing friends or family, not doing the hobbies you like, not the anxiety of messing stuff up.
For this, and many of the things I said before, when you bring it up with people of similar age, they say: "oh man, I know, it sucks", and they sympathise. When you bring it up with older academics, it's always "yeah man, that's how it is in science. You gotta weed out the weak!". Maybe this would have more clout with me if I didn't see the scientific method getting murdered constantly to get these people ahead.
___
Answered on July 21, 2016 00:30:19 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/137776650558



## When has science been wrong?
### asked by Anon

So I'm pretty bummed on science for a number of reasons. So let me explain them!
- assholes/eccentrics: Academia has a lot of assholes in it, and for some reason this is considered OK, or just eccentricity. I've known many people to have been yelled at, called stupid (and worse), and made to cry from feedback - both in private meetings or at talks. I've heard of people wearing tags saying "'s slave". Why? Why do we tolerate this? For scientific progress, apparently. Smart people just don't *get* the normal way of things. This is unacceptable to me, and is just bullsh*t begetting bullsh*t.
- thinking they're the best: Academics also tend to be quite arrogant. This is because, theoretically, yeah, they may be the best at whatever they do, and the arrogant ones will tell you and be up in your face about it. This leads to bad behaviour around their field - needless nit-picking, fierce competition and opposition, yadda yadda. So yeah, this one is more selection bias, but it does happen a lot.
- applying scientific thinking to everything: many scientists are like "oh, I can still appreciate the beauty of the sunset - the maths just makes it better!" - yeah, yeah, ok whatever mate, that doesn't matter. It's the complicated stuff that scientific thinking invades. We're trained to break things down into simple pieces to understand them, and for physics and stuff, this actually works. But I think with societal/human issues, this really doesn't work, and leads to harmful questions (cite: Richard Dawkins asking why it was bad to say that being sexually assulted by a family member would be better than a random person. He just want to know the *facts* ok!?). That is a relatively extreme example, but I've found similar workings in my own thinking, and of scientist peops around me.
- corruption, due to the system: Every academic over the age of 40 that I've worked with has done something 'crazy'. Crazy, as in, something really out of character that you would not have expected. And everytime it's happened, it's to either: get grant money; use grant money; publish a paper; or avoid not being able to publish in the future. The sh*t I've seen is like, use money from this student, give this a good review so they give us a good review, put this phd supervisor as a phd marker, put this guy on a panel to give this project a good review in exchange for a future good review, co-opt this student's paper with additional authors, etc. From people I would have never dreamed. This is partially due to my first point, but it's also driven by the system - it rewards publishing a lot, and in fancy journals. This leads to grant money, which allows for post-docs, to get more publishing, etc. The science is sometimes almost incidental.
In summary, science is flawed as hell because it's run by humans who think they think better than everyone, but actually they just suck. That being said, many young scientists are pure of heart, but will soon be broken.
___
Answered on July 20, 2016 13:46:00 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/137764859966



## re: red pill notch. ooh ooh, let me speculate! 

his wife left him. all done!
### asked by Robert Maynard

That is interesting, but I asked more to see what Neb thought about it, rather than the Notch goss.
From memory, Neb always really liked Notch, so I was curious to see how he was reacting to the clear slide into MRA madness, which seems to have really amped up recently.
I was never a huge fan of Notch, so when he first started being a bit weird I was vaguely smug to myself, but I didn't want to see it go like this, (now quoting Switch) "not like this.."
___
Answered on July 05, 2016 11:40:36 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/137459770430



## Election thoughts so far?
### asked by Anon

Pretty crazy. Have to wait and see though.
___
Answered on July 03, 2016 04:46:11 GMT

__Likes:__ 1

Original URL: /OlympusMonds/answers/137412730430



## Did you have any inspiration for your beard style?
### asked by Anon

"The less I need to do stuff, the better."
___
Answered on July 01, 2016 23:56:19 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/137388865598



## I think it ~is~ a bit odd to get 2 or 3 people a day commenting on your beard tbh! Judging from your avi your beard, although nice, doesnt seem unusual enough to draw such attention. Is it mainly men or women commenting?
### asked by Anon

Mate, the avi pic was months ago. See new beard below. Equal gender divide, though men are more likely to talk about it on public transport or similar.
___
Answered on July 01, 2016 23:13:21 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/137387370558



## what is a noob? :/
### asked by Anon

A newbie.
___
Answered on July 01, 2016 13:37:54 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/137377755966



## I mean that you saying Labor "upped the game" and are now promising to legalise same-sex marriage "first thing" if they are elected is beyond depressing. To be used like that. For the record I am pro same-sex marriage. It's the FIRST thing Labor is going to do? Something which will only benefit >
### asked by Anon

">very few, only same-sex couples who want to marry. And capitalism, of course. Where's the party promising to do something about homelessness or the closure of DV services "first thing". And dont tell me you can care for more than 1 thing. Of course you can but no-one is talking about the latter."
I don't know that I agree that Labor has been "used" - I would say rather, they have changed to reflect what voters want (which is good, yes?). Labor is made up of people who believe they are Labor voters, so when their views change, the party collective changes with it. In that vein, I would guess that Labor would not have made this move if the support for SSM was like ~10%.
BUT this of course is also the problem, as you say. The government should be protecting minorities, even if they have low popular support. DV and homelessness are I guess popular issues (no-one would say they don't want to help either group), but they also don't really get people going as a voting issue. As such, we don't really hear much about it. (Sometimes governments do do stuff like this though - the national disability insurance scheme seems like something that has a narrow benefit, and you don't really hear that much about it - but it got in, so there you go.)
I don't think this should undermine the support for same-sex marriage though. I know, and have read many more stories of, people who are directly affected by this, and are desperate to be treated as equals. Not like, "oh, I'm desperate for a coffee", but like, terribly anxious about this election, to know what Australians as a whole think people like them are worth. SSM is something that can make these people feel actually part of the society in which they live, and it can be done right now, with very little fuss.
I am super sympathetic with what you say though - and just because it isn't 'easy' doesn't mean we shouldn't focus on it. But really, you are probably right - helping DV victims or homeless people doesn't have much return on investment in the short term, so capitalism says no.
But the bits of your questions that worries me are the utilitarianism of them: "only benefit very few", "hardly the most pressing problem" - this is a slippery slope into doing SFA because someone else, somewhere, has it worse. Also, like, SSM is a serious issue, ranking it better than something else is of no help to the people effected.
___
Answered on July 01, 2016 13:05:01 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/137376764478



## lol you know damn well I can't grow a beard worth shit you mfer. Come around here complaining about beard compliments smdh (sorry if I made you feel bad I was just ribbing you, & I took your 'complaint' as how it was intended)
### asked by Soul James

you'll never know the eternal struggle/pain of beardiness.
(good answer though)
___
Answered on July 01, 2016 12:56:27 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/137377019710



## Is that how elections work in Australia? If another party gets more votes than you, can you just decide to bundle together with another party, which then counts as your collective votes combined?

Or is it like the American electoral college? Putting together enough 'seats won'?
### asked by Anon

Well, perhaps in contrast to the tone of my last answer, I am not an expert. But I'll give it a crack.
So there are two ways parties effect other parties in AusPol:1) Directly: So assuming neither major party can form a majority in the lower house, Labor would have two main options (re: the Greens). They can either form a coalition with the Greens, which would mean some Greens MPs would be put in charge of certain portfolios (for example, you would have a Green as the Minister for the Environment, etc.); or they can have a hung parliament, which happened in 2011 (I think..?), where the Greens and Labor agree to vote together, but it's less official, so the Greens don't have any portfolios. In this case, the Greens (and other minor parties/independents) can change their minds on particular votes, and block the government from getting legislation through.
So to answer your question directly: "If another party gets more votes than you, can you just decide to bundle together with another party, which then counts as your collective votes combined? Or is it like the American electoral college? Putting together enough 'seats won'?" - no, if another party gets more votes than you (enough to have their own majority), they will just ignore you and do what they want. If they don't get a majority, but you represent enough votes to allow them to form a majority, you have a lot of power with which to bargain with. And regarding the electoral college - while it is kinda first-past-the-post, the college has some weird rules where it can contravene the popular vote, so I can't really say how those details compare.
2) Indirectly: This is a large part of what the Greens have been doing in the past (and other minor parties that get some attention). Tanya and Albo have been fighting fires on the left side of their electorates. While currently it seems that they mostly want to complain about it, I'm sure they, and the party as a whole, is evaluating how they can appease the voters in these areas. A nice example is how much Labor is pushing for legislating same-sex marriage. They were initially against it (remember Gillard), but social change has been happening over the world, etc., and I'm sure they softened somewhat. But as the Greens have been gaining serious support, Labor really upped their game, and are now promising it will be the first thing they'll do if they win. This is a nice/good example, but it can only go so far (in fact, as far as the major parties think the votes are going) - we still see Labor saying they have the same policy regarding refugees (though with more transparency). Same-sex marriage already has like 70% public support, so Labor is hardly going out on a limb with it - but until more people say/vote for the Greens (or similar) because of the refugee policy, we're unlikely to see Labor chill out about it. So I'd rather just have direct interference of minor parties.
___
Answered on June 28, 2016 11:18:16 GMT

__Likes:__ 7

Original URL: /OlympusMonds/answers/137293535038



## As a geology expert, what type of rock would you say is objectively the Best Rock?
### asked by Anon

I'm not really that kinda of geologist, I do more structural geology/geodynamics.
Here is a cool example of what I mean:
___
Answered on June 27, 2016 07:44:28 GMT

__Likes:__ 7

Original URL: /OlympusMonds/answers/137187115838



## Soul's talking a bit abt communities on his page and I remembered you were going to a Men's Shed to do woodwork stuff. Do you still go? Whats the sense of community like there? Are you considered young to go there?
### asked by Anon

No, I stopped going, unfortunately. A mixture of bad timing (they're only open from like 10am to 2pm weekdays), and for me a *lack* of community.
So overall I think they are a great thing, but I'm not their target audience. It was old dudes, who were very nice, but I just didn't fit in well enough.
Similarly, and kinda not-greatly, there was a bit of low level casual racism and sexism, kinda in line with the stereotype of "old guys hanging out", which wasn't ideal for me, but I didn't really think it was my place to do anything about it. Also lots of sass towards noobs.
I sound kinda down on it, but if you are the target audience then it would be really good.
___
Answered on June 27, 2016 06:32:34 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/137102662462



## What, wait? Why am I at fault? I'm just tryna do my best here!
### asked by Anon

Sorry, you directly asked me to flaunt my privilege and luck, in the face of all others who may not have it as good. By blaming you, I manage to deflect the braggy-ness on to you, Anon.
___
Answered on October 22, 2016 03:05:36 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/139298562366



## Hearing how smoothly your life is going at the moment has put a big smile on my face. The struggle is real, but I'm happy whenever I learn it isn't omnipresent.
### asked by Tim Marsh

Oh good! I am also pleased, because over a year ago someone asked me what I wanted to do in the future (as I had just told them I did not want to be in academia), and I said "part-time programmer, part-time woodworker", which is pretty bang-on at the moment.
However, while I am overall very happy, the bliss of ignorance I had essentially pre-Ask.fm is now mostly gone (both correlation and causation in the timing there), and so the world's woes are now much closer to mind. It's particularly painful sometimes when people at work say some hectic shit, and I know that it's not good but I can't articulate as well you or @Soul_James. Practice does make perfect though - I'm getting better, doing my best to change hearts and minds.
However, despite the looming doom of the world being all up in my face now, I feel much happier with myself as a person - past-Olympus would have (and obviously did) reject many of the ideas I read on here and Twitter as mad ramblings. Only through the in-person persistence of @Soul_James, @Nebby_99, @happyhaps pulled me through, and the amazingly clarifying writings on here of you, @Soul_James, and @rmaynard85 (+ the others I follow).
Speaking of the writings, I feel like this tweet is a good summary of my experience with ask.fm: https://twitter.com/Soul_James/status/788991981875625984I was the person who would have liked the original tweet. I could not have comprehended another viewpoint. And yet in 10 words, the retweet completely flips the meaning and understanding of what's happening. Obviously Ask.fm is a bit more verbose, but all the people I follow on here have a knack for doing the same thing. It has taken years to adjust my world view, but thank christ I did. Here's hoping I don't stop!
___
Answered on October 22, 2016 03:04:20 GMT

__Likes:__ 6

Original URL: /OlympusMonds/answers/139298560062



## What are some good things in your life rn?
### asked by Anon

Mate, everything is going pretty swell at the moment. I continued my deferment on my PhD, and am therefore living the life:- Got married recently,- Part-time work, which is stimulating - The rest of my free time:  - woodworking,  - playing games,   - getting chores and stuff done, so my partner doesn't have to  - reading books,  - doing some exercise,  - reading Ask.fm.
I realise I'm extremely lucky, and that if I had a kid or a house to pay off, this would be extremely unpossible. So... sorry about that, but the questioner is 100% at fault here. Perhaps I should donate my time more graciously though.
___
Answered on October 22, 2016 02:34:51 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/139298459454



## You: http://i3.kym-cdn.com/photos/images/facebook/000/816/782/cce.jpg
### asked by Anon

No kiddin
___
Answered on October 19, 2016 06:16:43 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/139263303998



## Are you working on any woodwork projects atm? Have you ever carved patterns into stuff?
### asked by Anon

I'm building a work bench at the moment. It is based on the plans I bought from Jay Bates (a YouTuber who does woodworking).
So far I've built the legs and top, and am currently working on cutting the mortises in the top for the legs (the holes where the legs poke through). I'm using very large timber, so the top weighs about 80 kg, and is 14 cm thick, so it's taking some time. Here's a pic of one of the holes that's almost done.
___
Answered on October 03, 2016 09:54:42 GMT

__Likes:__ 7

Original URL: /OlympusMonds/answers/138163473214



## KFC nugs
### asked by OlympusMonds

I just walked past a KFC, and I thought "f*ck it, why not". I bought some nugs. They weren't bad, maybe even alright, but they weren't as good as maccas. They were weirdly floppy - where's the crispness? The sweet and sour sauce was also too solid. It's like they got the texture mixed up between the two components. I've had worse nugs though.
___
Answered on September 23, 2016 09:00:20 GMT

__Likes:__ 7

Original URL: /OlympusMonds/answers/138924726590



## How was your wedding? Did everything go to plan? Did you have the fun?
### asked by Anon

It was very good. Everything did go to plan. Not only did I have fun, but it appeared that all the guests had fun also! Who could ask for more??
___
Answered on September 21, 2016 13:06:14 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/138626881086



## Who is your favourite character from folklore / mythology? Does your chosen character represent anything special to you, or do you just enjoy their stories?
### asked by Tim Marsh

It's not so much a character, but when I was a kid/early teen, I used to love reading about ghosts and other paranormal stuff. I had a book that had a whole bunch of "real-world" ghost examples from around the world, and some of the legends surrounding them. It also had theories about why ghosts do certain things, behave certain ways, and how they were formed. An example: apparently if you put a coin on someones headstone, their ghostly hand will come up and take it. Another example: there was a talking mongoose on the Isle of White, called Geoff, who claimed he was a ghost. He was quite pleasant too, but someone shot him :'(
Anyway, I really enjoyed it, and I still enjoy reading good horror/ghost/supernatural pseudo-fiction. I say pseudo cause its gotta be put forth as if it were real, and if the story is good, it'll make me doubt my sureness about there being no ghosts. Ghosts caught on camera (again, if convincing enough) are also something I enjoy watching every now and then.
Unlike the questioner, I really liked Paranormal Activity because I found the actors very real (apparently every scene was ad-libbed, which I imagine helped). But yeah, not revealing the monster, and their normalness really hit my specific taste, and made it tough to sleep that night. My solution was to imagine the demon in the movie being extremely sassy as people talked about it like it wasn't there, and I was able to chill out a bit until I could sleep.
___
Answered on September 21, 2016 13:05:20 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/138885271870



## Can we see a wedding picture, m80? Please?
### asked by Anon

Here's a pic of me!
___
Answered on September 10, 2016 09:15:11 GMT

__Likes:__ 9

Original URL: /OlympusMonds/answers/138497304126



## what is your favorite gem? dont make me vom and say your wife please. i mean stones.
### asked by Anon

Gems, in my opinion, aren't really that interesting. They look good, but for example, sapphires and rubys are like chemically the same, just with different impurities to provide the colours.
Diamonds are probably the most interesting, because they need to have relatively extreme circumstances have happen to them for us to see them. They are only stable at very high pressures and temperatures (we're talking like, ~100 km down). If they come to the surface via normal means (convection or continental rifting or something), they just regress back into some other form of carbon. You need to move them quick AF to the surface (or close), so that there isn't enough energy to help them regress. You find diamonds in kimerberlites usually, which is the name of diamond baring rocks. Their form and structure suggest these sort of magma pipes, which transported rocks at like a metre per second (or something crazy like that) from way down to the surface.
So yeah, that's pretty cool.
___
Answered on September 08, 2016 10:47:08 GMT

__Likes:__ 7

Original URL: /OlympusMonds/answers/138695700030



## Uh it's built on your LACK of anonymity, else this nuggets guy would probably have a hard time knowing where to send his nugget-based imperatives.
### asked by Soul James

Yeah, more reasons!
___
Answered on August 31, 2016 05:36:16 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/138557477950



## Olympus, m8, are you stomping your foot like a goddamned child? Our relationship, if you can call it that, has been built on months of anonymity. If it aint broke why fix it I reckon.
### asked by Anon

You are the one asking something of me. You now know the price I ask in return.
___
Answered on August 31, 2016 05:20:18 GMT

__Likes:__ 2

Original URL: /OlympusMonds/answers/138557342014



## I'll send you a question that you can keep in your inbox should the momentous  occasion ever occur so I get a notif via email. I will be waiting. A month, a year, or a decade. I am very patient.
### asked by Anon

No, I demand non-anon if I'm gonna do this.
___
Answered on August 31, 2016 00:51:13 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/138554985278



## Ok, m8. I'm done. I've tried everything. BUT, if you ever try a KFC nug please tell me what you think. I beg of you. I will keep my Ask.fm for this reason and this reason alone on the chance it ever happens. Bless you, m8.
### asked by Anon

But how, you are anon.
___
Answered on August 31, 2016 00:26:38 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/138554442046



## Hey, @noboredghosts, a former Maccas worker tried KFC nugs and found them "obviously superior"!! See, I just want what's best for you! I want you to experience the best nugs our world has to offer. I'm nice like that.
### asked by Anon

Yeah but if I do try them, and they're better, I'm still not going to go to KFC very much, cause they're not as common as maccas. If I try them and they're worse, then I'll have wasted time, money, and lost face. This is a lose-lose for me.
___
Answered on August 30, 2016 23:30:25 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/138541755710



## What are things some people consider progressive which you deem stupid/annoying/anything else? Why?
### asked by Anon

When people ask me whether, in my opinion, if infinity is multidimensional.
___
Answered on August 30, 2016 23:27:52 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/138554072126



## Seen any good rocks lately, m80?
### asked by Anon

Enjoy some photo spheres:https://goo.gl/photos/jqKsuEHkVab1HGpe9https://goo.gl/photos/r6FMKFhzsC2rSs9o8
___
Answered on August 29, 2016 01:04:16 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/138516979774



## were you tryna start a beef with Kayla?
### asked by Anon

I'm going to take this opportunity to tell you one of my greatest recent puns.
In Kayla's house, there is a light that hangs down from the ceiling over their dining table. Attached to the light as decoration is a small toy Koala, which is clamped on. Kayla's sister was in the room, so I said to her "Oh, have you been to this city?", pointing at the light?"...no..? What city?"Gesturing to the light again, I said "Koala-lamp-por".
You can't pre-plan that sh*t.
___
Answered on August 26, 2016 01:54:54 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/138461015870



## What is the most interesting thing you have in your purse/wallet?
### asked by Anon

I have a receipt from a restaurant called "A piece of beef" (but spelled in French). I went to said restaurant when I went to a conference. When I got back, I put it in Kayla's wallet without her noticing. Over the next year, the receipt would change wallets, the subject only noticing after potentially weeks after the switch. Neither of us acknowledged this game until about a year. I was the sucka left with a piece of beef in my wallet.
___
Answered on August 25, 2016 16:06:21 GMT

__Likes:__ 5

Original URL: /OlympusMonds/answers/138452905278



## Hey can't remember if I said this already but thanks so much for once again archiving my ask.fm! <3
### asked by Soul James

No probs - don't think it was just for your enjoyment either ;) One day I'll actually make the program not a pile of crap, and setup some sort of scheduled dump. Perhaps people could submit their usernames and be able to download their own dumps at their leisure.
___
Answered on August 25, 2016 15:57:42 GMT

__Likes:__ 7

Original URL: /OlympusMonds/answers/138449540926



## wtf? your forehead is A+. i will fite that anon!
### asked by Anon

Good.
___
Answered on August 25, 2016 01:37:55 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/138443687998



## Do you know how many nightmares I've had about that forehead of yours? DO YOU? The answer is lots :(
### asked by Anon

Good..?
___
Answered on August 25, 2016 01:17:49 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/138443520062



## Do you feel any different now that you're married? Is it weird saying "my wife"?
### asked by Anon

That's about the only thing that is weird. Wearing a ring is also odd.
___
Answered on August 23, 2016 23:50:51 GMT

__Likes:__ 4

Original URL: /OlympusMonds/answers/138423240510



## Getting married today! How are you feeling?
### asked by happyhaps

I legit thought I had lost the rings for 5mins, but I'm fine now
___
Answered on August 20, 2016 18:29:54 GMT

__Likes:__ 9

Original URL: /OlympusMonds/answers/138363607102



## The magic of microwave? You nerd.
### asked by Anon

It's a cook book
___
Answered on August 11, 2016 15:38:15 GMT

__Likes:__ 3

Original URL: /OlympusMonds/answers/138189953598
